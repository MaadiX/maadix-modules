# Class: dovecot
# ===========================
#
# Full description of class dovecot here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'dovecot':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class dovecot (
  $ensure      = 'present',
  $autoupgrade = false,
  $quota_enabled = true,
  $quota = '1G',
  $quota_custom_message = 'No ha sido posible enviar el correo porque la cuenta de destino ha sobrepasado su cuota de almacenamiento',
) {

  require dovecot::params

  ##todo, require postfix::packages based on postfix related param
  ## postfix user/group needs to be created before dovecot conf
  require postfix::packages

  case $ensure {
    /(present)/: {
      $package_ensure = $autoupgrade ? {
        true    => 'latest',
        false   => 'present',
        default => 'present'
      }
    }
    /(absent)/: {
      $package_ensure = 'absent'
    }
    default: {
      fail('ensure parameter must be present or absent')
    }
  }
  #dovecot group
  group { 'dovecot':
  	ensure     	=> 'present',
  	gid             => 302,
  	system          => true,
  }

  #dovecot user with fixed uid/gid
  user { 'dovecot':
	ensure 		=> 'present',
        gid		=> 302,
	home		=> '/usr/lib/dovecot',
        managehome 	=> false,
        shell		=> '/bin/false',
        system		=> true,
        uid		=> 302,
  }

  #dovecot home
  file { '/usr/lib/dovecot':
    ensure 	=> directory,
    mode   	=> '0755',
    owner	=> 'root',
    group	=> 'root',
  }


  package { 'dovecot-core':
    ensure => $package_ensure,
  }
  package { 'dovecot-imapd':
    ensure => $package_ensure,
  }
  package { 'dovecot-pop3d':
    ensure => $package_ensure,
  }
  package { 'dovecot-ldap':
    ensure => $package_ensure,
  }

  #spamassassin: sieve packages to move spam mail to Spam folder
  package { 'dovecot-sieve':
    ensure => $package_ensure,
  }
  package { 'dovecot-managesieved':
    ensure => $package_ensure,
  } ->
  #sieve plugins folder
  file { '/var/lib/dovecot/sieve':
    ensure 	=> directory,
    mode   	=> '0755',
    owner	=> 'vmail',
    group	=> 'vmail',
    require     => User['vmail'],
  } ->
  #sieve plugin conf for spamassassin tagged mail
  file { '/var/lib/dovecot/sieve/default.sieve':
    mode   	=> '0644',
    owner	=> 'vmail',
    group	=> 'vmail',
    require     => User['vmail'],
    content     => template('dovecot/default.sieve'),
  } ->
  #precompiled sieve plugin for spamassassin tagged mail
  file { '/var/lib/dovecot/sieve/default.svbin':
    mode   	=> '0644',
    owner	=> 'vmail',
    group	=> 'vmail',
    require     => User['vmail'],
    content     => template('dovecot/default.svbin'),
  }
  
  
  #postfix private/auth folder
  ##todo, based on postfix related param
  file { '/var/spool/postfix':
    ensure => directory,
    mode   => '0755',
  }
  file { '/var/spool/postfix/private':
    ensure => directory,
    mode   => '0700',
    owner   => 'postfix',
  }


  #conf
  file { '/etc/dovecot':
    ensure => directory,
    mode   => '0755',
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d':
    ensure => directory,
    mode   => '0755',
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/10-auth.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/10-auth.conf.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/10-mail.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/10-mail.conf.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/10-master.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/10-master.conf.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/15-lda.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/15-lda.conf.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/10-ssl.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/10-ssl.conf.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/auth-ldap.conf.ext':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/auth-ldap.conf.ext.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/90-plugin.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/90-plugin.conf.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/dovecot-ldap-userdb.conf.ext':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => template('dovecot/dovecot-ldap-userdb.conf.ext.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/dovecot-ldap-passdb.conf.ext':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => template('dovecot/dovecot-ldap-passdb.conf.ext.erb'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/20-imap.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/20-imap.conf'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/20-pop3.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/20-pop3.conf'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/20-managesieve.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/20-managesieve.conf'),
    notify  => Service['dovecot'],
  }
  file { '/etc/dovecot/conf.d/90-quota.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/90-quota.conf.erb'),
    notify  => Service['dovecot'],
  }

  ### fail2ban jail
  file { 'dovecot: fail2ban jail':
    path    => '/etc/fail2ban/jail.d/dovecot.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dovecot/dovecot_fail2ban_jail.conf'),
    notify  => Service['fail2ban'],
  }

 
  service { 'dovecot':
    ensure  => 'running',
    enable  => true,
    require => Package['dovecot-core'],
  }

}


