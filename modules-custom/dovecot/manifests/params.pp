class dovecot::params (
  #TODO params
  $mail_location ='',
  $mail_uid = '',
  $mail_gid = '',
  $first_valid_uid = '',
  $first_valid_gid = '',
  $auth_userdb_user = '',
  $auth_userdb_group = '',
  $postfix_smtp_auth = true,
  #disable imap
  $imap_enabled = false,
  #disable pop3
  $pop3_enabled = false,
  $ldap_passdb_hosts = 'example.tld:389',
  $ldap_passdb_tls = 'yes',
  $ldap_passdb_auth_bind_userdn = 'mail=%u,vd=%d,o=hosting,dc=example,dc=tld',
  $ldap_passdb_auth_bind = 'yes',
  $ldap_passdb_version = '3',
  $ldap_passdb_base = 'o=hosting,dc=example,dc=tld',
  $ldap_passdb_scope = 'subtree',
  $ldap_passdb_deref = 'never',
  $ldap_passdb_user_attrs = 'quota=quota_rule=*:bytes=%$',
  $ldap_passdb_user_filter = '(&(objectClass=VirtualMailAccount)(accountActive=TRUE)(mail=%u))',
  $ldap_passdb_pass_attrs = 'mail,userPassword',
  $ldap_passdb_pass_filter = '(&(objectClass=VirtualMailAccount)(accountActive=TRUE)(mail=%u))',
  $ldap_passdb_default_pass_scheme = 'CRYPT',
  $ldap_userdb_uris = 'ldapi://',
  $ldap_userdb_sasl_bind = 'yes',
  $ldap_userdb_sasl_mech = 'EXTERNAL',
  $ldap_userdb_version = '3',
  $ldap_userdb_base = 'o=hosting,dc=example,dc=tld',
  $ldap_userdb_scope = 'subtree',
  $ldap_userdb_deref = 'never',
  $ldap_userdb_user_attrs = 'quota=quota_rule=*:bytes=%$',
  $ldap_userdb_user_filter = '(&(objectClass=VirtualMailAccount)(accountActive=TRUE)(mail=%u))',
  $ldap_userdb_pass_attrs = 'mail,userPassword',
  $ldap_userdb_pass_filter = '(&(objectClass=VirtualMailAccount)(accountActive=TRUE)(mail=%u))',
  $ldap_userdb_default_pass_scheme = 'CRYPT',

  $postmaster_address = 'postmaster@example.com',
  $disable_plaintext_auth = 'yes',
  $ssl = 'required',
  $ssl_cert = '',
  $ssl_key = '',
  $ssl_min_protocol = 'TLSv1.2',
  $ssl_cipher_list = $dovecot_ciphers,
  $ssl_dh_parameters_length = '2048',
  $ssl_prefer_server_ciphers = 'yes',

  $pop3_no_flag_updates = false,
) {


  case $::operatingsystem {
    ubuntu, debian: {
    }
    default: {
      fail("Unsupported platform: ${::operatingsystem}")
    }
  }

}
