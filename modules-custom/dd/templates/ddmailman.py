from mailmanclient import Client
import ldap3
import sys
import psycopg2
import subprocess
import json
import os
from operator import itemgetter

#import settings from django ddcp
sys.path.append('/usr/share/ddcp/ddcp/ddcp/')
from secret_settings import AUTH_LDAP_BIND_PASSWORD
from local_settings import SCHOOL_DOMAIN,SCHOOL_MAIL_DOMAIN,IMAP_SERVER,AUTH_LDAP_BIND_DN,LISTS_DOMAIN,LISTS_NAMES

#assign default values if empty
if LISTS_DOMAIN == "":
  LISTS_DOMAIN = 'llistes.' + SCHOOL_DOMAIN
if LISTS_NAMES == "":
  LISTS_NAMES = 'informacio'
#print("############ Lists domain and lists names")
#print(LISTS_DOMAIN,LISTS_NAMES)

#import settings from django postorius
sys.path.append('/opt/mailman/hyperkitty/example_project/')
from settings_local import MAILMAN_REST_API_PASS

#get mailman info
client = Client('http://localhost:8001/3.1', 'restadmin', MAILMAN_REST_API_PASS)
#print("############ Mailman info")
#print(client.system)

#create domain if not present
try:
  domain = client.get_domain(LISTS_DOMAIN)
  #print(domain.mail_host)
except:
  domain = client.create_domain(LISTS_DOMAIN)
  #print(domain.mail_host)

#get mailman admin email, main address of admin.
admin_mail = subprocess.run(["su", "postgres", "-c", "psql -d mailman -A -t -c 'select email FROM auth_user WHERE is_superuser=true LIMIT 1;'"], stdout=subprocess.PIPE, cwd="/tmp").stdout.decode("utf-8").splitlines()
#print("############ Mailman admin email")
#print(admin_mail[0])

#get lists created under lists domains
lists = []
for mlist in client.get_lists():
  #print(mlist.mail_host)
  #print(domain.mail_host)
  if mlist.mail_host == domain.mail_host:
    lists.append(mlist.list_name)
#print("############ Current lists")
#print(lists)

#create list with style if not present and configure settings
#create 30 lists by default with pattern LISTS_NAMES1/LISTS_NAMES2,etc
new_lists = []
for z in range(1, 31):
  new_lists.append(LISTS_NAMES + f'{z:02d}')
#print("############ Lists to create")
#print(new_lists)
for l in new_lists:
  if l not in lists:
    new = domain.create_list(l, style_name='legacy-announce')
    settings = new.settings
    #print settings
    #for attr in sorted(settings):
    #  print(attr + ': ' + str(settings[attr]))
    #set settings params
    #https://docs.mailman3.org/projects/mailmanclient/en/latest/src/mailmanclient/docs/using.html#list-settings
    settings['accept_these_nonmembers'] = [admin_mail[0]]
    settings['advertised'] = False
    settings['allow_list_posts'] = False
    settings['archive_policy'] = 'never'
    settings['max_message_size'] = 50000
    settings['preferred_language'] = 'ca'
    settings['reply_goes_to_list'] = 'no_munging'
    settings['send_welcome_message'] = False
    settings['subscription_policy'] = 'confirm_then_moderate'
    settings.save()

#get lists settings
#for mlist in client.get_lists():
#  for attr in sorted(mlist.settings):
#    print(attr + ': ' + str(mlist.settings[attr]))

#delete lists
#for mlist in client.get_lists():
#  if mlist.mail_host == domain.mail_host:
#    mlist.delete()

#get all subscribers and owners and moderators
mails = []
mails.append(admin_mail[0])
for member in client.members:
  if member.email not in mails:
    mails.append(member.email)
    #print(member.email)
#print("############ Subscribers, moderators and owners")
#print(mails)


#get mails in whitelist for this server and add to list
auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
ldap = ldap3.Connection(
  ldap3.Server(
    'ldap://ldap.digitaldemocratic.net:389',
    allowed_referral_hosts = [("*", True)],
    get_info               = ldap3.NONE,
    connect_timeout        = None,
  ),
  user             = AUTH_LDAP_BIND_DN,
  password         = AUTH_LDAP_BIND_PASSWORD,
  auto_bind        = auto_bind,
  raise_exceptions = True,
  receive_timeout  = None,
)
ldap.search('o=mails,dc=whitelist,dc=tld','(destinationIndicator=' + IMAP_SERVER + ')',attributes=['mail'])
currmails = []
for e in ldap.entries:
  currmails.append(e.mail.values[0])
#print("############ Mails in whitelist")
#print(currmails)

#make a list with new mails to add
newmails = []
for m in mails:
  if m not in currmails:
    #don't add mails of mail domain
    if m.split('@')[1] != SCHOOL_MAIL_DOMAIN:
      newmails.append(m)
#print("############ Subscribers, moderators and owners not in whiteliwst to add")
#print(newmails)

#add new mails to whitelist. new entries if not present or add this fqdn to destinationIndicator if present
for n in newmails:
  try:
    ldap.search('mail='+n+',o=mails,dc=whitelist,dc=tld','(&(objectClass=inetOrgPerson)(!(destinationIndicator=' + IMAP_SERVER + ')))',attributes=['mail'])
    if len(ldap.entries) > 0:
      #print(n + ' found, adding this fqdn')
      ldap.modify('mail='+n+',o=mails,dc=whitelist,dc=tld',{'destinationIndicator': [(ldap3.MODIFY_ADD, [IMAP_SERVER])]})
  except:
    #print(n + ' not found, adding it')
    ldap.add('mail='+n+',o=mails,dc=whitelist,dc=tld', attributes={'objectClass':  ['inetOrgPerson', 'top'], 'sn': n , 'mail': n, 'cn': n, 'destinationIndicator': IMAP_SERVER})

