class dd::ldap (
  $enabled                  = true,
  $set_repl_users_passwords = false,
  $repl_users               = undef,
) {

  validate_bool($enabled)

  if $enabled {

    #set repl_users passwords
    if $set_repl_users_passwords {

      #script to set pass
      file { 'ddcredentials script password':
        path    => "/etc/maadix/scripts/ddcredentials.sh",
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template("dd/ddcredentials.sh.erb"),
      }

      $repl_users. each |String $user| {

        #user passw file
        exec { "$user passw":
          command => "/bin/bash -c 'umask 077 && pwgen 64 -1 | tr -d \"\n\" > /etc/maadix/$user'",
          creates => "/etc/maadix/$user",
          require => Package['pwgen'],
        }


        #set pass
        exec { "set $user passw":
          command   => "/bin/bash /etc/maadix/scripts/ddcredentials.sh $user",
          creates   => "/etc/maadix/status/$user",
          logoutput => true,
          require   => [
                       Class['openldap::server'],
                       Ldapdn["$user user with default pass"],
                       ],
        }
      }
    }

  }

}
