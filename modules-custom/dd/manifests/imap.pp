class dd::imap(
  $enabled        = true,
  $mail_domain    = undef,
) {

  validate_bool($enabled)

  #get mailman domain
  $mailman_domain       = hiera('dd::ddcp::lists_domain', false)
  if $mailman_domain {
    $srs_mailman_domain = $mailman_domain
  } else {
    $school_domain      = hiera('dd::ddcp::school_domain')
    $srs_mailman_domain = "llistes.$school_domain"
  }

  if $enabled {

    #ddcp
    include dd::ddcp
    include dd::ddmailman

    #postfix
    file{ 'whitelist_domains':
      path    => '/etc/postfix/ldap/whitelist_domains.cf',
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('dd/whitelist_domains.cf.erb'),
      require => File['/etc/postfix/ldap']
    } ->
    file{ 'whitelist_mails':
      path    => '/etc/postfix/ldap/whitelist_mails.cf',
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('dd/whitelist_mails.cf.erb'),
    } ->
    file{ 'whitelist_clients':
      path    => '/etc/postfix/ldap/whitelist_clients.cf',
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('dd/whitelist_clients.cf.erb'),
    }

    #srs
    package{'postsrsd':
      ensure  => present,
      require => Package['postfix'],
    } ->
    file_line { 'postsrsd forward domain':
      path    => '/etc/default/postsrsd',
      line    => "SRS_DOMAIN='$mail_domain'",
      match   => 'SRS_DOMAIN.*$',
      notify  => Service['postsrsd']
    } ->
    file_line { 'postsrsd excluded domain':
      path    => '/etc/default/postsrsd',
      line    => "SRS_EXCLUDE_DOMAINS=$::fqdn $srs_mailman_domain",
      match   => '.*SRS_EXCLUDE_DOMAINS.*$',
      notify  => Service['postsrsd']
    }
    service { 'postsrsd':
      ensure  => 'running',
      enable  => true,
      require => [
                 Package['postsrsd'],
                 ]
    }    

    #gnarwl header and footer for html messages
    file { 'gnarwl header':
        path    => '/var/lib/gnarwl/header.txt',
        mode    => '0644',
        content => template('dd/gnarwl_header.txt'),
        require => [
                   Package['gnarwl']
                   ],
    }
    file { 'gnarwl footer':
        path    => '/var/lib/gnarwl/footer.txt',
        mode    => '0644',
        content => template('dd/gnarwl_footer.txt'),
        require => [
                   Package['gnarwl']
                   ],
    } 

  }

}
