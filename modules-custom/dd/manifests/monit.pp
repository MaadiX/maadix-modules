class dd::monit(
  $enabled        = true,
  $port           = 8000,
  $matching	  = '/usr/share/ddcp/express/httpd.conf',
  $binary	  = '/usr/share/ddcp/express/apachectl',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'ddcp':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}
