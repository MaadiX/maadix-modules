class dd::smtp(
  $enabled        = true,
) {

  validate_bool($enabled)

  if $enabled {

    #postfix
    file{ 'whitelist_domains':
      path    => '/etc/postfix/ldap/whitelist_domains.cf',
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('dd/whitelist_domains.cf.erb'),
      require => File['/etc/postfix/ldap']
    } ->
    file{ 'whitelist_mails':
      path    => '/etc/postfix/ldap/whitelist_mails.cf',
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('dd/whitelist_mails_smtp.cf.erb'),
    } ->
    file{ 'whitelist_clients':
      path    => '/etc/postfix/ldap/whitelist_clients.cf',
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('dd/whitelist_clients.cf.erb'),
    }

    #postfix mynetworks cidr file
    file{ 'postfix mynetworks':
      path    => '/etc/postfix/network_table.cidr',
      mode    => '0600',
      owner   => 'postfix',
      group   => 'postfix',
      replace => 'no',
      content => template('dd/network_table.cidr.erb'),
      require => Package['postfix'],
      notify  => Service['postfix'],
    }

  }

}
