class dd::ddmailman (
  $enabled               = str2bool($::mailman_enabled),
) {

  if $enabled {

    ########################## ddmailman cron ###################################

    #directory
    file { '/usr/share/ddmailman':
      ensure     => directory,
    } -> 
    #requirements
    file { 'ddmailman requirements':
      path    => '/usr/share/ddmailman/requirements.txt',
      content => template('dd/ddmailman_requirements.txt'),
    } ->
    #python script
    file { 'ddmailman python script':
      path    => '/usr/share/ddmailman/ddmailman.py',
      content => template('dd/ddmailman.py'),
    } ->
    #cron script
    file { 'ddmailman cron script':
      path    => '/usr/share/ddmailman/ddmailman_cron.sh',
      mode    => '755',
      content => template('dd/ddmailman_cron.sh'),
    }

    ##virtualenv
    $python3_version = $::lsbdistcodename ? {
      'buster'     => '3.7',
    }

    $virtualenv3 = "/usr/share/ddmailman/venv3/"

    python::virtualenv { $virtualenv3:
      ensure       => present,
      version      => $python3_version,
      owner        => 'root',
      group        => 'root', 
      distribute   => false,
    } ~>
    #pip install/update venv3
    exec { 'ddmailman: pip install venv3 requirements':
      user        => 'root',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; pip install -r /usr/share/ddmailman/requirements.txt'",
      cwd         => "/usr/share/ddmailman",
      logoutput   => true,
      require     => [
                     File['ddmailman requirements'],
                     ],
      subscribe   => [
                     File['ddmailman requirements'],
                     ],
      refreshonly => true,
      timeout    => 3600,
    } 

    #cron
    cron { 'ddmailman cron':
      command => '/bin/sleep `/usr/bin/numrandom /0..60/`m ; /usr/share/ddmailman/ddmailman_cron.sh',
      user    => 'root',
      minute  => '*/10',
    }

    
    ########################## mailman customizations ###################################

    #custom postorius forms
    file { 'postorius custom forms':
      path    => '/opt/mailman/venv3/lib/python3.7/site-packages/postorius/forms/list_forms.py',
      mode    => '644',
      content => template('dd/list_forms.py'),
      require => Exec['pip install venv3 requirements'],
      notify  => Service['httpd'],
    }


    #custom templates
    #https://docs.mailman3.org/projects/mailman/en/latest/src/mailman/rest/docs/templates.html#templated-texts
    file { '/opt/mailman/var/templates/site':
      ensure  => directory,
      require => Exec['mailman info'],
    } -> 
    file { '/opt/mailman/var/templates/site/ca':
      ensure  => directory,
    } -> 
    file { 'mailman dd custom footer':
      path    => '/opt/mailman/var/templates/site/ca/list:member:generic:footer.txt',
      mode    => '644',
      content => template('dd/footer.txt'),
    } ->
    file { 'mailman dd custom confirmation':
      path    => '/opt/mailman/var/templates/site/ca/list:user:action:unsubscribe.txt',
      mode    => '644',
      content => template('dd/unsubscribe.txt'),
    }

    ########################## mailman welcome ###################################

    file { 'dd mailman welcome':
      path    => '/etc/maadix/mail/dd_mailman_welcome.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("dd/dd_mailman_welcome.sh.erb"),
    }

  }
}


