class dd (
  $enabled                  = true,
  $role                     = undef,
) {

  validate_bool($enabled)

  if $enabled and $role{

    if ! defined(Package['iptables-persistent']) {
      ensure_resource('package','iptables-persistent', {
      ensure  => present,
      })
    }

    include "dd::$role"

  }

}
