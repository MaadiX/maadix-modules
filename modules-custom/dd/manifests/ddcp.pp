class dd::ddcp (
  $enabled               = true,
  $fqdn                  = undef,
  $ddcpversion           = 'master',
  $school_domain         = undef,
  $school_mail_domain    = undef,
  $smtp_server           = undef,
  $ldap_server           = undef,
  $lists_domain          = undef,
  $lists_names           = undef,
) {

  if $enabled {

    ########################## install ###################################

    ##dependencies

    ensure_resource('package','python3-dev', {
      ensure  => present,
      })
    ensure_resource('package','apache2-dev', {
      ensure  => present,
      })
    ensure_resource('package','libpq-dev', {
      ensure  => present,
      })
    ensure_resource('package','libsasl2-dev', {
      ensure  => present,
      })
    ensure_resource('package','xmlsec1', {
      ensure  => present,
      })

    ##user/group

    #ddcp group
    group { 'ddcp':
        ensure          => 'present',
        system          => true,
    }
    #ddcp user
    user { 'ddcp':
        ensure          => 'present',
        home            => '/usr/share/ddcp',
        managehome      => true,
        shell           => '/bin/false',
        system          => true,
    } ->
    #default .vimrc for ddcp user
    file { '/usr/share/ddcp/.vimrc':
        ensure          => file,
        owner           => 'ddcp',
        group           => 'ddcp',      
        content         => 'set mouse-=a',
    }

    #postgresql db
    postgresql::server::db { 'ddcp':
     user     => 'ddcp',
     password => postgresql_password('ddcp', 'ddcp'),
     before   => Exec['ddcp: makemigrations'],
    }

    #ddcp repo
    vcsrepo { '/usr/share/ddcp/ddcp':
        ensure   => latest,
        provider => git,
        source   => 'https://gitlab.com/MaadiX/dd-email-panel.git',
        user     => 'ddcp',
        revision => $ddcp_version,
        path     => '/usr/share/ddcp/ddcp',
        require  => [
                    User['ddcp'],
                    ],
    } ->
    #cert dir
    file { '/usr/share/ddcp/ddcp/ddcp/certs':
      ensure     => directory,
      owner      => 'ddcp',
      group      => 'ddcp',
      mode       => '0700',
    }

    #conf
    file { 'ddcp local_settings conf':
      path    => '/usr/share/ddcp/ddcp/ddcp/local_settings.py',
      owner   => 'ddcp',
      group   => 'ddcp',
      mode    => '0600',
      content => template('dd/ddcp_local_settings.py.erb'),
      require => [
                 Vcsrepo['/usr/share/ddcp/ddcp'],
                 ],
      notify  => Service['ddcp'],
    }

    ##virtualenv
    $python3_version = $::lsbdistcodename ? {
      'buster'     => '3.7',
    }

    $virtualenv3 = "/usr/share/ddcp/venv3/"

    python::virtualenv { $virtualenv3:
      ensure       => present,
      version      => $python3_version,
      owner        => 'ddcp',
      group        => 'ddcp', 
      distribute   => false,
      #see https://setuptools.readthedocs.io/en/latest/history.html#v50-0-0
      environment  => ['SETUPTOOLS_USE_DISTUTILS=stdlib'],
    } ~>
    exec { 'downgrade setuptools ddcp':
      user         => 'ddcp',
      command      => "bash -c 'source ${virtualenv3}/bin/activate; pip install setuptools==49.6.0'",
      cwd          => "/usr/share/ddcp",
      logoutput    => true,
      refreshonly  => true,
      before       => Exec['ddcp: pip install venv3 requirements'],
    }

    #pip install/update venv3
    exec { 'ddcp: pip install venv3 requirements':
      user        => 'ddcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; pip install -r /usr/share/ddcp/ddcp/requirements.txt'",
      cwd         => "/usr/share/ddcp/ddcp/",
      logoutput   => true,
      require     => [
                     Vcsrepo['/usr/share/ddcp/ddcp'],
                     ],
      subscribe   => [
                     Vcsrepo['/usr/share/ddcp/ddcp'],
                     ],
      refreshonly => true,
      notify     => [
                    Exec['ddcp: setup wsgi express'],
                    ],
      timeout    => 3600,
    } 


    ########################## migrations and statics ###################################


    exec { 'ddcp: makemigrations':
      user        => 'ddcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; python manage.py makemigrations'",
      cwd         => "/usr/share/ddcp/ddcp/",
      logoutput   => true,
      require     => [
                     Exec['ddcp: pip install venv3 requirements'],
                     ],
      subscribe   => [
                     Vcsrepo['/usr/share/ddcp/ddcp'],
                     ],
      refreshonly => true,
      notify      => Service['ddcp'],
    } ->
    exec { 'ddcp: migrate':
      user        => 'ddcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; python manage.py migrate'",
      cwd         => "/usr/share/ddcp/ddcp/",
      logoutput   => true,
      require     => [
                     Exec['ddcp: pip install venv3 requirements'],
                     ],
      subscribe   => [
                     Vcsrepo['/usr/share/ddcp/ddcp'],
                     ],
      refreshonly => true,
      notify      => Service['ddcp'],
    } ->
    exec { 'ddcp: static files':
      user        => 'ddcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; python manage.py collectstatic --noinput'",
      cwd         => "/usr/share/ddcp/ddcp/",
      logoutput   => true,
      require     => [
                     Exec['ddcp: pip install venv3 requirements'],
                     ],
      subscribe   => [
                     Vcsrepo['/usr/share/ddcp/ddcp'],
                     ],
      refreshonly => true,
      notify      => Service['ddcp'],
    }


    ########################## service ###################################

    #custom extra express conf / not used
    file { 'ddcp: extra express conf':
        path    => '/usr/share/ddcp/ddcp_extra_wsgi.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('dd/ddcp_extra_wsgi.conf'),
        require => [
                   Exec['ddcp: pip install venv3 requirements'],
                   ],
        notify  => Exec['ddcp: setup wsgi express'],
    }

    #setup wsgi express
    exec { 'ddcp: setup wsgi express':
      user        => 'ddcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; mod_wsgi-express setup-server ddcp/wsgi.py --host=127.0.0.1 --port=8000 --user ddcp --group ddcp --server-root=/usr/share/ddcp/express --log-directory /usr/share/ddcp/var --url-alias /static ./static'",
      cwd         => "/usr/share/ddcp/ddcp/",
      logoutput   => true,
      require     => [
                     Exec['ddcp: pip install venv3 requirements'],
                     ],
      refreshonly => true,
      notify      => Service['ddcp'],
    }


    #ddcp systemd
    file { 'ddcp: systemd service':
        path    => '/lib/systemd/system/ddcp.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('dd/ddcp.service.erb'),
        notify  => Service['ddcp'],
    }

    #ddcp service
    service { 'ddcp':
        ensure    => running,
        name      => ddcp,
        enable    => true,
        hasstatus => true,
        require   => [
                     File['ddcp: systemd service'],
                     Exec['ddcp: setup wsgi express'],
                     ],
    }


    ######################### ddcp apache ################################

    #override apache fqdn-ssl vhost
    $customfragment = '                     
                      #strip the X-Forwarded-Proto header from incoming requests
                      RequestHeader unset X-Forwarded-Proto
                      #set the header for requests using HTTPS
                      RequestHeader set X-Forwarded-Proto https env=HTTPS
                      #ddcp proxy
                      ProxyPass /mailman !
                      ProxyPass /mailman/ !
                      ProxyPass /hyperkitty/ !
                      ProxyPass / http://localhost:8000/
                      ProxyPassReverse / http://localhost:8000/
                      ' 

    Apache::Vhost <| title == "$fqdn-ssl" |> {
      custom_fragment => $customfragment,
    }

    ######################### fail2ban #####################################################

/*
    #ddcp fail2ban
    exec { 'ddcp: create initial log file if absent':
        command    => 'touch /usr/share/ddcp/var/error_log && chown ddcp:ddcp /usr/share/ddcp/var/error_log',
        logoutput  => true,
        creates    => '/usr/share/ddcp/var/error_log',
        require    => Exec['ddcp: setup wsgi express'],
    } -> 
    file { 'ddcp: fail2ban filter':
        path    => '/etc/fail2ban/filter.d/ddcp.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('dd/ddcp_fail2ban.conf'),
        notify  => Service['fail2ban'],
    } ->
    file { 'ddcp: fail2ban jail':
        path    => '/etc/fail2ban/jail.d/ddcp.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('dd/ddcp_fail2ban_jail.conf'),
        notify  => Service['fail2ban'],
    }
*/
    ########################## logrotate ###################################
/*
    logrotate::rule { 'ddcp':
      path         => '/usr/share/ddcp/var/error_log',
      rotate       => 10,
      rotate_every => 'week',
      create       => true,
      create_mode  => '640',
      create_owner => 'ddcp',
      create_group => 'ddcp',
      size         => '100k',
      postrotate   => '/usr/sbin/service ddcp restart',
    }
*/
    ######################### monit #####################################################

    if defined('::monit') and defined(Class['::monit']) {
      class {'dd::monit': enabled => true}
    }

  }
}


