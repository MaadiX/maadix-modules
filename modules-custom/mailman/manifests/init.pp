# mailman
#
#
#

class mailman (
  $fqdn = '',
  $enabled = str2bool($::mailman_enabled),
  $zeyple_enabled = str2bool($::zeyple_enabled),
  $hyperkitty_public = true,
  $remove            = $::mailman_remove,
) {

  validate_bool($enabled)

  if $enabled {

    ########################## install ###################################

    ##dependencies

    #gettext
    if ! defined(Package['gettext']) {
      ensure_resource('package','gettext', {
      ensure  => present,
      })
    }

    #ruby-sass
    package { 'ruby-sass':
      ensure => 'present',
    }

    #sassc
    if ! defined(Package['sassc']) {
      ensure_resource('package','sassc', {
      ensure  => present,
      })
    }


    ##user/group

    #mailman group
    group { 'mailman':
        ensure          => 'present',
        system          => true,
    }

    #mailman user
    user { 'mailman':
        ensure          => 'present',
        home            => '/opt/mailman',
        managehome      => true,
        shell           => '/bin/false',
        system          => true,
    } ->
    #/opt/mailman must have execution bit to others, because apache need execution along all the webroot path
    file { '/opt/mailman':
        ensure          => directory,
        mode            => '0751',
    }


    ##postgresql

    #postgresql db
    postgresql::server::db { 'mailman':
     user     => 'mailman',
     password => postgresql_password('mailman', 'mailman'),
     before   => Exec['mailman aliases'],
    }

    ##repos with fixed revisions

    #postorius repo for example_project folder
    vcsrepo { '/opt/mailman/postorius':
        ensure   => present,
        provider => git,
        source   => 'https://gitlab.com/mailman/postorius.git',
        user     => 'mailman',
        #revision => '52c9c3a5c630a59e88ff88ba0c7d66881e5b805d',
        #revision => '65c9107f6db66866160bc4bf45000b3c858f0629',
        #revision => 'e18643084e05983782c42f59cbfa3fa34d38a4f7',
        revision => '1.3.8',
        path     => '/opt/mailman/postorius',
        require  => [
                    User['mailman'],
                    ],
    }

    #hyperkitty repo
    vcsrepo { '/opt/mailman/hyperkitty':
        ensure   => present,
        provider => git,
        source   => 'https://gitlab.com/mailman/hyperkitty.git',
        user     => 'mailman',
        #revision => '1f6dba6f67e6496b09da14641ca73433e7312d59',
        #revision => '8cb1d841f257663aba3319a048249e21298e0b6c',
        #revision => 'cc37933eb16b4c47ddbe267e69a9543ee05cebfe',
        revision => '1.3.7',
        path     => '/opt/mailman/hyperkitty',
        require  => [
                    User['mailman'],
                    ],
    }

    ##virtualenv

    #python vitualenv 3.x

    $python3_version = $::lsbdistcodename ? {
      'stretch'    => '3.5',
      'buster'     => '3.7',
    }

    $virtualenv3 = "/opt/mailman/venv3/"

    python::virtualenv { $virtualenv3:
      ensure       => present,
      version      => $python3_version,
      owner        => 'mailman',
      group        => 'mailman', 
      distribute   => false,
      #see https://setuptools.readthedocs.io/en/latest/history.html#v50-0-0
      #environment  => ['SETUPTOOLS_USE_DISTUTILS=stdlib'],
      before       => Exec['pip install venv3 requirements'],
    } 

    ##one shot pip packages dependencies installation

    #pip venv3 requirements file
    file { 'pip venv3 requirements file':
      path    => '/opt/mailman/requirements_venv3.txt',
      owner   => 'mailman',
      group   => 'mailman',
      mode    => '0664',
      content => template('mailman/requirements_venv3.txt.erb'),
      notify  => Exec['expire sessions'],
    }

    #prepare updating mailman core only if it's not already installed
    exec { 'stop mailman core for update':
      command     => "service monit stop && service mailman stop",
      logoutput   => true,
      require     => [
                     File['pip venv3 requirements file'],
                     ],
      subscribe   => [
                     File['pip venv3 requirements file'],
                     ],
      before      => [
                     Exec['pip install venv3 requirements'],
                     ],
      onlyif      => "test -d /opt/mailman/var",
      refreshonly => true,
    }
    

    #pip install/update venv3 / including mailman and mailman-hyperkitty and  postorius and hyperkitty in requirements
    exec { 'pip install venv3 requirements':
      user        => 'mailman',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; pip install -r /opt/mailman/requirements_venv3.txt'",
      logoutput   => true,
      timeout     => 7200,
      require     => [
                     File['pip venv3 requirements file'],
                     ],
      subscribe   => [
                     File['pip venv3 requirements file'],
                     ],
      refreshonly => true,
    }

    #remove update_index.py job
    file { 'remove update_index.py job':
      path    => "/opt/mailman/venv3/lib/python$python3_version/site-packages/hyperkitty/jobs/update_index.py",
      ensure  => absent,
      require => Exec['pip install venv3 requirements'],
    }


    ########################## config ###################################

    ##secrets passwords
    
    #mailman restapi passw file
    exec { 'mailman restapi passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/mailman_restapi'",
      creates => '/etc/maadix/mailman_restapi',
      require => [
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }

    #mailman postorius + hyperkitty secret_key passw file
    #both djangos with the same secret
    exec { 'mailman postorius secret_key':
      command => "/bin/bash -c 'umask 077 && pwgen 50 -1 | tr -d \"\n\" > /etc/maadix/mailman_postorius_secret_key'",
      creates => '/etc/maadix/mailman_postorius_secret_key',
      require => [
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }

    #mailman hyperkitty archiver_key passw file
    exec { 'mailman hyperkitty archiver_key':
      command => "/bin/bash -c 'umask 077 && pwgen 20 -1 | tr -d \"\n\" > /etc/maadix/mailman_hyperkitty_archiver_key'",
      creates => '/etc/maadix/mailman_hyperkitty_archiver_key',
      require => [
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }

    #mailman script to set mailman admin pass
    file { 'mailman password':
      path    => '/etc/maadix/scripts/mailmanpass.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('mailman/mailmanpass.sh.erb'),
    }

    #mailman script to change fqdn
    file { 'mailman fqdn':
      path    => '/etc/maadix/scripts/fqdn_mailman.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('mailman/fqdn_mailman.sh.erb'),
    }

    ##default mailinglists options (dmarc, reply-to, etc)

    #munge-from to list address always, so messages are always accepted when dmarc domain posters send mails like yahoo
    file_line { 'mlist.dmarc_mitigate_action':
      path    => "/opt/mailman/venv3/lib/python$python3_version/site-packages/mailman/styles/base.py",
      line    => '        mlist.dmarc_mitigate_action = DMARCMitigateAction.munge_from',
      match   => '.*mlist.dmarc_mitigate_action.*$',
      require => Exec['pip install venv3 requirements'],
    }
    file_line { 'mlist.dmarc_mitigate_unconditionally':
      path    => "/opt/mailman/venv3/lib/python$python3_version/site-packages/mailman/styles/base.py",
      line    => '        mlist.dmarc_mitigate_unconditionally = True',
      match   => '.*mlist.dmarc_mitigate_unconditionally.*$',
      require => Exec['pip install venv3 requirements'],
    }
    
    #strip posters mails reply-to to avoid multiple reply-to mails when coming from some providers
    file_line { 'mlist.first_strip_reply_to':
      path    => "/opt/mailman/venv3/lib/python$python3_version/site-packages/mailman/styles/base.py",
      line    => '        mlist.first_strip_reply_to = True',
      match   => '.*mlist.first_strip_reply_to.*$',
      require => Exec['pip install venv3 requirements'],
    }

    #reply always to list
    file_line { 'mlist.reply_goes_to_list':
      path    => "/opt/mailman/venv3/lib/python$python3_version/site-packages/mailman/styles/base.py",
      line    => '        mlist.reply_goes_to_list = ReplyToMunging.point_to_list',
      match   => '.*mlist.reply_goes_to_list.*$',
      require => Exec['pip install venv3 requirements'],
    }

    #max_message_size
    file_line { 'mlist.max_message_size':
      path    => "/opt/mailman/venv3/lib/python$python3_version/site-packages/mailman/styles/base.py",
      line    => '        mlist.max_message_size = 3000',
      match   => '.*mlist.max_message_size.*$',
      require => Exec['pip install venv3 requirements'],
    }

    ##sass fix
    #doc: https://stackoverflow.com/questions/58835970/compass-compile-invalid-css-after-lor-value-expected-was
    file { 'hyperkitty bootstrap _root.scss':
      path    => "/opt/mailman/venv3/lib/python$python3_version/site-packages/hyperkitty/static/hyperkitty/libs/bootstrap/stylesheets/_root.scss",
      owner   => 'mailman',
      group   => 'mailman',
      mode    => '0644',
      content => template('mailman/bootstrap_root.scss.erb'),
      require => Exec['pip install venv3 requirements'],
      notify  => Service['httpd'],
    }

    ##config files

    #mailman info, we need to run this command first time to generate /opt/mailman/var structure
    exec { 'mailman info':
      user        => 'mailman',
      command     => "bash -c '/opt/mailman/venv3/bin/mailman info'",
      cwd         => "/opt/mailman",
      logoutput   => true,
      require     => [
                      Exec['pip install venv3 requirements'],
                     ],
      creates     => "/opt/mailman/var/etc",
    }

    #mailman config template
    #todo pass site_owner mail
    file { 'mailman.cfg':
        path    => '/opt/mailman/var/etc/mailman.cfg_template',
        owner   => 'mailman',
        group   => 'mailman',
        mode    => '0644',
        content => template('mailman/mailman.cfg.erb'),
        require => [
                   Exec['mailman info'],
                   ],
    }

    #mailman config with secrets
    exec { 'mailman.cfg with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/mailman_restapi | xargs -i sed \"s/restpassPLACEHOLDER/{}/\" /opt/mailman/var/etc/mailman.cfg_template > /opt/mailman/var/etc/mailman.cfg && chown mailman /opt/mailman/var/etc/mailman.cfg'",
      umask       => '077',
      require     => [
                     File['mailman.cfg'],
                     ],
      subscribe   => [ 
                     File['mailman.cfg'], 
                     ],
      refreshonly => true,
      notify      => Service['mailman'],
    }


    #postorius settings template
    #todo: repasar parametros de settings_local.py
    file { 'postorius settings_local.py':
        path    => '/opt/mailman/postorius/example_project/settings_local.py_template',
        owner   => 'mailman',
        group   => 'mailman',
        mode    => '0644',
        content => template('mailman/postorius_settings_local.py.erb'),
        require => [
                    Vcsrepo['/opt/mailman/postorius'],
                   ],
    }

    #postorius settings with secrets FIRST based on template
    exec { 'postorius settings_local.py with secrets 1':
      command     => "/bin/bash -c 'cat /etc/maadix/mailman_restapi | xargs -i sed \"s/restpassPLACEHOLDER/{}/\" /opt/mailman/postorius/example_project/settings_local.py_template > /opt/mailman/postorius/example_project/settings_local.py && chown mailman /opt/mailman/postorius/example_project/settings_local.py'",
      umask       => '077',
      require     => [
                     File['postorius settings_local.py'],
                     ],
      subscribe   => [ 
                     File['postorius settings_local.py'], 
                     ],
      refreshonly => true,
      notify      => Service['httpd'],
    }

    #postorius settings with secrets ANOTHER in place editing sed
    exec { 'postorius settings_local.py with secrets 2':
      command     => "/bin/bash -c 'cat /etc/maadix/mailman_postorius_secret_key | xargs -i sed -i \"s/secretkeyPLACEHOLDER/{}/\" /opt/mailman/postorius/example_project/settings_local.py'",
      umask       => '077',
      require     => [
                     Exec['postorius settings_local.py with secrets 1'],
                     File['postorius settings_local.py'],
                     ],
      subscribe   => [ 
                     File['postorius settings_local.py'], 
                     ],
      refreshonly => true,
      notify      => Service['httpd'],
    }

    #hyperkitty settings template
    #todo: repasar parametros de settings_local.py
    file { 'hyperkitty settings_local.py':
        path    => '/opt/mailman/hyperkitty/example_project/settings_local.py_template',
        owner   => 'mailman',
        group   => 'mailman',
        mode    => '0644',
        content => template('mailman/hyperkitty_settings_local.py.erb'),
        require => [
                    Vcsrepo['/opt/mailman/hyperkitty'],
                   ],
    }

    #hyperkitty settings with secrets FIRST based on template
    exec { 'hyperkitty settings_local.py with secrets 1':
      command     => "/bin/bash -c 'cat /etc/maadix/mailman_restapi | xargs -i sed \"s/restpassPLACEHOLDER/{}/\" /opt/mailman/hyperkitty/example_project/settings_local.py_template > /opt/mailman/hyperkitty/example_project/settings_local.py && chown mailman /opt/mailman/hyperkitty/example_project/settings_local.py'",
      umask       => '077',
      require     => [
                     File['hyperkitty settings_local.py'],
                     ],
      subscribe   => [ 
                     File['hyperkitty settings_local.py'], 
                     ],
      refreshonly => true,
      notify      => Service['httpd'],
    }

    #hyperkitty settings with secrets ANOTHER in place editing sed
    exec { 'hyperkitty settings_local.py with secrets 2':
      command     => "/bin/bash -c 'cat /etc/maadix/mailman_postorius_secret_key | xargs -i sed -i \"s/secretkeyPLACEHOLDER/{}/\" /opt/mailman/hyperkitty/example_project/settings_local.py'",
      umask       => '077',
      require     => [
                     Exec['hyperkitty settings_local.py with secrets 1'],
                     File['hyperkitty settings_local.py'],
                     ],
      subscribe   => [ 
                     File['hyperkitty settings_local.py'], 
                     ],
      refreshonly => true,
      notify      => Service['httpd'],
    }

    #hyperkitty settings with secrets ANOTHER in place editing sed
    exec { 'hyperkitty settings_local.py with secrets 3':
      command     => "/bin/bash -c 'cat /etc/maadix/mailman_hyperkitty_archiver_key | xargs -i sed -i \"s/archiverkeyPLACEHOLDER/{}/\" /opt/mailman/hyperkitty/example_project/settings_local.py'",
      umask       => '077',
      require     => [
                     Exec['hyperkitty settings_local.py with secrets 2'],
                     File['hyperkitty settings_local.py'],
                     ],
      subscribe   => [ 
                     File['hyperkitty settings_local.py'], 
                     ],
      refreshonly => true,
      notify      => Service['httpd'],
    }


    #mailman-hyperkitty settings template
    file { 'mailman-hyperkitty hyperkitty.cfg':
        path    => '/opt/mailman/hyperkitty/example_project/hyperkitty.cfg_template',
        owner   => 'mailman',
        group   => 'mailman',
        mode    => '0644',
        content => template('mailman/hyperkitty.cfg.erb'),
        require => [
                    Vcsrepo['/opt/mailman/hyperkitty'],
                   ],
    }

    #mailman-hyperkitty settings with secrets FIRST based on template
    exec { 'mailman-hyperkitty hyperkitty.cfg with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/mailman_hyperkitty_archiver_key | xargs -i sed \"s/archiverkeyPLACEHOLDER/{}/\" /opt/mailman/hyperkitty/example_project/hyperkitty.cfg_template > /opt/mailman/hyperkitty/example_project/hyperkitty.cfg && chown mailman /opt/mailman/hyperkitty/example_project/hyperkitty.cfg'",
      umask       => '077',
      require     => [
                     File['mailman-hyperkitty hyperkitty.cfg'],
                     ],
      subscribe   => [ 
                     File['mailman-hyperkitty hyperkitty.cfg'], 
                     ],
      refreshonly => true,
      notify      => Service['mailman'],
    }


    #generate postfix mailman aliases
    exec { 'mailman aliases':
      user        => 'mailman',
      command     => "bash -c '/opt/mailman/venv3/bin/mailman aliases'",
      cwd         => "/opt/mailman",
      logoutput   => true,
      creates     => '/opt/mailman/var/data/postfix_lmtp.db',
      require     => [
                     Exec['pip install venv3 requirements'],
                     File['mailman.cfg'],
                     ],
      notify      => Service['postfix'],
    }
    

    ##db and static migrations

    #postorius prepare database
    exec { 'postorius migrate':
      user        => 'mailman',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; python manage.py migrate'",
      cwd         => "/opt/mailman/postorius/example_project/",
      logoutput   => true,
      require     => [
                     File['postorius settings_local.py'],
                     Exec['mailman aliases'],
                     ],
      subscribe   => [
                     File['pip venv3 requirements file'],
                     ],
      refreshonly => true,
    }


    #hyperkitty prepare database
    exec { 'hyperkitty migrate':
      user        => 'mailman',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; django-admin migrate --pythonpath example_project --settings settings'",
      cwd         => "/opt/mailman/hyperkitty/",
      logoutput   => true,
      require     => [
                     File['hyperkitty settings_local.py'],
                     Exec['mailman aliases'],
                     ],
      subscribe   => [
                     File['pip venv3 requirements file'],
                     ],
      refreshonly => true,
    }


    #postorius static files
    exec { 'postorius static files':
      user        => 'mailman',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; echo \"yes\" | django-admin collectstatic --pythonpath example_project --settings settings'",
      cwd         => "/opt/mailman/postorius/",
      logoutput   => true,
      require     => [
                     Exec['postorius migrate'],
                     ],
      subscribe   => [
                     File['pip venv3 requirements file'],
                     ],
      refreshonly => true,
    }

    #hyperkitty static files
    exec { 'hyperkitty static files':
      user        => 'mailman',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; echo \"yes\" | django-admin collectstatic --pythonpath example_project --settings settings'",
      cwd         => "/opt/mailman/hyperkitty/",
      logoutput   => true,
      require     => [
                     Exec['hyperkitty migrate'],
                     ],
      subscribe   => [
                     File['pip venv3 requirements file'],
                     ],
      refreshonly => true,
    }
        
    #postorius compilemessages
    exec { 'postorius compilemessages':
      user        => 'mailman',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; django-admin compilemessages --pythonpath example_project --settings settings'",
      cwd         => "/opt/mailman/postorius/",
      logoutput   => true,
      require     => [
                     Exec['postorius migrate'],
                     Package['gettext'],
                     ],
      subscribe   => [
                     File['pip venv3 requirements file'],
                     ],
      refreshonly => true,
    }

    #fix bootstrap.scss
    #doc: https://stackoverflow.com/questions/49786406/sass-compileerror-error-its-not-clear-which-file-to-import-arising-when-usi
    exec {'fix hyperkitty bootstrap.scss':
      command => 'mv bootstrap.scss _bootstrap.scss',
      cwd     => '/opt/mailman/hyperkitty/example_project/static/hyperkitty/libs/bootstrap/stylesheets',
      onlyif  => "test -f /opt/mailman/hyperkitty/example_project/static/hyperkitty/libs/bootstrap/stylesheets/bootstrap.scss",
      require => Exec['hyperkitty static files'],
      notify  => Service['httpd'],
    }

    ##admin user

    #set mailman user and pass
    exec { 'set mailman passw':
      command => "/bin/bash /etc/maadix/scripts/mailmanpass.sh",
      creates => '/etc/maadix/status/mailman',
      logoutput   => true,
      require => [
                  File['mailman password'],
                  Exec['postorius migrate'],
                 ],
    }

    ## posgresql www-data user with access to domain table
    postgresql::server::role { 'www-data':
      password_hash => postgresql_password('www-data', 'www-data'),
      require => Exec['set mailman passw'],
    } ->
    postgresql::server::database_grant { 'www-data connect to mailman':
      privilege => 'CONNECT',
      db        => 'mailman',
      role      => 'www-data',
    } ->
    postgresql::server::table_grant { 'domain of mailman to www-data':
      privilege => 'SELECT',
      table     => 'domain',
      db        => 'mailman',
      role      => 'www-data',
    }

    ## posgresql mxcp user with access to domain table
    postgresql::server::role { 'mxcp':
      password_hash => postgresql_password('mxcp', 'mxcp'),
      require => Exec['set mailman passw'],
                  
    } ->
    postgresql::server::database_grant { 'mxcp connect to mailman':
      privilege => 'CONNECT',
      db        => 'mailman',
      role      => 'mxcp',
    } ->
    postgresql::server::table_grant { 'domain of mailman to mxcp':
      privilege => 'SELECT',
      table     => 'domain',
      db        => 'mailman',
      role      => 'mxcp',
    }

    ## fqdn change scripts
    exec { 'mailman fqdn change':
      command     => '/etc/maadix/scripts/fqdn_mailman.sh',
      logoutput   => true,
      require     => Exec['set mailman passw'],
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      notify      => [
                     Service['mailman'],
                     Service['httpd'],
                     ]
    }


    #expire sessions
    file { 'expire sessions script':
      path    => '/opt/mailman/mailman_expire_sessions.sh',
      owner   => 'mailman',
      group   => 'mailman',
      mode    => '0700',
      content => template('mailman/mailman_expire_sessions.sh'),
    } ->
    exec { 'expire sessions':
      command     => "/bin/bash /opt/mailman/mailman_expire_sessions.sh",
      user        => 'mailman',
      logoutput   => true,
      require     => [
                     Exec['set mailman passw'],
                     ],
      refreshonly => true,
    }



    ########################## services ###################################

    ##services

    #mailman systemd
    file { 'mailman systemd service':
        path    => '/lib/systemd/system/mailman.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('mailman/mailman.service.erb'),
    }

    #mailman service
    service { 'mailman':
        ensure    => running,
        name      => mailman,
        enable    => true,
        hasstatus => true,
        require => [
                  File['mailman systemd service'],
                  Exec['set mailman passw'],
                  Exec['mailman aliases'],
                 ],
    }

    ##oom conf
    file { '/lib/systemd/system/mailman.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/mailman.service.d/local.conf':
      content => template('mailman/service_mailman_local.conf.erb'),
      notify  => Service['mailman'],
    }

    #mailman-WSGI apache
    #doc: https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/modwsgi/
    file { 'apache-mailman-wsgi':
        path    => '/etc/apache2/conf-available/mailman-wsgi.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('mailman/mailman-wsgi.conf.erb'),
        require => [
                   Class['apache'],
                   Class['apache::mod::wsgi'],
                   ],
        notify  => Service['httpd'],
    }

    file { '/etc/apache2/conf.d/mailman-wsgi.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/mailman-wsgi.conf',
      notify => Service['httpd'],
      require => [
                 Class['apache'],
                 File['apache-mailman-wsgi'],
                 ],
    }

    #apache vhost to serve hyperkitty and postorius in localhost without ssl to allow mailman-hyperkitty to connect to hyperkitty
    apache::vhost{ "localhost" :
      servername      => 'localhost',
      port            => '80',
      docroot         => "/var/www/html/localhost",
      docroot_group   => '500',
      docroot_mode    => '2775',
      priority        => '20',
      override        => ['All'],
      notify => Class['apache::service'],
      require => [
                 File['/var/www/html'],
                 ],
    }

    #hyperkitty jobs are executed from crontab
    #https://gitlab.com/mailman/hyperkitty/blob/master/doc/install.rst / Asynchronous tasks section
    #jobs arein hyperkitty package /opt/mailman/venv3/lib/python$python3_version/site-packages/hyperkitty/jobs/
    file { 'hyperkitty cron':
        path    => '/etc/cron.d/hyperkitty',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('mailman/hyperkitty-cron.erb'),
        require => [
                   User['mailman'],
                   Exec['hyperkitty migrate'],
                   ],
    }
 
    #hyperkitty qcluster service file
    file { 'hyperkitty qcluster service':
        path    => '/lib/systemd/system/qcluster.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('mailman/qcluster.service.erb'),
    }

    #hyperkitty qcluster offloads long operations to separate processes, but eats cpu. disabled for now
    #TODO: check if we must enabled this service for heavy loaded mailmans
    #https://gitlab.com/mailman/hyperkitty/blob/master/doc/install.rst / Asynchronous tasks section
    service { 'qcluster':
        ensure    => stopped,
        name      => qcluster,
        enable    => false,
        hasstatus => true,
        require => [
                  File['hyperkitty qcluster service'],
                  Exec['hyperkitty migrate'],
                 ],
    }


    #postfix transport_maps, defined in helpers::postfix with other base transport_maps
    #postfix::config {
    #  'transport_maps':				value   => 'hash:/opt/mailman/var/data/postfix_lmtp', notify  => Service['postfix'];
    #}


    #monit
    #logrotate


    ########################## doc ###################################

    #exec /opt/mailman/venv3/bin/mailman aliases one time after first run to create postfix_lmtp and postfix_domains files and hashes

    #to create a list from command line (always after exec mailman aliases for the first time), as user mailman:
    #user mailman: sudo -u mailman -H bash -l
    #move to working directory: cd /opt/mailman
    #create: /opt/mailman/venv3/bin/mailman create test@correo.gadix.net
    #add member: echo "xxxxx@vvvvv.tld" | /opt/mailman/venv3/bin/mailman members -a - test@correo.gadix.net
    #list members: /opt/mailman/venv3/bin/mailman members test@correo.gadix.net



  } else {

    ##mailman service stopped
    service { 'mailman':
    	ensure    => $enabled,
    	name      => mailman,
    	enable    => $enabled,
    }

    #hyperkitty jobs
    file { 'hyperkitty cron':
        ensure  => absent,
        path    => '/etc/cron.d/hyperkitty',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('mailman/hyperkitty-cron.erb'),
    }

    #remove localhost vhost
    helpers::cleanvhost{'localhost':
      docroot    => '/var/www/html/localhost',
      priority   => 20,
    }

  }

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'mailman::external::monit':
      enabled  => $enabled,
    }
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'mailman':}
    helpers::remove {'mailman':
      pre_commands => ['rm -f /etc/apache2/conf.d/mailman-wsgi.conf && service apache2 restart'],
      files      => ['/etc/maadix/mailman_restapi',
                     '/etc/maadix/mailman_postorius_secret_key',
                     '/etc/maadix/mailman_hyperkitty_archiver_key',
                     '/etc/maadix/status/mailman',
                     '/lib/systemd/system/mailman.service',
                     '/etc/apache2/conf-available/mailman-wsgi.conf',
                     '/etc/apache2/conf.d/mailman-wsgi.conf',
                     '/lib/systemd/system/qcluster.service',
                     '/etc/maadix/status/mailman_domains_to_ldap',
                    ],
      dirs       => ['/lib/systemd/system/mailman.service.d'],
      big_dirs   => ['/opt/mailman'],
      psql_d     => ['mailman'],
      psql_u     => ['mailman'],
      users      => ['mailman'],
      notify     => [
                    Helpers::Resetldapgroup['mailman'],
                    Service['httpd']
                    ]
    }
  }


}


