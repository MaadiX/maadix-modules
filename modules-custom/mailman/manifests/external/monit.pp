class mailman::external::monit(
  $enabled        = true,
  $port           = 8024,
  #$matching	  = '/opt/mailman/venv3/bin/runner',
  $binary	  = '/opt/mailman/venv3/bin/mailman',
  $pidfile	  = '/opt/mailman/var/master.pid',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'mailman':
      init_system   => $init_system,
      pidfile       => $pidfile,
      #matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}
