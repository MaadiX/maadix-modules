#!/bin/bash

#sudo -u mailman -s
cd /opt/mailman/
source venv3/bin/activate

#for each project
for project in postorius hyperkitty
do
  cd /opt/mailman/
  cd $project/example_project/

  #add expire app
  ./manage.py startapp expire

  #add management command
  mkdir -p expire/management/commands
  cat <<EOT >> expire/management/commands/expire_sessions.py
import datetime

from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.management.base import BaseCommand
from django.http import HttpRequest
from importlib import import_module

def init_session(session_key):
    """
    Initialize same session as done for ``SessionMiddleware``.
    """
    engine = import_module(settings.SESSION_ENGINE)
    return engine.SessionStore(session_key)

class Command(BaseCommand):
    help = "Kill all active sessions"
    def handle(self, *args, **options):
        """
        Read all available users and all available not expired sessions. Then
        logout from each session. Start with a day ago to hack around the
        timezone issue instead of doing something smart.
        """
        start = datetime.datetime.now() - datetime.timedelta(days=1)
        request = HttpRequest()
        sessions = Session.objects.filter(expire_date__gt=start)
        print('Found %d not-expired session(s).' % len(sessions))
        for session in sessions:
            username = session.get_decoded().get('_auth_user_id')
            request.session = init_session(session.session_key)
            logout(request)
            print('Successfully logout %r user.' % username)
        print('All OK!')
EOT

  #add extra app to settings
  cat <<EOT >> expire_settings.py
from settings import *
INSTALLED_APPS + ('expire',)
EXTRA_APPS = ('expire',)
print(INSTALLED_APPS)
print(EXTRA_APPS)
INSTALLED_APPS = INSTALLED_APPS + EXTRA_APPS
print(INSTALLED_APPS)
EOT

  #launch management command to expire all sessions
  ./manage.py expire_sessions --settings=expire_settings

  #delete temporary files
  if [ -d expire ]; then rm -Rf expire; fi
  if [ -f expire_settings.py ]; then rm expire_settings.py; fi

done
