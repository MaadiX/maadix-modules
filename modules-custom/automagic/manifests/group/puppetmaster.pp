class automagic::group::puppetmaster {

  #we don't need to prive global hiera.yaml for now. we use per environment hiera.yaml
  /*
  file {'puppet_hiera_yaml':
    path   => "${::settings::confdir}/hiera.yaml",
    source => [
      #"puppet:///${::environment}_files/%{::inventory}/hiera/hiera.yaml",
      "puppet:///${::environment}_files/shared/hiera/hiera.yaml"
      ],
    notify => Service['puppetserver'],
  }
  */

  file {'puppet_fileserver_conf':
    path    => "${::settings::confdir}/fileserver.conf",
    content => template("${::settings::environmentpath}/${::environment}/files/shared/puppet/fileserver_mountpoints.erb"),
    notify  => Service['puppetserver'],
  }

}
