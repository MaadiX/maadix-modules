# == Class: phamm
#
# Full description of class phamm here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'phamm':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class phamm {

  include phamm::install

  #todo, ahora solo necesitamos los schemas, comentamos el resto. luego habrá que rehacer el modulo para que se pueda acceder a phamm en caso necesario, activando un grupo de cpanel

  #files
  #file { '/etc/phamm':
  #  ensure => directory,
  #  mode   => '0755',
  #}
  #file { '/etc/phamm/config.php':
  #  ensure  => $ensure,
  #  owner   => 'root',
  #  group   => 'www-data',
  #  mode    => '0640',
  #  content => template('phamm/config.php.erb'),
  #}

  #configuracion phamm /etc/phamm/config.php
  #file_line { 'SUFFIX':
  #  path  => '/etc/phamm/config.php',
  #  line  => 'define (\'SUFFIX\',\'dc=example,dc=tld\');',
  #  match => 'SUFFIX.*$',
  #}
  #file_line { 'BINDDN':
  #  path  => '/etc/phamm/config.php',
  #  line  => 'define (\'BINDDN\',\'cn=admin,dc=example,dc=tld\');',
  #  match => 'BINDDN.*$',
  #}
  #file_line { 'LDAP_BASE':
  #  path  => '/etc/phamm/config.php',
  #  line  => 'define (\'LDAP_BASE\',\'o=hosting,dc=example,dc=tld\');',
  #  match => 'LDAP_BASE.*$',
  #}
  #file_line { 'ENC_TYPE':
  #  path  => '/etc/phamm/config.php',
  #  line  => 'define (\'ENC_TYPE\',\'CRYPT\');',
  #  match => 'ENC_TYPE.*$',
  #}

  #schemas
  file { '/etc/ldap':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/ldap/schema':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/ldap/schema/ISPEnv2.schema':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('phamm/ISPEnv2.schema.erb'),
  }
  file { '/etc/ldap/schema/phamm.schema':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('phamm/phamm.schema.erb'),
  }
  file { '/etc/ldap/schema/amavis.schema':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('phamm/amavis.schema.erb'),
  }
  file { '/etc/ldap/schema/phamm-vacation.schema':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('phamm/phamm-vacation.schema.erb'),
  }

  ##TODO configurar transport dovecot por defecto en archivo de configuracion xml

  #apache
  #file { '/etc/apache2/conf.d/phamm.conf':
  #  ensure => 'link',
  #  target => '/etc/apache2/conf-enabled/phamm.conf',
  #  notify => Service['httpd'],
  #}

}
