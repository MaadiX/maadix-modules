# == Class: phamm::install
class phamm::install inherits phamm {

  #no necesitamos phamm, solo los esquemas que proveen los siguientes paquetes
  #package { 'phamm':
  #  ensure => installed,
  #}

  #not available in buster, provide schema files with puppet
  #package { 'phamm-ldap':
  #  ensure => installed,
  #}
  #package { 'phamm-ldap-amavis':
  #  ensure => installed,
  #}
}

