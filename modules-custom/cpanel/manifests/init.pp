# == Class: cpanel
#
# Full description of class cpanel here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'cpanel':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class cpanel (
  $version = '',
  $logmail = '',
  $puppet_cert_domain = '',
){

  #apache
  file { '/etc/apache2/conf-available/cpanel.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('cpanel/cpanel.conf.erb'),
    require => Class['apache'],
  }
  file { '/etc/apache2/conf.d/cpanel.conf':
    ensure => 'link',
    target => '/etc/apache2/conf-available/cpanel.conf',
    notify => Service['httpd'],
    require => File['/etc/apache2/conf-available/cpanel.conf'],
  }

  #apache cpanel/ldap enabled hosts
  file { '/etc/apache2/ldap-enabled':
    ensure => 'directory',
    require => 'Class[Apache]',
  }
  file_line { 'cpanel enabled hosts':
    path  => '/etc/apache2/apache2.conf',
    line  => 'IncludeOptional "/etc/apache2/ldap-enabled/*"',
    notify=> Service['httpd'],
  }

  #git repo
  vcsrepo { '/usr/share/cpanel':
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/MaadixNet/cpanel-ldap.git',
    revision => $version,
    #before =>File['/usr/share/cpanel/site-config.php'],
  }

  #git repo cpanel-puppet
  vcsrepo { '/usr/share/cpanel-puppet':
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/devwwb/cpanel-puppet.git',
    revision => $version,
  }

  #cron
  cron { 'cpanel cron':
    command => '/bin/sleep `/usr/bin/numrandom /0..60/`s ; /bin/bash /usr/share/cpanel/cron/ldapsearch.sh > /dev/null 2>&1',
    user    => 'root',
    minute  => '*/5',
  }

  #cron puppet file
  #todo, move this cron to cpanel repo
  file { '/usr/share/cpanel/cron/puppetcron.sh':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('cpanel/puppetcron.sh.erb'),
    require => Vcsrepo['/usr/share/cpanel'],
  }

  #cron with output errors
  cron { 'puppet cron':
    command => '/bin/sleep `/usr/bin/numrandom /30..59/`s ; /bin/bash /usr/share/cpanel/cron/puppetcron.sh > /dev/null',
    user    => 'root',
    minute  => '*/1',
  }

  #local puppet cron with output errors
  cron { 'local puppet cron':
    command => '/bin/sleep `/usr/bin/numrandom /1..29/`s ; /bin/bash /usr/share/cpanel-puppet/cron/cpanel-puppet.sh > /dev/null',
    user    => 'root',
    minute  => '*/1',
  }

  #release timestamp file
  #todo, move this file to cpanel repo
  file { '/usr/share/cpanel/cron/releasetimestamp.sh':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('cpanel/releasetimestamp.sh.erb'),
    require => Vcsrepo['/usr/share/cpanel'],
  }

  ##scripts to lock local puppet modules
  #lock reboot module
  file { 'lock reboot script':
    path    => '/etc/maadix/scripts/setlockedreboot.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('cpanel/setlockedreboot.sh.erb'),
  }

  #set attribute info to reboot in reboot module
  file { 'info to reboot script':
    path    => '/etc/maadix/scripts/setrebootinforeboot.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('cpanel/setrebootinforeboot.sh.erb'),
  }

  ##script to setlock cpanel status to running
  file { 'cpanel status to running script':
    path    => '/etc/maadix/scripts/setrunningcpanel.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('cpanel/setrunningcpanel.sh.erb'),
  }


  ##oneshot init script to set status=ready to cpanel object after reboot
  file { 'remove old cpanel ready init script link in rc2.d':
    path   => '/etc/rc2.d/S04setreadycpanel',
    ensure => 'absent',
  } ->
  file { 'cpanel ready init script':
    path    => '/etc/init.d/setreadycpanel',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => template('cpanel/setreadycpanel.erb'),
  } ->
  exec { 'update-rc.d for cpanel ready script':
    command     => "/bin/bash -c 'update-rc.d setreadycpanel defaults'",
    subscribe   => [
                    File['cpanel ready init script'],
                   ],
    refreshonly => true,
  }

  ##scripts to manage ldap objects
  #set groups fields/values
  file { 'set ldap group field':
    path    => '/etc/maadix/scripts/setldapgroupfield.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('cpanel/setldapgroupfield.sh.erb'),
  }


}
