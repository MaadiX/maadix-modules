#!/bin/bash

##### Config ###############################

url="ldapi://"
basedn="ou=api,dc=example,dc=tld"
vmname=$(hostname)
newapi="https://api2.maadix.org/api/1.0"

##### Utilities ###############################

apt install -y jq httpie

##### Tasks ###############################

## Get oldtoken from ldap and convert to meteor hashedtoken

#get token stored in ldap
oldb64token=`ldapsearch -Q -Y EXTERNAL -H "$url" -b "$basedn" -s base | awk -F ":: " '$1 == "userPassword" {print $2}'`
#base64 decode
oldtoken=`echo "$oldb64token" | perl -MMIME::Base64 -ne 'print decode_base64($_) . "\n"'`
#generate sha256 in base64 (used in meteor)
oldb64sha256token=`echo -n "$oldtoken" | openssl dgst -binary -sha256 | openssl base64`

## Login with oldb64sha256token as passwd to get new logintoken

newtoken=$(http --ignore-stdin post $newapi/api-token-auth/ username=$vmname password=$oldb64sha256token | jq --raw-output '.token')

## Save new api url and logintoken in ldap
ldapmodify -Q -Y EXTERNAL -H "$url"  << EOF
dn: $basedn
changetype: modify
replace: userPassword
userPassword: $newtoken
-
replace: host
host: $newapi

EOF
