#!/bin/bash

#grep zone-mta commands without grepping grep command
c=$(ps aux | grep [z]one-mta | wc -l)
if [[ $c -gt 12 ]]; then
  echo "zone-mta process running: $c"
  echo "restart mailtrain2"
  /usr/sbin/service monit stop
  /usr/sbin/service mailtrain2 stop
  sleep 120
  /usr/sbin/service monit start
  exit 1
fi

