#!/bin/bash

truncate -s 0 /var/lib/munin/storagebox_munin
#read csv file with NODE,USER and PASS
#echo "storagebox munin data"
while IFS="," read -r node user pass
do
  if [ ! -z "$user" ]; then
    #echo "reading df from $node $user $pass"
    export SSHPASS=$pass
    IN=$(echo "df -h"  | sshpass -e sftp -o PreferredAuthentications=password -o PubkeyAuthentication=no -q -o StrictHostKeyChecking=no -oBatchMode=no -b - $user@$user.your-storagebox.de | tail -n1)
    VALUE=$(echo ${IN##* } | sed 's/%//g')
    #echo "$node $user $VALUE"
    echo "$node $user $VALUE" >> /var/lib/munin/storagebox_munin
  fi
done < <(tail -n +2 /etc/maadix/storagebox.txt)
