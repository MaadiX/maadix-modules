cd /www
#override templates
sh -c 'cp /overrides/*.html /www/templates/'
#add timeout 3000s
sh -c 'python3 database.py; gunicorn --bind 0.0.0.0:5000 -w 8 --access-logfile - --error-logfile - --timeout 3000 wsgi'
