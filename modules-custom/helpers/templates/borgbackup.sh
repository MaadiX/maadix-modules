#!/bin/bash
#set -e

## params
url="ldapi://"
basedn="dc=example,dc=tld"
apiobject="ou=api,dc=example,dc=tld"
hostname=$(hostname)
fqdn=$(hostname -f)
email=root@$hostname
tokenbase64=`ldapsearch -Q -Y EXTERNAL -H "$url" -b "$apiobject" -s base | awk -F ":: " '$1 == "userPassword" {print $2}'`
host=`ldapsearch -Q -Y EXTERNAL -H "$url" -b "$apiobject" -s base | awk -F ": " '$1 == "host" {print $2}'`
apiurl="${host}/vm/${hostname}/"
token=`echo "$tokenbase64" | base64 --decode`
adminmail="admin@maadix.org"
logmail="logs@maadix.org"
date=$(date +%Y_%m_%d-%H_%M_%S)
adminuser=$(ldapsearch -Q -Y EXTERNAL -H ldapi:/// -b 'dc=example,dc=tld' -s one "(&(objectclass=simpleSecurityObject)(status=*))" | awk -F ": " '$1 == "cn" {print $2}')

## conf
conf=$(curl -s $apiurl -X GET -H "Content-Type: application/json" -H "X-HOSTNAME: ${hostname}" -H "Authorization: Token ${token}")
enabled=$(echo "${conf}" | jq -r '.backup_enabled')
initializedbackups=0
if [ -z "$conf" ]; then
  #if this server is not registered in api, exit
  exit 0
fi

# If borg create process exists, exit
if [[ $(pgrep -f "borg create") ]]; then
  pid=`pgrep -f "borg create"`
  echo "Borg create is running with pid $pid, exit"
  exit 1
fi

## functions
sendusermailnew ()
{
  useremail=`ldapsearch -Q -Y EXTERNAL -H "$url" -b "$basedn" -s one "(&(objectclass=simpleSecurityObject)(status=*))" | awk -F ": " '$1 == "email" {print $2}'`

# Body
read -r -d '' body << EOM
[English version below]

Hola,

El sistema de backup se ha activado correctamente en tu servidor

IMPORTANTE: Los backups se guardan cifrados en el servidor remoto. Para descifrarlo es necesario poseer las claves de desencriptación.
Ahora mismo hay una única copia de estas claves y están alojadas en tu servidor.
Accede inmediatamente al panel de control para exportar una copia y guardarlas en un dispositivo externo seguro.
El mismo panel de control te guiará en todo el proceso.

Puedes encontrar más información aquí:

https://docs.maadix.net/borg/


[EN]

Hello,

The backup system has been successfully enabled on your server.

WARNING: The backups are stored encrypted on the remote server. To be decrypted the decryption keys are required.
Right now there is only one copy of this keys and they are stored on your server.
Log in immediately to the control panel to export a copy and save them in a safe external storage device.

The same control panel will guide you through the whole process.

You can find more information here:

https://en.docs.maadix.net/borg/


----------
MaadiX.net
https://maadix.net

EOM

  #send mail
  echo "$body" | mail -s "Maadix: sistema de backup activado en ${fqdn}" $useremail
}

sendusermailmodify ()
{
  useremail=`ldapsearch -Q -Y EXTERNAL -H "$url" -b "$basedn" -s one "(&(objectclass=simpleSecurityObject)(status=*))" | awk -F ": " '$1 == "email" {print $2}'`

# Body
read -r -d '' body << EOM
[English version below]

Hola,

Se ha modificado la configuración del sistema de copias de seguridad en tu servidor y con ella las claves para desencriptar los backups.

Recuerda que los backups se guardan cifrados en el servidor remoto. Para descifrarlo es necesario poseer las claves de desencriptación.
Ahora mismo hay una única copia de estas claves y están alojadas en tu servidor. La copia que guardaste anteriormente ya no es válida así que es necesario que accedas inmediatamente al panel de control para volver a exportar una copia y guardarlas en un dispositivo externo seguro.

El mismo panel de control te guiará en todo el proceso.

Puedes encontrar más información aquí:

https://docs.maadix.net/borg/

[EN]

Hello,

The configuration of the backup system on your server has been modified as well as the keys for decrypting the backups.

Remember that the backups are stored encrypted on the remote server. To decrypt them you need to have the decryption keys.
Right now there is only one copy of these keys and they are hosted on your server. The copy you saved previously is no longer valid so you need to immediately access the control panel to re-export a copy and save it on a secure external device.

The same control panel will guide you through the whole process.

You can find more information here:

https://en.docs.maadix.net/borg/


----------
MaadiX.net
https://maadix.net

EOM

  #send mail
  echo "$body" | mail -s "Maadix: sistema de backup activado en ${fqdn}" $useremail
}

setkeycopy ()
{
ldapmodify -Q -Y EXTERNAL -H ldapi:/// << EOF
dn: ou=keycopy,ou=cpanel,dc=example,dc=tld
changetype: modify
replace: status
status: modify
EOF
}

databasedumps ()
{
  #mysql
  mkdir -p /ddbbs/mysql
  chmod 700 /ddbbs
  cd /ddbbs/mysql
  mysql -e 'show databases' -s --skip-column-names > /tmp/borgddbblist
  #allow spaces in database names
  mapfile -t ddbb < /tmp/borgddbblist
  for DB in "${ddbb[@]}"; do
    if [ "$DB" = "information_schema" ] || [ "$DB" = "performance_schema" ] ; then
      :
    else
      mysqldump "$DB" | gzip -9 > "$DB.sql.gz";
    fi
  done
  rm /tmp/borgddbblist
  #ldap
  ldapsearch -Q -H ldapi:// -Y EXTERNAL -b 'dc=example,dc=tld' > /ddbbs/ldap.ldif
  #mongo
  if test -f /usr/bin/mongod ; then
    dpkg -s mongodb-org-tools >/dev/null 2>&1
    if [ $? -ne 0 ]; then
      apt-get install mongodb-org-tools -y --force-yes
    fi
    mkdir /ddbbs/mongodb
    cd /ddbbs/mongodb
    #read pass from config file to avoid visibility whith ps
    echo -n "password: " > /etc/maadix/mongoconfig.yaml
    cat /etc/maadix/mongodbadmin | tr -d '\n' | sed "s@\\\\@@g" | tr -d \'\" >> /etc/maadix/mongoconfig.yaml
    mongodump --quiet --config /etc/maadix/mongoconfig.yaml --host localhost --port 27017 --ssl --sslCAFile /opt/mongod/certs/rootCA.crt -u admin
    #ls -l dump/
  fi
  #postgre
  if test -f /usr/lib/postgresql/<%=@postgresqlversion %>/bin/postgres ; then
    mkdir /tmp/postgres
    chmod 700 /tmp/postgres
    chown postgres /tmp/postgres
    cd /tmp
    sudo -u postgres pg_dumpall > /tmp/postgres/postgresql.sql
    mv /tmp/postgres/postgresql.sql /ddbbs/
    rmdir /tmp/postgres
  fi
}

serverbackup ()
{

  #servers vars
  if [[ "$1" -eq 1 ]]; then
    server=$(echo "${conf}" | jq -r '.backup_server')
    user=$(echo "${conf}" | jq -r '.backup_user')
  else
    server=$(echo "${conf}" | jq -r '.backup_secondary_server // empty')
    user=$(echo "${conf}" | jq -r '.backup_secondary_user // empty')
  fi

  #check vars
  if [ -z "$server" ] || [ -z "$user" ]; then
    return 0
  fi

  #backup vars
  port=$(echo "${conf}" | jq -r '.backup_port')
  daily=$(echo "${conf}" | jq -r '.backup_daily')
  weekly=$(echo "${conf}" | jq -r '.backup_weekly')
  monthly=$(echo "${conf}" | jq -r '.backup_monthly')

  #borg backup vars
  ssh-keygen -H -F [$server]:$port > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    export BORG_RSH="ssh -i /root/.ssh/id_rsa_borgbackup -o StrictHostKeyChecking=no"
  else
    export BORG_RSH="ssh -i /root/.ssh/id_rsa_borgbackup"
  fi
  export BORG_REPO="ssh://$user@$server:$port/./backup"
  export BORG_PASSPHRASE=""

  #borg init
  borg info > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    borg init --encryption=keyfile > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      let initializedbackups++
    else
      echo "" | mail -s "Borg init ${user}@${server} failed in vm ${hostname}" $adminmail
      exit 0
    fi
  fi

  #borg break
  borg break-lock

  #borg create
  borg create                                         \
      --filter AME                                    \
      --compression lz4                               \
      --exclude-caches                                \
      --exclude-from /etc/maadix/scripts/borgexclude  \
      --exclude /home/$adminuser/$hostname-backups    \
                                                      \
      ::'{hostname}-{now}'                            \
      /boot /etc /home /opt /root /usr /srv /var /ddbbs

  #borg prune
  borg prune                          \
      --prefix '{hostname}-'          \
      --keep-daily    $daily          \
      --keep-weekly   $weekly         \
      --keep-monthly  $monthly        \

  #borg compact / from 1.2.x version
  #borg compact

  #borg list
  if [[ "$1" -eq 1 ]]; then
    borg list --json > /usr/share/mxcp/borgbackup
    cat /usr/share/mxcp/borgbackup | mail -s "Borg backup list of ${hostname}" $logmail
  fi

}

deleterepo ()
{

  #servers vars
  server=$(echo "${conf}" | jq -r '.backup_server_old // empty')
  user=$(echo "${conf}" | jq -r '.backup_user_old // empty')

  #check vars
  if [ -z "$server" ] || [ -z "$user" ]; then
    return 0
  fi

  #backup vars
  port=$(echo "${conf}" | jq -r '.backup_port')

  #borg backup vars
  ssh-keygen -H -F [$server]:$port > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    export BORG_RSH="ssh -i /root/.ssh/id_rsa_borgbackup -o StrictHostKeyChecking=no"
  else
    export BORG_RSH="ssh -i /root/.ssh/id_rsa_borgbackup"
  fi
  export BORG_REPO="ssh://$user@$server:$port/./backup"
  export BORG_PASSPHRASE=""

  #delete repo
  export BORG_DELETE_I_KNOW_WHAT_I_AM_DOING="YES"
  borg delete ssh://$user@$server:$port/./backup

  #notify
  echo "" | mail -s "Borg Old Repo ${user}@${server} removed from vm ${hostname}" $adminmail

}


## backup
if [ $enabled == 'true' ]; then

  #ssh keys
  server1=$(echo "${conf}" | jq -r '.backup_server')
  server2=$(echo "${conf}" | jq -r '.backup_secondary_server')
  if [ ! -f /root/.ssh/id_rsa_borgbackup ]; then
    ssh-keygen -f /root/.ssh/id_rsa_borgbackup -t rsa -b 4096 -C "$email" -q -N ""
    cat /root/.ssh/id_rsa_borgbackup.pub | mail -s "Borg backup SSH key for ${hostname} in ${server1} and ${server2}" $adminmail
    exit 0
  fi

  #database dumps
  databasedumps

  #backup to main server
  serverbackup 1

  #backup to secondary server
  serverbackup 2

  #delete old repo
  deleterepo

  #clean tmp dirs
  if test -d /ddbbs ; then
    rm -r /ddbbs
  fi

  #notify user and modify keycopy ldap object
  if [ $initializedbackups -gt 0 ]; then
    if [ $initializedbackups -eq 2 ]; then
      sendusermailnew
      ldapdelete -Q -Y external -H ldapi:/// "ou=keycopy,ou=cpanel,dc=example,dc=tld" > /dev/null 2>&1
    else
      sendusermailmodify
      setkeycopy
    fi
  fi

fi

