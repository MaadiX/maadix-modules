#!/bin/bash

#list rules
#iptables -S
#list nat rules
#iptables -t nat -S

##delete rules
/sbin/iptables -D FORWARD -j DOCKER-USER
/sbin/iptables -D FORWARD -j DOCKER-ISOLATION-STAGE-1
/sbin/iptables -D FORWARD -o docker0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
/sbin/iptables -D FORWARD -o docker0 -j DOCKER
/sbin/iptables -D FORWARD -i docker0 ! -o docker0 -j ACCEPT
/sbin/iptables -D FORWARD -i docker0 -o docker0 -j DROP
/sbin/iptables -D DOCKER-ISOLATION-STAGE-1 -i docker0 ! -o docker0 -j DOCKER-ISOLATION-STAGE-2
/sbin/iptables -D DOCKER-ISOLATION-STAGE-1 -j RETURN
/sbin/iptables -D DOCKER-ISOLATION-STAGE-2 -o docker0 -j DROP
/sbin/iptables -D DOCKER-ISOLATION-STAGE-2 -j RETURN
/sbin/iptables -D DOCKER-USER -j RETURN
#specific for containers / one rule for each container / deleted automatically when containers are stopped
#/sbin/iptables -D DOCKER -d 172.17.0.2/32 ! -i docker0 -o docker0 -p tcp -m tcp --dport 80 -j ACCEPT
#/sbin/iptables -D DOCKER -d 172.17.0.3/32 ! -i docker0 -o docker0 -p tcp -m tcp --dport 80 -j ACCEPT

##delete chains
/sbin/iptables -X DOCKER
/sbin/iptables -X DOCKER-ISOLATION-STAGE-1
/sbin/iptables -X DOCKER-ISOLATION-STAGE-2
/sbin/iptables -X DOCKER-USER

##delete nat rules
/sbin/iptables -t nat -D OUTPUT -m addrtype --dst-type LOCAL -j DOCKER
/sbin/iptables -t nat -D PREROUTING -m addrtype --dst-type LOCAL -j DOCKER
/sbin/iptables -t nat -D POSTROUTING -o docker0 -m addrtype --src-type LOCAL -j MASQUERADE
/sbin/iptables -t nat -D POSTROUTING -s 172.17.0.0/16 ! -o docker0 -j MASQUERADE
#specific for containers / one rule for each container / deleted automatically when containers are stopped
#/sbin/iptables -t nat -D POSTROUTING -s 172.17.0.2/32 -d 172.17.0.2/32 -p tcp -m tcp --dport 80 -j MASQUERADE
#/sbin/iptables -t nat -D POSTROUTING -s 172.17.0.3/32 -d 172.17.0.3/32 -p tcp -m tcp --dport 80 -j MASQUERADE
#specific for containers / one rule for each container / deleted automatically when containers are stopped
#/sbin/iptables -t nat -D DOCKER -d 127.0.0.1/32 -p tcp -m tcp --dport 8090 -j DNAT --to-destination 172.17.0.2:80
#/sbin/iptables -t nat -D DOCKER -d 127.0.0.1/32 -p tcp -m tcp --dport 9080 -j DNAT --to-destination 172.17.0.3:80

##delete nat chains
/sbin/iptables -t nat -X DOCKER
