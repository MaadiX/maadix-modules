#!/usr/bin/ruby

#external fact to get public ip using resolv, available for all users
#facter --external-dir=/opt/puppetlabs/facter/facts.d public_ip

require "resolv"
require 'socket'

#fqdn = Addrinfo.getaddrinfo(Socket.gethostname, nil).first.getnameinfo.first
fqdn = Socket.gethostbyname(Socket.gethostname).first
begin
  dns = Resolv::DNS.new( :nameserver => ['127.0.0.1'] )
  public_ip = dns.getaddress( fqdn ).to_s
rescue
  dns = Resolv::DNS.new( :nameserver => ['1.1.1.1'] )
  public_ip = dns.getaddress( fqdn ).to_s
end
print "public_ip=" + public_ip

