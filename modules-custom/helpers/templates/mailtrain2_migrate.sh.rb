#!/bin/bash

#conf
olddomain="<%= scope.lookupvar('helpers::mailtrain2::mailtrain1domain') -%>"
newdomain="<%= scope.lookupvar('helpers::mailtrain2::domain') -%>"

#stop monit
echo "stop monit"
service monit stop
service cron stop

#wait untill al services are ready
echo "wait until all services are ready"
sleep 300

#backup origianl mosaico_templates table
echo "backup origianl mosaico_templates table"
docker exec mailtrain2_mysql_1 mysqldump --password=mailtrain mailtrain mosaico_templates > /tmp/mosaico_templates.sql

#fix hash_email migration script
echo "fix hash_email migration script"
docker exec mailtrain2_mailtrain_1 sed -i 's/ADD UNIQUE KEY/ADD KEY/g' /app/server/setup/knex/migrations/20190722110000_hash_email.js

#dump mailtrainv1 database
echo "dump mailtrainv1 database"
mysqldump mailtrain > /tmp/mailtrain.sql

#recreate database
echo "recreate database"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain -e 'drop database mailtrain;'
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain -e 'create database mailtrain;'

#import mailtrainv1 database
echo "import mailtrainv1 database"
cat /tmp/mailtrain.sql | docker exec -i mailtrain2_mysql_1 mysql --password=mailtrain mailtrain

#fix ddbb
echo "fix ddbb"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain -e "DELETE FROM campaigns WHERE list NOT IN (SELECT id FROM lists);"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain -e "delete from attachments;"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain -e "delete from templates where \`editor_name\`='';"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain -e "DELETE FROM settings WHERE \`key\`='default_homepage';"

#copy images from mailtrainv1
echo "copy images from mailtrainv1"
mkdir -p /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/grapejs/uploads
cp -R /var/www/mailtrain/mailtrain/public/mosaico/uploads/* /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/mosaico/uploads/
cp -R /var/www/mailtrain/mailtrain/public/grapejs/uploads/* /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/grapejs/uploads/

#copy mosaico templates from mailtrainv1, but versafix-1
echo "copy mosaico templates from mailtrainv1"
rsync -av --exclude versafix-1 /var/www/mailtrain/mailtrain/public/mosaico/templates/* /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/mosaico/templates/

#copy grapejs templates from mailtrainv1
#echo "copy grapejs templates from mailtrainv1"
#rsync -av /var/www/mailtrain/mailtrain/public/grapejs/templates /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/grapejs/


#restart mailtrainv2
echo "stop mailtrainv2"
service mailtrain2 stop
echo "sleep 60"
sleep 60
echo "start mailtrainv2"
service mailtrain2 start
echo "sleep 600"
sleep 600

#fix image urls
echo "fix image urls"
oldurl="https://$olddomain"
newurl="https://$newdomain"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update campaigns set data=REPLACE(data,'$oldurl/mosaico','$newurl/mosaico');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update campaigns set data=REPLACE(data,'$oldurl/grapejs','$newurl/grapejs');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set data=REPLACE(data,'$oldurl/mosaico','$newurl/mosaico');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set data=REPLACE(data,'$oldurl/grapejs','$newurl/grapejs');"

#restore mosaico_templates table
echo "restore mosaico_templates table"
cat /tmp/mosaico_templates.sql | docker exec -i mailtrain2_mysql_1 mysql --password=mailtrain mailtrain

#fix merge tags
echo "fix merge tags campaigns"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update campaigns set data=REPLACE(data,'show_link','LINK_BROWSER');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update campaigns set data=REPLACE(data,'unsubscribe_link','LINK_UNSUBSCRIBE');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update campaigns set data=REPLACE(data,'profile_link','LINK_PREFERENCES');"
echo "fix merge tags templates"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set data=REPLACE(data,'show_link','LINK_BROWSER');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set data=REPLACE(data,'unsubscribe_link','LINK_UNSUBSCRIBE');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set data=REPLACE(data,'profile_link','LINK_PREFERENCES');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set html=REPLACE(html,'show_link','LINK_BROWSER');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set html=REPLACE(html,'unsubscribe_link','LINK_UNSUBSCRIBE');"
docker exec mailtrain2_mysql_1 mysql -vv --password=mailtrain mailtrain <<< "update templates set html=REPLACE(html,'profile_link','LINK_PREFERENCES');"

#fix versafix html template
sed -i 's/profile_link/LINK_PREFERENCES/g' /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/mosaico/templates/versafix-1/template-versafix-1.html
sed -i 's/unsubscribe_link/LINK_UNSUBSCRIBE/g' /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/mosaico/templates/versafix-1/template-versafix-1.html
sed -i 's/show_link/LINK_BROWSER/g' /var/lib/docker/volumes/mailtrain2_mailtrain-static/_data/mosaico/templates/versafix-1/template-versafix-1.html

#delete temporal files
rm /tmp/mosaico_templates.sql
rm /tmp/mailtrain.sql

#reset ldap
echo "reset ldap"
ldapdelete -H ldapi:/// -Y external 'ou=migrate,ou=mailtrain2,ou=groups,dc=example,dc=tld'

#start monit
echo "start monit"
service monit start
service cron start
