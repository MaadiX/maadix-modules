#!/bin/bash

##### Info #################################

#Script to set php version in ldap. If the object doesn't exists, create it. Use: /etc/maadix/scripts/setldapphpversion.sh VERSION

##### Config ###############################

dn="ou=php,ou=conf,ou=cpanel,dc=example,dc=tld"
value=$1

##### MAIN ############################

ldapsearch -LLL -H ldapi:/// -Y external -b $dn | grep status | grep $value
if [[ $? -eq 0 ]]; then
  #nothing to do
  exit 0
fi

ldapsearch -LLL -H ldapi:/// -Y external -b $dn
if [[ $? -eq 0 ]]; then
  echo "modifying ldap object"
  ldapmodify -Q -Y EXTERNAL -H ldapi:///  << EOF
dn: $dn
changetype: modify
replace: status
status: $value
EOF

else
  echo "creating ldap object"
  ldapadd -Q -Y EXTERNAL -H ldapi:///  << EOF
dn: $dn
ou: php
objectClass: organizationalUnit
objectClass: metaInfo
status: $value
EOF

fi

