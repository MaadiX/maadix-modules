#!/bin/bash

#vars
hostname=$(hostname)

#unattended params
if [ -f /tmp/gpg-root ]; then
  rm /tmp/gpg-root
fi
touch /tmp/gpg-root
chmod 600 /tmp/gpg-root
cat >> /tmp/gpg-root <<EOF
     %echo Generating a basic OpenPGP key
     Key-Type: RSA
     Key-Length: 4096
     Name-Real: root
     Name-Email: root@$hostname
     Expire-Date: 0
     %no-ask-passphrase
     %no-protection
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
EOF

#generate key
gpg --batch --generate-key /tmp/gpg-root
rm /tmp/gpg-root

#export pub key
if [ -f /tmp/pubkey.txt ]; then
  rm /tmp/pubkey.txt
fi
gpg --armor --output /tmp/pubkey.txt --export 'root'

#import key for mxcp user
sudo -u mxcp gpg --import /tmp/pubkey.txt
rm /tmp/pubkey.txt

