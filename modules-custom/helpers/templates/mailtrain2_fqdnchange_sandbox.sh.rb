#!/bin/bash

#conf
olddomain="<%= scope.lookupvar('helpers::mailtrain2::sandboxdomain_old') -%>"
newdomain="<%= scope.lookupvar('helpers::mailtrain2::sandboxdomain') -%>"

#wait untill al services are ready
echo "wait until al services are ready"
sleep 60

#fix image urls
echo "fix urls"
oldurl="https://$olddomain"
newurl="https://$newdomain"
docker exec mailtrain2_mysql_1 mysql --password=mailtrain mailtrain -e "update campaigns set data=REPLACE(data,'$oldurl','$newurl');"
docker exec mailtrain2_mysql_1 mysql --password=mailtrain mailtrain -e "update campaigns set data=REPLACE(data,'$oldurl','$newurl');"
docker exec mailtrain2_mysql_1 mysql --password=mailtrain mailtrain -e "update templates set data=REPLACE(data,'$oldurl','$newurl');"
docker exec mailtrain2_mysql_1 mysql --password=mailtrain mailtrain -e "update templates set data=REPLACE(data,'$oldurl','$newurl');"
