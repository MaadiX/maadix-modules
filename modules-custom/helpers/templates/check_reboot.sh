#!/bin/bash

#use: /etc/maadix/scripts/check_reboot.sh 1|2|3|4
#1 = 0:00
#2 = 3:00
#3 = 6:00
#4 = 9:00

##params
url="ldapi://"
basedn="ou=timereboot,ou=conf,ou=cpanel,dc=example,dc=tld"
thistime=$1

if [ -f /usr/bin/ldapsearch ]; then
  #if system must reboot
  if [ -f /var/run/reboot-required ]; then
    echo "reboot required"
    #if puppet and unattended-upgrades are not running
    if [ "$(pidof puppet-agent)" ] || [ "$(pidof unattended-upgrade)" ] || [ "$(pidof unattended-upgrades)" ]; then
      echo "puppet or upgrades are running, nothing to do"
    else
      echo "puppet and upgrades are not running, proceed"
      #get configured time
      timereboot=`ldapsearch -Q -Y EXTERNAL -H "$url" -b "$basedn" | awk -F ": " '$1 == "status" {print $2}'`
      echo "timereboot: $timereboot"
      echo "thistime: $thistime"
      if [ "$timereboot" == "$thistime" ]; then
        echo "reboot now"
        /lib/molly-guard/reboot
      fi
    fi
  fi
fi

