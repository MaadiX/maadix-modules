#!/bin/bash

#doc: https://major.io/2014/06/26/install-debian-packages-without-starting-daemons/

#disable triggers
cat > /usr/sbin/policy-rc.d << EOF
#!/bin/sh
echo "All runlevel operations denied by policy" >&2
exit 101
EOF
chmod +x /usr/sbin/policy-rc.d

#install package
apt-get install -y samhain

#touch ddbb file
touch /var/lib/samhain/samhain_file

#restore policy-rc.d
rm /usr/sbin/policy-rc.d

