#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 odoodomain' in the agent
Facter.add(:odoodomain) do
  # THEN read fqdn domain defined in ou=domain,ou=odoo from ldap and add to odoodomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=odoo,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
