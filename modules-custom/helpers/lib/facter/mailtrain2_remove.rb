#check if mailtrain2 is to be removed
Facter.add(:mailtrain2_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      mailtrain2_remove = true
    else
      mailtrain2_remove = false
    end
  end
end



