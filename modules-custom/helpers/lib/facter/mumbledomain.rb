#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mumbledomain' in the agent
Facter.add(:mumbledomain) do
  # THEN read fqdn domain defined in ou=domain,ou=mumble from ldap and add to mumbledomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=mumble,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
