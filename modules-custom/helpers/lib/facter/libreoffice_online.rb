#check if libreoffice-online is installed
Facter.add(:libreoffice_online) do
  setcode do
    package = Facter::Util::Resolution.exec('apt-show-versions | grep libreoffice-online')
    if (package.empty?)
      false
    else
      true
    end
  end
end

