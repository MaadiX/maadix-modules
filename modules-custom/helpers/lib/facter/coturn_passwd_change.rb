#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 coturn_passwd_change' in the agent
Facter.add(:coturn_passwd_change) do
  # Check if coturn ldap password match coturn stored password in /etc/maadix
  setcode do
    storedpass = Facter::Core::Execution.execute('cat /etc/maadix/coturn')
    ldappass = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=password,ou=coturn,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    #if ldapsearch returns nothing (coturn passwd does not exists)
    if (storedpass == ldappass)
      false
    else
      true
    end
  end
end
