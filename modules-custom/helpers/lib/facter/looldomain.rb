#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 looldomain' in the agent
Facter.add(:looldomain) do
  # THEN read fqdn domain defined in ou=domain,ou=lool from ldap and add to looldomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=lool,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
