#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 etherpaddomain' in the agent
Facter.add(:etherpaddomain) do
  # THEN read fqdn domain defined in ou=domain,ou=etherpad from ldap and add to etherpaddomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=etherpad,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
