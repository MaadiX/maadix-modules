#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 nextcloudversion' in the agent
Facter.add(:nextcloudversion) do
  # THEN read version defined in ou=version,ou=nextcloud from ldap and add to nextcloudversion fact
  setcode do
    version = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=version,ou=nextcloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
    #if ldapsearch is installed
    if not version.nil?
      if (version.empty?)
        #if version is not set in ldap, set to ''
        ''
      else
        #return version in ldap
        version
      end
    end
  end
end




