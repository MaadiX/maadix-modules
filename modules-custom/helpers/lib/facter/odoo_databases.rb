#list postgres databases of user odoo
Facter.add(:odoo_databases) do
  setcode do
    databases = []
    user = Facter::Core::Execution.execute("sudo -u postgres psql -A -t -c \"SELECT usesysid FROM pg_user WHERE usename='odoo';\"") rescue 'false'
    if user
      Facter::Core::Execution.execute("sudo -u postgres psql -A -t -c \"SELECT datname FROM pg_database where datdba=" + user + ";\"").each_line do |database|
        databases.push(database.strip)
      end
    end
    databases
  end
end
