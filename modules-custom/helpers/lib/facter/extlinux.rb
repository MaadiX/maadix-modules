#check if extlinux is the bootloader
Facter.add(:extlinux) do
  setcode do
    if File.file? '/boot/extlinux/extlinux.conf'
      'true'
    else
      'false'
    end
  end
end

