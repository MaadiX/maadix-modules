#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 apache_ssl_protocols' in the agent
Facter.add(:apache_ssl_protocols) do
  # Check in ldap if apache_ssl_protocols is set
  # If apache_ssl_protocols is not set, use default option
  setcode do
    #default value for apache_ssl_protocols if ldapsearch is not installed
    #default set to tls1.2 ciphers
    protocols = 'all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1'
    protocols_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=apache,ou=conf,ou=cpanel,dc=example,dc=tld" | grep ipServiceProtocol: | sed "s|.*: \(.*\)|\1|"') rescue protocols
    #if ldapsearch returns nothing
    if (protocols_custom.empty?)
      protocols
    else
      case protocols_custom
      #1-insecure, 2-moderated, 3-secure
      when '1'
        'all -SSLv2 -SSLv3 -TLSv1'
      when '2'
        'all -SSLv2 -SSLv3 -TLSv1'
      when '3'
        'all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1'
      when '4'
        'all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1 -TLSv1.2'
      else
        'all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1'
      end
    end
  end
end

