#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_fpm_pools' in the agent
Facter.add(:php_fpm_pools) do
  # Get number of pools from ldap and then generate pools/users name and confs
  setcode do
    #hash with pools name > conf
    pools = {}
    #configure 3 pools by default
    num = 3
    num_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue '0'
    #if ldapsearch returns value assign to number of pools
    if (!num_custom.empty?)
      if (num_custom.to_i > 3)
        num = num_custom.to_i
      end
    end
    i = 1
    while i <= num
      pool_conf = {}
      #default type and max_childrens
      type = 'dynamic'
      max = Facter.value(:php_fpm_limit)
      type_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=' + 'fpm' + i.to_s + ',ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld" | grep type: | sed "s|.*: \(.*\)|\1|"') rescue type
      if (!type_custom.empty?)
        pool_conf['type'] = type_custom
      else
        pool_conf['type'] = type
      end
      max_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=' + 'fpm' + i.to_s + ',ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue max.to_s
      if (!max_custom.empty?)
        pool_conf['max'] = max_custom.to_i
      else
        pool_conf['max'] = max
      end
      pools['fpm' + i.to_s] = pool_conf
      i +=1
    end
    pools
  end
end
