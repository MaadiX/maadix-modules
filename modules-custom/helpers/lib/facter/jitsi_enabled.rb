#check if jitsi is enabled
Facter.add(:jitsi_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=jitsi,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      jitsi_enabled = true
    else
      jitsi_enabled = false
    end
  end
end



