#check if phpmyadmin is enabled
Facter.add(:phpmyadmin_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=phpmyadmin,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      phpmyadmin_enabled = true
    else
      phpmyadmin_enabled = false
    end
  end
end



