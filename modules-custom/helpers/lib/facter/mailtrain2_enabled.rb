#check if mailtrain2 is enabled
Facter.add(:mailtrain2_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      mailtrain2_enabled = true
    else
      mailtrain2_enabled = false
    end
  end
end



