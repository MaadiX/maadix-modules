require 'etc'

#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 owncloudversion_update' in the agent
Facter.add(:owncloudversion_update) do
  # THEN read version defined in ou=version,ou=owncloud from ldap and set to owncloudversion_update to the next major release available
  ## default LATEST version
  latest = '10.8.0'
  #OC version in stretch since release 1803
  #pinned 10.0.x = 10.0.10 / installed in 1803
  #pinned 10.1.x = 10.1.1 / never installed per se / out of cases
  #pinned 10.2.x = 10.2.1 / never installed per se / out of cases
  #pinned 10.3.x = 10.3.0 / installed in 1901
  #pinned 10.8.x = 10.8.0 / installed in 2103
  #add here next pinned version and correspondent cases
  setcode do
    installed = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=version,ou=owncloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
    #if ldapsearch is installed
    if not installed.nil?
      if (installed.empty?)
        #if version is not set in ldap try to get it from occ
        dir = '/var/www/owncloud'
        if File.exist?(dir)
          uid = File.stat(dir).uid
          owner = Etc.getpwuid(uid).name
        else
          owner = 'www-data'
        end
        if File.file?('/var/www/owncloud/owncloud/occ')
          installed = Facter::Core::Execution.execute("sudo -u ' +owner+ ' php /var/www/owncloud/owncloud/occ status | grep 'versionstring:' | cut -d' ' -f5")
        end
      end
      #if no version in ldap or from occ
      if (installed.empty?)
        #use latest
        latest
      else
        case installed
        when '10.0.10'
          '10.1.1,10.2.1,10.3.0,10.8.0'
        when '10.3.0'
          '10.8.0'
        else
          latest
        end
      end
    end
  end
end
