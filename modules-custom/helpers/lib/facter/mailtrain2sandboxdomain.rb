#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtrain2sandboxdomain' in the agent
Facter.add(:mailtrain2sandboxdomain) do
  # THEN read sandbox domain defined in ou=sandbox,ou=mailtrain2 from ldap and add to mailtrain2sandboxdomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=sandbox,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
