#check if rainloop is to be removed
Facter.add(:rainloop_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=rainloop,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      rainloop_remove = true
    else
      rainloop_remove = false
    end
  end
end



