#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_fpm_pools_apps' in the agent
Facter.add(:php_fpm_pools_apps) do
  # Generate pools/users name and confs for apps
  setcode do
    #hash with pools name > conf
    pools = {}
    system_pools = ['nextcloud','owncloud','dolibarr','limesurvey','phpmyadmin','rainloop']
    system_pools.each do |pool|
      pool_conf = {}
      #default type, max_childrens, enabled
      enabled = 'disabled'
      type = 'dynamic'
      max = Facter.value(:php_fpm_limit)
      type_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=' + pool + ',ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld" | grep type: | sed "s|.*: \(.*\)|\1|"') rescue type
      if (!type_custom.empty?)
        pool_conf['type'] = type_custom
      else
        pool_conf['type'] = type
      end
      max_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=' + pool + ',ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue max.to_s
      if (!max_custom.empty?)
        pool_conf['max'] = max_custom.to_i
      else
        pool_conf['max'] = max
      end
      enabled_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=' + pool + ',ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue enabled
      if (!enabled_custom.empty?)
        pool_conf['enabled'] = enabled_custom
      else
        pool_conf['enabled'] = enabled
      end
      pools['fpm' + pool] = pool_conf
    end
    pools
  end
end
