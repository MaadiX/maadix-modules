#check if openvpn is to be removed
Facter.add(:openvpn_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=openvpn,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      openvpn_remove = true
    else
      openvpn_remove = false
    end
  end
end



