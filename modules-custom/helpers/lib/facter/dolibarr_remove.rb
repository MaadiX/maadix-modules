#check if dolibarr is to be removed
Facter.add(:dolibarr_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=dolibarr,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      dolibarr_remove = true
    else
      dolibarr_remove = false
    end
  end
end



