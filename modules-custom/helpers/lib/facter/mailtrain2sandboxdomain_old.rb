#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtrain2sandboxdomain_old' in the agent
Facter.add(:mailtrain2sandboxdomain_old) do
  # THEN read sandbox domain old defined in ou=sandbox_old,ou=mailtrain2 from ldap and add to mailtrain2sandboxdomain_old fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=sandbox_old,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
