#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 loolwopi' in the agent
Facter.add(:loolwopi) do
  # THEN read fqdn domain defined in ou=domain,ou=lool from ldap and add to loolwopi fact
  wopi = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=wopi,ou=lool,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
  if not wopi.nil?
    #replace dots with scaped dots
    wopi.gsub!(".", '\\.')
    setcode do
      wopi
    end
  end
end

