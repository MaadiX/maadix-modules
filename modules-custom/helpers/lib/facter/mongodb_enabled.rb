#check if mongodb is enabled
Facter.add(:mongodb_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mongodb,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      mongodb_enabled = true
    else
      mongodb_enabled = false
    end
  end
end



