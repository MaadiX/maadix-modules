#check if onlyoffice is to be removed
Facter.add(:onlyoffice_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=onlyoffice,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      onlyoffice_remove = true
    else
      onlyoffice_remove = false
    end
  end
end



