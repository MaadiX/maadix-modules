#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 limesurveydomain' in the agent
Facter.add(:limesurveydomain) do
  # THEN read fqdn domain defined in ou=domain,ou=limesurvey from ldap and add to limesurveydomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=limesurvey,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
