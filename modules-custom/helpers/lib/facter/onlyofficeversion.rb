#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 onlyofficeversion' in the agent
Facter.add(:onlyofficeversion) do
  # THEN read current image tag of onlyoffice and add to onlyofficeversion fact
  setcode do
    #set version only if docker is installed
    docker = Facter::Core::Execution.execute('which docker')
    if (!docker.empty?)
      #set version only if onlyoffice is installed
      version = Facter::Core::Execution.execute('docker image ls --filter "reference=onlyoffice/documentserver" --format \'{{.Tag}}\'')
      if (!version.empty?)
        onlyofficeversion = version
      end
    end
  end
end
