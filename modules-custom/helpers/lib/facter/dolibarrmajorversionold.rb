#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 dolibarrmajorversionold' in the agent
Facter.add(:dolibarrmajorversionold) do
  # THEN read majorversionold defined in ou=majorversionold,ou=dolibarr from ldap and add to dolibarrmajorversionold fact
  setcode do
    majorversionold = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=majorversionold,ou=dolibarr,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
    #if ldapsearch is installed
    if not majorversionold.nil?
      if (majorversionold.empty?)
        #if majorversionold is not set in ldap, set to ''
        ''
      else
        #return majorversionold in ldap
        majorversionold
      end
    end
  end
end




