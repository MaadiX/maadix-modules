#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 adminmail' in the agent
Facter.add(:adminmail) do
  # Check in ldap if custom fqdn domain is used (domain!=maadix.org)
  # Then, read admin mail defined in ldap admin user and add to adminmail fact
  # If custom fqdn domain is not set or is not maadix.org, use default admin@maadix.org
  setcode do
    #default value for domain_custom if ldapsearch is not installed
    domain = Facter::Core::Execution.execute('hostname -d')
    domain_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=fqdn_domain,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue domain
    #if ldapsearch returns nothing (fqdn_domain does not exists)
    if (domain_custom.empty?)
      'admin@maadix.org'
    else
      case domain_custom
      when 'maadix.org'
        #if custom fqdn is maadix.org, use default
        'admin@maadix.org'
      else
        #else use user mail
        Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s one -b "dc=example,dc=tld" "(&(objectClass=extensibleObject)(status=active))" | grep email: | sed "s|.*: \(.*\)|\1|"') rescue 'admin@maadix.org'
      end
    end
  end
end
