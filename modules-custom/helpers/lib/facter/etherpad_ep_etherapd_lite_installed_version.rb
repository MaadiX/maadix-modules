#check ep_etherpad_lite installed version
require 'json'
Facter.add(:etherpad_ep_etherapd_lite_installed_version) do
  version = false
  setcode do
    if File.directory?('/var/www/etherpad/etherpad-lite')
      version = Facter::Core::Execution.execute('sudo -u etherpad /bin/bash -c "cd /var/www/etherpad/etherpad-lite && source /var/www/etherpad/.nvm/nvm.sh && npm ls --json ep_etherpad-lite"')
      obj = JSON.parse(version)
      version = obj['dependencies']['ep_etherpad-lite']['version'] rescue false
    end
    version
  end
end
