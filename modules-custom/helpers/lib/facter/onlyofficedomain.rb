#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 onlyofficedomain' in the agent
Facter.add(:onlyofficedomain) do
  # THEN read fqdn domain defined in ou=domain,ou=onlyoffice from ldap and add to onlyofficedomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=onlyoffice,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
