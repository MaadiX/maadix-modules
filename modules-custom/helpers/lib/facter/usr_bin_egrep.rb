#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 usr_bin_egrep' in the agent
Facter.add(:usr_bin_egrep) do
  # THEN check if /usr/bin/egrep exists
  setcode do
    if File.file?('/usr/bin/egrep')
      true
    else
      false
    end
  end
end
