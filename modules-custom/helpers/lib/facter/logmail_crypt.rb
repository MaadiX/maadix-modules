#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 logmail_crypt' in the agent
Facter.add(:logmail_crypt) do
  # Check in ldap if logmail_custom is true | false | both
  # Then, read mail for notifications defined in ldap admin user or if present, in logmail_custom in attribute postalAddress
  # If logmail_custom is true logmail_crypt is mail for notifications
  # If mail for notifications is still not defined or logmail_custom is 'false', leave empty
  setcode do
    logmail_conf = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=logmail_custom,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'    
    case logmail_conf
    when 'true', 'True'
      mail = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=logmail_custom,ou=conf,ou=cpanel,dc=example,dc=tld" | grep postalAddress: | sed "s|.*: \(.*\)|\1|"')
      if mail.empty?
        mail = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s one -b "dc=example,dc=tld" "(&(objectClass=extensibleObject)(status=active))" | grep email: | sed "s|.*: \(.*\)|\1|"')
      end
      mail
    else
      ''
    end
  end
end

