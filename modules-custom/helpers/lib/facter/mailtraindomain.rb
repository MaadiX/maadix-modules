#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtraindomain' in the agent
Facter.add(:mailtraindomain) do
  # THEN read fqdn domain defined in ou=domain,ou=mailtrain from ldap and add to mailtraindomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=mailtrain,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
