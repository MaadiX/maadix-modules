#check if rocketchat is to be removed
Facter.add(:rocketchat_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=rocketchat,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      rocketchat_remove = true
    else
      rocketchat_remove = false
    end
  end
end



