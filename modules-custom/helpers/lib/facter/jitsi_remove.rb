#check if jitsi is to be removed
Facter.add(:jitsi_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=jitsi,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      jitsi_remove = true
    else
      jitsi_remove = false
    end
  end
end



