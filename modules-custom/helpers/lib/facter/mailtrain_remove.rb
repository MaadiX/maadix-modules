#check if mailtrain is to be removed
Facter.add(:mailtrain_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mailtrain,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      mailtrain_remove = true
    else
      mailtrain_remove = false
    end
  end
end



