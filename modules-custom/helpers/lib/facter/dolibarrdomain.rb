#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 dolibarrdomain' in the agent
Facter.add(:dolibarrdomain) do
  # THEN read fqdn domain defined in ou=domain,ou=dolibarr from ldap and add to dolibarrdomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=dolibarr,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
