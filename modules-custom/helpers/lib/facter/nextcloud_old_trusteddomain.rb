require 'etc'
#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 nextcloud_old_trusteddomain' in the agent
Facter.add(:nextcloud_old_trusteddomain) do
  # THEN get trusted_domain from nc system config and add to nextcloud_trusteddomain fact. If not present use fqdn
  setcode do
    dir = '/var/www/nextcloud'
    if File.exist?(dir)
      uid = File.stat(dir).uid
      owner = Etc.getpwuid(uid).name
    else
      owner = 'www-data'
    end
    trusted=Facter::Core::Execution.execute('sudo -u ' +owner+ ' php /var/www/nextcloud/nextcloud/occ config:system:get trusted_domains') rescue 'false'
    case trusted
    when 'false'
      trusted=Facter.value(:fqdn)
    when ''
      trusted=Facter.value(:fqdn)
    end
    trusted
  end
end

