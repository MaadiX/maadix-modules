require 'yaml'

#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 groups.rb' in the agent

groups = nil
Facter.add("groups") do

  groups = [[], []]

  # FIRST read default groups from classifier.yaml and add to groups fact
  config=YAML.load_file('/etc/facter/facts.d/classifier.yaml')
  #STDERR.puts config['default_groups']
  default_groups = config['default_groups']
  default_groups.each{|group|
    groups[0].push(group.strip)
  }

  # THEN read custom groups defined in cpanel from ldap and add to groups fact
  groups_result = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s one -b "ou=groups,dc=example,dc=tld" "(&(objectClass=*)(|(status=install)(status=enabled)))" | grep ou: | sed "s|.*: \(.*\)|\1|"')
  if not groups_result.nil?
      groups_result.each_line do |line|
          if !groups[0].include?(line.strip)
            groups[0].push(line.strip)
          end
      end
  end

  #check if server is vm checking default_groups, then add php only form vms
  if (not default_groups.include? 'bbb') && (not default_groups.include? 'storage') && (not default_groups.include? 'onehost') && (not default_groups.include? 'puppetmaster') && (not default_groups.include? 'onefrontend') && (not default_groups.include? 'api') && (not default_groups.include? 'api2') && (not default_groups.include? 'gitlab') && (not default_groups.include? 'mailsystem')
    groups[0].push('php')
  end

  setcode do
    if Facter.version < '2.0.0'
      groups[0].join(',')
    else
      groups[0]
    end
  end
end

