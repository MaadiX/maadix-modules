#check if mongodb is to be removed
Facter.add(:mongodb_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mongodb,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      mongodb_remove = true
    else
      mongodb_remove = false
    end
  end
end



