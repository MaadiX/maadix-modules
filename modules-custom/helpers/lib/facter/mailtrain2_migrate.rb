#check if mailtrain2 is to be migrated
Facter.add(:mailtrain2_migrate) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=migrate,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /1/
      mailtrain2_migrate = true
    else
      mailtrain2_migrate = false
    end
  end
end



