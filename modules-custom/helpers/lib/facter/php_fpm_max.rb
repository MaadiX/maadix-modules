#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_fpm_max' in the agent
Facter.add(:php_fpm_max) do
  # THEN build from cpu. 12 servers by cpu. the value must be lowest than php_fpm_limit
  setcode do
    phplimit = Facter.value(:php_fpm_limit)
    cpu = Facter.value(:processors)['physicalcount']
    limit = cpu*12 < phplimit ? cpu*12 : phplimit
    limit
  end
end

