#check if etherpad is to be removed
Facter.add(:etherpad_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=etherpad,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      etherpad_remove = true
    else
      etherpad_remove = false
    end
  end
end



