#check if odoo is to be removed
Facter.add(:odoo_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=odoo,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      odoo_remove = true
    else
      odoo_remove = false
    end
  end
end



