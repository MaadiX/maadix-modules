#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 jitsidomain' in the agent
Facter.add(:jitsidomain) do
  # THEN read fqdn domain defined in ou=domain,ou=jitsi from ldap and add to jitsidomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=jitsi,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
