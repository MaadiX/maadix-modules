Facter.add(:docker_extra_parameters) do
  setcode do
    #check if docker is installed
    if Facter::Util::Resolution.exec('apt-show-versions | grep docker-ce 2>/dev/null')
      #if docker is installed, check if dockremap user exists
      dockremap_uid = Facter::Util::Resolution.exec("/usr/bin/id -u dockremap 2>/dev/null")
      if (dockremap_uid.empty?)
        #previous release docker installed without userns-remap option / leave userns-remap disabled
        ''
      else
        #docker already installed with userns-remap option
         '--userns-remap=default'
      end
    else
      #new docker installation, add userns-remap option
      '--userns-remap=default'
    end
  end
end
