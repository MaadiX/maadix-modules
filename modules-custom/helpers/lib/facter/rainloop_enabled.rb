#check if rainloop is enabled
Facter.add(:rainloop_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=rainloop,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      rainloop_enabled = true
    else
      rainloop_enabled = false
    end
  end
end



