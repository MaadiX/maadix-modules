#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 jitsidomain_old' in the agent
Facter.add(:jitsidomain_old) do
  # THEN read fqdn domain_old defined in ou=domain_old,ou=jitsi from ldap and add to jitsidomain_old fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=jitsi,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
