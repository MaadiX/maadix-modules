Facter.add(:has_php70_cli) do
  setcode do
    File.exist?('/etc/php/7.0/cli/php.ini')
  end
end
