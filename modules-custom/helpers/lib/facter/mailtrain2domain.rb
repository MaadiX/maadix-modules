#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtrain2domain' in the agent
Facter.add(:mailtrain2domain) do
  # THEN read fqdn domain defined in ou=domain,ou=mailtrain2 from ldap and add to mailtrain2domain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
