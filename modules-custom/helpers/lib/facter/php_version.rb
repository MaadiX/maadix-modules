#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_version' in the agent
Facter.add(:php_version) do
  # Check in ldap if php_version is set
  # If php_version is not set, use default option
  setcode do
    #default value for php_version if ldapsearch is not installed
    #default set to 8.1
    version = '8.1'
    # since release 2302 version 8.1 is mandatory
    #version_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=php,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue version
    #if ldapsearch returns nothing
    #if (version_custom.empty?)
    #  version
    #else
    #  version_custom
    #end
    version
  end
end
