Facter.add(:has_php73_apache) do
  setcode do
    File.exist?('/etc/php/7.3/apache2/php.ini')
  end
end
