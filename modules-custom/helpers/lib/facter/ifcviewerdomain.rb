#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 ifcviewerdomain' in the agent
Facter.add(:ifcviewerdomain) do
  # THEN read fqdn domain defined in ou=domain,ou=ifcviewer from ldap and add to ifcviewerdomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=ifcviewer,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
