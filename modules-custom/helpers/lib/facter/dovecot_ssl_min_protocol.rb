#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 dovecot_ssl_min_protocol' in the agent
Facter.add(:dovecot_ssl_min_protocol) do
  # Check in ldap if dovecot_ssl_min_protocol is set
  # If dovecot_ssl_min_protocol is not set, use default option
  setcode do
    #default value for dovecot_ssl_min_protocol if ldapsearch is not installed
    protocol = 'TLSv1.2'
    protocol_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=email,ou=conf,ou=cpanel,dc=example,dc=tld" | grep ipServiceProtocol: | sed "s|.*: \(.*\)|\1|"') rescue protocol
    #if ldapsearch returns nothing
    if (protocol_custom.empty?)
      protocol
    else
      case protocol_custom
      #1-insecure, 2-moderated, 3-secure
      when '1'
        'TLSv1.1'
      when '2'
        'TLSv1.1'
      when '3'
        'TLSv1.2'
      when '4'
        #there's a bug in openssl that avoid setting here TLSv1.3 as ssl_min_protocol
        #https://dovecot.org/pipermail/dovecot/2020-April/118628.html
        'TLSv1.2'
      else
        'TLSv1.2'
      end
    end
  end
end

