#check if nextcloud is to be removed
Facter.add(:nextcloud_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=nextcloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      nextcloud_remove = true
    else
      nextcloud_remove = false
    end
  end
end



