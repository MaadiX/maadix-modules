#check if discourse is to be removed
Facter.add(:discourse_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=discourse,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      discourse_remove = true
    else
      discourse_remove = false
    end
  end
end



