#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtraindomain_old' in the agent
Facter.add(:mailtraindomain_old) do
  # THEN read fqdn domain_old defined in ou=domain_old,ou=mailtrain from ldap and add to mailtraindomain_old fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=mailtrain,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
