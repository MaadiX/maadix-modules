#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_fpm_pools_purge' in the agent
Facter.add(:php_fpm_pools_purge) do
  # Get number of pools from ldap and then build array with pools to delete
  setcode do
    #array with pool names
    pools = []
    #3 pools by default
    num = 3
    num_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue '0'
    #if ldapsearch returns value assign to number of pools
    if (!num_custom.empty?)
      if (num_custom.to_i > 3)
        num = num_custom.to_i
      end
    end
    php_version = Facter.value(:php_version)
    system_pools = ['www','nextcloud','owncloud','dolibarr','limesurvey','phpmyadmin','rainloop']
    Facter::Util::Resolution.exec('/bin/ls /etc/php/' +php_version+ '/fpm/pool.d/ | sed "s/ *$//" | sed -e "s/\.conf$//"').each_line do |pool|
      if not system_pools.include? pool.strip
        if pool.strip.delete('fpm').to_i > num
          pools.push(pool.strip)
        end
      end
    end
    pools
  end
end
