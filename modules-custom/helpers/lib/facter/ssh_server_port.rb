#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 ssh_server_port' in the agent
Facter.add(:ssh_server_port) do
  # Check in ldap if custom ssh_server_port is used
  # If custom ssh_server_port is not set, use default port
  setcode do
    #default value for ssh_server_port if ldapsearch is not installed
    port = '22'
    port_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=sshd,ou=conf,ou=cpanel,dc=example,dc=tld" | grep ipServicePort: | sed "s|.*: \(.*\)|\1|"') rescue port
    #if ldapsearch returns nothing
    if (port_custom.empty?)
      '22'
    else
      port_custom
    end
  end
end
