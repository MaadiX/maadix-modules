
#check if coturn is enabled
Facter.add(:coturn_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=coturn,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      coturn_enabled = true
    else
      coturn_enabled = false
    end
  end
end



