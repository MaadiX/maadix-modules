#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 discoursedomain' in the agent
Facter.add(:discoursedomain) do
  # THEN read fqdn domain defined in ou=domain,ou=discourse from ldap and add to discoursedomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=discourse,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
