#check if spamassassin is enabled
Facter.add(:spamassassin_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=spamassassin,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      spamassassin_enabled = true
    else
      spamassassin_enabled = false
    end
  end
end



