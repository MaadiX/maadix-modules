require 'yaml'

#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 cpaneldomains' in the agent

cpaneldomains = nil
Facter.add("cpaneldomains") do

  cpaneldomains = [[], []]

  # THEN read mail domains defined in cpanel from ldap and add to cpaneldomains fact
  cpaneldomains_result = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s one -b "o=hosting,dc=example,dc=tld" "objectClass=VirtualDomain" | grep vd: | sed "s|.*: \(.*\)|\1|"')
  if not cpaneldomains_result.nil?
      cpaneldomains_result.each_line do |line|
          if !cpaneldomains[0].include?(line.strip)
            cpaneldomains[0].push(line.strip)
          end
      end
  end

  setcode do
    if Facter.version < '2.0.0'
      cpaneldomains[0].join(',')
    else
      cpaneldomains[0]
    end
  end
end

