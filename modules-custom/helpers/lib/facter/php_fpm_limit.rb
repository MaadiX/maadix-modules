#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_fpm_limit' in the agent
Facter.add(:php_fpm_limit) do
  # THEN build from memory in bytes fact. 20 clients by G / each client consumes about 12MB under high loads / 256MB for php by G of RAM / like in php_memory_limit fact
  setcode do
    memory = 1.0*Facter.value(:memory)['system']['total_bytes']
    limit = memory/1024/1024/1024*20
    # override with custom value if present in ldap
    limit_custom= nil
    limit_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=php_fpm_limit,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue limit_custom
    if (limit_custom.nil? || limit_custom.empty?)
      limit.round.to_i
    else
      limit_custom.to_i
    end
  end
end

