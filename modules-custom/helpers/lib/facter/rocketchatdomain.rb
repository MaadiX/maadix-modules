#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 rocketchatdomain' in the agent
Facter.add(:rocketchatdomain) do
  # THEN read fqdn domain defined in ou=domain,ou=rocketchat from ldap and add to rocketchatdomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=rocketchat,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
