#check if phpmyadmin is to be removed
Facter.add(:phpmyadmin_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=phpmyadmin,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      phpmyadmin_remove = true
    else
      phpmyadmin_remove = false
    end
  end
end



