require 'yaml'

#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 isvm' in the agent

Facter.add("isvm") do

  groups = [[], []]

  # FIRST read default groups from classifier.yaml and add to groups fact
  config=YAML.load_file('/etc/facter/facts.d/classifier.yaml')
  #STDERR.puts config['default_groups']
  default_groups = config['default_groups']
  default_groups.each{|group|
    groups[0].push(group.strip)
  }

  setcode do
    #check if server is vm checking default_groups
    if (not default_groups.include? 'bbb') && (not default_groups.include? 'storage') && (not default_groups.include? 'onehost') && (not default_groups.include? 'puppetmaster') && (not default_groups.include? 'onefrontend') && (not default_groups.include? 'api') && (not default_groups.include? 'api2') && (not default_groups.include? 'gitlab')
      true
    else
      false
    end
  end
end
