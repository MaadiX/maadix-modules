#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 fqdn_old' in the agent
Facter.add(:fqdn_old) do
  # THEN build from hostname + fqd_domain_old facts
  setcode do
    hostname = Facter.value(:hostname)
    fqdn_domain_old = Facter.value(:fqdn_domain_old)
    fqdn_old = "#{hostname}.#{fqdn_domain_old}"
    fqdn_old
  end
end

