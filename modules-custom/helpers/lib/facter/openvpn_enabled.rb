#check if openvpn is enabled
Facter.add(:openvpn_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=openvpn,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      openvpn_enabled = true
    else
      openvpn_enabled = false
    end
  end
end



