#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 spamassassin_score' in the agent
Facter.add(:spamassassin_score) do
  # THEN read score defined in ou=score,ou=spamassassin from ldap and add to spamassassin_score fact
  setcode do
    score = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=score,ou=spamassassin,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
    #if ldapsearch is installed
    if not score.nil?
      if (score.empty?)
        #if score is not set in ldap, set to ''
        '5.0'
      else
        #return score in ldap
        score
      end
    end
  end
end




