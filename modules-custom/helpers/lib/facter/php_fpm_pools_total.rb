#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_fpm_pools_total' in the agent
Facter.add(:php_fpm_pools_total) do
  # THEN sum up pools web defined in ldap plus system pools enabled
  setcode do
    # Get number of pools from ldap
    num = 1
    num_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue '1'
    #if ldapsearch returns value assign no to numbder of pools
    if (!num_custom.empty?)
      num = num_custom.to_i
    end
    pools_apps = Facter.value(:php_fpm_pools_apps)
    pools_apps.each do |pool, conf|
      if conf['enabled']=='enabled'
        num = num +1
      end
    end
    num
  end
end
