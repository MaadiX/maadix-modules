#check if zeyple is to be removed
Facter.add(:zeyple_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=zeyple,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      zeyple_remove = true
    else
      zeyple_remove = false
    end
  end
end



