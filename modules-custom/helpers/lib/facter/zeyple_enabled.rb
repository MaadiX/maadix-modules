#check if zeyple is enabled
Facter.add(:zeyple_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=zeyple,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      zeyple_enabled = true
    else
      zeyple_enabled = false
    end
  end
end



