#check if owncloud is to be removed
Facter.add(:owncloud_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=owncloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      owncloud_remove = true
    else
      owncloud_remove = false
    end
  end
end



