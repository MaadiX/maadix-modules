#check if mumble is to be removed
Facter.add(:mumble_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mumble,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      mumble_remove = true
    else
      mumble_remove = false
    end
  end
end



