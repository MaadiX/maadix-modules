#check if spamassassin is to be removed
Facter.add(:spamassassin_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=spamassassin,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      spamassassin_remove = true
    else
      spamassassin_remove = false
    end
  end
end



