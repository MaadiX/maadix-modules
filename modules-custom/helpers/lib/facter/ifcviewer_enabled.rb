#check if ifcviewer is enabled
Facter.add(:ifcviewer_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=ifcviewer,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      ifcviewer_enabled = true
    else
      ifcviewer_enabled = false
    end
  end
end



