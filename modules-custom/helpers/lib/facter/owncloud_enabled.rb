#check if owncloud is enabled
Facter.add(:owncloud_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=owncloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      owncloud_enabled = true
    else
      owncloud_enabled = false
    end
  end
end



