#check ep_maadix installed version
require 'json'
Facter.add(:etherpad_ep_maadix_installed_version) do
  version = false
  setcode do
    if File.directory?('/var/www/etherpad/etherpad-lite/node_modules/ep_maadix')
      version = Facter::Core::Execution.execute('sudo -u etherpad /bin/bash -c "cd /var/www/etherpad/etherpad-lite && source /var/www/etherpad/.nvm/nvm.sh && npm ls --json ep_maadix"')
      obj = JSON.parse(version)
      version = obj['dependencies']['ep_maadix']['version']
    end
    version
  end
end
