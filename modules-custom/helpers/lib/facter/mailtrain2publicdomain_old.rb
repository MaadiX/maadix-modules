#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtrain2publicdomain_old' in the agent
Facter.add(:mailtrain2publicdomain_old) do
  # THEN read public domain old defined in ou=public_old,ou=mailtrain2 from ldap and add to mailtrain2publicdomain_old fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=public_old,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
