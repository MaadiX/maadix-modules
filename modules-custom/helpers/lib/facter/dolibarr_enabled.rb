#check if dolibarr is enabled
Facter.add(:dolibarr_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=dolibarr,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      dolibarr_enabled = true
    else
      dolibarr_enabled = false
    end
  end
end



