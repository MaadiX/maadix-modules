#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 postfix_ssl_protocols' in the agent
Facter.add(:postfix_ssl_protocols) do
  # Check in ldap if postfix_ssl_protocols is set
  # If postfix_ssl_protocols is not set, use default option
  setcode do
    #default value for postfix_ssl_protocols if ldapsearch is not installed
    protocols = '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1'
    protocols_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=email,ou=conf,ou=cpanel,dc=example,dc=tld" | grep ipServiceProtocol: | sed "s|.*: \(.*\)|\1|"') rescue protocols
    #if ldapsearch returns nothing
    if (protocols_custom.empty?)
      protocols
    else
      case protocols_custom
      #1-insecure, 2-moderated, 3-secure, 4-secure with tls1.3 only
      when '1'
        '!SSLv2,!SSLv3,!TLSv1'
      when '2'
        '!SSLv2,!SSLv3,!TLSv1'
      when '3'
        '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1'
      when '4'
        '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1,!TLSv1.2'
      else
        '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1'
      end
    end
  end
end

