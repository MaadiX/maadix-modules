#check if nextcloud is enabled
Facter.add(:nextcloud_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=nextcloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      nextcloud_enabled = true
    else
      nextcloud_enabled = false
    end
  end
end



