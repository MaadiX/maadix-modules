#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtrain2publicdomain' in the agent
Facter.add(:mailtrain2publicdomain) do
  # THEN read public domain defined in ou=public,ou=mailtrain2 from ldap and add to mailtrain2publicdomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=public,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
