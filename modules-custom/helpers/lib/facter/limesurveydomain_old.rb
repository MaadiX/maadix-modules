#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 limesurveydomain_old' in the agent
Facter.add(:limesurveydomain_old) do
  # THEN read fqdn domain_old defined in ou=domain_old,ou=limesurvey from ldap and add to limesurveydomain_old fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=limesurvey,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
