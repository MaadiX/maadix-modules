#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 is_luks' in the agent
Facter.add(:is_luks) do
  # THEN check vda2 FSTYPE
  setcode do
    if(File.exist?('/bin/lsblk'))
      fstype = Facter::Core::Execution.execute('/bin/lsblk --nodeps /dev/vda2 -n -o FSTYPE')
      if (!fstype.empty?) && (fstype=='crypto_LUKS')
        isluks = true
      else
        isluks = false
      end
      isluks
    end
  end
end
