#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_opcache_buffer' in the agent
Facter.add(:php_opcache_buffer) do
  # THEN build from memory in bytes fact. 8M by G. 16M minimum
  setcode do
    memory = 1.0*Facter.value(:memory)['system']['total_bytes']
    limit = memory/1024/1024/1024*8
    mem = limit.round > 16 ? limit.round : 16
    mem = mem.to_s
    mem
  end
end

