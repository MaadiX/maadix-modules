#check if onlyoffice is enabled
Facter.add(:onlyoffice_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=onlyoffice,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      onlyoffice_enabled = true
    else
      onlyoffice_enabled = false
    end
  end
end



