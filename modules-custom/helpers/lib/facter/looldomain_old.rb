#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 looldomain_old' in the agent
Facter.add(:looldomain_old) do
  # THEN read fqdn domain_old defined in ou=domain_old,ou=lool from ldap and add to looldomain_old fact
  # only if it is not equal to looldomain
  ldapsearch = Facter::Core::Execution.execute('which ldapsearch')
  if (!ldapsearch.empty?)
    setcode do
      domain_old = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=lool,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
      domain = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=lool,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
      if (!domain_old.empty?) && (domain_old != domain)
        domain_old
      end
    end
  end
end
