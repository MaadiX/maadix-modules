Facter.add(:has_php73_cli) do
  setcode do
    File.exist?('/etc/php/7.3/cli/php.ini')
  end
end
