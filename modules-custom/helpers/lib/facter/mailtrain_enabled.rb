#check if mailtrain is enabled
Facter.add(:mailtrain_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mailtrain,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      mailtrain_enabled = true
    else
      mailtrain_enabled = false
    end
  end
end



