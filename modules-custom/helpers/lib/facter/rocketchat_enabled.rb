#check if rocketchat is enabled
Facter.add(:rocketchat_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=rocketchat,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      rocketchat_enabled = true
    else
      rocketchat_enabled = false
    end
  end
end



