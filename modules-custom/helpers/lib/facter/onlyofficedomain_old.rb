#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 onlyofficedomain_old' in the agent
Facter.add(:onlyofficedomain_old) do
  # THEN read fqdn domain_old defined in ou=domain_old,ou=onlyoffice from ldap and add to onlyofficedomain_old fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=onlyoffice,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
