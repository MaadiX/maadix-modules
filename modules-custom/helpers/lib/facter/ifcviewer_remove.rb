#check if ifcviewer is to be removed
Facter.add(:ifcviewer_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=ifcviewer,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      ifcviewer_remove = true
    else
      ifcviewer_remove = false
    end
  end
end



