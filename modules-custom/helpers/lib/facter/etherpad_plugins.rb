#list etherpad plugins from main node_modules folder
Facter.add(:etherpad_plugins) do
  plugins = []
  setcode do
    if File.directory?('/var/www/etherpad')
      if File.directory?('/var/www/etherpad/etherpad-lite/node_modules/')
        dir = '/var/www/etherpad/etherpad-lite/node_modules/'
        # files = Dir.foreach(dir).select { |x| File.file?("#{dir}/#{x}") }
        files = Dir.entries(dir)
        files.each do |i|
          if i.include? "ep_"
            if (i !='ep_maadix') && (i != 'ep_etherpad-lite')
              plugins.append(i)
            end
          end
        end
      end
    end
    plugins
  end
end

