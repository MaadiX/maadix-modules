#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_opcache_memory' in the agent
Facter.add(:php_opcache_memory) do
  # THEN build from memory in bytes fact. 64M by G. 128M minimum
  setcode do
    memory = 1.0*Facter.value(:memory)['system']['total_bytes']
    limit = memory/1024/1024/1024*64
    mem = limit.round > 128 ? limit.round : 128
    mem = mem.to_s
    mem
  end
end

