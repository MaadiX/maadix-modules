#check if coturn is to be removed
Facter.add(:coturn_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=coturn,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      coturn_remove = true
    else
      coturn_remove = false
    end
  end
end



