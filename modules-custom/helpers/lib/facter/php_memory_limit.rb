#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_memory_limit' in the agent
Facter.add(:php_memory_limit) do
  # THEN build from memory in bytes fact. 256M by G. 512M minimum. max 2G
  setcode do
    memory = 1.0*Facter.value(:memory)['system']['total_bytes']
    limit = memory/1024/1024/1024*256
    mem = limit.round > 512 ? limit.round : 512
    mem = mem < 2048 ? mem : 2048
    mem = mem.to_s + 'M'
    mem
  end
end

