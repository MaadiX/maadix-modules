#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 rocketchatversion' in the agent
Facter.add(:rocketchatversion) do
  # THEN read version defined in ou=version,ou=rocketchat from ldap and add to rocketchatversion fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=version,ou=rocketchat,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
