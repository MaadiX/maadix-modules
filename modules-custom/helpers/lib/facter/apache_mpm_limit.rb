#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 apache_mpm_limit' in the agent
Facter.add(:apache_mpm_limit) do
  # THEN build from memory in bytes fact. 24 clients by G. The value must be an integer multiple of ThreadsPerChild of 24
  setcode do
    memory = 1.0*Facter.value(:memory)['system']['total_bytes']
    limit = memory/1024/1024/1024*24
    (limit/24.0).ceil*24
  end
end

