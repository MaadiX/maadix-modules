#check if limesurvey is enabled
Facter.add(:limesurvey_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=limesurvey,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      limesurvey_enabled = true
    else
      limesurvey_enabled = false
    end
  end
end



