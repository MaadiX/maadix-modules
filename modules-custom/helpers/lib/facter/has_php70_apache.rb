Facter.add(:has_php70_apache) do
  setcode do
    File.exist?('/etc/php/7.0/apache2/php.ini')
  end
end
