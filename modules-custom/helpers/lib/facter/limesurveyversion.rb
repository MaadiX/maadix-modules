#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 limesurveyversion' in the agent
Facter.add(:limesurveyversion) do
  # THEN read version defined in ou=version,ou=limesurvey from ldap and add to limesurveyversion fact
  setcode do
    version = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=version,ou=limesurvey,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
    #if ldapsearch is installed
    if not version.nil?
      if (version.empty?)
        #if version is not set in ldap, set to ''
        ''
      else
        #return version in ldap
        version
      end
    end
  end
end




