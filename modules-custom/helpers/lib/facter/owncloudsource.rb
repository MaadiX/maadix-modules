#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 owncloudsource' in the agent
Facter.add(:owncloudsource) do
  # THEN check occ command location. if located in /var/www/owncloud/occ set owncloudsource fact to package
  if File.file?('/var/www/owncloud/occ')
    setcode do
      'package'
    end
  end
end
