#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtrain2domain_old' in the agent
Facter.add(:mailtrain2domain_old) do
  # THEN read fqdn domain_old defined in ou=domain_old,ou=mailtrain2 from ldap and add to mailtrain2domain_old fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
