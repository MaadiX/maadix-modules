#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_opcache_files' in the agent
Facter.add(:php_opcache_files) do
  # THEN build from memory in bytes fact. 5000 by G. 10000 minimum
  setcode do
    memory = 1.0*Facter.value(:memory)['system']['total_bytes']
    limit = memory/1024/1024/1024*5000
    mem = limit.round > 10000 ? limit.round : 10000
    mem = mem.to_s
    mem
  end
end

