#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 nextcloudversion_update' in the agent
Facter.add(:nextcloudversion_update) do
  # THEN read version defined in ou=version,ou=nextcloud from ldap and set to nextcloudversion_update to the next major release available
  ## default LATEST version
  latest = '26.0.0'
  #NC version in stretch since release 1803
  #pinned 14.x = 14.0.0 / installed in 1802 / 1803
  #pinned 14.x = 14.0.7 / installed in 1803
  #pinned 15.x = 15.0.5 / installed in 1804
  #pinned 16.x = 16.0.5 / never installed per se / out of cases
  #pinned 17.x = 17.0.0 / installed in 1901
  #pinned 18.x = 18.0.4 / installed in 1901
  #pinned 19.x = 19.0.7 / never installed per se / out of cases
  #pinned 21.x = 21.0.0 / installed in 2101 with bug in OO app
  #pinned 21.x = 21.0.2 / installed in 2101
  #pinned 22.x = 22.2.0 / installed in 2103
  #pinned 23.x = 23.0.6 / never installed per se / out of cases
  #pinned 24.x = 24.0.2 / installed in 2202
  #pinned 24.x = 24.0.6 / installed in 2203
  #pinned 25.x = 25.0.1 / installed in 2301
  #pinned 26.x = 26.0.0 / installed in 2302
  #add here next pinned version and correspondent cases
  setcode do
    installed = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=version,ou=nextcloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
    if not installed.nil?
      if (installed.empty?)
        #use latest
        latest
      else
        case installed
        when '14.0.0'
          '15.0.5,16.0.5,17.0.0'
        when '14.0.7'
          '15.0.5,16.0.5,17.0.0'
        when '15.0.5'
          '16.0.5,17.0.0'
        when '17.0.0'
          '18.0.4,19.0.7,20.0.8,21.0.2'
        when '18.0.4'
          '19.0.7,20.0.8,21.0.2,22.2.0,22.2.9,23.0.6,24.0.6,25.0.1'
        when '21.0.0'
          '22.2.0,22.2.9,23.0.6,24.0.6,25.0.1'
        when '21.0.2'
          '22.2.0,22.2.9,23.0.6,24.0.6,25.0.1'
        when '22.2.0'
          '22.2.9,23.0.6,24.0.6,25.0.1'
        when '24.0.2'
          '24.0.6,25.0.1'
        when '24.0.6'
          '25.0.1'
        when '25.0.1'
          '26.0.0'
        else
          latest
        end
      end
    end
  end
end

