#check if limesurvey is to be removed
Facter.add(:limesurvey_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=limesurvey,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      limesurvey_remove = true
    else
      limesurvey_remove = false
    end
  end
end



