#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 php_fpm_min' in the agent
Facter.add(:php_fpm_min) do
  # THEN build from cpu. 6 servers by cpu / divided by total pools. the value must be lowest than php_fpm_limit and greater than 0
  setcode do
    phplimit = Facter.value(:php_fpm_limit)
    cpu = Facter.value(:processors)['physicalcount']
    pools = Facter.value(:php_fpm_pools_total)
    limit = (cpu*6.to_f/pools).ceil < phplimit ? (cpu*6.to_f/pools).ceil : phplimit
    limit.to_i
  end
end
