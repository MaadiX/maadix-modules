#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 random_0_59' in the agent
Facter.add(:random_0_59) do
  setcode do
    rand(59)
  end
end
