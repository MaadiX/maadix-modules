#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 nextclouddomain' in the agent
Facter.add(:nextclouddomain) do
  # THEN read fqdn domain defined in ou=domain,ou=nextcloud from ldap and add to nextclouddomain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=nextcloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
