#check if mailman is to be removed
Facter.add(:mailman_remove) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=mailman,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /remove/
      mailman_remove = true
    else
      mailman_remove = false
    end
  end
end



