#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 fqdn_domain' in the agent
Facter.add(:fqdn_domain) do
  # THEN read fqdn domain defined in cpanel from ldap and add to fqdn_domain fact
  setcode 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=fqdn_domain,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"'
end
