require 'etc'

#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 owncloudversion' in the agent
Facter.add(:owncloudversion) do
  # THEN read version defined in ou=version,ou=owncloud from ldap and add to owncloudversion fact
  setcode do 
    version = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=version,ou=owncloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"')
    #if ldapsearch is installed
    if not version.nil?
      if (version.empty?)
        #if version is not set in ldap, try to get it from occ
        dir = '/var/www/owncloud'
        if File.exist?(dir)
          uid = File.stat(dir).uid
          owner = Etc.getpwuid(uid).name
        else
          owner = 'www-data'
        end
        if File.file?('/var/www/owncloud/owncloud/occ')
          Facter::Core::Execution.execute("sudo -u ' +owner+ ' php /var/www/owncloud/owncloud/occ status | grep 'versionstring:' | cut -d' ' -f5")
        else
          #if version is not set in ldap nor occ, set to ''
          ''
        end
      else
        #return version in ldap
        version
      end
    end
  end
end
