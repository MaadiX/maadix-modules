#check if etherpad is enabled
Facter.add(:etherpad_enabled) do
  setcode do
    status = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=etherpad,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case status
    when /enabled|install/
      etherpad_enabled = true
    else
      etherpad_enabled = false
    end
  end
end



