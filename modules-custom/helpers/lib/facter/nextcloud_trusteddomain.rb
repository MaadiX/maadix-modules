#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 nextcloud_trusteddomain' in the agent
Facter.add(:nextcloud_trusteddomain) do
  # THEN read fqdn domain defined in ou=domain,ou=nextcloud from ldap and add to nextcloud_trusteddomain fact. If not present use fqdn
  setcode do
    trusted=Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain,ou=nextcloud,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    case trusted
    when 'false'
      trusted=Facter.value(:fqdn)        
    when ''
      trusted=Facter.value(:fqdn)        
    end
    trusted
  end
end

