#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 ssh_server_password_auth' in the agent
Facter.add(:ssh_server_password_auth) do
  # Check in ldap if ssh_server_password_auth is set (only auth with keys allowed)
  # If ssh_server_password_auth is not set, use default option
  setcode do
    #default value for ssh_server_password_auth if ldapsearch is not installed
    password_auth = 'yes'
    password_auth_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=sshd,ou=conf,ou=cpanel,dc=example,dc=tld" | grep ipServiceProtocol: | sed "s|.*: \(.*\)|\1|"') rescue password_auth
    #if ldapsearch returns nothing
    if (password_auth_custom.empty?)
      'yes'
    else
      case password_auth_custom
      when 'True'
        'no'
      else
        'yes'
      end
    end
  end
end

