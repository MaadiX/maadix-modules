#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 random_0_23' in the agent
Facter.add(:random_0_23) do
  setcode do
    rand(23)
  end
end
