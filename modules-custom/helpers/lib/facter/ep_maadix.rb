#check if ep_maadix is installed
Facter.add(:ep_maadix) do
  setcode do
    if File.directory?('/var/www/etherpad')
      if File.directory?('/var/www/etherpad/etherpad-lite/node_modules/ep_maadix')
        true
      else
        false
      end
    else
      true
    end
  end
end
