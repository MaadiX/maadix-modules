#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 onlyoffice_passwd_change' in the agent
Facter.add(:onlyoffice_passwd_change) do
  # Check if onlyoffice ldap password match onlyoffice stored password in /etc/maadix
  setcode do
    storedpass = Facter::Core::Execution.execute('cat /etc/maadix/onlyoffice')
    ldappass = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=password,ou=onlyoffice,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    #if ldapsearch returns nothing (onlyoffice passwd does not exists)
    if (storedpass == ldappass)
      false
    else
      true
    end
  end
end
