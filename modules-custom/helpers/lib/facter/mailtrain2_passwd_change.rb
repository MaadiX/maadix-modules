#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 mailtrain2_passwd_change' in the agent
Facter.add(:mailtrain2_passwd_change) do
  # Check if mailtrain2 ldap password match mailtrain2 stored password in /etc/maadix
  setcode do
    storedpass = Facter::Core::Execution.execute('cat /etc/maadix/mailtrain2')
    ldappass = Facter::Core::Execution.execute('ldapsearch -o ldif-wrap=no -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=password,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status: | sed "s|.*: \(.*\)|\1|"') rescue 'false'
    #if ldapsearch returns nothing (mailtrain2 passwd does not exists)
    if (storedpass == ldappass)
      false
    else
      true
    end
  end
end
