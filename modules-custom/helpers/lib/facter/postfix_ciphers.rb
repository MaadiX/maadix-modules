#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 postfix_ciphers' in the agent
Facter.add(:postfix_ciphers) do
  # Check in ldap if postfix_ciphers is set
  # If postfix_ciphers is not set, use default option
  setcode do
    #default value for postfix_ciphers if ldapsearch is not installed
    #postfix default insecure = 'aNULL:-aNULL:HIGH:MEDIUM:+RC4:@STRENGTH'
    ciphers = 'AES256+EECDH+AESGCM:AES256+EDH+AESGCM:AES128+EECDH+AESGCM:AES128+EDH+AESGCM'
    ciphers_custom = Facter::Core::Execution.execute('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=email,ou=conf,ou=cpanel,dc=example,dc=tld" | grep ipServiceProtocol: | sed "s|.*: \(.*\)|\1|"') rescue ciphers
    #if ldapsearch returns nothing
    if (ciphers_custom.empty?)
      ciphers
    else
      case ciphers_custom
      #1-insecure, 2-moderated, 3-secure
      when '1'
        #for TLSv1.1 + 1.1
        'AES256+EECDH+AESGCM:AES256+EDH+AESGCM:AES128+EECDH+AESGCM:AES128+EDH+AESGCM:AES256+DHE+SHA:AES256+EECDH+SHA'
      when '2'
        #for TLSv1.1 + 1.1
        'AES256+EECDH+AESGCM:AES256+EDH+AESGCM:AES128+EECDH+AESGCM:AES128+EDH+AESGCM:AES256+DHE+SHA:AES256+EECDH+SHA'
      when '3'
        #for TLSv1.2 disable CBC ciphers
        'AES256+EECDH+AESGCM:AES256+EDH+AESGCM:AES128+EECDH+AESGCM:AES128+EDH+AESGCM'	
      when '4'
        #for TLSv1.3
        'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384'
      else
        'AES256+EECDH+AESGCM:AES256+EDH+AESGCM:AES128+EECDH+AESGCM:AES128+EDH+AESGCM'
      end
    end
  end
end
