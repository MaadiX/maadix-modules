#check if old jitsi apt key is present
Facter.add(:jitsi_old_key) do
  setcode do
    Facter::Core::Execution.execute('apt-key list 2> /dev/null | grep -c "66A9 CD05 95D6 AFA2 4729  0D3B EF8B 479E 2DC1 389C"') != '0'
  end
end
