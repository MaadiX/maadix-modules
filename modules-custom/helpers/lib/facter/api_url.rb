#to debug, use STDERR and run 'puppet facts --debug | grep -A 20 api_url' in the agent
Facter.add(:api_url) do
  # THEN read url defined in ou=api from ldap and add to api_url fact
  setcode do
    version = Facter::Util::Resolution.exec('ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=api,dc=example,dc=tld" | grep host: | sed "s|.*: \(.*\)|\1|"')
    #if ldapsearch is installed
    if not version.nil?
      if (version.empty?)
        #if version is not set in ldap, set to ''
        ''
      else
        #return version in ldap
        version
      end
    else
        #if ldap is not installed set to ''
        ''
    end
  end
end

