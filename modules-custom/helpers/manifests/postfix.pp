class helpers::postfix (
  $enabled = true,
  $gnarwl_ldap_server = 'example.tld',
) {

  validate_bool($enabled)

  if $enabled {

    #postfix group
    group { 'postfix':
        ensure          => 'present',
        gid             => 301,
        system          => true,
        before          => [
                            Package['postfix'],
                            Package['postfix-ldap'],
                           ]
    }

    #postfix user with fixed uid/gid
    user { 'postfix':
        ensure          => 'present',
        gid             => 301,
        home            => '/var/spool/postfix',
        managehome      => false,
        shell           => '/bin/false',
        system          => true,
        uid             => 301,
        before          => [
                            Package['postfix'],
                            Package['postfix-ldap'],
                           ]
    }

    Package['postfix']->Package['postfix-ldap']

    #postfix home, already declared in dovecot module
    #file { '/var/spool/postfix':
    #	ensure      => directory,
    #	mode        => '0755',
    #	owner       => 'root',
    #	group       => 'root',
    #}

    #add rbl whitelist
    #doc https://www.serveradminblog.com/2010/03/how-to-whitelist-hosts-or-ip-addresses-in-postfix/
    postfix::map { 'rbl_whitelist':
      ensure => present,
      notify=> Service['postfix'],
    }

    ## gnarwl
    if ! defined(Package['gnarwl']) {
      package { 'gnarwl':
        ensure   => present,
      }
    }


    # gnarwl block files owned by vmail
    file { '/var/lib/gnarwl/block/':
    	ensure      => directory,
    	mode        => '0700',
    	owner       => 'vmail',
    	group       => 'vmail',
    }


    # gnarwl postfix transport_maps (conditionally with mailman)
    if $::mailman_enabled {
      postfix::config {
        'transport_maps':                                
          value   => 'ldap:/etc/postfix/ldap/transport.cf, hash:/opt/mailman/var/data/postfix_lmtp', 
          notify  => Service['postfix'],
      }
    } else {
      postfix::config {
        'transport_maps':                                
          value   => 'ldap:/etc/postfix/ldap/transport.cf', 
          notify  => Service['postfix'],
      }
    }

    # gnarwl transport in postfix master
    file_line { 'postfix master gnarwl':
      path  => '/etc/postfix/master.cf',
      line  => 'gnarwl    unix  -       n       n       -       -       pipe  flags=F  user=vmail argv=/usr/bin/gnarwl -a ${user}@${nexthop} -s ${sender}',
      match => 'gnarwl.*$',
      notify=> Service['postfix'],
    }

    # gnarwl conf template
    file { 'gnarwl conf':
        path    => '/etc/gnarwl.cfg_template',
        owner   => 'vmail',
        group   => 'vmail',
        mode    => '0644',
        content => template('helpers/gnarwl.cfg.erb'),
        require => [
                   User['vmail'],
                   Package['gnarwl']
                   ],
    }
     

    #gnarwl conf with secrets based on template
    exec { 'gnarwl conf with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/gnarwl | xargs -i sed \"s/gnarlwPLACEHOLDER/{}/\" /etc/gnarwl.cfg_template > /etc/gnarwl.cfg && chown vmail /etc/gnarwl.cfg && chmod 600 /etc/gnarwl.cfg'",
      require     => [
                     File['gnarwl conf'],
                     ],
      subscribe   => [ 
                     File['gnarwl conf'], 
                     ],
      refreshonly => true,
    }


    # when root mailalias is an array we use a fact array tha can be used here in manifest
    postfix::mailalias {'root':
      recipient => $::logmail,
    }

  }

}
