define helpers::cleanvhost(
  $fqdn      = $name,
  $docroot   = undef,
  $priority  = 90,
) {
  
  #delete fqdn (nonssl) vhost
  apache::vhost{ "$fqdn":
    ensure         => absent,
    manage_docroot => false,
    docroot        => $docroot,
    priority       => $priority,
  }

  #remove certs
  letsencrypt::certonly { "$fqdn":
    ensure      => 'absent',
    domains     => ["$fqdn"],
  } ~>
  #remove renewal if cert is removed
  exec {"remove letsencrypt renewal $fqdn":
    command     => "rm /etc/letsencrypt/renewal/$fqdn.conf",
    onlyif      => "test -f /etc/letsencrypt/renewal/$fqdn.conf",
    refreshonly => true,
  }

  #delete fqdn-ssl vhost
  apache::vhost{ "$fqdn-ssl":
    ensure         => absent,
    manage_docroot => false,
    docroot        => $docroot,
    priority       => $priority,
  } ~>
  Exec['reload apache']
  
}
