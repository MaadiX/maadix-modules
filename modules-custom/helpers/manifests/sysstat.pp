#hardening / sysstat
class helpers::sysstat (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #package
    package { 'sysstat':
      ensure => 'present',
    }

    #/etc/default/sysstat
    file { '/etc/default/sysstat':
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/sysstat.erb'),
        require => Package['sysstat'],
        notify  => Service['sysstat'],
    }

    #service
    service { 'sysstat':
      ensure  => 'running',
      enable  => true,
      require => [
                 Package['sysstat'],
                 File['/etc/default/sysstat'],
                 ]
    }


  }

}

