class helpers::fail2ban::monit(
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {
    $pidfile = $::osfamily ? {
      'Debian' => '/var/run/fail2ban/fail2ban.pid',
    }

    $init_system = $::operatingsystem ? {
      'Ubuntu' => $::lsbmajdistrelease ? {
        /(12\.|14\.)/ => 'sysv',
        default       => undef,
      },
      'Debian' => $::lsbdistcodename ? {
        'jessie' => 'sysv',
        'stretch' => 'systemd',
        'buster'  => 'systemd',
        'bullseye'=> 'systemd',
        default  => undef,
      },
      default  => undef,
    }

    monit::check::service { 'fail2ban':
      init_system => $init_system,
      pidfile     => $pidfile,
      binary      => $::osfamily ? {
        'Debian' => '/usr/bin/fail2ban-server',
        default  => undef,
      },
#      program_start => '/usr/sbin/service fail2ban start',
#      program_stop  => '/usr/sbin/service fail2ban stop',
      notify        => Service['monit'],
    }

  }

}


