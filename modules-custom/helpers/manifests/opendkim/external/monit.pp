class helpers::opendkim::external::monit(
  $enabled = true,
  $port    = 8891,
) {

  validate_bool($enabled)

  if $enabled {
    $pidfile = $::osfamily ? {
      /(RedHat|Debian)/ => '/var/run/opendkim/opendkim.pid',
    }
    $test = {
      type     => connection,
      protocol => generic,
      port     => $port,
      action   => 'restart',
    }
    monit::check::process { 'opendkim':
      pidfile       => $pidfile,
      tests         => [$test,],
      program_start => '/etc/init.d/opendkim start',
      program_stop  => '/etc/init.d/opendkim stop',
      notify        => Service['monit'],
    }
  }

}

