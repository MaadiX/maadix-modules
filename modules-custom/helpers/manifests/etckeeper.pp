class helpers::etckeeper {

  service { 'etckeeper':
    ensure  => 'running',
    enable  => true,
  }

  Package <| title == "etckeeper" |> {
    require => Package['git-core'],
  }

  File <| title == "etckeeper.conf" |> {
    require => Package['etckeeper'],
  }

  file {'/etc/apt/apt.conf.d/05etckeeper':
    ensure  => absent,
    require => Package['etckeeper'],
  } ->
  file_line { 'disable etckeeper before apt':
    path    => '/etc/etckeeper/etckeeper.conf',
    line    => 'AVOID_COMMIT_BEFORE_INSTALL=1',
    match   => '^AVOID_COMMIT_BEFORE_INSTALL.*$',
    require => File['etckeeper.conf'],
    notify  => Service['etckeeper'],
  }

}

