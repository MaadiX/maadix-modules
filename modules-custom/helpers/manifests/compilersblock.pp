#hardening / compilersblock
class helpers::compilersblock (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #change compilers permissions to allow only root to use them
    exec { "/bin/chmod 750 /usr/bin/x86_64-linux-gnu-as":
      onlyif  => "test -f /usr/bin/x86_64-linux-gnu-as"
    }
    exec { "/bin/chmod 750 /usr/bin/x86_64-linux-gnu-gcc-6":
      onlyif  => "test -f /usr/bin/x86_64-linux-gnu-gcc-6"
    }
    exec { "/bin/chmod 750 /usr/bin/x86_64-linux-gnu-gcc-8":
      onlyif  => "test -f /usr/bin/x86_64-linux-gnu-gcc-8"
    }

  }

}

