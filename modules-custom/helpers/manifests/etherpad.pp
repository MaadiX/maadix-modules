class helpers::etherpad (
  $enabled = str2bool($::etherpad_enabled),
  $fqdn = '',
  #for nvm in stretch/buster
  $node_version      = '',
  $domain            = '',
  $ep_revision        = '',
  $ep_maadix_revision = '',
  $remove             = $::etherpad_remove,
) {

  #required classes
  require mysql::server

  validate_bool($enabled)

  if $enabled {

    ##dependencies
    package { 'gzip':
      ensure => 'present',
    }
    package { 'git':
      ensure => 'present',
    }
    if ! defined(Package['curl']) {
      ensure_resource('package','curl', {
      ensure  => present,
      })
    }
    package { 'libssl-dev':
      ensure => 'present',
    }
    package { 'pkg-config':
      ensure => 'present',
    }
    package { 'build-essential':
      ensure => 'present',
    }

    #etherpad group
    group { 'etherpad':
        ensure          => 'present',
        system          => true,
    }

    #etherpad user
    user { 'etherpad':
        ensure          => 'present',
        home            => '/var/www/etherpad',
        managehome      => true,
        shell           => '/bin/false',
        system          => true,
        require         => Package['httpd'],
    } ->
    #set default user umask set in .bashrr
    file_line { 'etherpd user umask in .bashrc':
        path  => '/var/www/etherpad/.bashrc',
        line  => 'umask 027',
    } ->
    file_line { 'etherpd user umask in .profile':
        path  => '/var/www/etherpad/.profile',
        line  => 'umask 027',
    } ->
    file { '/var/www/etherpad/.npm':
        ensure => directory,
        owner  => etherpad,
        group  => etherpad,
    }

    #etherpad repo
    #buster
    if $::lsbdistcodename=='buster'{
      exec { 'update remote etherpad':
          command  => "/bin/bash -c 'cd /var/www/etherpad/etherpad-lite && git remote set-url origin https://github.com/ether/etherpad-lite.git'",
          onlyif   => "/bin/bash -c 'cd /var/www/etherpad/etherpad-lite && git remote -v | grep git:'",
          user     => 'etherpad',
          path     => '/usr/bin:/bin',
      } ->
      exec { 'fix .git perms':
          command  => 'chown -R etherpad:etherpad /var/www/etherpad/etherpad-lite/.git/*',
          onlyif   => "/bin/bash -c 'ls -l /var/www/etherpad/etherpad-lite/.git/config | grep root'",
          path     => '/usr/bin:/bin',
      } ->
      vcsrepo { '/var/www/etherpad/etherpad-lite':
    	  ensure   => present,
  	  provider => git,
    	  source   => 'https://github.com/ether/etherpad-lite.git',
          revision => "$ep_revision",
          user     => 'etherpad',
          path	 => '/var/www/etherpad/etherpad-lite',
          require  => [
                      User['etherpad'],
                      ],
          notify   => Service['etherpad'],
      }
    }

    #repo permissions
    file { '/var/www/etherpad/etherpad-lite':
      ensure  => 'directory',
      owner   => 'etherpad',
      group   => 'etherpad',
      mode    => '0640',
      require => Vcsrepo['/var/www/etherpad/etherpad-lite'],     
    }

        
    #etherpad node
    #buster
    if $::lsbdistcodename=='buster'{

      #override nvn::node::install exec to avoid issues
      Exec <| title == "nvm install node version $node_version for etherpad" |> {
        timeout  => 7200,
      }

      nvm::install { 'etherpad':
        home         => '/var/www/etherpad',
        nvm_dir      => '/var/www/etherpad/.nvm',
        profile_path => '/var/www/etherpad/.bashrc',
        require      => [
                        Class['nvm'],
                        User['etherpad'],
                        ]
      }->
      nvm::node::install {$node_version :
        user         => 'etherpad',
        set_default  => true,
        nvm_dir      => '/var/www/etherpad/.nvm',
      }
    }


    ##etherpad mysql
    # set in helpers::passwd
    ##etherpad admin user
    # set in helpers::passwd

    ##etherpad settings.json header with generic options
    file { 'etherpad-settings':
        path    => '/var/www/etherpad/etherpad-lite/settings_etherpad_header',
        owner   => 'etherpad',
        group   => 'etherpad',
        mode    => '0600',
        content => template('helpers/settings_etherpad_header.erb'),
        require => Vcsrepo['/var/www/etherpad/etherpad-lite'],
    } ->

    ##etherpad settings.json with database and mysql user and admin credentials + mysql info
    #doc: https://github.com/ether/etherpad-lite/wiki/How-to-use-Etherpad-Lite-with-MySQL
    #doc: https://github.com/ether/etherpad-lite/blob/develop/settings.json.template 
    exec { 'set etherpad passw':
      command => "/bin/bash /etc/maadix/scripts/etherpad.sh",
      creates => '/var/www/etherpad/etherpad-lite/settings.json',
      require => [
		  File['etherpad password'],
		  File['etherpad-settings'],
                  Vcsrepo['/var/www/etherpad/etherpad-lite'],
		 ],
    } ->

    ##etherpad utf8mb4
    file { 'etherpad utf8mb4 script':
      path    => '/etc/maadix/scripts/etherpad-utf8mb4.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/etherpad-utf8mb4.sh.erb'),
    } ->
    exec { 'set etherpad database utf8mb4':
      command => "/bin/bash /etc/maadix/scripts/etherpad-utf8mb4.sh",
      unless => "/usr/bin/mysql etherpad -e 'SELECT @@character_set_database, @@collation_database' | grep utf8mb4",
    }


    #etherpad groups plugin https://github.com/MaadixNet/ep_maadix
    #buster
    if $::lsbdistcodename=='buster'{

      #when installing or updating
      if $::etherpad_ep_maadix_installed_version != $ep_maadix_revision {

        if $::etherpad_ep_etherapd_lite_installed_version != $ep_revision {
          #backup etherpad
          $timestamp = Timestamp.new().strftime('%Y_%m_%d-%H_%M_%S')
          exec { "updating etherpad: backup directories $timestamp":
            command     => "service monit stop && service etherpad stop && tar -czf /home/.trash/backups/etherpad-$timestamp.tar.gz /var/www/etherpad",
            logoutput   => true,
            timeout     => 28800,
            onlyif      => "mysql -B etherpad --disable-column-names -e 'show tables;' | grep store",
            require     => [
                           Vcsrepo['/var/www/etherpad/etherpad-lite'],
                           Nvm::Node::Install["$node_version"],
                           ],
          } ->
          exec { "updating etherpad: ddbb backup $timestamp":
            command     => "mysqldump --defaults-extra-file=/root/.my.cnf etherpad > /home/.trash/backups/etherpad-$timestamp.sql",
            logoutput   => true,
            timeout     => 28800,
            onlyif      => "mysql -B etherpad --disable-column-names -e 'show tables;' | grep store",
          }

        }

        #upgrade ep_maadix only for new installations or upgrades, this avoid issues if user deactivates the group manually
        if $::ep_maadix {
          #with each new update, launch npm update to clean all packages, create temporal package.json with npm init, npm install ep_maadix@version
          exec { 'ep-maadix-repo':
            umask       => '027',
            command     => "sudo -u etherpad /bin/bash -c 'source /var/www/etherpad/.nvm/nvm.sh && export NODE_ENV=production && npm update && npm init -y && npm install --legacy-peer-deps ep_maadix@$ep_maadix_revision'",
            cwd         => '/var/www/etherpad/etherpad-lite',
            logoutput   => true,
            notify      => [
                           Service['etherpad'],
                           File['etherpad package.json'],
                           ],
            require     => [
                           Nvm::Node::Install["$node_version"],
                           Vcsrepo['/var/www/etherpad/etherpad-lite'],
                           ],
          } ->
  
          #etherpad groups plugin mysql tables
          exec { 'ep-maadix-mysql':
            command => "mysql --defaults-extra-file=/root/.my.cnf etherpad < node_modules/ep_maadix/sql_listing.sql && touch /etc/maadix/status/ep-maadix-mysql",
            cwd     => '/var/www/etherpad/etherpad-lite/',
            creates => '/etc/maadix/status/ep-maadix-mysql',
            require => Exec['set etherpad passw'],
          } ->

          ##etherpad groups email.json
          file { 'ep-maadix-email':
            path    => '/var/www/etherpad/etherpad-lite/node_modules/ep_maadix/email.json',
            owner   => 'etherpad',
            group   => 'etherpad',
            mode    => '0640',
            content => template('helpers/ep-maadix-email.json.erb'),
            notify  => Service['etherpad'],
          }

        }

        #reinstall all plugins when updating
        $::etherpad_plugins.each |$plugin| {
          exec { "install plugin $plugin":
            umask     => '027',
            command   => "sudo -u etherpad /bin/bash -c 'source /var/www/etherpad/.nvm/nvm.sh && export NODE_ENV=production && npm install --legacy-peer-deps $plugin'",
            cwd       => '/var/www/etherpad/etherpad-lite',
            logoutput => true,
            require   => Nvm::Node::Install["$node_version"],
            notify    => [
                         Service['etherpad'],
                         File['etherpad package.json'],
                         ],
          }
        }

        #ensure packaje.json is missing after upgrade
        file { 'etherpad package.json':
          path        => '/var/www/etherpad/etherpad-lite/package.json',
          ensure      => absent,
        }

      }

    }


    ##oom conf
    file { '/lib/systemd/system/etherpad.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/etherpad.service.d/local.conf':
      content => template('helpers/service_etherpad_local.conf.erb'),
      notify  => Service['etherpad'],
    }

    ##etherpad systemd
    #jessie|stretch|buster
    #doc: https://github.com/ether/etherpad-lite/wiki/How-to-deploy-Etherpad-Lite-as-a-service
    file { 'etherpad systemd service':
        path    => '/lib/systemd/system/etherpad.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template("helpers/etherpad.service-$::lsbdistcodename.erb"),
    }

    ##etherpad service
    service { 'etherpad':
    	ensure    => running,
    	name      => etherpad,
    	enable    => true,
    	hasstatus => true,
        require => [
		  File['etherpad-settings'],
		  File['etherpad systemd service'],
		  Exec['set etherpad passw'],
                  Vcsrepo['/var/www/etherpad/etherpad-lite'],
		 ],
    }

    #if domain not defined, add standar apache alias /etherpad with proxy conf
    if($domain==''){
      ##etherpad proxy apache https
      file { 'apache-etherpad':
          path    => '/etc/apache2/conf-available/etherpad.conf',
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => template('helpers/etherpad.conf.erb'),
          require => Class['apache'],
      }
      file { '/etc/apache2/conf.d/etherpad.conf':
        ensure => 'link',
        target => '/etc/apache2/conf-available/etherpad.conf',
        notify => Service['httpd'],
        require => Class['apache'],
      }
    } else {


      ##etherpad proxy mods
      file { 'apache-etherpad':
          path    => '/etc/apache2/conf-available/etherpad.conf',
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => template('helpers/etherpad_mods.conf.erb'),
          require => Class['apache'],
      }
      file { '/etc/apache2/conf.d/etherpad.conf':
        ensure => 'link',
        target => '/etc/apache2/conf-available/etherpad.conf',
        notify => Service['httpd'],
        require => Class['apache'],
      }

      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/etherpad",
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/etherpad"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/etherpad",
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        notify => Class['apache::service'],
        custom_fragment => '

  #Not allow any connection using HTTP
  Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"

  ProxyVia On
  ProxyRequests Off
  ProxyPreserveHost on

  <Location />
        ProxyPass http://localhost:9001/ retry=0 timeout=30
        ProxyPassReverse http://localhost:9001/
  </Location>

  <Location /socket.io>
        # This is needed to handle the websocket transport through the proxy, since
        # etherpad does not use a specific sub-folder, such as /ws/ to handle this kind of traffic.
        # Taken from https://github.com/ether/etherpad-lite/issues/2318#issuecomment-63548542
        # Thanks to beaugunderson for the semantics
        RewriteEngine On
        RewriteCond %{QUERY_STRING} transport=websocket    [NC]
        RewriteRule /(.*) ws://localhost:9001/socket.io/$1 [P,L]
        ProxyPass http://localhost:9001/socket.io retry=0 timeout=30
        ProxyPassReverse http://localhost:9001/socket.io
  </Location>

  <Proxy *>
      Options FollowSymLinks MultiViews
      AllowOverride All
      Order allow,deny
      allow from all
  </Proxy>

        ',
      }

    }

    #clean old etherpad domain
    if($::etherpaddomain_old){
      helpers::cleanvhost{"$::etherpaddomain_old":
        docroot  => '/var/www/etherpad',
      }
    }

  } else {


    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/etherpad',
      }

    }

    ##etherpad service stopped
    service { 'etherpad':
    	ensure    => $enabled,
    	name      => etherpad,
    	enable    => $enabled,
    }

  }

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::etherpad::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'etherpad':}
    helpers::remove {'etherpad':
      files      => ['/lib/systemd/system/etherpad.service',
                     '/etc/apache2/conf-available/etherpad.conf',
                     '/etc/apache2/conf.d/etherpad.conf',
                     '/etc/maadix/status/ep-maadix-mysql',
                     '/etc/maadix/status/ep-maadix-commit-aecaa80',
                    ],
      dirs       => ['/lib/systemd/system/etherpad.service.d'],
      big_dirs   => ['/var/www/etherpad'],
      tidy       => {'/etc/maadix/status' => 'ep-maadix-commit*'},
      mysql_d    => ['etherpad'],
      mysql_u    => ['etherpad@%'],
      users      => ['etherpad'],
      notify     => [
                    Helpers::Resetldapgroup['etherpad'],
                    Service['httpd']
                    ]
    }
  }

}
