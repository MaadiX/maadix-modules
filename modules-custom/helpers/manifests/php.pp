class helpers::php (
  $version = '7.2',
  $purge_unused_fpm = true,
) {

    Php::Extension <| |> -> Alternatives <| |>

    #needed for php-imagick svg support
    if ! defined(Package['libmagickcore-6.q16-6-extra']) {
      ensure_resource('package','libmagickcore-6.q16-6-extra', {
      ensure  => present,
      notify  => Service["php$version-fpm"],
      })
    }

    #sury source
    apt::source { 'sury':
      location => 'https://packages.sury.org/php/',
      release  => "$::lsbdistcodename",
      repos    => 'main',
      include  => {
        'src' => false,
        'deb' => true,
      },
      key      => {
        id     => '15058500A0235D97F5D10063B188E2B695BD4743',
        source => 'https://packages.sury.org/php/apt.gpg',
      },
    }

    #disable dotdeb repo
    Apt::Source <| title == "source_php_$::lsbdistcodename" |> {
      ensure  => absent,
    }

    #these folders must exist when installing or updating php version
    file { "/etc/php/":
      ensure  => 'directory',
    } ->
    file { "/etc/php/$version":
      ensure  => 'directory',
    } ->
    file { "/etc/php/$version/mods-available/":
      ensure  => 'directory',
      before  => Class['ducktape::php'],
    }
    
    #php alternatives must be set before php::extensions
    #update php alternatives
    alternatives { 'php':
      path => "/usr/bin/php${version}",
      require => Class['php::packages'],
    } ->
    alternatives { 'phar':
      path => "/usr/bin/phar${version}",
    } ->
    alternatives { 'phar.phar':
      path => "/usr/bin/phar.phar${version}",
    } ->
    #alternatives { 'phpize':
    #  path => "/usr/bin/phpize${version}",
    #  require => Package["php${version}-dev"],
    #} ->
    #php-fpm listen on a TCP socket in localhost, we don't need php-fpm.sock alternative
    #alternatives { 'php-fpm.sock':
    #  path => "/run/php/php${version}-fpm.sock",
    #  require => Package["php${version}-fpm"],
    #} ->
    alternatives { 'php-config':
      path => "/usr/bin/php-config${version}",
      before  => Class['php::pear'],
    }

    #override: disable ini_file adding ; override=99 in ducktape::php::conf
    Ini_setting <| title == "overrides" |> {
      ensure  => absent,
    }

    #php fpm pool, override params using integer facters
    Php::Fpm::Pool <| title == "www" |> {
      listen                 => '127.0.0.1:9000',
      pm                     => 'dynamic',
      pm_max_children        => $facts['php_fpm_limit'],
      pm_start_servers       => $facts['php_fpm_min'],
      pm_min_spare_servers   => $facts['php_fpm_min'],
      pm_max_spare_servers   => $facts['php_fpm_max'],
      pm_max_requests        => 4000,
    }

    #install specific extensions based on $version
    if ($version=='7.3') or ($version=='7.4'){
      php::extension {'json':
        ensure   => 'latest',
        provider => 'apt',
        require  => Class['php::packages'],
      }
    }

    #remove fpm packages of unused php versions
    if $purge_unused_fpm {
      $php_versions = ['7.3', '7.4', '8.0', '8.1', '8.2', '8.3']
      $php_versions.each |$php_version| {
        if $php_version != $version {
          #notify {"remove php$php_version-fpm":}
          ensure_resource('package',"php$php_version-fpm", {
            ensure  => absent,
            before  => Class['php'],
          })
        }
      }
    }


    #set php version in ldap
    exec { 'set php version in ldap':
      command     => "/etc/maadix/scripts/setldapphpversion.sh $version",
      refreshonly => true,
      require     => File['setldapphpversion script'],
    }


    #php opcache and apc conf
    exec { 'restart php-fpm service':
      command     => "service php$version-fpm restart",
      path        => ['/bin', '/usr/bin', '/usr/sbin'],
      refreshonly => true,
    }

    file { 'php-fpm opcache conf':
      path    => "/etc/php/$version/fpm/conf.d/90-opcache.ini",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/90-opcache.ini.erb'),
      notify  => Service["php$version-fpm"],
      require => Class['php::packages'],
    } ->
    file { 'php-cli opcache conf':
      path    => "/etc/php/$version/cli/conf.d/90-opcache.ini",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/90-opcache.ini.erb'),
    } ->
    #https://docs.nextcloud.com/server/21/admin_manual/configuration_server/caching_configuration.html#id1
    file { 'php-fpm apc conf':
      path    => "/etc/php/$version/fpm/conf.d/90-apc.ini",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/90-apc.ini.erb'),
      notify  => Service["php$version-fpm"],
    } ->
    file { 'php-cli apc conf':
      path    => "/etc/php/$version/cli/conf.d/90-apc.ini",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/90-apc.ini.erb'),
    }

    #pools
    include helpers::php::pools
}
