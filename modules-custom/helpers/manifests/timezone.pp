class helpers::timezone (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    if $::lsbdistcodename=='stretch'{

      #fix timezone bootstrap for stretch
      #the configured  /etc/timezone will be overwritten by the old value after executing dpkg-reconfigure
      #if /etc/timezone is overwritted by default values reconfigure timezone
      #doc: https://www.illucit.com/en/linux/timezone-in-debian-9-stretch/
      file { 'set timezone file':
        path    => '/etc/timezone',
        content => template('helpers/timezone'),
        notify  => Exec['reconfigure timezone'],
      }

      #vms upgraded from jessie need to set localtime link and the reconfigure timezone
      file { '/etc/localtime':
        ensure  => 'link',
        target  => '/usr/share/zoneinfo/Europe/Madrid',
        notify  => Exec['reconfigure timezone'],
      }

      exec { 'reconfigure timezone':
        command     => '/usr/sbin/dpkg-reconfigure -f noninteractive tzdata',
        logoutput   => true,
        refreshonly => true,
      }

    }

    if ($::lsbdistcodename=='buster') or ($::lsbdistcodename=='bullseye'){

      #fix timezone bootstrap for stretch
      #the configured  /etc/timezone will be overwritten by the old value after executing dpkg-reconfigure
      #if /etc/timezone is overwritted by default values reconfigure timezone
      #doc: https://www.illucit.com/en/linux/timezone-in-debian-9-stretch/
      file { 'set timezone file':
        path    => '/etc/timezone',
        content => template('helpers/timezone'),
        notify  => Exec['reconfigure timezone'],
      }

      #vms upgraded from jessie need to set localtime link and the reconfigure timezone
      file { '/etc/localtime':
        ensure  => 'link',
        target  => '/usr/share/zoneinfo/Europe/Madrid',
        notify  => Exec['reconfigure timezone'],
      }

      exec { 'reconfigure timezone':
        command     => '/usr/sbin/dpkg-reconfigure -f noninteractive tzdata',
        logoutput   => true,
        refreshonly => true,
      }

    }



  }

}





