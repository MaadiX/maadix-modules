class helpers::rainloop (
  $enabled = str2bool($::rainloop_enabled),
  $version = '1.10.5.192',
  $apache_conf = true,
  $remove  = $::rainloop_remove,
) {

  #required classes
  require php
  require helpers::php

  validate_bool($enabled)
  validate_string ($version)

  if $enabled {


    ##rainloop directory
    file { '/var/www/rainloop':
      ensure => directory,
      mode   => '0755',
      group   => 'fpmrainloop',
      owner   => 'fpmrainloop',
    }->
    #rainloop perms to allow updating
    exec { 'rainloop perms required to update':
      command => 'find rainloop -exec chmod 755 {} \;',
      cwd     => '/var/www/rainloop',
      onlyif  => 'test -d /var/www/rainloop/rainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    }

    ##rainloop source
    archive {'/tmp/rainloop.zip':
      ensure        => present,
      extract       => true,
      extract_path  => '/var/www/rainloop',
      source        => "https://github.com/RainLoop/rainloop-webmail/releases/download/v${version}/rainloop-legacy-${version}.zip",
      creates       => "/var/www/rainloop/rainloop/v/${version}",
      cleanup       => true,
      before	    =>[
                       Exec['set rainloop passw'],
                       Exec['rainloop folders perms'],                       
                      ],
      require	    =>[
                      Package['unzip'],
                      Exec['rainloop perms required to update'],
                      ],
    }

    #patch XSS, fixed in 1.17
    /*
    file {'rainloop_xss.patch':
      path    => '/var/www/rainloop/rainloop_xss.patch',
      owner   => 'fpmrainloop',
      group   => 'fpmrainloop',
      mode    => '0444',
      content => template('helpers/rainloop_xss.patch'),
      require => Archive['/tmp/rainloop.zip'],
    } ->
    exec { 'apply rainloop_xss.patch':
      command => "patch rainloop/v/${version}/app/libraries/MailSo/Base/HtmlUtils.php < rainloop_xss.patch && touch /etc/maadix/status/rainloop-xss.patch",
      cwd     => '/var/www/rainloop',
      creates => '/etc/maadix/status/rainloop-xss.patch',
    }
    */

    #rainloop pass
    exec { 'set rainloop passw':
      command => "/bin/bash /etc/maadix/scripts/rainlooppass.sh",
      creates => '/etc/maadix/status/rainloop',
      require => [ 
                  File['rainloop password'],
                  Archive['/tmp/rainloop.zip'],
                 ],
    }

    ##application.ini tunning
    file_line { 'rainloop allow_html_editor_source_button':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'allow_html_editor_source_button = On',
      match   => '.*allow_html_editor_source_button.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop allow_html_editor_biti_buttons':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'allow_html_editor_biti_buttons = On',
      match   => '.*allow_html_editor_biti_buttons.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop verify_certificate on':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'verify_certificate = On',
      match   => '^verify_certificate.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop allow_self_signed off':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'allow_self_signed = Off',
      match   => 'allow_self_signed.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop allow_two_factor_auth on':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'allow_two_factor_auth = On',
      match   => 'allow_two_factor_auth.*$',
      require => Exec['set rainloop passw'],
    }
    #disable gravatar
    file_line { 'rainloop allow_gravatar off':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'allow_gravatar = Off',
      match   => '^allow_gravatar.*$',
      require => Exec['set rainloop passw'],
    }
    #fail2ban conf
    file_line { 'rainloop auth_logging':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'auth_logging = On',
      match   => '^auth_logging =.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop auth_logging_filename':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'auth_logging_filename = "auth.txt"',
      match   => '^auth_logging_filename.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop auth_logging_format':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'auth_logging_format = "[{date:Y-m-d H:i:s}] Auth failed: ip={request:ip} user={imap:login} host={imap:host} port={imap:port}"',
      match   => '^auth_logging_format.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop cafile':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'cafile = "/usr/share/ca-certificates/mozilla/ISRG_Root_X1.crt"',
      match   => 'cafile =.*$',
      require => Exec['set rainloop passw'],
    }
    file_line { 'rainloop capath':
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      line    => 'capath = "/etc/ssl/certs/"',
      match   => 'capath =.*$',
      require => Exec['set rainloop passw'],
    }
    #enable contacts
    ini_setting { "[contacts] enable":
      ensure  => present,
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      section => 'contacts',
      setting => 'enable',
      value   => 'On',
      require => Exec['set rainloop passw'],
    }
    ini_setting { "[contacts] allow_sync":
      ensure  => present,
      path    => '/var/www/rainloop/data/_data_/_default_/configs/application.ini',
      section => 'contacts',
      setting => 'allow_sync',
      value   => 'On',
      require => Exec['set rainloop passw'],
    }

    ##files owner and perms
    #general perms
    exec { 'rainloop folders perms':
      command => 'find . -type d -mindepth 1 -exec chmod 555 {} \;',
      cwd     => '/var/www/rainloop',
      require => Archive['/tmp/rainloop.zip'],
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop files perms':
      command => 'find . -type f -exec chmod 444 {} \;',
      cwd     => '/var/www/rainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop files owner':
      command => 'chown -R fpmrainloop:fpmrainloop /var/www/rainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    #root data folder perms: apache must have write permissions to avoid rainloop error
    file { '/var/www/rainloop/data':
      ensure => directory,
      mode   => '0750',
      group   => 'mxcp',
      owner   => 'fpmrainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    #general data folder perms
    exec { 'rainloop data folders perms':
      command => 'find . -type d -mindepth 1 -exec chmod 550 {} \;',
      cwd     => '/var/www/rainloop/data',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop data files perms':
      command => 'find . -type f -exec chmod 440 {} \;',
      cwd     => '/var/www/rainloop/data',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    #allow mxcp to write domains ini files
    file { '/var/www/rainloop/data/_data_':
      ensure => directory,
      mode   => '0550',
      group   => 'mxcp',
      owner   => 'fpmrainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    file { '/var/www/rainloop/data/_data_/_default_':
      ensure => directory,
      mode   => '0750',
      group   => 'mxcp',
      owner   => 'fpmrainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop domains owner':
      command => 'chown -R mxcp:mxcp /var/www/rainloop/data/_data_/_default_/domains',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop domains perms':
      command => 'chmod 755 /var/www/rainloop/data/_data_/_default_/domains',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop domains files perms':
      command => 'chmod 644 /var/www/rainloop/data/_data_/_default_/domains/*',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    #storage data folder perms, add write perm
    file { '/var/www/rainloop/data/_data_/_default_/storage':
      ensure => directory,
      mode   => '0750',
      group   => 'fpmrainloop',
      owner   => 'fpmrainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop storage data folders perms':
      command => 'find . -type d -exec chmod 750 {} \;',
      cwd     => '/var/www/rainloop/data/_data_/_default_/storage',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop storage data files perms':
      command => 'find . -type f -exec chmod 640 {} \;',
      cwd     => '/var/www/rainloop/data/_data_/_default_/storage',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    #logs data folder perms, add write perm
    file { '/var/www/rainloop/data/_data_/_default_/logs':
      ensure => directory,
      mode   => '0750',
      group   => 'fpmrainloop',
      owner   => 'fpmrainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop logs data folders perms':
      command => 'find . -type d -exec chmod 750 {} \;',
      cwd     => '/var/www/rainloop/data/_data_/_default_/logs',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    exec { 'rainloop logs data files perms':
      command => 'find . -type f -exec chmod 640 {} \;',
      cwd     => '/var/www/rainloop/data/_data_/_default_/logs',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    #cache data folder perms, add write perm
    file { '/var/www/rainloop/data/_data_/_default_/cache':
      ensure => directory,
      mode   => '0750',
      group   => 'fpmrainloop',
      owner   => 'fpmrainloop',
      #don't log if not in debug mode
      loglevel => 'debug',
    } ->
    #app ini file perms
    file { '/var/www/rainloop/data/_data_/_default_/configs/application.ini':
      mode   => '0640',
      group   => 'fpmrainloop',
      owner   => 'fpmrainloop',
      loglevel => 'debug',
    } ->
    #change .sqlite files mode
    exec { 'rainloop sqlite files perms':
      command => 'find . -type f -name "*.sqlite" -exec chmod 640 {} \;',
      cwd     => '/var/www/rainloop/data',
      loglevel => 'debug',
    }

    
    #apache conf
    if $apache_conf {
      file { 'apache-rainloop':
        path    => '/etc/apache2/conf-available/apache-rainloop.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/apache-rainloop.conf.erb'),
        require => Class['apache'],
        notify  => Service['httpd'],
      }
      file { '/etc/apache2/conf.d/apache-rainloop.conf':
        ensure => 'link',
        target => '/etc/apache2/conf-available/apache-rainloop.conf',
        notify => Service['httpd'],
        require => Class['apache'],
      }
    } else {
      file { 'apache-rainloop':
        path    => '/etc/apache2/conf-available/apache-rainloop.conf',
        ensure  => absent,
        require => Class['apache'],
        notify  => Service['httpd'],
      }
      file { '/etc/apache2/conf.d/apache-rainloop.conf':
        ensure => absent,
        notify => Service['httpd'],
        require => Class['apache'],
      }

    }


    ######################### fail2ban #####################################################
    #doc: https://github.com/RainLoop/rainloop-webmail/issues/1580

    #rainloop fail2ban
    exec { 'rainloop: create initial log file if absent':
        command    => 'touch /var/www/rainloop/data/_data_/_default_/logs/auth.txt && chown fpmrainloop /var/www/rainloop/data/_data_/_default_/logs/auth.txt',
        logoutput  => true,
        creates    => '/var/www/rainloop/data/_data_/_default_/logs/auth.txt',
        require => Exec['set rainloop passw'],
    } ->
    file { 'rainloop: fail2ban filter':
        path    => '/etc/fail2ban/filter.d/rainloop.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/rainloop_fail2ban.conf'),
        notify  => Service['fail2ban'],
    } ->
    file { 'rainloop: fail2ban jail':
        path    => '/etc/fail2ban/jail.d/rainloop.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/rainloop_fail2ban_jail.conf'),
        notify  => Service['fail2ban'],
    }

    ##add domains ini files to rainloop
    helpers::rainloop::domain {$::cpaneldomains:}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'rainloop':}
    helpers::remove {'rainloop':
      files      => ['/etc/maadix/status/rainloop',
                     '/etc/maadix/status/rainloop-xss.patch',
                     '/etc/apache2/conf-available/apache-rainloop.conf',
                     '/etc/apache2/conf.d/apache-rainloop.conf',
                     '/etc/fail2ban/filter.d/rainloop.conf',
                     '/etc/fail2ban/jail.d/rainloop.conf',
                    ],
      big_dirs   => ['/var/www/rainloop'],
      notify     => [
                    Helpers::Resetldapgroup['rainloop'],
                    Service['httpd'],
                    Service['fail2ban'],
                    ]
    }
  }

}
