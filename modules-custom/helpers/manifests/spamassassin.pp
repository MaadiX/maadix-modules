class helpers::spamassassin (
  $enabled           = str2bool($::spamassassin_enabled),
  $remove            = $::spamassassin_remove,
) {

  validate_bool($enabled)

  if $enabled {

    #### packages ####
    package {'spamassassin':
      ensure  => 'latest',
    } ->
    package {'spamc':
      ensure  => 'latest',
    } ->
    package {'spamass-milter':
      ensure  => 'latest',
    } ->
    user {'debian-spamd':
      shell  => '/bin/bash',
    }

    #smtpd_milter_map to apply only dkim milter when mail arrives from fqdn vm ip
    file { '/etc/postfix/smtpd_milter_map':
      ensure  => present,
      content => "$::ipaddress inet:127.0.0.1:8891",
      notify  => Service['postfix'],
    }

    #### logdir ###
    file { '/var/log/spamassassin':
      ensure  => directory,
      owner   => 'debian-spamd',
      group   => 'debian-spamd',
      mode    => '0755',
      require => Package['spamassassin'],
      notify  => Service['spamassassin'],
    }


    #### conf ###
    ## spamassassin conf, to enable logs and configure to run as debian-spamd
    file_line { 'enable spamassassin logs':
      path    => '/etc/default/spamassassin',
      line    => 'OPTIONS="-u debian-spamd --create-prefs --max-children 5 --helper-home-dir -s /var/log/spamassassin/spamd.log"',
      match   => 'OPTIONS.*$',
      require => Package['spamassassin'],
      notify  => Service['spamassassin'],
    }
    ## spamassassin conf, pid file jessie|stretch|buster
    $pidfile = $::osfamily ? {
      'Debian' => $::lsbdistcodename ? {
        'jessie' => '/var/run/spamassassin.pid',
        'stretch' => '/var/run/spamd.pid',
        'buster'  => '/var/run/spamd.pid',
        default  => undef,
      },
      default  => undef,

    }
    file_line { 'spamassassin pid file':
      path    => '/etc/default/spamassassin',
      line    => "PIDFILE='$pidfile'",
      match   => 'PIDFILE.*$',
      require => Package['spamassassin'],
      notify  => Service['spamassassin'],
    }
    
    ## spamassassin conf, to enable cron
    file_line { 'enable spamassassin cron':
      path    => '/etc/default/spamassassin',
      line    => 'CRON=1',
      match   => 'CRON.*$',
      require => Package['spamassassin'],
      notify  => Service['spamassassin'],
    }
    ## spamassassin conf, to use custon nameserver
    file_line { 'spamassassin nameserver':
      path    => '/etc/spamassassin/local.cf',
      line    => 'dns_server 127.0.0.1',
      match   => 'dns_server.*$',
      require => Package['spamassassin'],
      notify  => Service['spamassassin'],
    }
    
    #scores
    file_line { 'spamassassin score':
      path    => '/etc/spamassassin/local.cf',
      line    => "required_score $::spamassassin_score",
      match   => '^required_score.*$',
      require => Package['spamassassin'],
      notify  => Service['spamassassin'],
    }

    # NOTE: smtp master conf is in spamassassin.yaml

    #### service ####
    service { 'spamassassin':
      ensure  => 'running',
      enable  => true,
    }

    #logrotate
    file { 'spamassassin logrotate':
        path    => '/etc/logrotate.d/spamassassin',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/spamassassin_logrotate'),
    }

  } else {

    #### disable service ####
    service { 'spamassassin':
      ensure  => $enabled,
      enable  => $enabled,
    }

    #logrotate
    file { 'spamassassin logrotate':
        path    => '/etc/logrotate.d/spamassassin',
        ensure  => absent,
    }

  }


  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::spamassassin::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'spamassassin':}
    helpers::remove {'spamassassin':
      files         => ['/etc/postfix/smtpd_milter_map','/etc/default/spamassassin'],
      dirs          => ['/etc/spamassassin','/var/log/spamassassin'],
      packages      => ['debian-spamd','spamass-milter','spamc','spamassassin'],
      users         => ['debian-spamd','spamass-milter'],
      notify        => [
                       Helpers::Resetldapgroup['spamassassin'],
                       Service['httpd']
                       ]
    }
  }

}

