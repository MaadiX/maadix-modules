class helpers::unbound {

    #unbound dns cache server, to mitigate banning in uribl and spamhaus
    package {'unbound':
      ensure  => 'latest',
    } ->
    #unbound conf
    file { '/etc/unbound/unbound.conf.d/maadix.conf':
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/unbound.conf'),
      notify  => Service['unbound'],
      before  => File['/etc/resolv.conf'],
    }

    #service
    service { 'unbound':
      ensure  => 'running',
      enable  => true,
    }

    #set nameserver 127.0.0.1 for servers with dinamyc ip
    file_line { 'dhclient domain-search':
      path    => '/etc/dhcp/dhclient.conf',
      line    => 'supersede domain-search "maadix.org";',
      require => Service['unbound'],
    } ->
    file_line { 'dhclient domain-name-servers':
      path    => '/etc/dhcp/dhclient.conf',
      line    => 'supersede domain-name-servers 127.0.0.1;',
    }

    # Monit
    if defined('::monit') and defined(Class['::monit']) {
      class {'helpers::unbound::external::monit': }
    }

}

