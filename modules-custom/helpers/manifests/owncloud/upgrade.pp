define helpers::owncloud::upgrade(
  $major_version       = undef,
  $version_installed   = undef,
) {
  
      notify {"major version: $major_version": 
      } ->
      notify {"upgrade: prepare to update if version changes / stop apache and backup folder-$major_version": 
      } ->
      exec { "owncloud appstore force before updating-$major_version":
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set appstoreurl --value='https://marketplace.owncloud.com'",
        logoutput  => true,
      } ->
      exec { "updating owncloud: enable manteinance-$major_version":
        command    => "sudo -u fpmowncloud php occ maintenance:mode --on",
        cwd        => "/var/www/owncloud/owncloud",
        logoutput  => true,
      } ->
      exec { "updating owncloud: disable apps-$major_version":
        #doc: https://doc.owncloud.org/server/latest/admin_manual/maintenance/manual_upgrade.html#disable-core-apps
        command    => 'sudo -u fpmowncloud bash -c "php occ app:disable rainloop; php occ app:disable documents; php occ app:disable appvncsafe; php occ app:disable activity; php occ app:disable files_pdfviewer; php occ app:disable files_texteditor; php occ app:disable gallery"',
        cwd        => "/var/www/owncloud/owncloud",
        logoutput  => true,
      } ->
      exec { "updating owncloud: stop apache and backup directories-$major_version":
        command    => "service apache2 stop && mv /var/www/owncloud/owncloud /var/www/owncloud/owncloud-$version_installed-$major_version",
        logoutput  => true,
        timeout    => 28800,
      } ->
      exec { "updating owncloud: ddbb backup-$major_version":
        command    => "mysqldump --defaults-extra-file=/root/.my.cnf owncloud > /var/www/owncloud/owncloud-$version_installed-$major_version.sql && chmod 700 /var/www/owncloud/owncloud-$version_installed-$major_version.sql",
        logoutput  => true,
        timeout    => 28800,
      } ->
      exec { "updating owncloud: download owncloud-$major_version.tar.bz2":
        command    => "wget https://download.owncloud.com/server/stable/owncloud-$major_version.tar.bz2 && tar -xjf owncloud-$major_version.tar.bz2",
        cwd        => '/var/www/owncloud',
        user       => 'fpmowncloud',
      } ->
      file { 'apps-external folder':
        path       => '/var/www/owncloud/owncloud/apps-external',
        ensure     => directory,
        owner      => fpmowncloud,
        group      => fpmowncloud,
      } ->
      file { 'apps-external folder $version_installed-$major_version':
        path       => "/var/www/owncloud/owncloud-$version_installed-$major_version/apps-external",
        ensure     => directory,
        owner      => fpmowncloud,
        group      => fpmowncloud,
      } ->
      exec { "updating owncloud: copying 3rd party apps-$major_version":
        command    => "find * -type d -prune -print0 | xargs -0 -n1 -I % sh -c \"if [ ! -d /var/www/owncloud/owncloud/apps/% ]; then echo % && cp -Rp % /var/www/owncloud/owncloud/apps/; fi\" ",
        cwd        => "/var/www/owncloud/owncloud-$version_installed-$major_version/apps",
        logoutput  => true,
      } ->
      exec { "updating owncloud: copying 3rd party apps-external-$major_version":
        command    => "find * -type d -prune -print0 | xargs -0 -n1 -I % sh -c \"if [ ! -d /var/www/owncloud/owncloud/apps/% ]; then echo % && cp -Rp % /var/www/owncloud/owncloud/apps/; fi\" ",
        cwd        => "/var/www/owncloud/owncloud-$version_installed-$major_version/apps-external",
        logoutput  => true,
      } ->
      exec { "updating owncloud: copying config file-$major_version":
        command    => "sudo -u fpmowncloud cp /var/www/owncloud/owncloud-$version_installed-$major_version/config/config.php /var/www/owncloud/owncloud/config/config.php",
        logoutput  => true,
      } ->
      exec { "updating owncloud: occ upgrade-$major_version":
        command    => "sudo -u fpmowncloud php occ upgrade",
        cwd        => "/var/www/owncloud/owncloud",
        logoutput  => true,
        timeout    => 28800,
        notify     => [
                      Service['httpd'],
                      Exec['set owncloud installed version'],
                      ],
      } ->
      exec { "updating owncloud: disable manteinance-$major_version":
        command    => "sudo -u fpmowncloud php occ maintenance:mode --off",
        cwd        => "/var/www/owncloud/owncloud",
        logoutput  => true,
      } ->
      exec { "updating owncloud: restart apache-$major_version":
        command    => "service apache2 restart",
        before     => [
                      Notify['upgrade: set OC options on first install or on version update'],
                      ],
      }

}




