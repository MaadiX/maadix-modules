class helpers::reboot (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {


    #check_reboot.sh
    file { 'check_reboot.sh':
        path    => '/etc/maadix/scripts/check_reboot.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/check_reboot.sh'),
        require => File['/etc/maadix/scripts'],
    }

    #notify cpanel when reboot is required
    cron { 'cpanel reboot notify':
      command => 'test -f /var/run/reboot-required && /bin/bash -c "/etc/maadix/scripts/setrebootinforeboot.sh" > /dev/null',
      user    => 'root',
      minute  => '1',
      hour    => '*/6',
    }

    #cron to reboot on sunday. at 00.00 (1) 03:00 (2) 06:00(3) 09:00 (4)
    #/etc/maadix/scripts/check_reboot.sh 1|2|3|4
    cron { 'reboot when required / sunday / 00:00':
      command => '/bin/sleep `/usr/bin/numrandom /0..60/`m ; /bin/bash -c "/etc/maadix/scripts/check_reboot.sh 1" > /dev/null',
      user    => 'root',
      minute  => '0',
      hour    => '0',
      weekday => '0',
    }
    cron { 'reboot when required / sunday / 03:00':
      command => '/bin/sleep `/usr/bin/numrandom /0..60/`m ; /bin/bash -c "/etc/maadix/scripts/check_reboot.sh 2" > /dev/null',
      user    => 'root',
      minute  => '0',
      hour    => '3',
      weekday => '0',
    }
    cron { 'reboot when required / sunday / 06:00':
      command => '/bin/sleep `/usr/bin/numrandom /0..60/`m ; /bin/bash -c "/etc/maadix/scripts/check_reboot.sh 3" > /dev/null',
      user    => 'root',
      minute  => '0',
      hour    => '6',
      weekday => '0',
    }
    cron { 'reboot when required / sunday / 09:00':
      command => '/bin/sleep `/usr/bin/numrandom /0..60/`m ; /bin/bash -c "/etc/maadix/scripts/check_reboot.sh 4" > /dev/null',
      user    => 'root',
      minute  => '0',
      hour    => '9',
      weekday => '0',
    }

  }

}





