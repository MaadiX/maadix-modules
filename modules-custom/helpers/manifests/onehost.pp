class helpers::onehost (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    include helpers::onehost::repo
    include helpers::onehost::setup

  anchor { 'helpers::onehost::begin': } ->
   Class['::helpers::onehost::repo' ] ->
   Class['::helpers::onehost::setup' ] ->
  anchor { 'helpers::onehost::end': }

  }

}





