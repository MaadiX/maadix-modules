class helpers::coturn::external::monit(
  $enabled        = true,
  $ip             = $helpers::coturn::ipaddress,
  $port           = '3478',
) inherits helpers::coturn {

  validate_bool($enabled)

  if $enabled {

    $matching	  = "/usr/bin/turnserver"
    $binary	  = "/usr/bin/turnserver"

    $init_system = sysv

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'GENERIC',
      socket_type  => 'TCP',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'coturn':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      #when coturn hangs, need restart instead start
      program_start => '/usr/sbin/service coturn restart',
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}

