#hardening / apache modules security + evasive
class helpers::apacheevasive (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    ### evasive ###
    apache::mod { 'evasive20': 
      package => 'libapache2-mod-evasive',
      lib     => 'mod_evasive20.so'
    } ->
    #evasive log dir
    file { '/var/log/mod_evasive':
      ensure  => directory,
      mode    => '0755',
      group   => 'www-data',
      owner   => 'www-data',
      notify  => Service['httpd'],
    }->
    #evasive conf
    file { 'apache2 mod_evasive conf':
      path    => '/etc/apache2/mods-enabled/evasive.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/evasive.conf.erb'),
      notify  => Service['httpd'],
    }    
    #doc: to check if it's working, just point browser to a random resource in server and reload multiple times

    ### security ###
    #doc: https://www.linode.com/docs/guides/securing-apache2-with-modsecurity/
    package { 'modsecurity-crs':
      ensure  => 'present',
    } ->
    apache::mod { 'unique_id': 
    } -> 
    apache::mod { 'security2': 
      package => 'libapache2-mod-security2',
    } -> 
    #security conf
    file { 'apache2 mod_security2 conf':
      path    => '/etc/apache2/mods-enabled/security2.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/security2.conf.erb'),
      notify  => Service['httpd'],
    } ->
    file { '/etc/modsecurity/modsecurity.conf':
      ensure  => 'link',
      target  => '/etc/modsecurity/modsecurity.conf-recommended',
      notify  => Service['httpd'],
    } ->
    file_line { 'enable modsecurity SecRuleEngine':
      path    => '/etc/modsecurity/modsecurity.conf-recommended',
      line    => 'SecRuleEngine On',
      match   => '^SecRuleEngine.*$',
      notify  => Service['httpd'],
    }
    #doc: to check if it's working, curl https://$FQDN/index.php?exec=/bin/bash

  }

}





