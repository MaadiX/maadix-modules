class helpers::mcollective (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    #remove mcollective logrotate conf
    file { '/etc/logrotate.d/mcollective':
        ensure    => absent,
        require => Package['logrotate'],
    }

    ##stop/disable mcollective service
    service { 'mcollective':
        ensure    => stopped,
        enable    => false,
    }

  }

}





