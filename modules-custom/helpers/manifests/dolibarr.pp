class helpers::dolibarr (
  $enabled           = str2bool($::dolibarr_enabled),
  $adminuser         = undef,
  $usermail          = undef,
  $version           = '',
  $major_version     = '',
  $version_installed = undef,
  $major_version_old = '',
  $domain            = '',
  $remove            = $::dolibarr_remove,
) {

  #required classes
  require php
  require helpers::php

  validate_bool($enabled)

  if $enabled {


    ##dolibarrdb user passw file
    exec { 'dolibarrdb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/dolibarrdb'",
      creates => '/etc/maadix/dolibarrdb',
      require => Package['pwgen'],
    }

    if $adminuser != undef {
      ##dolibarr script
      file { 'dolibarr password':
        path    => '/etc/maadix/scripts/dolibarrpass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/dolibarrpass.sh.erb'),
      }
    }

    ##dolibarr pass mail script
    file { 'dolibarr mail password':
      path    => '/etc/maadix/mail/dolibarr.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/dolibarr_mail_password.sh.erb'),
    }

    #dolibarr directory
    file { '/var/www/dolibarr':
      ensure => directory,
      mode   => '0750',
      group   => 'fpmdolibarr',
      owner   => 'fpmdolibarr',
    } ->
    exec { 'dolibarr files owner':
      command      => 'chown -R fpmdolibarr:fpmdolibarr /var/www/dolibarr',
      timeout      => 28800,
      #don't log if not in debug mode
      #loglevel    => 'debug',
    }


    #dolibarr directory
    file { '/var/www/dolibarr/dolibarr/documents':
      ensure => directory,
      mode   => '0700',
      group   => 'fpmdolibarr',
      owner   => 'fpmdolibarr',
    }

    
    #dolibarr repo
    #fix repo tags. doc: https://stackoverflow.com/questions/60455045/how-to-fix-the-would-clobber-existing-tag-problem
    exec { 'fix repo tags':
      command   => "/bin/bash -c 'cd /var/www/dolibarr/dolibarr && git fetch --tags --force'",
      onlyif    => 'test -d /var/www/dolibarr/dolibarr',
      path      => '/usr/bin:/bin',
      user      => 'fpmdolibarr',
      logoutput => true,
    } ->
    vcsrepo{ '/var/www/dolibarr/dolibarr':
      ensure   => present,
      provider => git,
      source   => 'https://github.com/Dolibarr/dolibarr.git',
      revision => "$version",
      user     => 'fpmdolibarr',
      before   => [
                  File['dolibarr conf'],
                  File['/var/www/dolibarr/dolibarr/documents'],
                  ],
      notify   => [
                  Exec['set dolibarr installed version'],
                  Exec['set dolibarr installed majorversionold'],
                  ],
    }

    #dolibarr conf template
    file { 'dolibarr conf':
        path    => '/var/www/dolibarr/dolibarr/htdocs/install/install.forced.php_template',
        owner   => 'fpmdolibarr',
        group   => 'fpmdolibarr',
        mode    => '0644',
        content => template('helpers/dolibarr_install.forced.php.erb'),
    } ->
    #dolibarr conf with secrets based on template
    exec { 'dolibarr conf with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/dolibarrdb | xargs -i sed \"s/dolibarrPLACEHOLDER/{}/\" /var/www/dolibarr/dolibarr/htdocs/install/install.forced.php_template > /var/www/dolibarr/dolibarr/htdocs/install/install.forced.php && chown fpmdolibarr:fpmdolibarr /var/www/dolibarr/dolibarr/htdocs/install/install.forced.php && chmod 600 /var/www/dolibarr/dolibarr/htdocs/install/install.forced.php'",
      subscribe   => [ 
                     File['dolibarr conf'],
                     ],
      refreshonly => true,
    } ->
    #dolibarr db installation
    exec { 'dolibarr db and cli installation':
      command => "/bin/bash /etc/maadix/scripts/dolibarrpass.sh",
      creates => '/etc/maadix/status/dolibarr',
      require => [
                 File['dolibarr password'],
                 ],
      notify  => [
                 Exec['set dolibarr installed version'],
                 Exec['set dolibarr installed majorversionold'],
                 ],
    }

    #upgrade ddbb when major version changes and backup ddbb and move old backups to trash
    if ( $major_version_old != '' ) and ( $major_version != $major_version_old ) {

      $timestamp = Timestamp.new().strftime('%Y_%m_%d-%H_%M_%S')

      exec { 'dolibarr: mv old backups to trash':
        command   => 'mv /var/www/dolibarr/dolibarr-* /home/.trash/backups/',
        onlyif    => 'ls -l /var/www/dolibarr/dolibarr-*',
        path      => '/usr/bin/:/bin/',
        require   => Vcsrepo['/var/www/dolibarr/dolibarr'],
      } ->

      exec { "upgrading ddbd dolibarr: ddbb backup of $version_installed":
        command   => "mysqldump --defaults-extra-file=/root/.my.cnf dolibarr > /var/www/dolibarr/dolibarr-$version_installed-$timestamp.sql && chmod 700 /var/www/dolibarr/dolibarr-$version_installed-$timestamp.sql",
        logoutput => true,
        timeout   => 28800,
      } ->

      exec { "upgrading ddbd dolibarr: delete install.lock":
        command   => 'rm /var/www/dolibarr/dolibarr/documents/install.lock',
        onlyif    => 'test -f /var/www/dolibarr/dolibarr/documents/install.lock',
        path      => '/usr/bin/:/bin/',
      } ->

      exec { "upgrade ddbb dolibarr from $major_version_old.0 to $major_version.0":
        cwd       => '/var/www/dolibarr/dolibarr/htdocs/install',
        user      => 'fpmdolibarr',
        logoutput => true,
        timeout   => 28800,
        command   => "php upgrade.php $major_version_old.0 $major_version.0 && php upgrade2.php $major_version_old.0 $major_version.0 && php step5.php $major_version_old.0 $major_version.0",
        notify    => [
                     Exec['set dolibarr installed version'],
                     Exec['set dolibarr installed majorversionold'],
                     ],
      } ->

      file { 'dolibarr install.lock file':
        path    => '/var/www/dolibarr/dolibarr/documents/install.lock',
        owner   => 'fpmdolibarr',
        group   => 'fpmdolibarr',
        mode    => '0444',
      }


    }

    #set installed version only run when first installation or upgrade are ok
    exec { 'set dolibarr installed version':
      command     => "/etc/maadix/scripts/setldapgroupfield.sh dolibarr version $version",
      logoutput   => true,
      refreshonly => true,
    }

    #set installed majorversionold only run when first installation or upgrade are ok
    exec { 'set dolibarr installed majorversionold':
      command     => "/etc/maadix/scripts/setldapgroupfield.sh dolibarr majorversionold $major_version",
      logoutput   => true,
      refreshonly => true,
    }

    #apache vhosts and certs
    apache::vhost{"$domain" :
      servername      => $domain,
      port            => 80,
      docroot         => "/var/www/dolibarr/dolibarr/htdocs",
      docroot_mode    => '0755',
      docroot_group   => 'fpmdolibarr',
      docroot_owner   => 'fpmdolibarr',
      priority        => 90,
      override        => ['All'],
      rewrites   => [
        {
          comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
          rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
          rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
        }
      ],
      require => Vcsrepo['/var/www/dolibarr/dolibarr'],
      notify => Class['apache::service'],
    } ~>
    Exec['reload apache'] ->
    letsencrypt::certonly{$domain :
      domains   => [$domain],
      plugin    => 'webroot',
      webroot_paths => ["/var/www/dolibarr/dolibarr/htdocs"],
      require       => Package['python-ndg-httpsclient'],
    } ->
    ini_setting { "certbot webroot_map $domain":
      ensure  => present,
      path    => "/etc/letsencrypt/renewal/$domain.conf",
      section => 'webroot_map',
      setting => "$domain",
      value   => '/var/www/dolibarr/dolibarr/htdocs',
      section_prefix => '[[',
      section_suffix => ']]',
    } ->
    apache::vhost{"$domain-ssl" :
      servername               => $domain,
      port                     => 443,
      docroot                  => "/var/www/dolibarr/dolibarr/htdocs",
      docroot_group            => 'fpmdolibarr',
      docroot_owner            => 'fpmdolibarr',
      priority                 => 90,
      ssl                      => true,
      ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
      ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
      ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
      notify => Class['apache::service'],
      custom_fragment => '

## Directories, there should at least be a declaration for /var/www/html
<Directory "/var/www/dolibarr/dolibarr">

  Options +FollowSymLinks
  AllowOverride All

  #Not allow any connection using HTTP
  Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"

</Directory>

#php-fpm
ProxyTimeout 360
<FilesMatch "\.php$">
  SetHandler "proxy:unix:/run/php/fpmdolibarr-fpm.sock|fcgi://localhost"
</FilesMatch>

        ',
    }

    #clean old dolibarr domain
    if($::dolibarrdomain_old){
      helpers::cleanvhost{"$::dolibarrdomain_old":
        docroot  => '/var/www/dolibarr/dolibarr/htdocs',
      }
    }

  } else {

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/dolibarr/dolibarr',
      }

    }

  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'dolibarr':}
    helpers::remove {'dolibarr':
      files      => ['/etc/maadix/dolibarrdb','/etc/maadix/status/dolibarr','/etc/maadix/scripts/dolibarrpass.sh'],
      big_dirs   => ['/var/www/dolibarr'],
      mysql_d    => ['dolibarr'],
      notify     => [
                    Helpers::Resetldapgroup['dolibarr'],
                    Service['httpd']
                    ]
    }
  }

}

