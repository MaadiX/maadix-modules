class helpers::fqdncerts::sslvhosts(
  $fqdn 	= '',
  $port		= 443,
  #$docroot	= '/var/www/html',
  $priority	= '15',
  
){

    #delete old fqdn apache
    if defined('$::fqdn_domain_old') {
      apache::vhost{ "$::hostname.$::fqdn_domain_old-ssl":
        ensure         => absent,
        manage_docroot => false,
        docroot        => "/var/www/html/$::hostname.$::fqdn_domain_old",
        priority       => $priority,
      }
    }

    $customfragment = $::lsbdistcodename ? {
      'jessie'  => '
                   #jessie custom fragment
                   #none
                   ',
      /(stretch|buster)/ => '
                   #stretch/buster custom fragment
                   <FilesMatch "\.php$">
                     SetHandler "proxy:fcgi://127.0.0.1:9000/" 
                   </FilesMatch>
                   ## cpanel ##
                   #strip the X-Forwarded-Proto header from incoming requests
                   RequestHeader unset X-Forwarded-Proto
                   #set the header for requests using HTTPS
                   RequestHeader set X-Forwarded-Proto https env=HTTPS
                   #mxcp proxy
                   ProxyPass /cpanel http://localhost:8000/
                   ProxyPassReverse /cpanel http://localhost:8000/
                   ',
    }

    apache::vhost{ "$fqdn-ssl" :
      servername      => $fqdn,
      port            => $port,
      docroot         => "/var/www/html/$fqdn",
      priority        => $priority,
      directories     => [
                           { path    => "/var/www/html/$fqdn",
                             options => ['FollowSymLinks','MultiViews'],
                           },
                         ],
      ssl             => true,
      ssl_cert        => "/etc/letsencrypt/live/$fqdn/cert.pem",
      ssl_chain       => "/etc/letsencrypt/live/$fqdn/chain.pem",
      ssl_key         => "/etc/letsencrypt/live/$fqdn/privkey.pem",
      override	      => ['All'],
      headers 	      => [
                          'always set Strict-Transport-Security "max-age=15768000; includeSubDomains; preload"',
                         ],
      setenvif        => [
                          'Request_URI ^/etherpad/socket.* maadix',
                          'Request_URI ^/owncloud/remote.php/we.* maadix',
                          'Request_URI ^/nextcloud/remote.php/.* maadix',
                         ],
      access_log_env_var => '!maadix',
      custom_fragment => $customfragment,
    }

}

