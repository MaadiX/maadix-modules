class helpers::fqdncerts::letsencryptcerts(
  $fqdn 	= '',
  $adminmail    = undef,
){

    ##dependencies
    package { 'python-ndg-httpsclient':
      ensure => 'present',
      before => Exec['initialize letsencrypt'],
    }


    letsencrypt::certonly{$fqdn :
      domains   => [$fqdn],
      plugin    => 'webroot',
      additional_args => ['-n'],      
      webroot_paths => ["/var/www/html/$fqdn"],
      require       => Package['python-ndg-httpsclient'],
      #we don't need specific cron. with default certbot in cron.d is enought
      #manage_cron      => true,
      #before           => Class['postfix::packages'],
      #before    => Exec['update registration certbot email'],
    } ->
    ini_setting { "certbot webroot_map $fqdn":
      ensure  => present,
      path    => "/etc/letsencrypt/renewal/$fqdn.conf",
      section => 'webroot_map',
      setting => "$fqdn",
      value   => "/var/www/html/$fqdn",
      section_prefix => '[[',
      section_suffix => ']]',
    }

    #delete old fqdn certbot
    if defined('$::fqdn_domain_old') {
      file { "/etc/letsencrypt/renewal/$::hostname.$::fqdn_domain_old.conf":
        ensure   => absent,
      }
    }

    #change email from certbot registration when admin mail changes
    #this feature is commented until new certbot client with at least 0.13.0 version is installed
    #issue: https://github.com/certbot/certbot/issues/4330
    /*
    exec { 'update registration certbot email':
      command     => "certbot register --update-registration --email $adminmail",
      logoutput   => true,
      require     => [
                     Package['certbot'],
                     ],
      subscribe   => [
                     File['conf adminmail'],
                     ],
      refreshonly => true,
    }
    */


}


