class helpers::fqdncerts::certbotcron
{

  # post-hook to certbot renew
  file_line { 'certbot renew cron':
    path  => '/etc/cron.d/certbot',
    line  => '0 */12 * * * root test -x /usr/bin/certbot -a \! -d /run/systemd/system && perl -e "sleep int(rand(3600))" && certbot -q renew --post-hook "service apache2 reload && service postfix restart && service dovecot restart"',
    match => 'certbot -q renew.*$',
  }

}

