class helpers::fqdncerts::nonsslvhosts(
  $fqdn 	= '',
  $port		= 80,
  #$docroot	= "/var/www/html/$fqdn",
  $priority	= '15',
  
){

    #000-default vhost absent
    apache::vhost{ '000-default':
      ensure   => absent,
      docroot  => '/var/www/default',
      priority => false,
    }

    #delete old fqdn apache
    if defined('$::fqdn_domain_old') {
      #delete old fqdn vhost
      apache::vhost{ "$::hostname.$::fqdn_domain_old-nonssl":
        ensure         => absent,
        manage_docroot => false,
        docroot        => "/var/www/html/$::hostname.$::fqdn_domain_old",
        priority       => $priority,
      }
      #if we want to delete also vhost webroot
      #file { "/var/www/html/$::hostname.$::fqdn_domain_old":
      #  ensure  => absent,
      #  force   => true,
      #  recurse => true,
      #  purge   => true,
      #}

      #delete old monit directory and vhost
      $vhost_seed              = "${::inventory}"
      $vhost_prefix            = 'monit-test-'
      $vhost_suffix            = ".${::hostname}.${::fqdn_domain_old}"

      $rand_fragment_old       = fqdn_old_rand(1000000, $vhost_seed)
      $servername_monit_old    = "${vhost_prefix}${rand_fragment_old}${vhost_suffix}"
      $docroot_monit_old       = "/var/www/${servername_monit_old}"

      file { "${docroot_monit_old}":
        ensure  => absent,
        force   => true,
        recurse => true,
        purge   => true,
      }

      apache::vhost{ "${servername_monit_old}":
        ensure         => absent,
        manage_docroot => false,
        docroot        => "${docroot_monit_old}",
        priority       => 99,
      }

      file_line{ "${servername_monit_old}":
        match    => ".*${servername_monit_old}.*$",
        path     => '/etc/hosts',
        line     => "#removed host ${servername_monit_old}",
        multiple => true,
      }

    }

    file { '/var/www/html':
      ensure => directory,
      mode   => '0755',
      group   => 'root',
      owner   => 'root',
    }

    apache::vhost{ "$fqdn-nonssl" :
      servername      => $fqdn,
      port            => $port,
      docroot         => "/var/www/html/$fqdn",
      docroot_group   => '500',
      docroot_mode    => '2775',
      priority        => $priority,
      override        => ['All'],
      rewrites   => [
        {
          comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
          rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
          rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
        }
      ],
      notify  => [
                 Class['apache::service'],
                 Exec['reload apache'],
                 ],
      require => [
                 File['/var/www'],
                 File['/var/www/html'],
                 Class['apache'],
                 ],
    }

}

