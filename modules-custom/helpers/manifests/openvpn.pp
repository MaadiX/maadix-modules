class helpers::openvpn (
  $enabled = str2bool($::openvpn_enabled),
  $fqdn = '',
  $remove = $::openvpn_remove,
) {

  validate_bool($enabled)

  if $enabled {

    ##chroot
    #jail
    file { '/etc/openvpn/chroot':
      ensure  => directory,
      owner   => 'nobody',
      group   => 'nogroup',
      mode    => '0775',
      require => Class['openvpn::install'],
    } ->
    file { '/etc/openvpn/chroot/etc':
      ensure  => directory,
      owner   => 'nobody',
      group   => 'nogroup',
      mode    => '0775',
    } ->
    file { '/etc/openvpn/chroot/etc/openvpn':
      ensure  => directory,
      owner   => 'nobody',
      group   => 'nogroup',
      mode    => '0775',
    } ->
    file { '/etc/openvpn/chroot/tmp':
      ensure  => directory,
      owner   => 'nobody',
      group   => 'nogroup',
      mode    => '0777',
      before  => Class['openvpn::config'],
    }
    #link to conf outside jail
    file { "/etc/openvpn/chroot/etc/openvpn/$fqdn":
      ensure  => 'link',
      target  => "/etc/openvpn/$fqdn",
      require => [
                 File["/etc/openvpn/$fqdn"],
                 ],
    }

    #override options of openvpn::ca Exec["generate dh param $fqdn"] to avoid timeout error generating dh params key size 4096
    #doc: https://puppet.com/docs/puppet/5.3/lang_resources_advanced.html#adding-or-modifying-attributes
    #doc: https://serverfault.com/a/439004
    #this method is only allowed in subclass
    #Exec["generate dh param $fqdn"] {
    #  timeout  => 7200,
    #}
    Exec <| title == "generate dh param $fqdn" |> { 
      timeout  => 7200,
    }

    #openvpn pam service
    file { 'openvpn pam':
        path    => '/etc/pam.d/openvpn',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/openvpn.erb'),
    }

    #default pam plugin configured from module doesn't exist, so we make a link to the correct file
    # todo, fix this with a PR to openvpn repo / fixed in 4.1.1
    #file { '/usr/lib/openvpn/openvpn-auth-pam.so':
    #  ensure => 'link',
    #  target => '/usr/lib/openvpn/openvpn-plugin-auth-pam.so',
    #  require => Package['openvpn'],
    #}

    #we need a readable copy of ca.crt to generate the zip that cpanel sends to clients
    exec { 'readable copy of ca.crt':
      command => "/bin/bash -c 'cp /etc/openvpn/$fqdn/easy-rsa/keys/ca.crt /etc/openvpn/ && /bin/chmod 644 /etc/openvpn/ca.crt'",
      creates => '/etc/openvpn/ca.crt',
    }

    #if fqdn changes, we need to regenerate ca.crt readable copy
    #if ca params changes, regenerate ca.crt readable copy
    exec { 'regenerate copy of ca.crt':
      command => "/bin/bash -c 'cp /etc/openvpn/$fqdn/easy-rsa/keys/ca.crt /etc/openvpn/ && /bin/chmod 644 /etc/openvpn/ca.crt'",
      subscribe   => [
                     File['conf fqdn'],
                     Openvpn::Ca["$fqdn"],
                     ],
      refreshonly => true,
      notify      => Service["openvpn@$fqdn"],
    }

    #delete old fqdn openvpn conf
    if defined('$::fqdn_domain_old') {

      file { "/etc/openvpn/$::hostname.$::fqdn_domain_old":
        ensure  => absent,
        force   => true,
        recurse => true,
        purge   => true,
      }

      file { "/etc/openvpn/$::hostname.$::fqdn_domain_old.conf":
        ensure  => absent,
      }
    }

    ## fqdn change scripts
    exec { 'openvpn fqdn change':
      command     => "service openvpn stop",
      before      => [
                     File['systemd openvpn@service'],
                     ],
      logoutput   => true,
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
    }

    ##systemd openvpn@.service modified to be compatible with vmcontext
    #jessie and stretch and buster versions
    file { 'systemd openvpn@service':
        path    => '/lib/systemd/system/openvpn@.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template("helpers/openvpn@.service-$::lsbdistcodename.erb"),
        require => Package['openvpn'],
        notify  => Service["openvpn@$fqdn"],
    }

    ##systemd openvpn-iptables.service
    ### include iptables rules
    ## iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE
    ## para listar reglas nat: iptables -t nat --line-numbers -L
    ## para eliminar reglas nat: iptables -t nat -D POSTROUTING XX
    ### iptables FORWARD rules
    ##doc: https://arashmilani.com/post?id=53
    ##doc: http://dnaeon.github.io/firewall-rules-with-iptables-for-openvpn/
    ##doc: https://raw.githubusercontent.com/Nyr/openvpn-install/master/openvpn-install.sh
    ##add specific forward rules to openvpn to allow forwadding even if policy forward is set to DROP
    ##iptables -A FORWARD -i tun0 -j ACCEPT
    ##iptables -A FORWARD -i tun0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
    ##iptables -A FORWARD -i eth0 -o tun0 -m state --state RELATED,ESTABLISHED -j ACCEPT
    ##systemd openvpn@.service modified to be compatible with vmcontext
    file { 'systemd openvpn-iptables':
        path    => '/lib/systemd/system/openvpn-iptables.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template("helpers/openvpn-iptables.service.erb"),
        require => Package['openvpn'],
        notify  => Service["openvpn@$fqdn"],
    }

    ##service
    service { "openvpn@$fqdn":
        ensure   => running,
        enable   => true,
        provider => 'systemd',
        require  => [
                     File["/etc/openvpn/$fqdn.conf"],
                     File["/etc/openvpn/chroot/etc/openvpn/$fqdn"],
                    ],
    }

    #openvpn iptables servive
    service { "openvpn-iptables":
        ensure   => running,
        enable   => true,
        provider => 'systemd',
        require  => [
                     File["/etc/openvpn/$fqdn.conf"],
                     File["/etc/openvpn/chroot/etc/openvpn/$fqdn"],
                    ],
    }

  } else {

    ##openvpn service stopped
    service { 'openvpn':
    	ensure    => $enabled,
    	name      => openvpn,
    	enable    => $enabled,
    }
    service { "openvpn@${::fqdn}":
    	ensure    => $enabled,
    	name      => "openvpn@${::fqdn}",
    	enable    => $enabled,
    }
    service { 'openvpn-iptables':
    	ensure    => $enabled,
    	name      => openvpn-iptables,
    	enable    => $enabled,
    }

  }

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::openvpn::external::monit':
      enabled  => $enabled,
    }
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'openvpn':}
    helpers::remove {'openvpn':
      packages   => ['openvpn','openvpn-auth-ldap'],
      files      => ['/lib/systemd/system/openvpn-iptables.service','/lib/systemd/system/openvpn@.service','/etc/pam.d/openvpn'],
      dirs       => ['/etc/openvpn','/var/log/openvpn'],
      notify     => Helpers::Resetldapgroup['openvpn'],
    }
  }

}
