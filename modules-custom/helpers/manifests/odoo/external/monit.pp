class helpers::odoo::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 8069,
  $matching	  = '/var/www/odoo/odoo/odoo-bin',
  $binary	  = '/var/www/odoo/odoo/odoo-bin',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'odoo':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
    }
  }

}

