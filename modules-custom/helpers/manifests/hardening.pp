class helpers::hardening (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {


    #hardening perms
    file { '/etc/crontab':
      ensure => file,
      mode   => '0600',
    }
    file { '/etc/group-':
      ensure => file,
      mode   => '0644',
    }
    file { '/etc/passwd-':
      ensure => file,
      mode   => '0644',
    }
    file { '/etc/cron.d':
      ensure => directory,
      mode   => '0700',
    }
    file { '/etc/cron.daily':
      ensure => directory,
      mode   => '0700',
    }
    file { '/etc/cron.hourly':
      ensure => directory,
      mode   => '0700',
    }
    file { '/etc/cron.weekly':
      ensure => directory,
      mode   => '0700',
    }
    file { '/etc/cron.monthly':
      ensure => directory,
      mode   => '0700',
    }

    #user dir creation mode
    file_line { 'userdir mode 750':
      path     => '/etc/adduser.conf',
      line     => 'DIR_MODE=0750',
      match    => '^DIR_MODE.*$',
    }

    #ansible user, set password to *
    #user {'ansible':
    #  password => '*',
    #}

    #passwords sha rounds
    #doc: https://wiki.archlinux.org/title/SHA_password_hashes
    file_line { 'login.defs sha min rounds':
      path    => '/etc/login.defs',
      line    => 'SHA_CRYPT_MIN_ROUNDS 640000',
      match   => 'SHA_CRYPT_MIN_ROUNDS.*$',
    }
    file_line { 'login.defs sha max rounds':
      path    => '/etc/login.defs',
      line    => 'SHA_CRYPT_MAX_ROUNDS 640000',
      match   => 'SHA_CRYPT_MAX_ROUNDS.*$',
    }

    #only for vms
    if $::isvm {
      file { '/home/ansible':
        ensure => directory,
        mode   => '0750',
      }
      file { '/home/debian':
        ensure => directory,
        mode   => '0750',
      }
      file { '/home/vmail':
        ensure  => directory,
        mode    => '0750',
        require => Class['dovecot'],
      }

      if defined('::php') and defined(Class['::php']) {
        #this lines are only needed to avoid false positives in lynis
        if $::has_php70_apache {
          file_line { '/etc/php/7.0/apache2/php.ini allow_url_fopen':
            path    => '/etc/php/7.0/apache2/php.ini',
            line    => 'allow_url_fopen = Off',
            match   => '.*allow_url_fopen.*$',
            require => [
                       Class['php'],
                       Package[php-dev],
                       ]
          }
        }
        if $::has_php70_cli {
          file_line { '/etc/php/7.0/cli/php.ini expose_php':
            path    => '/etc/php/7.0/cli/php.ini',
            line    => 'expose_php = Off',
            match   => '.*expose_php.*$',
            require => [
                       Class['php'],
                       Package[php-dev],
                       ]
          }
          file_line { '/etc/php/7.0/cli/php.ini allow_url_fopen':
            path    => '/etc/php/7.0/cli/php.ini',
            line    => 'allow_url_fopen = Off',
            match   => '.*allow_url_fopen.*$',
            require => [
                       Class['php'],
                       Package[php-dev],
                       ]
          }
        }
        if $::has_php73_apache {
          file_line { '/etc/php/7.3/apache2/php.ini allow_url_fopen':
            path    => '/etc/php/7.3/apache2/php.ini',
            line    => 'allow_url_fopen = Off',
            match   => '.*allow_url_fopen.*$',
            require => [
                       Class['php'],
                       Package[php-dev],
                       ]
          }
        }
        if $::has_php73_cli {
          file_line { '/etc/php/7.3/cli/php.ini expose_php':
            path    => '/etc/php/7.3/cli/php.ini',
            line    => 'expose_php = Off',
            match   => '.*expose_php.*$',
            require => [
                       Class['php'],
                       Package[php-dev],
                       ]
          }
          file_line { '/etc/php/7.3/cli/php.ini allow_url_fopen':
            path    => '/etc/php/7.3/cli/php.ini',
            line    => 'allow_url_fopen = Off',
            match   => '.*allow_url_fopen.*$',
            require => [
                       Class['php'],
                       Package[php-dev],
                       ]
          }
        }
      }

    }
  }

}
