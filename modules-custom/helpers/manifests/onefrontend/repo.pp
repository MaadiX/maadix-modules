class helpers::onefrontend::repo{

        case $::lsbdistcodename {
          jessie: {
            $location = 'http://downloads.opennebula.org/repo/5.0/Debian/8'
          }
          stretch: {
            $location = 'http://downloads.opennebula.org/repo/5.10/Debian/9'
          }
          buster: {
            $location = 'http://downloads.opennebula.org/repo/5.10/Debian/10'
          }
          default: {
            fail("Unsupported platform: ${module_name} currently doesn't support ${::osfamily} or ${::operatingsystem}")
          }
        }

        #apt must be included when using apt::source
        include apt

        case $::osfamily {
          'Debian': {
            apt::source { 'opennebula':
              location => "$location",
              release     => 'stable',
              repos       => 'opennebula',
              require     => Exec['opennebula apt key'],
              #before   => Package['opennebula-node'],
            }
            #a bug in apt:key prevent adding keys this way
            #doc: https://github.com/puppetlabs/puppetlabs-apt/pull/698/commits
            #apt::key { 'opennebula':
            #    key     => "85E16EBF",
            #    source => "http://downloads.opennebula.org/repo/Debian/repo.key",
            #}
            #using exec as workaround with --no-tty to avoid gpg: cannot open '/dev/tty': No such device or address
            exec {'opennebula apt key':
              #command   => '/usr/bin/apt-key adv --no-tty --keyserver keyserver.ubuntu.com --recv-keys 6CCEA47B2281732DF5D504D00C54D189F4BA284D',
              command   => '/usr/bin/wget -q -O- https://downloads.opennebula.org/repo/repo.key | /usr/bin/apt-key add -',
              unless    => 'apt-key list | grep "92B7 7188 854C F23E 1634  DA89 592F 7F05 85E1 6EBF"',
              logoutput => true,
            }
          }
          default : {
            fail("Unsupported platform: ${module_name} currently doesn't support ${::osfamily} or ${::operatingsystem}")
          }
        }
}
