class helpers::onefrontend::setup (
) {

  ##password directory
  file { '/etc/maadix':
    ensure => directory,
    mode   => '0700',
  }

  ##password status directory
  file { '/etc/maadix/status':
    ensure => directory,
    mode   => '0700',
  }

  ##password scripts directory
  file { '/etc/maadix/scripts':
    ensure => directory,
    mode   => '0700',
  }

  ##log directory
  file { '/etc/maadix/logs':
    ensure => directory,
    mode   => '0700',
  }

  ##conf directory
  file { '/etc/maadix/conf':
    ensure => directory,
    mode   => '0700',
  }

  ##pwgen package
  package { 'pwgen':
    ensure => 'present',
  }

  #install package after apt_update
  Exec[apt_update] -> Package['opennebula']

  ## opennebula packages
  package { 'opennebula':
    ensure => 'present',
  } ->
  package { 'opennebula-sunstone':
    ensure => 'present',
  } ->
  package { 'opennebula-gate':
    ensure => 'present',
  } ->
  package { 'opennebula-flow':
    ensure => 'present',
  }

  ##one mysql passw file
  exec { 'onemysql passw':
    command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/onemysql'",
    creates => '/etc/maadix/onemysql',
    require => Package['pwgen'],
  }

  ##onemysql script
  file { 'onemysql password':
    path    => '/etc/maadix/scripts/onemysqlpass.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('helpers/onemysqlpass.sh.erb'),
  }

  #one db conf
  file_line { 'disable sqlite in oned.conf':
    path    => '/etc/one/oned.conf',
    ensure  => absent,
    line    => 'DB = [ BACKEND = "sqlite" ]',
    require => Package['opennebula'],
  } ->
  exec { 'setup one mysql':
    command => "/bin/bash /etc/maadix/scripts/onemysqlpass.sh",
    creates => '/etc/maadix/status/onemysql',
    require => [
               File['onemysql password'],
               Exec['onemysql passw'],
               Package['mysql_client'],
               Package['mysql-server'],
               ],
  }

  ##opennebula sudoers using sudo::conf
  sudo::conf { 'opennebula':
      priority => 20,
      content  => template('helpers/opennebula_sudoers.erb'),
      require => Package['opennebula'],
  }
  #remove opennebula sudoers file privided from package to avoid duplication
  file { 'remove opennebula sudo from package':
      path	=> '/etc/sudoers.d/opennebula',
      ensure	=> absent,
      require => Package['opennebula'],
  }

  ##services
  service { 'opennebula':
    ensure    => running,
    name      => opennebula,
    enable    => true,
    hasstatus => true,
    require   => Package['opennebula'],
    notify =>  [
      Exec['opennebula-sunstone restart'],
    ],
  }
  #restart/start opennebula-sunstone a while after opennebula service so opennebula creates all the files needed
  exec { 'opennebula-sunstone restart':
    command => "/bin/bash -c 'sleep 30 && service opennebula-sunstone restart'",
    require   => Service['opennebula'],
    refreshonly => true,
  }

}
