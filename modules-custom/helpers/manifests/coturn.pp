class helpers::coturn (
  $enabled              = str2bool($::coturn_enabled),
  $coturn_passwd_change = str2bool($::coturn_passwd_change),
  $fqdn                 = '',
  $ipaddress            = '',
  $remove               = $::coturn_remove,
) {


  validate_bool($enabled)

  if $enabled {

    #coturn dependencies
    if ! defined(Package['coturn']) {
      ensure_resource('package','coturn', {
      ensure  => present,
      })
    }

    #/etc/default/coturn
    file_line { 'enable coturn':
      path  => '/etc/default/coturn',
      line  => 'TURNSERVER_ENABLED=1',
      match => 'TURNSERVER_ENABLED.*$',
      require => Package['coturn'],
    }

    #coturn base conf stretch | buster
    file { 'turnserver conf template':
      path    => '/etc/turnserver_template.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("helpers/turnserver.conf-$::lsbdistcodename.erb"),
      require => Package['coturn'],
    }

    #coturn config with secrets
    exec { 'turnsever conf with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/coturn | xargs -i sed \"s/turnserverpassPLACEHOLDER/{}/\" /etc/turnserver_template.conf > /etc/turnserver.conf'",
      require     => [
                     File['turnserver conf template'],
                     ],
      subscribe   => [
                     File['turnserver conf template'],
                     ],
      refreshonly => true,
      notify      => Service['coturn'],
    } ->
    #set permissions to conf file, db and logs
    file { 'turnserver conf':
      path    => '/etc/turnserver.conf',
      owner   => 'turnserver',
      group   => 'turnserver',
      mode    => '0640',
      notify  => Service['coturn'],
    } ->
    file { '/var/lib/turn':
      ensure  => 'directory',
      owner   => 'turnserver',
      group   => 'turnserver',
      mode    => '0640',
      notify  => Service['coturn'],
    } ->
    file { '/var/lib/turn/turndb':
      owner   => 'turnserver',
      group   => 'turnserver',
      mode    => '0640',
      notify  => Service['coturn'],
    } ->
    file { '/var/log/turnserver':
      ensure  => 'directory',
      owner   => 'turnserver',
      group   => 'turnserver',
      mode    => '0640',
      notify  => Service['coturn'],
    }

    if ($coturn_passwd_change) or ($::fqdn_domain_old){

      if ($coturn_passwd_change){
        #grab coturn passwd from ldap
        exec { 'grab coturn passwd from ldap':
          command     => "/bin/bash -c 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b \"ou=password,ou=coturn,ou=groups,dc=example,dc=tld\" | grep status: | sed \"s|.*: \(.*\)|\1|\" > /etc/maadix/coturn'",
          onlyif      => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=password,ou=coturn,ou=groups,dc=example,dc=tld" | grep status',
          notify      => Exec['turnsever conf with secrets'],          
        }
      }

    }

    if $::lsbdistcodename=='buster'{
      #in buster the systemd service is set to use User turnserver. it must be root and then drop privileges to turnserver in order to read certs.
      ini_setting {'coturn systemd user':
        ensure            => present,
        section           => 'Service',
        key_val_separator => '=',
        path              => '/lib/systemd/system/coturn.service',
        setting           => 'User',
        value             => 'root',
        notify            => Service['coturn']
      }
    }

    service { 'coturn':
      ensure  => 'running',
      enable  => true,
      require => [                 
                 Package['coturn'],
                 Class['helpers::fqdncerts::letsencryptcerts']
                 ]
    }


  } else {


    #/etc/default/coturn
    exec { 'disable coturn':
      command  => "echo 'TURNSERVER_ENABLED=0' > /etc/default/coturn",
      onlyif   => "test -f /etc/default/coturn"
    }


    ##coturn service stopped
    service { 'coturn':
    	ensure    => $enabled,
    	name      => coturn,
    	enable    => $enabled,
    }


  } 

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::coturn::external::monit': enabled => $enabled}
  }


  # Uninstall
  if $remove {
    helpers::resetldapgroup{'coturn':}
    helpers::remove {'coturn':
      packages   => ['coturn'],
      files      => ['/etc/default/coturn','/etc/turnserver_template.conf','/etc/maadix/coturn'],
      dirs       => ['/var/lib/turn','/var/log/turnserver'],
      notify     => Helpers::Resetldapgroup['coturn'],
    }
  }

}

