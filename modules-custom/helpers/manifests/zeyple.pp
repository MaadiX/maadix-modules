class helpers::zeyple (
  $enabled              = $::zeyple_enabled,
  $logmail_crypt        = $::logmail_crypt,
  $version              = udef,
  $ldap_host            = 'example,tld',
  $remove               = $::zeyple_remove,
  $srs_disabled         = true,
) {

  validate_bool($enabled)

  if $enabled and $srs_disabled {

    #zeyple user
    user { 'zeyple':
      ensure          => 'present',
      managehome      => false,
      shell           => '/bin/false',
      system          => true,
    }

    #zeyple keys dir
    file { '/var/lib/zeyple':
      ensure  => directory,
      owner   => 'zeyple',
      group   => 'root',
      mode    => '0700',
    }
    file { '/var/lib/zeyple/keys':
      ensure  => directory,
      owner   => 'zeyple',
      group   => 'root',
      mode    => '0700',
    }

    #zeyple conf
    file { '/etc/zeyple.conf':
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/zeyple.conf'),
    }

    ##zeyple repo
    vcsrepo { '/var/lib/zeyple/zeyple':
        ensure    => latest,
        provider  => git,
        source    => 'https://github.com/infertux/zeyple',
        revision  => $version,
        path      => '/var/lib/zeyple/zeyple',
        user      => 'zeyple',
    } ~>
    exec { 'zeyple bin and logs':
      command     => 'cp zeyple/zeyple.py /usr/local/bin && chmod 744 /usr/local/bin/zeyple.py && chown zeyple: /usr/local/bin/zeyple.py && touch /var/log/zeyple.log && chown zeyple: /var/log/zeyple.log',
      cwd         => '/var/lib/zeyple/zeyple',
      refreshonly => true,
    }

    if $logmail_crypt != '' {
      #zeypel doesn't use /etc/aliases as final recipents of local mail, it uses recipient_canonical_maps instead. it supports single destination map only
      #recipient_canonical stores the final recipient of root and logcheck mails only (system logs)
      postfix::hash { "/etc/postfix/recipient_canonical":
        ensure => present,
      }
      postfix::config { "recipient_canonical_maps":
        value => "hash:/etc/postfix/recipient_canonical"
      }
      postfix::canonical {
        'root':
          file        => "/etc/postfix/recipient_canonical",
          ensure      => present,
          destination => "$logmail_crypt";
        'logcheck':
          file        => "/etc/postfix/recipient_canonical",
          ensure      => present,
          destination => "$logmail_crypt";
      }
    } else {
      postfix::hash { "/etc/postfix/recipient_canonical":
        ensure => absent,
      }
      postfix::config { "recipient_canonical_maps":
        ensure => absent,
        value => "hash:/etc/postfix/recipient_canonical"
      }
    }


    #gpg dirmngr conf template
    file { 'zeyple dirmngr conf':
        path    => '/var/lib/zeyple/keys/dirmngr.conf_template',
        owner   => 'zeyple',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/zeyple_dirmngr.conf'),
    }
     

    #gpg dirmngr conf with secrets based on template
    exec { 'zeyple dirmngr conf conf with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/zeyple | xargs -i sed \"s/zeyplePLACEHOLDER/{}/\" /var/lib/zeyple/keys/dirmngr.conf_template > /var/lib/zeyple/keys/dirmngr.conf && chown zeyple /var/lib/zeyple/keys/dirmngr.conf && chmod 600 /var/lib/zeyple/keys/dirmngr.conf'",
      require     => [
                     File['zeyple dirmngr conf'],
                     ],
      subscribe   => [ 
                     File['zeyple dirmngr conf'], 
                     ],
      refreshonly => true,
    } ~>
    #refresh dirmngr process
    exec { 'kill running dirmngr process':
      command     => 'pkill dirmngr',
      onlyif      => "ps aux | grep -F 'dirmngr' | grep -v -F 'grep'",
      refreshonly => true,
    }

    #logrotate
    file { 'zeyple logrotate':
        path    => '/etc/logrotate.d/zeyple',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/zeyple_logrotate'),
    }

  } else {

    if $srs_disabled {
      postfix::config { "recipient_canonical_maps":
        value   => "hash:/etc/postfix/recipient_canonical",
        ensure  => absent
      }
    }
    postfix::hash { "/etc/postfix/recipient_canonical":
      ensure  => absent,
    }
    exec { 'kill running dirmngr process of zeyple user':
      command     => 'pkill dirmngr',
      onlyif      => "ps aux | grep -F 'dirmngr' | grep -v -F 'grep' | grep zeyple",
    }
    exec { 'kill running gpg-agent process of zeyple user':
      command     => 'pkill gpg-agent',
      onlyif      => "ps aux | grep -F 'gpg-agent' | grep -v -F 'grep' | grep zeyple",
    }

    #logrotate
    file { 'zeyple logrotate':
        path    => '/etc/logrotate.d/zeyple',
        ensure  => absent,
    }

  } 

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'zeyple':}
    helpers::remove {'zeyple':
      files      => ['/etc/zeyple.conf','/usr/local/bin/zeyple.py','/var/log/zeyple.log'],
      dirs       => ['/var/lib/zeyple/keys','/var/lib/zeyple'],
      users      => ['zeyple'],
      notify     => [
                    Helpers::Resetldapgroup['zeyple'],
                    ]
    }
  }

}
