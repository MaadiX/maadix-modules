class helpers::netprotocols (
  $enabled              = true,
) {


  validate_bool($enabled)

  if $enabled {

    #hardening / disable net protocols
    file { 'hardening / disable net protocols':
      path    => '/etc/modprobe.d/netprotocols.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/netprotocols.conf.erb'),
    }

  }

}

