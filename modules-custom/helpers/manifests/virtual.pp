class helpers::virtual {

  if $::virtual=='gce'{
    #not compatible with google-compute-engine package
    package { 'irqbalance':
      ensure => absent,
    }

    #needed by google-compute-engine
    file { '/etc/sudoers.d/google_sudoers':
      content => '%google-sudoers ALL=(ALL:ALL) NOPASSWD:ALL',
      mode    => '0440',
    }
  }

}

