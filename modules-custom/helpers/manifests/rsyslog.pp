class helpers::rsyslog (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    #rsyslog maadix conf
    file { '01-maadix-rsyslog.conf':
        path    => '/etc/rsyslog.d/01-maadix-rsyslog.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/01-maadix-rsyslog.conf.erb'),
        require => Package['rsyslog'],
        notify  => Service['rsyslog'],
    }

  }

}





