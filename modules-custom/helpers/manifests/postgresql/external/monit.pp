class helpers::postgresql::external::monit(
  Boolean $enabled        = true,
  String $action          = 'restart',
  String $systemd_file    = '/lib/systemd/system/postgresql.service',
  Hash $conn_tolerance    = { cycles => 1 },
) {

  if $enabled {
    $init_system       = systemd
    $binary            = '/usr/bin/pg_ctlcluster'
    $postgresqlversion = $::lsbdistcodename ? {
       'buster'   => '11',
       'bullseye' => '13',
    }

    $connection_test = {
      type             => 'connection',
      unixsocket       => '/var/run/postgresql/.s.PGSQL.5432',
      protocol         => 'pgsql',
      action           => $action,
      tolerance        => $conn_tolerance,
    }

    monit::check::service { "postgresql":
      init_system      => $init_system,
      pidfile          => "/var/run/postgresql/$postgresqlversion-main.pid",
      binary           => $binary,
      systemd_file     => $systemd_file,
      tests            => [$connection_test, ],
      program_start    => "/usr/sbin/service postgresql@$postgresqlversion-main start",
      program_stop     => "/usr/sbin/service postgresql@$postgresqlversion-main stop",
    }
  }

}

