class helpers::security {


  ##apache headers
  file { 'security apache headers':
    path    => '/etc/apache2/conf.d/020security.conf',
    content => template('helpers/apache-security.conf.erb'),
    require => Class['Apache'],
    notify  => Service['httpd'],
  }

  #remove security2.conf file provided by package
  #enabled in next release
  /*
  file {'security2.conf':
    path    => '/etc/apache2/mods-enabled/security2.conf',
    ensure  => absent,
    require =>[
              Package['modsecurity-crs'],
              ],
    before  =>[
              Exec['reload apache'],
              ]
  }
  */

}

