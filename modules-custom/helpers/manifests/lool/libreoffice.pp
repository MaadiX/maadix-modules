class helpers::lool::libreoffice (
  $domain            = '',
  $wopi              = '',
  $version           = '',
) {


    #apt must be included when using apt::source
    include apt

    #script to create selfsigned certs
    #jessie|stretch|buster
    file { 'lool selfsigned certs script':
      path    => '/etc/maadix/scripts/lool-selfsigned-certs.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/lool-selfsigned-certs-$::lsbdistcodename.sh.erb"),
    }
    

    #create selfsigned certs
    exec { 'create lool selfsigned certs':
      command => "/bin/bash /etc/maadix/scripts/lool-selfsigned-certs.sh",
      creates => '/etc/maadix/status/lool-selfsigned-certs',
      logoutput   => true,
      require => [
                  File['lool selfsigned certs script'],
                 ],
      before  => Package['libreoffice-online'],
    }

    #lool signed repo
    #jessie|stretch
    if $::lsbdistcodename=='jessie'{
      $repos = 'repos'
    }
    if $::lsbdistcodename=='stretch'{
      $repos = 'stretch'
    }
    if $::lsbdistcodename=='buster'{
      $repos = 'buster'
    }

    apt::key { 'lool':
      key      => "E92A336C6A856880",
      source   => "https://assets.maadix.net/$repos/apt/debian/Release.key",
    }

    apt::source { 'lool':
      location => "https://assets.maadix.net/$repos/apt/debian",
      release  => "$::lsbdistcodename",
      repos    => 'main',
      require  => Apt::Key['lool'],
      before   => Package['libreoffice-online'],
    } ->
    exec { 'apt get update lool':
      command => 'apt-get -o Acquire::Check-Valid-Until=false update',
      unless  => 'apt-show-versions | grep libreoffice-online',
    } ->
    #lool package with specific version of maadix repo
    package {'libreoffice-online':
      ensure  => "$version",
    } ->
    #ensure key is owned by lool
    file { '/etc/loolwsd/key.pem':
      owner   => 'lool',
      mode    => '0600',
    }

    #lool conf
    file { 'loolwsd.conf':
        path    => '/etc/libreoffice-online/loolwsd.xml',
        owner   => 'lool',
        group   => 'lool',
        mode    => '0640',
        content => template('helpers/loolwsd.xml.erb'),
        require => Package['libreoffice-online'],
        notify  => Service['libreoffice-online'],
    }

    #service libreoffice-online
    service { 'libreoffice-online':
      ensure  => 'running',
      enable  => true,
      require => [
                 Package['libreoffice-online'],
                 ]
    }


    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/lool",
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/lool"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/lool",
        directories              => [
                                      { path    => "/var/www/lool",
                                        options => ['FollowSymLinks','MultiViews'],
                                      },
                                    ],
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/fullchain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        override                 => ['All'],
        custom_fragment          => '
          <IfModule mod_proxy.c>
          ##lool
          SSLProtocol             all -SSLv2 -SSLv3
          SSLCipherSuite ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS
          SSLHonorCipherOrder     on

          # Encoded slashes need to be allowed
          AllowEncodedSlashes NoDecode

          # Container uses a unique non-signed certificate
          SSLProxyEngine On
          SSLProxyVerify None
          SSLProxyCheckPeerCN Off
          SSLProxyCheckPeerName Off

          # keep the host
          ProxyPreserveHost On

          # static html, js, images, etc. served from loolwsd
          # loleaflet is the client part of LibreOffice Online
          ProxyPass           /loleaflet https://127.0.0.1:9980/loleaflet retry=0
          ProxyPassReverse    /loleaflet https://127.0.0.1:9980/loleaflet

          # WOPI discovery URL
          ProxyPass           /hosting/discovery https://127.0.0.1:9980/hosting/discovery retry=0
          ProxyPassReverse    /hosting/discovery https://127.0.0.1:9980/hosting/discovery

          # Main websocket
          ProxyPassMatch "/lool/(.*)/ws$" wss://127.0.0.1:9980/lool/$1/ws nocanon

          # Admin Console websocket
          ProxyPass   /lool/adminws wss://127.0.0.1:9980/lool/adminws

          # Download as, Fullscreen presentation and Image upload operations
          ProxyPass           /lool https://127.0.0.1:9980/lool
          ProxyPassReverse    /lool https://127.0.0.1:9980/lool
          </IfModule>
        ',
        notify => Class['apache::service'],
      }
    }

}

