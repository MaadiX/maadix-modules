class helpers::lool::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 9980,
  $matching	  = '/usr/bin/coolwsd',
  $binary	  = '/usr/bin/coolwsd',
  $service        = '',
  $protocol       = '',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => $protocol,
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { $service:
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}

