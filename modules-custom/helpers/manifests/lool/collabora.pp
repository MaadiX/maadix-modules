class helpers::lool::collabora (
  $domain            = '',
  $wopi              = '',
  $memproportion     = '',
) {

    #package for spelling checker,
    package { 'hunspell':
      ensure => 'present',
      notify => Service['coolwsd'],
    }
    package { 'hunspell-es':
      ensure => 'present',
      notify => Service['coolwsd'],
    }
    package { 'hunspell-de-de-frami':
      ensure => 'present',
      notify => Service['coolwsd'],
    }
    package { 'hunspell-fr':
      ensure => 'present',
      notify => Service['coolwsd'],
    }
    package { 'hunspell-en-gb':
      ensure => 'present',
      notify => Service['coolwsd'],
    }
    package { 'hunspell-ca':
      ensure => 'present',
      notify => Service['coolwsd'],
    }

    /*
    #create coolwsd dir
    file { '/etc/coolwsd':
      ensure => 'directory',
    }->
    #this fix a bug in coolwsd package
    exec { 'ensure coolwsd.xml is present before installing coolwsd':
      command => 'touch /etc/coolwsd/coolwsd.xml',
      unless  => 'test -e /etc/coolwsd/coolwsd.xml',
    }
    */
    #apt must be included when using apt::source
    include apt

    #script to create selfsigned certs
    #jessie|stretch|buster
    file { 'cool selfsigned certs script':
      path    => '/etc/maadix/scripts/cool-selfsigned-certs.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/cool-selfsigned-certs-$::lsbdistcodename.sh.erb"),
    }

    #create selfsigned certs
    exec { 'create cool selfsigned certs':
      command => "/bin/bash /etc/maadix/scripts/cool-selfsigned-certs.sh",
      creates => '/etc/maadix/status/cool-selfsigned-certs',
      logoutput   => true,
      require => [
                  File['cool selfsigned certs script'],
                 ],
      before  => Package['coolwsd'],
      notify  => Service['httpd'],
    }

    #purge libreoffice-online if present
    if ($::libreoffice_online){
      file { 'libreoffice-online removal script':
        path      => '/etc/maadix/scripts/delete-libreoffice-online.sh',
        owner     => 'root',
        group     => 'root',
        mode      => '0700',
        content   => template('helpers/delete-libreoffice-online.sh.erb'),
      } ->
      exec { 'remove libreoffice-online packages':
        command   => '/bin/bash /etc/maadix/scripts/delete-libreoffice-online.sh',
        logoutput => true,
        before    => Package['coolwsd'],
      }
    }

    #code signed repo
    #a bug in apt:key prevent adding keys this way
    #doc: https://github.com/puppetlabs/puppetlabs-apt/pull/698/commits
    #apt::key { 'code':
    #  key      => '6CCEA47B2281732DF5D504D00C54D189F4BA284D',
    #  server   => 'keyserver.ubuntu.com',
    #} ->
    #using exec as workaround with --no-tty to avoid gpg: cannot open '/dev/tty': No such device or address
    #protocol, used for monit
    $code_release = $::lsbdistcodename ? {
      'stretch' => 'CODE-debian9',
      'buster'  => 'CODE-debian10',
    }
    exec {'collabora apt key':
      command   => '/usr/bin/apt-key adv --no-tty --keyserver keyserver.ubuntu.com --recv-keys 6CCEA47B2281732DF5D504D00C54D189F4BA284D',
      unless    => 'apt-key list | grep "6CCE A47B 2281 732D F5D5  04D0 0C54 D189 F4BA 284D"',
      logoutput => true,
    } ->
    apt::source { 'lool':
      location => "https://www.collaboraoffice.com/repos/CollaboraOnline/$code_release",
      release  => '',
      repos    => './',
      before   => [
                   Package['coolwsd'],
                   Package['code-brand'],
                  ],
    } ->
    exec { 'apt get update code':
      command => 'apt-get update',
      unless  => 'apt-show-versions | grep coolwsd',
    } ->
    #code package with specific version of maadix repo
    package {'code-brand':
      ensure  => 'latest',
    } ->
    package {'coolwsd':
      ensure  => 'latest',
    } ->
    package {'collaboraoffice':
      ensure  => 'latest',
      notify  => Service['coolwsd'],
    }

    #tile cache directory perms: /var/cache/coolwsd
    file { '/var/cache/coolwsd':
      ensure  => directory,
      mode    => '0750',
      require => Package['coolwsd'],
      notify  => Service['coolwsd'],
    }

    #coolwsd conf
    file { 'coolwsd.conf':
        path    => '/etc/coolwsd/coolwsd.xml',
        owner   => 'cool',
        group   => 'cool',
        mode    => '0640',
        content => template('helpers/coolwsd-code.xml.erb'),
        require => Package['coolwsd'],
        notify  => Service['coolwsd'],
    }

    #/etc/coolwsd/key.pem permissions
    file { '/etc/coolwsd/key.pem':
        owner   => 'cool',
        group   => 'cool',
        mode    => '0600',
        require => Package['coolwsd'],
        notify  => Service['coolwsd'],
    }

    #service coolwsd
    service { 'coolwsd':
      ensure  => 'running',
      enable  => true,
      require => [
                 Package['coolwsd'],
                 ]
    }

    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/lool",
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/lool"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/lool',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/lool",
        directories              => [
                                      { path    => "/var/www/lool",
                                        options => ['FollowSymLinks','MultiViews'],
                                      },
                                    ],
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        override                 => ['All'],
        custom_fragment          => '
          <IfModule mod_proxy.c>
          ##lool
          SSLProtocol             all -SSLv2 -SSLv3
          SSLCipherSuite ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS
          SSLHonorCipherOrder     on

          # Encoded slashes need to be allowed
          AllowEncodedSlashes NoDecode

          # Container uses a unique non-signed certificate
          SSLProxyEngine On
          SSLProxyVerify require
          SSLProxyCACertificateFile /etc/coolwsd/ca-chain.cert.pem
          SSLProxyCheckPeerCN Off
          SSLProxyCheckPeerName Off

          # keep the host
          ProxyPreserveHost On

          # static html, js, images, etc. served from coolwsd
          # loleaflet is the client part of LibreOffice Online
          ProxyPass           /browser https://localhost:9980/browser retry=0
          ProxyPassReverse    /browser https://localhost:9980/browser

          # WOPI discovery URL
          ProxyPass           /hosting/discovery https://localhost:9980/hosting/discovery retry=0
          ProxyPassReverse    /hosting/discovery https://localhost:9980/hosting/discovery

          # capabilities discovery
          ProxyPass           /hosting/capabilities https://localhost:9980/hosting/capabilities retry=0
          ProxyPassReverse    /hosting/capabilities https://localhost:9980/hosting/capabilities

          # Main websocket
          ProxyPassMatch "/cool/(.*)/ws$" wss://localhost:9980/cool/$1/ws nocanon

          # Admin Console websocket
          ProxyPass   /cool/adminws wss://localhost:9980/cool/adminws

          # Download as, Fullscreen presentation and Image upload operations
          ProxyPass           /cool https://localhost:9980/cool
          ProxyPassReverse    /cool https://localhost:9980/cool
          </IfModule>
        ',
        notify => Class['apache::service'],
      }
    }


}

