class helpers::wp {

  archive { 'wp-cli source':
    ensure => present,
    source => 'https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar',
    path   => '/usr/local/bin/wp',
  } ->
  file {'/usr/local/bin/wp':
    ensure => present,
    mode   => '0755',
  }
  
}

