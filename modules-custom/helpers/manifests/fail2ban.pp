class helpers::fail2ban {


    #wordpress xmlrpc
    file { 'fail2ban filter xmlrpc':
        path    => '/etc/fail2ban/filter.d/xmlrpc.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/fail2ban_xmlrpc.conf'),
        notify  => Service['fail2ban'],
        require => Package['fail2ban'],
    } ->
    file { 'fail2ban jail xmlrpc':
        path    => '/etc/fail2ban/jail.d/xmlrpc.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/fail2ban_xmlrpc_jail.conf'),
        notify  => Service['fail2ban'],
    }

}


