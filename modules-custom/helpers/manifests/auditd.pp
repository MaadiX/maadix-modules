#hardening / auditd
class helpers::auditd (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #package
    package { 'auditd':
      ensure => 'present',
    }

    #rules folder
    file { '/etc/audit/rules.d':
      ensure => directory,
      require => Package['auditd'],
    }
    #conf
    #doc: https://github.com/Neo23x0/auditd
    file { '/etc/audit/rules.d/audit.rules':
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/audit.rules.erb'),
        notify  => Service['auditd'],
        require => File['/etc/audit/rules.d']
    }

    #service
    service { 'auditd':
      ensure  => 'running',
      enable  => true,
      require => [
                 File['/etc/audit/rules.d/audit.rules'],
                 ]
    }


  }

}

