class helpers::kernel (
  $enabled = true,
  $extlinux = str2bool("$::extlinux"),
  $version = '',
  $kernelrelease = '',
) {

  validate_bool($enabled)
  validate_bool($extlinux)

  #if current kernel is old
  if ($enabled) and ($version!='') and ($version!=$kernelrelease){

    ##install defined version kernel, update extlinux if available and lock module puppet local reboot

    #if extlinux is the bootloader, update it
    if ($extlinux) {
      #update extlinux.conf
      file_line { 'extlinux kernel conf':
        path    => '/boot/extlinux/extlinux.conf',
        line    => "kernel /boot/vmlinuz-${version}",
        match   => '.*kernel.*$',
      }->
      file_line { 'extlinux append conf':
        path    => '/boot/extlinux/extlinux.conf',
        line    => "append initrd=/boot/initrd.img-${version} root=/dev/vda1 console=tty0 console=ttyS0,115200 ro quiet",
        match   => '.*append.*$',
      }->
      #update extlinux
      exec { 'update extlinux':
        command => "/bin/bash -c 'extlinux --update /boot/extlinux'",
      }
    }
    #update kernel
    if $::lsbdistcodename=='jessie'{
      exec { 'update kernel':
        command => "/bin/bash -c 'apt-get update -o Acquire::Check-Valid-Until=false && apt-get install -y linux-image-amd64 linux-image-${version}'",
        creates => "/boot/initrd.img-${version}",
        timeout => 7200,
      }
    }
    #we don't want to install kernels in stretch/buster manually anymore, we use unattended-upgrades
    #so old automatically installed kernels can be removed afterwards by unattended-upgrades through 'Remove-Unused-Dependencies "true"' option
    #else {
    #  exec { 'update kernel':
    #    command => "/bin/bash -c 'apt-get update && apt-get install -y linux-image-amd64 linux-image-${version}'",
    #    creates => "/boot/initrd.img-${version}",
    #    timeout   => 7200,
    #  }
    #}

    #info to reboot when kernel is updated
    #we don't need to trigger this script anymore, because check_reboot.sh manage it (see reboot manifest)
    #exec { 'info to reboot':
    #  command     => "/bin/bash -c '/etc/maadix/scripts/setrebootinforeboot.sh'",
    #}
    
  }

}
