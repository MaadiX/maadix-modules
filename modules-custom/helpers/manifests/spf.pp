class helpers::spf(
  $enabled          = true,
  $helo_reject      = 'Softfail',
  $mail_from_reject = 'Softfail',
) {

  validate_bool($enabled)

  if $enabled {

    #packages
    if ! defined(Package['postfix-policyd-spf-python']) {
      ensure_resource('package','postfix-policyd-spf-python', {
      ensure  => present,
      require => Package['postfix'],
      })
    }

    #spf
    file{ 'policyd-spf.conf':
      path    => '/etc/postfix-policyd-spf-python/policyd-spf.conf',
      owner   => 'postfix',
      group   => 'postfix',
      mode    => '0644',
      content => template('helpers/policyd-spf.conf.erb'),
      require => Package['postfix-policyd-spf-python'],
    }
  }

}
