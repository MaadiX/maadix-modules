class helpers::apachepam (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    apache::mod { 'authnz_pam':
      package => 'libapache2-mod-authnz-pam',
    }
    

    #apache pam service
    file { 'apache pam':
        path    => '/etc/pam.d/apache',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/apache.erb'),
    }


  }

}





