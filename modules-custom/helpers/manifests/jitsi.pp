class helpers::jitsi (
  Boolean $enabled  = str2bool($::jitsi_enabled),
  Boolean $constrainframerate = false,
  String $domain    = '',
  Boolean $logging  = true,
  String $ipaddress = '',
  Boolean $auth  = true,
  String $log_format = 'combined',
  String $ldap_hostname = 'example.tld:389',
  Boolean $remove = $::jitsi_remove,
) {

  validate_bool($enabled)

  if $enabled {

    #debconf script
    file { 'jitsi deconf script':
        path    => '/etc/maadix/scripts/jitsi-debconf.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '700',
        content => template("helpers/jitsi-debconf.sh.erb"),
    }->
    #configure jitsi debconf only if it's not installed
    exec { 'configure jitsi debconf':
        command => '/etc/maadix/scripts/jitsi-debconf.sh',
        unless  => 'dpkg -l | grep jitsi',
        require => File['jitsi deconf script'],
        before  => Package['jitsi-meet'],
    } 

    ##add authorizedservice: jitsi if not present
    file { 'authorizedservice jitsi script':
      path    => '/etc/maadix/scripts/jitsi_authorizedservices.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/jitsi_authorizedservices.sh.erb'),
    } ->
    exec { 'add authorizedservice jitsi':
      command => "/bin/bash /etc/maadix/scripts/jitsi_authorizedservices.sh",
      creates => '/etc/maadix/status/jitsi_authorizedservices',
    }

    #buster, purge and install new key
    if($::lsbdistcodename=='buster') and ($::jitsi_old_key){

      exec {'jitsi delete old key':
        command   => '/usr/bin/apt-key del 2DC1389C',
        logoutput => true,
      } ->
      exec {'jitsi apt new key':
        command   => '/usr/bin/wget -q -O- https://download.jitsi.org/jitsi-key.gpg.key | /usr/bin/apt-key add -',
        logoutput => true,
      } ->
      exec {'jitsi apt update':
        command   => '/usr/bin/apt update',
        logoutput => true,
      }

    }

    #reinstall if domain changes or if key has changed
    if($::jitsidomain_old) or ($::jitsi_old_key){
      exec { 'purge jitsi':
        command => '/usr/bin/apt remove --purge -y jicofo jitsi-meet jitsi-meet-prosody jitsi-meet-web jitsi-meet-web-config jitsi-videobridge2 prosody prosody-modules lua-ldap',
        require => File['jitsi deconf script'],
      }->
      exec { 'purge jitsi passwords':
        command => 'rm /etc/maadix/jicofo && rm /etc/maadix/jicofoauth && rm /etc/maadix/jitsivideobridge',
        onlyif  => 'test -f /etc/maadix/jicofo',
        path    => '/usr/bin:/bin',
      }->
      exec { 'configure jitsi debconf with new domain':
        command => '/etc/maadix/scripts/jitsi-debconf.sh',
      }->
      exec { 'purge prosody conf':
        command => 'rm -r /etc/prosody',
        onlyif  => 'test -d /etc/prosody',
        path    => '/usr/bin:/bin',
      }->
      exec { 'purge jitsi conf':
        command => 'rm -r /etc/jitsi',
        onlyif  => 'test -d /etc/jitsi',
        path    => '/usr/bin:/bin',
      }->
      exec { 'purge prosody var':
        command => 'rm -r /var/lib/prosody',
        onlyif  => 'test -d /var/lib/prosody',
        path    => '/usr/bin:/bin',
      }->
      exec { 'reinstall jitsi':
        command => '/usr/bin/apt install -y --no-install-recommends jitsi-meet',
      }->
      exec { 'reinstall prosody ldap support':
        command => '/usr/bin/apt install -y prosody-modules lua-ldap',
        before  => [
                   Package['jitsi-meet'],
                   Package['prosody-modules'],
                   Package['lua-ldap'],
                   ],
      }
    }


    #jitsi packages
    include apt

    file { 'pin prosody from backports':
      path    => '/etc/apt/preferences.d/90prosody',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/90prosody'),
    } ->
    exec {'jitsi apt key':
        command   => '/usr/bin/wget -q -O- https://download.jitsi.org/jitsi-key.gpg.key | /usr/bin/apt-key add -',
        unless    => 'apt-key list | grep "FFD6 5A0D A2BE BDEB 73D4  4C8B B4D2 D216 F1FD 7806"',
        logoutput => true,
    } ->
    apt::source { 'jitsi':
        location    => 'https://download.jitsi.org',
        repos       => '',
        release     => 'stable/',
    } ->
    package { 'prosody':
        ensure          => 'present',
        install_options => ['-t', 'buster-backports'],
        require         => Exec[apt_update],
    }->
    package { 'jitsi-meet':
        ensure  => 'latest',
        #avoid installing recomended dependencies jitsi-meet-turnserver that install nginx and uninstall apache2
        install_options => ['--no-install-recommends'],
        require => Exec[apt_update],
        notify  => [
                   Service['jitsi-videobridge2'],
                   Service['prosody'],
                   Service['jicofo'],
                   ],
    }->
    package { 'prosody-modules':
        ensure  => 'present',
        require => Exec[apt_update],
    }->
    package { 'lua-ldap':
        ensure  => 'present',
        require => Exec[apt_update],
    } ->
    package { 'jitsi-meet-prosody':
        ensure  => 'latest',
        require => Exec[apt_update],
        notify  => [
                   Service['jitsi-videobridge2'],
                   Service['prosody'],
                   Service['jicofo'],
                   ],
    } ->
    file { 'purge jitsi vhost-enabled provided by package':
      path      => "/etc/apache2/sites-enabled/$domain.conf",
      ensure    => absent,
    } ->
    file { 'purge jitsi vhost-available provided by package':
      path      => "/etc/apache2/sites-available/$domain.conf",
      ensure    => absent,
      before    => Exec['reload apache'],
    }


    ### conf with secrets

    #jitsi videobridge domain config
    file { "jitsi videobridge $domain-config.js":
        path    => "/etc/jitsi/meet/$domain-config.js",
        content => template("helpers/domain-config.js-$::lsbdistcodename.erb"),
        require => [
                    Package['lua-ldap'],
                   ],
        notify  => Service['jitsi-videobridge2'],
    }

    #jicofo sip-communicator.properties
    file { "jicofo sip-communicator.properties":
        path    => "/etc/jitsi/jicofo/sip-communicator.properties",
        content => template('helpers/sip-communicator.properties.erb'),
        require => [
                    Package['lua-ldap'],
                   ],
        notify  => Service['jicofo'],
    }

    if ($auth){
      #prosody ldap settings template
      file { 'prosody ldap.cfg.lua_template':
          path    => '/etc/prosody/conf.avail/ldap.cfg.lua_template',
          content => template('helpers/ldap.cfg.lua_template.erb'),
          require => [
                      Package['lua-ldap'],
                     ],
      }

      #prosody ldap settings with secrets based on template
      exec { 'prosody ldap.cfg.lua with secrets':
        command     => "/bin/bash -c 'cat /etc/maadix/jitsildap | xargs -i sed \"s/jitsildapPLACEHOLDER/{}/\" /etc/prosody/conf.avail/ldap.cfg.lua_template > /etc/prosody/conf.d/ldap.cfg.lua && chown prosody /etc/prosody/conf.d/ldap.cfg.lua'",
        umask       => '077',
        require     => [
                       File['prosody ldap.cfg.lua_template'],
                       ],
        subscribe   => [ 
                       File['prosody ldap.cfg.lua_template'], 
                       ],
        refreshonly => true,
        notify      => Service['prosody'],
      }
    } else {
      file { 'prosody ldap.cfg.lua_template':
          path    => '/etc/prosody/conf.avail/ldap.cfg.lua_template',
          ensure  => absent,
      }
      file { 'prosody ldap.cfg.lua':
          path    => '/etc/prosody/conf.d/ldap.cfg.lua',
          ensure  => absent,
      }
    }

    #prosody conf
    file { 'prosody prosody.cfg.lua':
        path    => '/etc/prosody/prosody.cfg.lua',
        content => template("helpers/prosody.cfg.lua-$::lsbdistcodename.erb"),
        owner   => 'root',
        group   => 'prosody',
        mode    => '0640',
        require => [
                    Package['lua-ldap'],
                   ],
        notify  => Service['prosody'],
    }

    #prosody override conf
    file { "prosody conf overrides":
        path    => "/etc/prosody/conf.avail/overrides.cfg.lua",
        content => template("helpers/prosody_domain_extra.cfg.lua.erb"),
        require => [
                    Package['lua-ldap'],
                   ],
        notify      => [
                       Service['jitsi-videobridge2'],
                       Service['prosody'],
                       Service['jicofo'],
                       ]
    } ->
    file_line { 'add prosody conf overrides include':
        path    => "/etc/prosody/conf.avail/$domain.cfg.lua",
        line    => 'Include "../conf.avail/overrides.cfg.lua"',
        notify  => Service['prosody'],
    }

    #videobridge TCP conf
    file_line { 'JVB DISABLE_TCP_HARVESTER':
      path  => '/etc/jitsi/videobridge/sip-communicator.properties',
      line  => 'org.jitsi.videobridge.DISABLE_TCP_HARVESTER=false',
      match => 'org.jitsi.videobridge.DISABLE_TCP_HARVESTER.*$',
      require => Package['jitsi-meet'],
      notify => Service['jitsi-videobridge2'],
    }
    file_line { 'JVB TCP_HARVESTER_PORT':
      path  => '/etc/jitsi/videobridge/sip-communicator.properties',
      line  => 'org.jitsi.videobridge.TCP_HARVESTER_PORT=4443',
      match => 'org.jitsi.videobridge.TCP_HARVESTER_PORT.*$',
      require => Package['jitsi-meet'],
      notify => Service['jitsi-videobridge2'],
    }

    #logging
    if !$logging {
      file {'jicofo logging':
        path   => '/etc/jitsi/jicofo/logging.properties',
        ensure => absent,
        notify => Service['jicofo'],
      }
      file {'videobridge logging':
        path   => '/etc/jitsi/videobridge/logging.properties',
        ensure => absent,
        notify => Service['jitsi-videobridge2'],
      }
    }

    #jitsi mods
    file { 'apache-jitsi':
        path    => '/etc/apache2/conf-available/jitsi.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/jitsi.conf.erb'),
        require => Class['apache'],
    }
    file { '/etc/apache2/conf.d/jitsi.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/jitsi.conf',
      notify => Service['httpd'],
      require => Class['apache'],
    }


    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/jitsi",
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/jitsi"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/jitsi',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/usr/share/jitsi-meet",
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        access_log_format        => $log_format,
        directories              => [
                                      { path              => '/usr/share/jitsi-meet',
                                        options           => ['FollowSymLinks','MultiViews', 'Includes', 'Indexes'],
                                        custom_fragment   => 'AddOutputFilter Includes html',
                                        allow_override    => 'All',
                                      },
                                    ],
        custom_fragment => "
  
  SSLProxyEngine on

  Header set Strict-Transport-Security 'max-age=31536000'

  ErrorDocument 404 /static/404.html

  Alias '/config.js' '/etc/jitsi/meet/$domain-config.js'
  <Location /config.js>
    Require all granted
  </Location>

  Alias '/external_api.js' '/usr/share/jitsi-meet/libs/external_api.min.js'
  <Location /external_api.js>
    Require all granted
  </Location>

  ProxyPreserveHost on
  ProxyPass /http-bind http://localhost:5280/http-bind/
  ProxyPassReverse /http-bind http://localhost:5280/http-bind/

  <Location '/colibri-ws'>
        ProxyPreserveHost On
        ProxyPass 'ws://localhost:9090/colibri-ws'
  </Location>

#  <Location '/xmpp-websocket'>
#        ProxyPreserveHost On
#        ProxyPass 'ws://localhost:5280/xmpp-websocket'
#  </Location>

  #https://httpd.apache.org/docs/2.4/mod/mod_proxy_wstunnel.html
  ProxyPass /xmpp-websocket http://localhost:5280/xmpp-websocket
  RewriteEngine on
  RewriteCond %{HTTP:Upgrade} websocket [NC]
  RewriteCond %{HTTP:Connection} upgrade [NC]
  RewriteRule ^/xmpp-websocket?(.*) 'ws://localhost:5280/xmpp-websocket\$1' [P,L]

  RewriteEngine on
  RewriteRule ^/([a-zA-Z0-9]+)$ /index.html

          ",
        notify => Class['apache::service'],
      }
    }

    ##oom conf
    file { '/lib/systemd/system/jitsi-videobridge2.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/jitsi-videobridge2.service.d/local.conf':
      content => template('helpers/service_jitsi-videobridge2_local.conf.erb'),
      notify  => Service['jitsi-videobridge2'],
    }
    
    #start services
    service { 'jitsi-videobridge2':
        ensure    => $enabled,
        name      => jitsi-videobridge2,
        enable    => $enabled,
        require   => Package['jitsi-meet'],
    }->
    service { 'prosody':
        ensure    => $enabled,
        name      => prosody,
        enable    => $enabled,
    }->
    service { 'jicofo':
        ensure    => $enabled,
        name      => jicofo,
        enable    => $enabled,
    }

    #clean old jitsi domain
    if($::jitsidomain_old){
      helpers::cleanvhost{"$::jitsidomain_old":
        docroot  => '/usr/share/jitsi-meet',
      }
    }


  } else {

    ##disable jitsi, prosody and jicofo
    service { 'jitsi-videobridge2':
        ensure    => $enabled,
        name      => jitsi-videobridge2,
        enable    => $enabled,
    }
    service { 'jicofo':
        ensure    => $enabled,
        name      => jicofo,
        enable    => $enabled,
    }
    service { 'prosody':
        ensure    => $enabled,
        name      => prosody,
        enable    => $enabled,
    }

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/jitsi',
      }

    }

  }

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::jitsi::external::monit':
      enabled  => $enabled,
    }
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'jitsi':}
    helpers::remove {'jitsi':
      files      => ['/etc/maadix/scripts/jitsi-debconf.sh',
                     '/etc/maadix/scripts/jitsi_authorizedservices.sh',
                     '/etc/maadix/jicofo',
                     '/etc/maadix/jicofoauth',
                     '/etc/maadix/jitsivideobridge',
                     '/etc/apache2/conf-available/jitsi.conf',
                     '/etc/apache2/conf.d/jitsi.conf',
                     '/etc/maadix/status/jitsi_authorizedservices',
                    ],
      dirs       => ['/etc/prosody','/etc/jitsi','/var/lib/prosody','/lib/systemd/system/jitsi-videobridge2.service.d','/var/www/jitsi','/var/log/prosody','/var/log/jitsi'],
      packages   => ['jicofo','jitsi-meet','jitsi-meet-prosody','jitsi-meet-web','jitsi-meet-web-config','jitsi-videobridge2','prosody','prosody-modules','lua-ldap'],
      repos      => ['jitsi'],
      groups     => ['jitsi'],
      notify     => [
                    Helpers::Resetldapgroup['jitsi'],
                    Service['httpd']
                    ]
    }
  }

}
