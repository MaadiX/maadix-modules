## module inspired in https://github.com/meldsza/discourse_deploy ##
#
class helpers::discourse (
  String $revision ='',
  Boolean $enabled = str2bool($::discourse_enabled),
  String $type = 'standalone',
  Array  $templates = [],
  String $postgres_socket = '',
  String $postgres_username ='',
  String $postgres_password ='',
  String $postgres_host ='',
  String $redis_host = '',
  String $dev_emails = '' ,
  String $domain = '',
  String $smtp_address = '',
  String $smtp_username = '',
  Integer $smtp_port = 587,
  String $smtp_password = '',
  String $letsencrypt = '',
  Boolean $smtp_tls  = true ,
  Array $after_install =[],
  Array $plugins = [],
  Integer $sidekiqs = 0,
  Boolean $manage  = true ,
  Boolean $remove  = $::discourse_remove,
) {

  validate_bool($enabled)

  #todo check if domain is ok
  if $enabled {

    #required classes
    require docker
    require helpers::docker

    $allowed_types = ['^standalone$','^web_only$']
    validate_re($type, $allowed_types)


    #to update discourse, add new discourse_docker hash revision
    #latest bf323d999826e94c1ed7c1a3c6871bda70298080
    vcsrepo{ '/var/discourse/':
      ensure   => present,
      provider => git,
      source   => 'https://github.com/discourse/discourse_docker.git',
      revision => "$revision",
    }
    ->file{ '/var/discourse/containers/app.yml':
      ensure  => 'file',
      content => epp("helpers/${type}.epp")
    }
    ->file { '/usr/bin/docker.io':
      ensure => 'link',
      target => '/usr/bin/docker',
      require => [
                 Class['docker'],
                 Service['docker'],
                 ],
    }


    if $manage {
      exec { 'build discourse':
        command     => 'sudo /var/discourse/launcher bootstrap app',
        cwd         => '/var/discourse/',
        refreshonly => true,
        timeout     => 7200,
        creates     => "/var/discourse/launcher/shared/${type}",
        subscribe   => [
                        File['/var/discourse/containers/app.yml'],
                        Vcsrepo['/var/discourse/'],
                       ],
        unless      => 'sudo /var/discourse/launcher rebuild app',
        path        => ['/usr/bin', '/usr/sbin'],
        require     => File['/usr/bin/docker.io'],
      } ->
      #rebuild twice fixes postgresql upgrades
      #doc: https://meta.discourse.org/t/upgrade-attempt-broke-my-forum-please-help/178155
      #doc: https://meta.discourse.org/t/postgresql-13-update/172563
      exec { 'rebuild discourse':
        command     => 'sudo /var/discourse/launcher rebuild app',
        cwd         => '/var/discourse/',
        path        => ['/usr/bin', '/usr/sbin', '/bin'],
        refreshonly => true,
        timeout     => 7200,
        subscribe   => [
                        Vcsrepo['/var/discourse/'],
                       ],
      } ->
      exec { 'clean dangling discourse images':
        command     => 'docker image prune -f',
        path        => ['/usr/bin', '/usr/sbin', '/bin'],
        onlyif      => 'docker images --filter=dangling=true | grep none',
        logoutput   => true,
        refreshonly => true,
        subscribe   => Exec['rebuild discourse'],
      } ->
      exec { 'launch discourse':
        command     => 'sudo /var/discourse/launcher start app',
        cwd         => '/var/discourse/',
        path        => ['/usr/bin', '/usr/sbin', '/bin'],
        unless      => 'docker ps | grep discourse',
      } ->
      ##enable discourse restart
      exec { 'discourse enable restart':
        command     => 'docker update --restart=always app',
        path        => ['/usr/bin', '/usr/sbin', '/bin'],
        refreshonly => true,
        subscribe   => [
                        Exec['build discourse'],
                       ],
      } ->
      ##discourse shared directories permissions
      exec { 'discourse shared permissions':
        command     => "/bin/bash -c 'sleep 60 && /bin/chmod -R o-r /var/discourse/shared'",
        path        => ['/usr/bin', '/usr/sbin', '/bin'],
        refreshonly => true,
        subscribe   => [
                        Exec['build discourse'],
                       ],
      }

    }


    #discourse mods
    file { 'apache-discourse':
        path    => '/etc/apache2/conf-available/discourse.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/discourse.conf.erb'),
        require => Class['apache'],
    }
    file { '/etc/apache2/conf.d/discourse.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/discourse.conf',
      notify => Service['httpd'],
      require => Class['apache'],
    }


    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/discourse",
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/discourse"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/discourse',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/discourse",
        directories              => [
                                      { path    => "/var/www/discourse",
                                        options => ['FollowSymLinks','MultiViews'],
                                      },
                                    ],
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        override                 => ['All'],
        custom_fragment => '
          #strip the X-Forwarded-Proto header from incoming requests
          RequestHeader unset X-Forwarded-Proto
          #set the header for requests using HTTPS
          RequestHeader set X-Forwarded-Proto https env=HTTPS
          # keep the host
          ProxyPreserveHost On
          ProxyPass           / http://127.0.0.1:8090/
          ProxyPassReverse    / http://127.0.0.1:8090/
          ',
        notify => Class['apache::service'],
      }
    }


    #clean old discourse domain
    if($::discoursedomain_old){
      helpers::cleanvhost{"$::discoursedomain_old":
        docroot  => '/var/www/discourse',
      }
    }


  } else {

    ##disable discourse restart, to disable starting after reboot
    exec { 'discourse disable restart':
      command     => 'docker update --restart=no app',
      onlyif      => 'ls /usr/bin/docker && docker inspect app',
      path        => ['/usr/bin', '/usr/sbin', '/bin'],
    } ->
    ##discourse service stopped
    exec { 'discourse stop':
      command     => 'sudo /var/discourse/launcher stop app',
      onlyif      => 'ls /var/discourse/launcher && docker ps | grep discourse',
      path        => ['/usr/bin', '/usr/sbin', '/bin'],
    }

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/discourse',
      }

    }

  } 

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'discourse':}
    helpers::remove {'discourse':
      pre_commands  => ['/bin/bash -c "if [ -d /var/discourse ]; then cd /var/discourse && ./launcher destroy app --skip-prereqs; fi"','/bin/bash -c "if [ -f /usr/bin/docker ]; then docker system prune -f; fi"'],
      files         => ['/etc/apache2/conf.d/discourse.conf','/etc/apache2/conf-available/discourse.conf','/usr/bin/docker.io'],
      big_dirs      => ['/var/discourse'],
      dirs          => ['/var/www/discourse'],
      docker_i      => ['local_discourse/app','discourse/base'],
      notify        => [
                       Helpers::Resetldapgroup['discourse'],
                       Service['httpd']
                       ]
    }
  }

}


