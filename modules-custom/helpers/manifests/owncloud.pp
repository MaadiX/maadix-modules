class helpers::owncloud (
  $enabled           = str2bool($::owncloud_enabled),
  $trusted_domains   = '',
  $adminuser         = undef,
  $version           = '',
  $version_installed = undef,
  $datadir           = '',
  $update_path       = undef,
  $remove            = $::owncloud_remove,
  $php_version       = '7.4',
) {

  #required classes
  require php
  require helpers::php
  
  validate_bool($enabled)

  if $enabled and $php_version=='7.4' {

    #owncloud directory
    file { '/var/www/owncloud':
      ensure => directory,
      mode   => '0750',
      group   => 'fpmowncloud',
      owner   => 'fpmowncloud',
    } ->
    exec { 'owncloud files owner':
      command      => 'chown -R fpmowncloud:fpmowncloud /var/www/owncloud',
      timeout      => 28800,
      #don't log if not in debug mode
      #loglevel    => 'debug',
    }

    #owncloud data directory
    file { "$datadir":
      ensure => directory,
      mode   => '0770',
      group   => 'fpmowncloud',
      owner   => 'fpmowncloud',
    } ->
    exec { 'owncloud data files owner':
      command      => "chown -R fpmowncloud:fpmowncloud $datadir",
      timeout      => 28800,
      #don't log if not in debug mode
      #loglevel    => 'debug',
    }

    #owncloud source when installing from scratch
    if $version_installed == '' {
     archive { "/var/www/owncloud/owncloud-$version.tar.bz2":
      ensure        => present,
      extract       => true,
      extract_path  => '/var/www/owncloud',
      source        => "https://download.owncloud.com/server/stable/owncloud-$version.tar.bz2",
      creates       => '/var/www/owncloud/owncloud',
      cleanup       => false,
      user          => 'fpmowncloud',
      require       => File["$datadir"],
      before        => Exec['owncloud db and occ installation'],
     }
    }

    ##owncloud script
    file { 'owncloud password':
      path    => '/etc/maadix/scripts/owncloudpass.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/owncloudpass.sh.erb'),
    }

    #owncloud db and installation
    exec { 'owncloud db and occ installation':
      command => "/bin/bash /etc/maadix/scripts/owncloudpass.sh",
      creates => '/etc/maadix/status/owncloud',
      require => [
                 File['owncloud password'],
                 ],
      notify  => Exec['set owncloud installed version'],
    }


    #perform upgrade through all major versions
    $update_array = split($update_path, ',')
    $update_array.each |String $major_version| {
      #upgrade: prepare to update if version changes / stop apache and backup folder
      #upgrade: copy current config file and 3rd party apps to new installation and launch occ upgrade
      if($version_installed != $major_version) and ($version_installed != ''){

        helpers::owncloud::upgrade{ "$version_installed-$major_version":
          version_installed => "$version_installed",
          major_version     => "$major_version",
        }
      }
    }


    #set OC options on first install or on version update
    if($version_installed != $version) {
      notify {'upgrade: set OC options on first install or on version update':
        require => [
                   Exec['owncloud db and occ installation'],
                   ],
      } ->
      exec { 'owncloud trusted_domains':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set trusted_domains 0 --value=$trusted_domains",
      } ->
      exec { 'owncloud appstore':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set appstoreurl --value='https://marketplace.owncloud.com'",
      } ->
      exec { 'owncloud filelocking':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set filelocking.enabled --value=true",
      } ->
      exec { 'owncloud memcache filelocking':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set memcache.locking --value='\OC\Memcache\APCu'",
      } ->
      exec { 'owncloud opcache':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set memcache.local --value='\OC\Memcache\APCu'",
      } ->
      exec { 'owncloud disable web upgrade':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set upgrade.disable-web --value=true --type=boolean",
      } ->
      exec { 'owncloud disable update checker':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set updatechecker --value=false --type=boolean",
      } ->
      exec { 'owncloud mail domain':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_domain --value='$::fqdn'",
      } ->
      exec { 'owncloud mail from':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_from_address --value='owncloud'",
      } ->
      exec { 'owncloud mail mail_smtpmode':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_smtpmode --value='smtp'",
      } ->
      exec { 'owncloud mail mail_smtpauthtype':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_smtpauthtype --value='LOGIN'",
      } ->
      exec { 'owncloud mail mail_smtphost':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_smtphost --value='$::fqdn'",
      } ->
      exec { 'owncloud mail mail_smtpport':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_smtpport --value='465'",
      } ->
      exec { 'owncloud mail mail_smtpsecure':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_smtpsecure --value='ssl'",
      } ->
      exec { 'owncloud system cron':
        command => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ background:cron",
        notify  => Service['httpd'],
      }
    }

    #cron
    cron { 'owncloud':
      ensure  => absent,
      command => '/bin/sleep `/usr/bin/numrandom /0..5/`m ; php -f /var/www/owncloud/owncloud/occ system:cron',
      user    => 'www-data',
      minute  => '*/15',
    }
    cron { 'owncloud fpm':
      command => '/bin/sleep `/usr/bin/numrandom /0..5/`m ; php -f /var/www/owncloud/owncloud/occ system:cron',
      user    => 'fpmowncloud',
      minute  => '*/15',
    }

    #set installed version only run when first installation or upgrade are ok
    exec { 'set owncloud installed version':
      command     => "/etc/maadix/scripts/setldapgroupfield.sh owncloud version $version",
      logoutput   => true,
      refreshonly => true,
    }

    ## fqdn change scripts
    exec { 'owncloud fqdn change trusted_domains':
      command     => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set trusted_domains 0 --value=$trusted_domains",
      require     => [
                     Exec['owncloud db and occ installation'],
                     ],
      logoutput   => true,
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      notify      => [
                     Service['httpd'],
                     ]
    }
    exec { 'owncloud fqdn change mail domain':
      command     => "sudo -u fpmowncloud php /var/www/owncloud/owncloud/occ config:system:set mail_domain --value='$::fqdn'",
      logoutput   => true,
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      require     => [
                     Exec['owncloud db and occ installation'],
                     ],
      notify      => Service['httpd'],
    }

    #apache conf
    file { 'apache owncloud.conf':
      path    => '/etc/apache2/conf-available/owncloud.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/owncloud.conf.erb'),
      notify => Service['httpd'],
    } ->
    file { '/etc/apache2/conf.d/owncloud.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/owncloud.conf',
      notify => Service['httpd'],
    }


  } else {

    #disable cron
    cron { 'owncloud fpm':
      ensure  => absent,
      command => '/bin/sleep `/usr/bin/numrandom /0..5/`m ; php -f /var/www/owncloud/owncloud/occ system:cron',
      user    => 'fpmowncloud',
      minute  => '*/15',
    }

  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'owncloud':}
    helpers::remove {'owncloud':
      files      => ['/etc/maadix/status/owncloud','/etc/apache2/conf-available/owncloud.conf','/etc/apache2/conf.d/owncloud.conf'],
      big_dirs   => ['/var/www/owncloud'],
      mysql_d    => ['owncloud'],
      notify     => [
                    Helpers::Resetldapgroup['owncloud'],
                    Service['httpd']
                    ]
    }
  }

}

