class helpers::nextcloud (
  $enabled           = str2bool($::nextcloud_enabled),
  $trusted_domains   = '',
  $adminuser         = undef,
  $usermail          = undef,
  $version           = '',
  $version_installed = undef,
  $datadir           = '/var/www/nextcloud/data',
  $update_path       = undef,
  $domain            = '',
  $remove            = $::nextcloud_remove,
  $php_version       = '8.1',
) {
  
  #required classes
  require php
  require helpers::php

  validate_bool($enabled)

  if $enabled and $php_version=='8.1' {


    ##nextclouddb user passw file
    exec { 'nextclouddb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/nextclouddb'",
      creates => '/etc/maadix/nextclouddb',
      require => Package['pwgen'],
    }

    if $adminuser != undef {
      ##nextcloud script
      file { 'nextcloud password':
        path    => '/etc/maadix/scripts/nextcloudpass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/nextcloudpass.sh.erb'),
      }
    }

    ##nextcloud pass mail script
    file { 'nextcloud mail password':
      path    => '/etc/maadix/mail/nextcloud.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/nextcloud_mail_password.sh.erb'),
    }

    #nextcloud directory
    file { '/var/www/nextcloud':
      ensure       => directory,
      mode         => '0750',
      group        => 'fpmnextcloud',
      owner        => 'fpmnextcloud',      
    } ->
    exec { 'nextcloud files owner':
      command      => 'chown -R fpmnextcloud:fpmnextcloud /var/www/nextcloud',
      timeout      => 28800,      
      #don't log if not in debug mode
      #loglevel    => 'debug',
    }

    #nextcloud data directory
    file { "$datadir":
      ensure => directory,
      mode   => '0770',
      group   => 'fpmnextcloud',
      owner   => 'fpmnextcloud',
      require => File['/var/www/nextcloud'],
    }

    
    #nextcloud source when installing from scratch
    if $version_installed == '' {
     archive { "/var/www/nextcloud/nextcloud-$version.tar.bz2":
      ensure        => present,
      extract       => true,
      extract_path  => '/var/www/nextcloud',
      source        => "https://download.nextcloud.com/server/releases/nextcloud-$version.tar.bz2",
      creates       => '/var/www/nextcloud/nextcloud',
      cleanup       => false,
      user          => 'fpmnextcloud',
      require       => File["$datadir"],
      before        => Exec['nextcloud db and occ installation'],
     }
    }

    #nextcloud db and installation
    exec { 'nextcloud db and occ installation':
      command => "/bin/bash /etc/maadix/scripts/nextcloudpass.sh",
      creates => '/etc/maadix/status/nextcloud',
      logoutput => true,
      require => [
                 File['nextcloud password'],
                 ],
      notify  => Exec['set nextcloud installed version'],
    }

    #nextcloud db fixes when installing from scratch
    if $version_installed == '' {
      exec { 'nextclud add-missing-indices':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ db:add-missing-indices",
        require => Exec['nextcloud db and occ installation']
      } ->
      exec { 'nextclud convert-filecache-bigint':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ db:convert-filecache-bigint --no-interaction",
      }
    }

    #mv old backups to trash
    if($version_installed != $version) and ($version_installed != ''){
      exec { 'nc: mv old backups to trash':
        command => '/bin/mv /var/www/nextcloud/nextcloud-* /home/.trash/backups/',
        onlyif  => '/bin/ls -l /var/www/nextcloud/nextcloud-*',
      }
    }

    #perform upgrade through all major versions
    $update_array = split($update_path, ',')
    $update_array.each |String $major_version| {
      #upgrade: prepare to update if version changes / stop apache and backup folder
      #upgrade: copy current config file and 3rd party apps to new installation and launch occ upgrade
      if($version_installed != $major_version) and ($version_installed != ''){

        helpers::nextcloud::upgrade{ "$version_installed-$major_version":
          version_installed => "$version_installed",
          major_version     => "$major_version",
        }

      }
    }

    #notification link
    if($domain==''){
      $notification_link = "https://$::fqdn/nextcloud"
    } else {
      $notification_link = "https://$domain"
    }

    #set NC options on first install or on version update
    if($version_installed != $version) or ($::nextcloud_old_trusteddomain != $trusted_domains){
      notify {'upgrade: set NC options on first install or on version update or on fqdn change':
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
      } ->
      exec { 'nextcloud trusted_domains':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set trusted_domains 0 --value=$trusted_domains",
      } ->
      exec { 'nextcloud opcache':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set memcache.local --value='\OC\Memcache\APCu'",
      } ->
      exec { 'nextcloud disable web upgrade':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set upgrade.disable-web --value=true --type=boolean",
      } ->
      exec { 'nextcloud disable update checker':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set updatechecker --value=false --type=boolean",
      } ->
      exec { 'nextcloud mail domain':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_domain --value='$::fqdn'",
      } ->
      exec { 'nextcloud mail from':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_from_address --value='nextcloud'",
      } ->
      exec { 'nextcloud mail mail_smtpmode':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_smtpmode --value='smtp'",
      } ->
      exec { 'nextcloud mail mail_smtpauthtype':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_smtpauthtype --value='LOGIN'",
      } ->
      exec { 'nextcloud mail mail_smtphost':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_smtphost --value='$::fqdn'",
      } ->
      exec { 'nextcloud mail mail_smtpport':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_smtpport --value='465'",
      } ->
      exec { 'nextcloud mail mail_smtpsecure':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_smtpsecure --value='ssl'",
      } ->
      exec { 'nextcloud notifications links':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set overwrite.cli.url --value='$notification_link'",
      } ->
      exec { 'nextcloud system cron':
        command => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ background:cron",
        notify  => Service['httpd'],
      }
    }

    #cron
    cron { 'nextcloud':
      ensure  => absent,
      command => '/bin/sleep `/usr/bin/numrandom /0..5/`m ; php -f /var/www/nextcloud/nextcloud/cron.php',
      user    => 'www-data',
      minute  => '*/15',
    }
    cron { 'nextcloud fpm':
      command => '/bin/sleep `/usr/bin/numrandom /0..5/`m ; php -f /var/www/nextcloud/nextcloud/cron.php',
      user    => 'fpmnextcloud',
      minute  => '*/15',
    }

    #set installed version only run when first installation or upgrade are ok
    exec { 'set nextcloud installed version':
      command     => "/etc/maadix/scripts/setldapgroupfield.sh nextcloud version $version",
      logoutput   => true,
      refreshonly => true,
    }


    ## fqdn change scripts
    exec { 'nextcloud fqdn change trusted_domains':
      command     => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set trusted_domains 0 --value=$trusted_domains",
      require     => [
                     Exec['nextcloud db and occ installation'],
                     ],
      logoutput   => true,
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      notify      => [
                     Service['httpd'],
                     ]
    }
    exec { 'nextcloud fqdn change mail domain':
      command     => "sudo -u fpmnextcloud php --define apc.enable_cli=1 /var/www/nextcloud/nextcloud/occ config:system:set mail_domain --value='$::fqdn'",
      logoutput   => true,
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      require     => [
                     Exec['nextcloud db and occ installation'],
                     ],
      notify      => Service['httpd'],
    }

    #if domain not defined, add /nextcloud apache alias
    if($domain==''){
     
      #apache conf
      file { 'apache nextcloud.conf':
        path    => '/etc/apache2/conf-available/nextcloud.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/nextcloud.conf.erb'),
        notify => Service['httpd'],
      } ->
      file { '/etc/apache2/conf.d/nextcloud.conf':
        ensure => 'link',
        target => '/etc/apache2/conf-available/nextcloud.conf',
        notify => Service['httpd'],
      }
   
    } else {

      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/nextcloud/nextcloud",
        docroot_mode    => '0755',
        docroot_group   => 'fpmnextcloud',
        docroot_owner   => 'fpmnextcloud',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/nextcloud/nextcloud"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/nextcloud/nextcloud",
        docroot_group            => 'fpmnextcloud',
        docroot_owner            => 'fpmnextcloud',
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        notify => Class['apache::service'],
        custom_fragment => '

Redirect 301 /.well-known/carddav /remote.php/dav
Redirect 301 /.well-known/caldav /remote.php/dav
Redirect 301 /.well-known/webfinger /index.php/.well-known/webfinger
Redirect 301 /.well-known/nodeinfo /index.php/.well-known/nodeinfo

## Directories, there should at least be a declaration for /var/www/html
<Directory "/var/www/nextcloud/nextcloud">

  Options +FollowSymLinks
  AllowOverride All

  <IfModule mod_dav.c>
        Dav off
  </IfModule>

  SetEnv HOME /var/www/nextcloud/nextcloud
  SetEnv HTTP_HOME /var/www/nextcloud/nextcloud

  #Not allow any connection using HTTP
  Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"

</Directory>

<Directory "/var/www/nextcloud/data/">
  # just in case if .htaccess gets disabled
    Require all denied
</Directory>

#disable web access to updater
<Directory "/var/www/nextcloud/nextcloud/updater/">
  Require all denied
</Directory>

#php-fpm
<FilesMatch "\.php$">
  SetHandler "proxy:unix:/run/php/fpmnextcloud-fpm.sock|fcgi://localhost"
</FilesMatch>

        ',
      }

    }


    #clean old nextcloud domain
    if($::nextclouddomain_old){
      helpers::cleanvhost{"$::nextclouddomain_old":
        docroot  => '/var/www/nextcloud/nextcloud',
      }
    }


    ########### upgrade ########################################
    #backup mysql / todo
    #disable 3rd party apps / todo
    #stop apache / done
    #rename current nextcloud to nexcloud-$version_installed / done
    #download and extract new version to nextcloud / done
    #copy config.php from current version to new installation / done
    #copy non-existent 3rd party apps from current version to new installation /done
    ##find * -type d -prune -print0 | xargs -0 -n1 -I % sh -c 'echo %; mkdir /var/www/nextcloud/nextcloud/apps/% && echo % && cp -Rp % /var/www/nextcloud/nextcloud/apps/'
    #start apache / done
    #launch occ upgrade with HIGH TIMEOUT / done
    ##sudo -u fpmnextcloud php occ upgrade
    #enable 3rd party apps / todo

  } else {

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/nextcloud/nextcloud',
      }

    }

    #disable cron
    cron { 'nextcloud fpm':
      ensure  => absent,
      command => '/bin/sleep `/usr/bin/numrandom /0..5/`m ; php -f /var/www/nextcloud/nextcloud/cron.php',
      user    => 'fpmnextcloud',
      minute  => '*/15',
    }

  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'nextcloud':}
    helpers::remove {'nextcloud':
      files      => ['/etc/maadix/status/nextcloud','/etc/maadix/nextclouddb','/etc/apache2/conf-available/nextcloud.conf','/etc/apache2/conf.d/nextcloud.conf'],
      big_dirs   => ['/var/www/nextcloud'],
      mysql_d    => ['nextcloud'],
      notify     => [
                    Helpers::Resetldapgroup['nextcloud'],
                    Service['httpd']
                    ]
    }
  }

}

