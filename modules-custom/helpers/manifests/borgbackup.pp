class helpers::borgbackup {


     #service name, used for service and monit
     $postgresqlversion = $::lsbdistcodename ? {
       'buster'     => '11',
       'bullseye'   => '13',
    }

    #packages
    if ! defined(Package['borgbackup']) {
      ensure_resource('package','borgbackup', {
      ensure  => present,
      })
    }
    if ! defined(Package['jq']) {
      ensure_resource('package','jq', {
      ensure  => present,
      })
    }

    #borgbackup script
    file { 'borgbackup script':
      path    => '/etc/maadix/scripts/borgbackup.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/borgbackup.sh"),
    }

    #borgbackup exclude patterns
    file { 'borgbackup exclude':
      path    => '/etc/maadix/scripts/borgexclude',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/borgexclude"),
    }

    #cron
    $hour = fqdn_rand(2) + 5
    $minute = fqdn_rand(59)
    cron { 'borgbackup cron':
      command => '/bin/bash /etc/maadix/scripts/borgbackup.sh > /dev/null',
      user    => 'root',
      hour    => $hour,
      minute  => $minute,
    }

}

