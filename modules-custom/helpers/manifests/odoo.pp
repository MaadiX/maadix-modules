# odoo
#
#
#

class helpers::odoo (
  $enabled           = str2bool($::odoo_enabled),
  $domain            = '',
  $version           = '',
  $remove            = $::odoo_remove,
) {

  validate_bool($enabled)

  if $enabled {

    ########################## install ###################################

    ##user/group

    #odoo group
    group { 'odoo':
        ensure          => 'present',
        system          => true,
    }

    #odoo user
    user { 'odoo':
        ensure          => 'present',
        home            => '/var/www/odoo',
        managehome      => true,
        shell           => '/bin/false',
        system          => true,
    }

    ##postgresql role, with grant to create databases and login
    #CREATE ROLE odoo NOSUPERUSER CREATEDB INHERIT LOGIN;
    postgresql::server::role { 'odoo':
        createdb        => true,
        login           => true,
        superuser       => false,
        inherit         => true,
    }

    ##odoo repo
    vcsrepo { '/var/www/odoo/odoo':
        ensure   => present,
        provider => git,
        source   => 'https://www.github.com/odoo/odoo',
        user     => 'odoo',
        revision => $version,
        path     => '/var/www/odoo/odoo',
        require  => [
                    User['odoo'],
                    ],
    }

    ##virtualenv

    $python3_version = $::lsbdistcodename ? {
      'buster'     => '3.7',
    }

    $virtualenv3 = "/var/www/odoo/venv3/"

    python::virtualenv { $virtualenv3:
      ensure       => present,
      version      => $python3_version,
      owner        => 'odoo',
      group        => 'odoo', 
      distribute   => false,
      #see https://setuptools.readthedocs.io/en/latest/history.html#v50-0-0
      environment  => ['SETUPTOOLS_USE_DISTUTILS=stdlib'],
    } ~>
    exec { 'downgrade setuptools odoo':
      user         => 'odoo',
      command      => "bash -c 'source ${virtualenv3}/bin/activate; pip install setuptools==49.6.0'",
      cwd          => "/var/www/odoo",
      logoutput    => true,
      refreshonly  => true,
      before	   => Exec['odoo: pip install venv3 requirements'],
    } ~>
    #pip install/update venv3
    exec { 'odoo: pip install venv3 requirements':
      user        => 'odoo',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; pip install -r /var/www/odoo/odoo/requirements.txt'",
      cwd         => "/var/www/odoo",
      logoutput   => true,
      require     => [
                     Vcsrepo['/var/www/odoo/odoo'],
                     ],
      subscribe   => [
                     Vcsrepo['/var/www/odoo/odoo'],
                     ],
      refreshonly => true,
      notify     => [
                    Service['odoo'],
                    ],
      timeout    => 3600,

    }

    ########################## conf #####################################

    #odoo base conf stretch | buster
    file { 'odoo conf template':
      path    => '/var/www/odoo/odoo/odoo_template.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/odoo.conf.erb'),
      require    => [
                    Vcsrepo['/var/www/odoo/odoo'],
                    ],
    }

    #grab odoo passwd from ldap
    exec { 'grab odoo passwd from ldap':
      command     => "/bin/bash -c '/etc/maadix/scripts/getgrouppass.sh odoo'",
      onlyif      => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=password,ou=odoo,ou=groups,dc=example,dc=tld" | grep status',
    } ->
    #odoo config with secrets
    exec { 'odoo conf with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/odoo | xargs -i sed \"s/odoopassPLACEHOLDER/{}/\" /var/www/odoo/odoo/odoo_template.conf > /var/www/odoo/odoo/odoo.conf'",
      require     => [
                     File['odoo conf template'],
                     ],
      subscribe   => [
                     File['odoo conf template'],
                     ],
      refreshonly => true,
      notify     => [
                    Service['odoo'],
                    ],
    } ->
    #set permissions to conf file
    file { 'odoo conf':
      path    => '/var/www/odoo/odoo/odoo.conf',
      owner   => 'odoo',
      group   => 'odoo',
      mode    => '0600',
      notify  => Service['odoo'],
    }


    ########################## service ###################################

    #odoo systemd
    file { 'odoo: systemd service':
      path    => '/lib/systemd/system/odoo.service',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/odoo.service.erb'),
    }

    #odoo service
    service { 'odoo':
        ensure    => running,
        name      => odoo,
        enable    => true,
        hasstatus => true,
        require   => [
                     File['odoo: systemd service'],
                     ],
    }


    ######################### odoo apache ################################

    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/odoo",
        docroot_group   => 'odoo',
        docroot_owner   => 'odoo',
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
        require => [
                   User['odoo'],
                   ],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/odoo"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/odoo',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/odoo",
        directories              => [
                                      { path    => "/var/www/odoo",
                                        options => ['FollowSymLinks','MultiViews'],
                                      },
                                    ],
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        override                 => ['All'],
        custom_fragment => "
        ProxyRequests Off
        ProxyPass        '/' 'http://127.0.0.1:8069/'
        ProxyPassReverse '/' 'http://127.0.0.1:8069/'
        ",
        notify => Class['apache::service'],
      }
    }

    #clean old odoo domain
    if($::odoodomain_old){
      helpers::cleanvhost{"$::odoodomain_old":
        docroot  => '/var/www/odoo',
      }
    }

  } else {

    ##disable app   
    #odoo service
    service { 'odoo':
        ensure    => false,
        name      => odoo,
        enable    => false,
        hasstatus => true,
    }

   if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/odoo',
      }

    }

  }

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::odoo::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'odoo':}
    helpers::remove {'odoo':
      files      => ['/lib/systemd/system/odoo.service','/etc/maadix/odoo'],
      big_dirs   => ['/var/www/odoo'],
      psql_d     => $::odoo_databases,
      psql_u     => ['odoo'],
      users      => ['odoo'],
      notify     => [
                    Helpers::Resetldapgroup['odoo'],
                    Service['httpd']
                    ]
    }
  }


}


