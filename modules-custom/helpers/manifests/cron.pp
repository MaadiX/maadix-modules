class helpers::cron (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    ##num-utils package
    if ! defined(Package['num-utils']) {
      package { 'num-utils':
        ensure => 'present',
      }
    }

    ##add random sleep time to cron hourly
    file_line { 'random cron hourly':
      path    => '/etc/crontab',
      line    => '17 *  * * *	root	/bin/sleep `/usr/bin/numrandom /0..30/`m ; cd / && run-parts --report /etc/cron.hourly',
      match   => '.*cron.hourly.*$',
      require => Package['num-utils'],
    }

    ##add random sleep time to cron daily
    file_line { 'random cron daily':
      path    => '/etc/crontab',
      line    => '25 6	* * *	root	/bin/sleep `/usr/bin/numrandom /0..110/`m ; test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )',
      match   => '.*cron.daily.*$',
      require => Package['num-utils'],
    }

    ##add random sleep time to cron weekly
    file_line { 'random cron weekly':
      path    => '/etc/crontab',
      line    => '47 6	* * 7	root	/bin/sleep `/usr/bin/numrandom /0..110/`m ; test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )',
      match   => '.*cron.weekly.*$',
      require => Package['num-utils'],
    }

    ##add random sleep time to cron monthly
    file_line { 'random cron monthly':
      path    => '/etc/crontab',
      line    => '52 6	1 * *	root	/bin/sleep `/usr/bin/numrandom /0..110/`m ; test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )',
      match   => '.*cron.monthly.*$',
      require => Package['num-utils'],
    }

    ##add random sleep time to logcheck
    file_line { 'random logcheck':
      path    => '/etc/cron.d/logcheck',
      line    => '2 4 * * *       logcheck    /bin/sleep `/usr/bin/numrandom /0..120/`m ; if [ -x /usr/sbin/logcheck ]; then nice -n19 /usr/sbin/logcheck; fi',
      match   => '2.*logcheck.*$',
      require => [
                 Package['num-utils'],
                 Package['logcheck'],
                 ],
    }

  }

}





