define helpers::nextcloud::upgrade(
  $major_version       = undef,
  $version_installed   = undef,
) {
    
      #timestamp
      #$timestamp = generate('/bin/date', '+%Y_%m_%d-%H_%M_%S')
      #$timestamp = generate('/usr/bin/printf', '%(%Y_%m_%d-%H_%M_%S)T')
      $timestamp = Timestamp.new().strftime('%Y_%m_%d-%H_%M_%S')
  
      notify {"major version: $major_version": 
      } ->
      notify {"upgrade: prepare to update if version changes / stop apache and backup folder-$major_version": 
      } ->
      exec { "updating nextcloud: list available app updates before upgrading to $major_version":
        command    => 'sudo -u fpmnextcloud bash -c "php --define apc.enable_cli=1 occ app:update --showonly"',
        cwd        => "/var/www/nextcloud/nextcloud",
        logoutput  => true,
      } ->
      exec { "updating nextcloud: disable apps-$major_version":
        command    => 'sudo -u fpmnextcloud bash -c "php --define apc.enable_cli=1 occ app:disable spreed; php --define apc.enable_cli=1 occ app:disable mail; php --define apc.enable_cli=1 occ app:disable ocr; php --define apc.enable_cli=1 occ app:disable documentserver_community; php --define apc.enable_cli=1 occ app:disable circles; php --define apc.enable_cli=1 occ app:disable maps; php --define apc.enable_cli=1 occ app:disable mediadc; php --define apc.enable_cli=1 occ app:disable permissions_overwrite; php --define apc.enable_cli=1 occ app:disable fulltextsearch; php --define apc.enable_cli=1 occ app:disable libresign; php --define apc.enable_cli=1 occ app:disable bbb; php --define apc.enable_cli=1 occ app:disable pdf_downloader; php --define apc.enable_cli=1 occ app:disable solid; php --define apc.enable_cli=1 occ app:disable polls; php --define apc.enable_cli=1 occ app:disable workspace; php --define apc.enable_cli=1 occ app:disable duplicatefinder; php --define apc.enable_cli=1 occ app:disable snappymail"',
        cwd        => "/var/www/nextcloud/nextcloud",
        logoutput  => true,
      } ->
      exec { "updating nextcloud: download nextcloud-$major_version.tar.bz2 to nextcloud-$major_version folder":
        command    => "wget https://download.nextcloud.com/server/releases/nextcloud-$major_version.tar.bz2 && mkdir nextcloud-$major_version && tar -xjf nextcloud-$major_version.tar.bz2 --directory nextcloud-$major_version --strip-components 1 && mv nextcloud-$major_version.tar.bz2 nextcloud-$major_version-$timestamp.tar.bz2",
        cwd        => '/var/www/nextcloud',
        user       => 'fpmnextcloud',
        timeout    => 7200,
      } ->
      exec { "updating nextcloud: stop apache and backup directories-$major_version":
        command    => "service apache2 stop && mv /var/www/nextcloud/nextcloud /var/www/nextcloud/nextcloud-$version_installed-$major_version-$timestamp",
        logoutput  => true,
        timeout    => 28800,
      } ->
      exec { "updating nextcloud: ddbb backup-$major_version":
        command    => "mysqldump --defaults-extra-file=/root/.my.cnf nextcloud > /var/www/nextcloud/nextcloud-$version_installed-$major_version-$timestamp.sql && chmod 700 /var/www/nextcloud/nextcloud-$version_installed-$major_version-$timestamp.sql",
        logoutput  => true,
        timeout    => 28800,
      } ->
      exec { "updating nextcloud: mv nextcloud-$major_version folder to nextcloud":
        command    => "mv nextcloud-$major_version nextcloud",
        cwd        => '/var/www/nextcloud',
        user       => 'fpmnextcloud',
        timeout    => 7200,
      } ->
      exec { "updating nextcloud: copying 3rd party apps-$major_version":
        command    => "find * -type d -prune -print0 | xargs -0 -n1 -I % sh -c \"if [ ! -d /var/www/nextcloud/nextcloud/apps/% ]; then echo % && cp -Rp % /var/www/nextcloud/nextcloud/apps/; fi\" ",
        cwd        => "/var/www/nextcloud/nextcloud-$version_installed-$major_version-$timestamp/apps",
        logoutput  => true,
      } ->
      exec { "updating nextcloud: copying config file-$major_version":
        command    => "sudo -u fpmnextcloud cp /var/www/nextcloud/nextcloud-$version_installed-$major_version-$timestamp/config/config.php /var/www/nextcloud/nextcloud/config/config.php",
        logoutput  => true,
      } ->
      exec { "updating nextcloud: occ upgrade-$major_version":
        command    => "sudo -u fpmnextcloud php --define apc.enable_cli=1 occ upgrade",
        cwd        => "/var/www/nextcloud/nextcloud",
        logoutput  => true,
        timeout    => 28800,
        notify     => [
                      Service['httpd'],
                      Exec['set nextcloud installed version'],
                      ],
      } ->
      exec { "updating nextcloud: list available app updates after upgrading to $major_version":
        command    => 'sudo -u fpmnextcloud bash -c "php --define apc.enable_cli=1 occ app:update --showonly"',
        cwd        => "/var/www/nextcloud/nextcloud",
        logoutput  => true,
      } ->
      exec { "updating nextcloud: restart apache-$major_version":
        command    => "service apache2 restart",
        before     => [
                      Notify['upgrade: set NC options on first install or on version update or on fqdn change'],
                      ],
      }

  
}
