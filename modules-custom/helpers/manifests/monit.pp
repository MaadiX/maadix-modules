class helpers::monit (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    #in buster, install monit from buster-backports // not needed in bullseye
    if $::lsbdistcodename=='buster'{    

      include apt
      #this install lsbdistcodename-backports with low priotity (200)
      #https://forge.puppet.com/puppetlabs/apt#prioritize-backports
      include apt::backports

      #install package after apt::backports
      Exec[apt_update] -> Package['monit']

      Package <| title == "monit" |> { 
        install_options => ['-t', 'buster-backports'],
      }

    }

    #systemd monit service
    #doc /usr/share/doc/monit/examples/monit.service
    file { 'systemd monit.service':
        path    => '/lib/systemd/system/monit.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template("helpers/monit.service.erb"),
        require => Package['monit'],
        notify  => Service['monit'],
    }

    ##oom conf
    file { '/lib/systemd/system/monit.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/monit.service.d/local.conf':
      content => template('helpers/service_monit_local.conf.erb'),
      notify  => Service['monit'],
    }

  }

}
