class helpers::opendmarc (
  $enabled    = true,
  $reject     = true,
) {


  validate_bool($enabled)

  if $enabled {

    #opendmarc dependencies
    if ! defined(Package['opendmarc']) {
      ensure_resource('package','opendmarc', {
      ensure  => present,
      before  => User['postfix'],
      })
    }

    #opendmarc conf
    file { 'opendmarc conf':
      path    => '/etc/opendmarc.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("helpers/opendmarc.conf.erb"),
      require => Package['opendmarc'],
    }

    service { 'opendmarc':
      ensure  => 'running',
      enable  => true,
      require => [                 
                 Package['opendmarc'],
                 File['opendmarc conf']
                 ]
    }

    #add user postfix to group opendmarc
    User <| title == postfix |> { groups +> "opendmarc" }

  } else {


    ##opendmarc service stopped
    service { 'opendmarc':
      ensure    => $enabled,
      name      => opendmarc,
      enable    => $enabled,
    }


  } 

/*

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::opendmarc::external::monit': enabled => $enabled}
  }

*/

}

