#hardening / acct
class helpers::acct (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #package
    package { 'acct':
      ensure => 'present',
    }

    #service
    service { 'acct':
      ensure  => 'running',
      enable  => true,
      require => [
                 Package['acct'],
                 ]
    }


  }

}

