class helpers::backports {

  include apt
  if $::lsbdistcodename=='jessie'{
    #para cargar esta fuente no podemos usar apt::sources porque da error de dependencia ciclica al ponerlo en el stage first
    Exec { 'jessie apt backports':
      command    => 'echo "deb http://archive.debian.org/debian/ jessie-backports main" > /etc/apt/sources.list.d/backports.list',
      creates => '/etc/apt/sources.list.d/backports.list'
    }

# OJO, estos paquetes pueden ser requeridos para que el apt update posterior funcione correctamente
#    required_packages => 'debian-keyring debian-archive-keyring',

  }

}

