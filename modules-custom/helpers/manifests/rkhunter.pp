#hardening / rkhunter
class helpers::rkhunter (
  $enabled           = true,
  $auto              = true,
) {

  validate_bool($enabled)

  if $enabled {

    #package
    package { 'unhide':
      ensure  => 'present',
    }
    package { 'rkhunter':
      ensure  => 'present',
      require => Package['etckeeper'],
    }

    #### conf ###
    ## doc: https://gitlab.com/MaadiX/server-stats-and-check/-/wikis/intrusion-detection

    ## rkhunter conf, to enable crons
    file_line { 'enable rkhunter daily cron':
      path    => '/etc/default/rkhunter',
      line    => 'CRON_DAILY_RUN="true"',
      match   => 'CRON_DAILY_RUN.*$',
      require => Package['rkhunter'],
    }
    file_line { 'enable rkhunter weekly cron':
      path    => '/etc/default/rkhunter',
      line    => 'CRON_DB_UPDATE="true"',
      match   => 'CRON_DB_UPDATE.*$',
      require => Package['rkhunter'],
    }
    if $auto {
      file_line { 'enable rkhunter automatic database updates':
        path    => '/etc/default/rkhunter',
        line    => 'APT_AUTOGEN="true"',
        match   => 'APT_AUTOGEN.*$',
        require => Package['rkhunter'],
      }
    } else {
      file_line { 'enable rkhunter automatic database updates':
        path    => '/etc/default/rkhunter',
        line    => 'APT_AUTOGEN="false"',
        match   => 'APT_AUTOGEN.*$',
        require => Package['rkhunter'],
      }
    }

    ## rkunter local conf
    if $facts['usr_bin_egrep'] {
      file { '/etc/rkhunter.conf.local':
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => template('helpers/rkhunter.conf.local'),
          require => Package['rkhunter'],
          notify  => Exec['rkhunter --update --propupd'],
      }
    } else {
      #in vms migrated from stretch /usr/bin/egrep is missing
      file { '/etc/rkhunter.conf.local':
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => template('helpers/rkhunter.conf.without_egrep.local'),
          require => Package['rkhunter'],
          notify  => Exec['rkhunter --update --propupd'],
      }
    }
    exec { 'rkhunter --update --propupd':
      command     => 'rkhunter --update --propupd',
      require     => Package['rkhunter'],
      logoutput   => true,
      #--update exit codes can be 0,1,2, https://linux.die.net/man/8/rkhunter
      returns     => [0,1,2],
      refreshonly => true,
      timeout     => 3600,
    }
    

  }

}
