class helpers::ssh::external::monit(
  $enabled = true,
  $port    = '22',
) {

  validate_bool($enabled)

  if $enabled {
    $pidfile = $::osfamily ? {
      /(RedHat|Debian)/ => '/var/run/sshd.pid',
    }

    $test = {
      type     => connection,
      port     => $port,
      protocol => ssh,
    }
    monit::check::service { 'ssh':
      pidfile => $pidfile,
      binary  => $::osfamily ? {
        'Debian' => '/usr/sbin/sshd',
        default  => undef,
      },
      tests   => [$test, ],
    }
  }

}

