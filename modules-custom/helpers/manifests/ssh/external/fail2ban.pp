class helpers::ssh::external::fail2ban(
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    #support for stretch|buster|bullseye
    #override fail2ban ssh port
    file { 'fail2ban ssh with custom port':
        path    => '/etc/fail2ban/jail.d/ssh.conf',
        content => template("helpers/ssh_fail2ban.conf-$::lsbdistcodename.erb"),
        require => Package['fail2ban'],
        notify  => Service['fail2ban'],
    }

  }

}
