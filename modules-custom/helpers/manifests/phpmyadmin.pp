class helpers::phpmyadmin (
  $enabled           = str2bool($::phpmyadmin_enabled),
  $remove            = $::phpmyadmin_remove,
  $version           = '',
  $version_installed = undef,
  $php_version       = '8.1',
) {

  #required classes
  require php
  require helpers::php

  validate_bool($enabled)

  if $enabled and $php_version=='8.1' {


    #purge phpmyadmin package keeping phpmyadmin database
    exec {'debconf phpmyadmin':
      command => 'echo "phpmyadmin      phpmyadmin/dbconfig-remove      boolean false" | debconf-set-selections',
      onlyif  => 'dpkg -l | grep phpmyadmin',      
    } ->
    package { 'phpmyadmin':
      ensure => 'purged',
    } ->
    package { 'php-phpmyadmin-motranslator':
      ensure => 'purged',
    } ->
    package { 'php-phpmyadmin-shapefile':
      ensure => 'purged',
    } ->
    package { 'php-phpmyadmin-sql-parser':
      ensure => 'purged',
    }

    #packages
    if ! defined(Package['php-gettext']) {
      ensure_resource('package','php-gettext', {
      ensure  => present,
      })
    }

    #scripts
    file { 'phpmyadmin blowfish_secret script':
      path          => '/etc/maadix/scripts/phpmyadminpass.sh',
      mode          => '700',
      content       => template("helpers/phpmyadminpass.sh.erb"),
      notify        => Service['httpd'],
    } 

    #tasks to upgrade
    if($version_installed != $version) and ($version_installed != ''){
      exec { 'phpmyadmin reset webroot':
        command     => 'rm -r /var/www/phpmyadmin/public_html',
        onlyif      => 'test -d /var/www/phpmyadmin/public_html',
        before      => File['/var/www/phpmyadmin'],
      }
      #check if there is database upgrade script
      #https://docs.phpmyadmin.net/en/latest/setup.html#manual-configuration
    }

    #install a new version
    file { '/var/www/phpmyadmin':
      ensure        => directory,
      mode          => '0750',
      owner         => 'fpmphpmyadmin',
      group         => 'www-data',
    } ->
    archive { "/tmp/phpMyAdmin-$version-all-languages.tar.gz":
      ensure        => present,
      extract       => true,
      extract_path  => '/var/www/phpmyadmin/',
      source        => "https://files.phpmyadmin.net/phpMyAdmin/$version/phpMyAdmin-$version-all-languages.tar.gz",
      creates       => '/var/www/phpmyadmin/public_html/index.php',
      cleanup       => true,
      user          => 'fpmphpmyadmin',
      group         => 'www-data',
    } ~>
    exec {'phpmyadmin webroot':
      command       => "mv /var/www/phpmyadmin/phpMyAdmin-$version-all-languages /var/www/phpmyadmin/public_html",
      user          => 'fpmphpmyadmin',
      refreshonly   => true,
    } ~>
    exec {'phpmyadmin config.inc.php':
      command       => 'cp /var/www/phpmyadmin/public_html/config.sample.inc.php /var/www/phpmyadmin/public_html/config.inc.php',
      user          => 'fpmphpmyadmin',
      refreshonly   => true,
    } ~>
    exec {'phpmyadmin blowfish_secret':
      command       => '/etc/maadix/scripts/phpmyadminpass.sh',
      refreshonly   => true,
    } ~>
    exec { 'set phpmyadmin installed version':
      command       => "/etc/maadix/scripts/setldapgroupfield.sh phpmyadmin version $version",
      logoutput     => true,
      refreshonly   => true,
      notify        => Service['httpd'],
    }

    #custom phpmyadmin apache conf forcing SSL
    #jessie | stretch | buster
    file { 'phpmyadmin apache conf':
      path          => '/etc/apache2/conf.d/phpmyadmin.conf',
      owner         => 'root',
      group         => 'root',
      mode          => '700',
      content       => template("helpers/phpmyadmin-apache-$::lsbdistcodename.conf.erb"),
      notify        => Service['httpd'],
    }


  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'phpmyadmin':}
    helpers::remove {'phpmyadmin':
      files      => ['/etc/apache2/conf.d/phpmyadmin.conf'],
      big_dirs   => ['/var/www/phpmyadmin'],
      #remove user and ddbb if user@localhost is present
      mysql_d    => ['phpmyadmin'],
      notify     => [
                    Helpers::Resetldapgroup['phpmyadmin'],
                    Service['httpd']
                    ]
    }
  }

}
