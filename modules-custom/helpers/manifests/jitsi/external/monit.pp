class helpers::jitsi::external::monit(
  $enabled        = true,
  $ip             = $helpers::jitsi::ipaddress,
  $port           = '10000',
) inherits helpers::jitsi {

  validate_bool($enabled)

  if $enabled {

    $init_system    = systemd
    $pidfile        = '/var/run/jitsi-videobridge/jitsi-videobridge.pid'
    $binary         = '/usr/share/jitsi-videobridge/jvb.sh'

    #disable connection test. new videobridge only open this port when a room is online
    $connection_test = {
      type          => 'connection',
      host          => $ip,
      protocol      => 'GENERIC',
      socket_type   => 'UDP',
      port          => $port,
      action        => 'restart',
    }

    monit::check::service { 'jitsi-videobridge2':
      init_system   => $init_system,
      pidfile       => $pidfile,
      binary        => $binary,      
      #disable connection test. new videobridge only open this port when a room is online
      #tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}

