class helpers::onlyoffice (
  $enabled           = str2bool($::onlyoffice_enabled),
  $onlyoffice_passwd_change = str2bool($::onlyoffice_passwd_change),
  $domain            = '',
  $version           = '',
  $remove            = $::onlyoffice_remove,
) {


  validate_bool($enabled)

  if $enabled {

    #required classes
    require docker
    require helpers::docker

    contain helpers::onlyoffice::install
    #contain helpers::onlyoffice::clean

    #Class['helpers::onlyoffice::install'] ->
    #Class['helpers::onlyoffice::clean']

  } else {

    contain helpers::onlyoffice::disable

  } 

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::onlyoffice::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'onlyoffice':}
    helpers::remove {'onlyoffice':
      files         => ['/etc/apache2/conf.d/onlyoffice.conf','/etc/apache2/conf-available/onlyoffice.conf','/etc/maadix/onlyoffice'],
      dirs          => ['/opt/onlyoffice','/var/www/onlyoffice'],
      docker_i      => ['onlyoffice/documentserver'],
      docker_v      => ['onlyoffice-log',
                        'onlyoffice-www-data',
                        'onlyoffice-lib',
                        'onlyoffice-db',
                        'onlyoffice-redis',
                        'onlyoffice-rabbitmq',
                        'onlyoffice-fonts',
                       ],
      notify        => [
                       Helpers::Resetldapgroup['onlyoffice'],
                       Service['httpd']
                       ]
    }
  }
}

