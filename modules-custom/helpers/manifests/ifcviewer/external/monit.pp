class helpers::ifcviewer::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 5000,
  $matching	  = '"sh -c python3 database.py; gunicorn --bind 0.0.0.0:5000 -w 8"',
  $binary         = '/usr/bin/docker',
  $program_start  = '/usr/sbin/service ifcviewer restart',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'ifcviewer':
      init_system   => $init_system,
      matching	    => $matching,
      program_start => $program_start,
      binary        => $binary,
      tests         => [$connection_test, ],
    }
  }

}


