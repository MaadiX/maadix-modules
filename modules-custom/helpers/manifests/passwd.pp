class helpers::passwd (
  $enabled = true,
  $adminuser = undef,
  $sshduser = undef,
  $adminmail = undef,
  $fqdn = undef,
  $lsbdistcodename = $::lsbdistcodename,
) {

  validate_bool($enabled)
  validate_string ($adminuser)
  validate_string ($sshduser)

  if $enabled {

    ##password directory
    file { '/etc/maadix':
      ensure => directory,
      mode   => '0700',
    }

    ##password status directory
    file { '/etc/maadix/status':
      ensure => directory,
      mode   => '0700',
    }

    ##password scripts directory
    file { '/etc/maadix/scripts':
      ensure => directory,
      mode   => '0700',
    }

    ##log directory
    file { '/etc/maadix/logs':
      ensure => directory,
      mode   => '0700',
    }

    ##conf directory
    file { '/etc/maadix/conf':
      ensure => directory,
      mode   => '0700',
    }

    ##pwgen package
    package { 'pwgen':
      ensure => 'present',
    }

    ### conf files ###

    ##fqdn sensor, to trigger actions when changes
    file { 'conf fqdn':
      path    => '/etc/maadix/conf/fqdn',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/conf_fqdn.erb'),
    }

    ##adminmail sensor, to trigger actions when changes
    file { 'conf adminmail':
      path    => '/etc/maadix/conf/adminmail',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/conf_adminmail.erb'),
    }

    ### plain text password files ###

    ##ldap passw file
    exec { 'ldap passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/ldap'",
      creates => '/etc/maadix/ldap',
      require => Package['pwgen'],
    }

    ##mysql passw file
    exec { 'mysql passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/mysql'",
      creates => '/etc/maadix/mysql',
      require => Package['pwgen'],
    }

    ##sudo user passw file
    exec { 'sudo passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/sudo'",
      creates => '/etc/maadix/sudo',
      require => Package['pwgen'],
    }

    ##ownclouddb user passw file
    exec { 'ownclouddb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/ownclouddb'",
      creates => '/etc/maadix/ownclouddb',
      require => Package['pwgen'],
    }

    ##etherpad mysql user passw file
    exec { 'etherpaddb admin passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/etherpaddb'",
      creates => '/etc/maadix/etherpaddb',
      require => Package['pwgen'],
    }

    ##gnarwl user passw file
    exec { 'gnarwl passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/gnarwl'",
      creates => '/etc/maadix/gnarwl',
      require => Package['pwgen'],
    }

    ##mxcp user passw file
    exec { 'mxcp passw':
      command => "/bin/bash -c 'umask 077 && pwgen 100 -1 | tr -d \"\n\" > /etc/maadix/mxcp'",
      creates => '/etc/maadix/mxcp',
      require => Package['pwgen'],
    }

    ##jitsildap user passw file
    exec { 'jitsildap passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/jitsildap'",
      creates => '/etc/maadix/jitsildap',
      require => Package['pwgen'],
    }

    ##mongodb user passw file
    exec { 'mongodb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/mongodbadmin'",
      creates => '/etc/maadix/mongodbadmin',
      require => Package['pwgen'],
    }

    ##zeyple user passw file
    exec { 'zeyple passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/zeyple'",
      creates => '/etc/maadix/zeyple',
      require => Package['pwgen'],
    }

    #hardening / grub password
    ##grub user passw file
    exec { 'grub passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/grub'",
      creates => '/etc/maadix/grub',
      require => Package['pwgen'],
    }


    ### scripts to set passwords ###

    if $adminuser != undef {
      ##ldappass script
      file { 'ldap password':
        path    => '/etc/maadix/scripts/ldappass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/ldappass.sh.erb'),
      }

      ##rainloop script
      file { 'rainloop password':
        path    => '/etc/maadix/scripts/rainlooppass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/rainlooppass.sh.erb'),
      }

      ##etherpad script
      file { 'etherpad password':
        path    => '/etc/maadix/scripts/etherpad.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/etherpad.sh.erb'),
      }

      ##gnarwl script
      file { 'gnarwl password':
        path    => '/etc/maadix/scripts/gnarwlpass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/gnarwlpass.sh.erb'),
      }

      ##mxcp script
      file { 'mxcp password':
        path    => '/etc/maadix/scripts/mxcppass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/mxcppass.sh.erb'),
      }

      ##jitsildap script
      file { 'jitsildap password':
        path    => '/etc/maadix/scripts/jitsildap.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/jitsildap.sh.erb'),
      }

      ##mongodb script
      file { 'mongodb password':
        path    => '/etc/maadix/scripts/mongodbpass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/mongodbpass.sh.erb'),
      }

      ##zeyple script
      file { 'zeyple password':
        path    => '/etc/maadix/scripts/zeyplepass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/zeyplepass.sh.erb'),
      }

    }
    if $sshduser != undef {
      ##sudopass script
      file { 'sudo password':
        path    => '/etc/maadix/scripts/sudopass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/sudopass.sh.erb'),
      }
      ##add authorizedservice: sudo if not present
      file { 'authorizedservice sudo script':
        path    => '/etc/maadix/scripts/sudo_authorizedservices.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/sudo_authorizedservices.sh.erb'),
      }
      ##add authorizedservice: systemd-user if not present
      file { 'authorizedservice systemd-user script':
        path    => '/etc/maadix/scripts/systemd-user_authorizedservices.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/systemd-user_authorizedservices.sh.erb'),
      }
    }


    ##mysqlpass script jessie/stretch/buster
    file { 'mysql password':
      path    => "/etc/maadix/scripts/mysqlpass.sh",
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/mysqlpass-$lsbdistcodename.sh.erb"),
    }

    #hardening / grub password
    ##grub pass script
    file { 'grub password':
      path    => "/etc/maadix/scripts/grubpass.sh",
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/grubpass.sh.erb"),
    }

    ### set passwords one time ###

    if $adminuser != undef {
      exec { 'set ldap passw':
        command => "/bin/bash /etc/maadix/scripts/ldappass.sh",
        creates => '/etc/maadix/status/ldap',
        require => [File['ldap password'],Class['openldap::server']],    
      }
    }

    if defined('::mysql::server') and defined(Class['::mysql::server']) {
      exec { 'set mysql passw':
        command => "/bin/bash /etc/maadix/scripts/mysqlpass.sh",
        creates => '/etc/maadix/status/mysql',
        require => [File['mysql password'],Class['mysql::server::account_security']],
      }
    
      #mysql /root/.my.cnf as symbolic link of /etc/mysql/debian.cnf
      file { '/root/.my.cnf':
        ensure  => 'link',
        target  => '/etc/mysql/debian.cnf',
        require => Class['mysql::server'],
      }
    }

    if $sshduser != undef and defined(Class['::helpers::pamldap']) {
      exec { 'set sudo passw':
        command => "/bin/bash /etc/maadix/scripts/sudopass.sh",
        creates => '/etc/maadix/status/sudo',
        require => [File['sudo password'],Class['helpers']],
      }
      
      exec { 'add authorizedservice sudo':
        command => "/bin/bash /etc/maadix/scripts/sudo_authorizedservices.sh",
        creates => '/etc/maadix/status/sudo_authorizedservices',
        require => [File['authorizedservice sudo script'],Class['helpers']],
      }

      if $::lsbdistcodename=='buster'{
        exec { 'add authorizedservice systemd-user':
          command => "/bin/bash /etc/maadix/scripts/systemd-user_authorizedservices.sh",
          creates => '/etc/maadix/status/systemd-user_authorizedservices',
          require => [File['authorizedservice systemd-user script'],Class['helpers']],
        }
      }

    }

    exec { 'set gnarwl passw':
      command => "/bin/bash /etc/maadix/scripts/gnarwlpass.sh",
      creates => '/etc/maadix/status/gnarwl',
      require => [File['gnarwl password'],Class['openldap::server']],
    }

    exec { 'set mxcp passw':
      command => "/bin/bash /etc/maadix/scripts/mxcppass.sh",
      creates => '/etc/maadix/status/mxcp',
      require => [File['mxcp password'],Class['openldap::server']],
    }

    exec { 'set jitsildap passw':
      command => "/bin/bash /etc/maadix/scripts/jitsildap.sh",
      creates => '/etc/maadix/status/jitsildap',
      require => [File['jitsildap password'],Class['openldap::server']],
    }

    exec { 'set zeyple passw':
      command => "/bin/bash /etc/maadix/scripts/zeyplepass.sh",
      creates => '/etc/maadix/status/zeyple',
      require => [File['zeyple password'],Class['openldap::server']],
    }

    #hardening / grub password
    ##grub conf
    file_line { 'grub cmdline CLASS':
      path      => '/etc/grub.d/10_linux',
      line      => 'CLASS="--class gnu-linux --class gnu --class os --unrestricted"',
      match     => '.*CLASS="--class gnu-linux --class gnu --class os.*$',
    } ->
    exec { 'set grub passw':
      command   => "/bin/bash /etc/maadix/scripts/grubpass.sh",
      creates   => '/etc/maadix/status/grub',
      logoutput => true,
      require   => File['grub password'],
    }

#    if defined('::mongodb::server') and defined(Class['::mongodb::server']) {
#      exec { 'set mongodb passw':
#        command => "/bin/bash /etc/maadix/scripts/mongodbpass.sh",
#        creates => '/etc/maadix/status/mongodb',
#        require => [File['mongodb password'],Class['mongodb::server']],
#      }
#    }

    ### scripts to send passwords by mail###

    ##password mail scripts directory
    file { '/etc/maadix/mail':
      ensure => directory,
      mode   => '0700',
    }

    ##welcome pass mail script for jessie/stretch/buster
    file { 'welcome mail password':
      path    => '/etc/maadix/mail/welcome.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/welcome-$lsbdistcodename.sh.erb"),
    }

    ##etherpad pass mail script
    file { 'etherpad mail password':
      path    => '/etc/maadix/mail/etherpad.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/etherpad_mail_password.sh.erb'),
    }

    ##mailman pass mail script
    file { 'mailman mail password':
      path    => '/etc/maadix/mail/mailman.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailman_mail_password.sh.erb'),
    }

    ##owncloud pass mail script
    file { 'owncloud mail password':
      path    => '/etc/maadix/mail/owncloud.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/owncloud_mail_password.sh.erb'),
    }

    ##rocketchat pass mail script
    file { 'rocketchat mail password':
      path    => '/etc/maadix/mail/rocketchat.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/rocketchat_mail_password.sh.erb'),
    }

    ##lool pass mail script
    file { 'lool mail password':
      path    => '/etc/maadix/mail/lool.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/lool_mail_password.sh.erb'),
    }

    ##discourse pass mail script
    file { 'discourse mail password':
      path    => '/etc/maadix/mail/discourse.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/discourse_mail_password.sh.erb'),
    }

    ##mailtrain pass mail script
    file { 'mailtrain mail password':
      path    => '/etc/maadix/mail/mailtrain.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrain_mail_password.sh.erb'),
    }

    ##coturn pass mail script
    file { 'coturn mail password':
      path    => '/etc/maadix/mail/coturn.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/coturn_mail_password.sh.erb'),
    }

    ##onlyoffice pass mail script
    file { 'onlyoffice mail password':
      path    => '/etc/maadix/mail/onlyoffice.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/onlyoffice_mail_password.sh.erb'),
    }

    ##jitsi pass mail script
    file { 'jitsi mail password':
      path    => '/etc/maadix/mail/jitsi.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/jitsi_mail_password.sh.erb'),
    }

    ##rainloop pass mail script
    file { 'rainloop mail password':
      path    => '/etc/maadix/mail/rainloop.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/rainloop_mail_password.sh.erb'),
    }

    ##odoo pass mail script
    file { 'odoo mail password':
      path    => '/etc/maadix/mail/odoo.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/odoo_mail_password.sh.erb'),
    }

  }

}
