class helpers::mongo (
  $enabled = str2bool($::mongodb_enabled),
  $version = '',
  $service = 'mongod',
  $remove  = $::mongodb_remove,
) {

  #upgrade mongo if present
  include helpers::mongo::upgrade

  validate_bool($enabled)

  if $enabled {

    #udate repo when version changes or at first install
    if $::mongodb_version != $version {
      if $::lsbdistcodename=='jessie'{
        exec { 'apt get update mongorepo':
          command => 'apt-get -o Acquire::Check-Valid-Until=false update && echo "mongorepo"',
          #unless  => 'apt-show-versions | grep mongodb-org-shell',
          before  => Package['mongodb_client'],
          require => Class['Mongodb::Repo::Apt'],
        }
      } else {
        exec { 'apt get update mongorepo':
          command => 'apt-get update && echo "mongorepo"',
          #unless  => 'apt-show-versions | grep mongodb-org-shell',
          before  => Package['mongodb_client'],
          require => Class['Mongodb::Repo::Apt'],
        }
      }
    }  

    Package <| title == "mongodb_server" |> {
      notify => Service['mongod'],
    }

    ##cert directory
    file { '/opt/mongod':
      ensure => directory,
      owner  => 'mongodb',
      group  => 'mongodb',
      mode   => '0755',
      require => Class['mongodb::server::install'],
    } ->
    file { '/opt/mongod/certs':
      ensure => directory,
      owner  => 'mongodb',
      group  => 'mongodb',
      mode   => '0755',
    } ->
    ##openssl conf to generate certs
    file { 'mongod certs conf':
      path    => '/opt/mongod/certs/csr.cnf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/mongod-selfsigned-certs.cnf'),
    } ->
    ##script to generate certs
    file { 'mongod certs script':
      path    => '/etc/maadix/scripts/mongod-selfsigned-certs.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/mongod-selfsigned-certs-$::lsbdistcodename.sh.erb"),
      require => [
                  File['/etc/maadix/scripts'],
                 ]
    } ->
    exec { 'generate mongod certs':
      command   => "/bin/bash /etc/maadix/scripts/mongod-selfsigned-certs.sh",
      logoutput => true,
      creates   => '/etc/maadix/status/mongod-selfsigned-certs',
      require   => [
                   File['/etc/maadix/status'],
                   ],
      before    => Class['mongodb::server::config'],
      notify    => Service['mongod'],
    }

    #extra mongod.conf not available in module
    #fips mode not supported
    #file_line{ "FIPSMode in mongod.conf":
    #  path     => '/etc/mongod.conf',
    #  line     => "net.ssl.FIPSMode: true",
    #}->
    #verbosity levels
    #file_line{ "systemLog.component.accessControl.verbosity in mongod.conf":
    #  path     => '/etc/mongod.conf',
    #  line     => "systemLog.component.accessControl.verbosity: 3",
    #}->
    #file_line{ "systemLog.component.command.verbosity in mongod.conf":
    #  path     => '/etc/mongod.conf',
    #  line     => "systemLog.component.command.verbosity: 3",
    #  before   => Class['mongodb::server::service'],
    #}

    class {'::mongodb::globals':
      manage_package_repo => true,
      version => $version,
      service_provider => 'systemd',
    }->
    class {'::mongodb::client': } ->
    class {'::mongodb::server': 
      handle_creds   => false,
      store_creds    => false,
      auth           => true,
      create_admin   => false,
      rcfile         => '/root/.mongorc.js',
      pidfilepath    => '/tmp/mongod.pid',
      manage_pidfile => true,
      journal        => true,
      #verbose
      verbose        => true,
      logappend      => true,
      #tls conf
      ssl            => true,
      ssl_key        => '/opt/mongod/certs/mongodb.pem',
      ssl_mode       => 'requireSSL',
      #replica
      replset        => 'rs01',
      storage_engine => 'wiredTiger',
      keyfile        => '/opt/mongod/certs/keyfile',      
    }->
    exec { 'set mongodb passw':
      command => "/bin/bash /etc/maadix/scripts/mongodbpass.sh",
      creates => '/etc/maadix/status/mongodb',
      require => [File['mongodb password'],Exec['mongodb passw']],
      logoutput => true,
    }->
    exec { 'initiate replicaset':
      command => "mongo admin --host localhost --port 27017 --ssl --sslCAFile /opt/mongod/certs/rootCA.crt --eval \"load('/root/.mongorc.js'); printjson(rs.initiate())\" && touch /etc/maadix/status/mongodb_rs_initiate",
      creates => '/etc/maadix/status/mongodb_rs_initiate',
      logoutput => true,
    }->

    ##replicaset user
    #doc: https://stackoverflow.com/a/42930607
    #replicaset passw file
    exec { 'replicaset user oplogger mongodb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/mongodboplogger'",
      creates => '/etc/maadix/mongodboplogger',
    } ->
    #script to create oplogger user
    file { 'oplogger mongodb script':
      path    => '/etc/maadix/scripts/mongodboplogger.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mongodboplogger.sh.erb'),
    } ->
    #create mongodb oplogger user
    exec { 'create mongodb oplogger user':
      command => "/bin/bash /etc/maadix/scripts/mongodboplogger.sh",
      creates => '/etc/maadix/status/mongodboplogger',
      logoutput   => true,
    }

    ##oom conf
    file { '/lib/systemd/system/mongod.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/mongod.service.d/local.conf':
      content => template('helpers/service_mongod_local.conf.erb'),
      notify  => Service['mongod'],
    }



  } else {

    ##mongod service stopped
    service { $service:
        ensure    => $enabled,
        name      => $service,
        enable    => $enabled,
    }

  }

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::mongo::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'mongodb':}
    helpers::remove {'mongodb':
      files      => ['/etc/maadix/status/update_mongodb_42-44',
                     '/etc/maadix/status/mongod-selfsigned-certs',
                     '/etc/maadix/status/mongodb',
                     '/etc/maadix/status/mongodb_rs_initiate',
                     '/etc/maadix/mongodboplogger',
                     '/etc/maadix/status/mongodboplogger',
                     '/root/.mongorc.js'
                    ],
      dirs       => ['/opt/mongod','/lib/systemd/system/mongod.service.d','/var/log/mongodb'],
      big_dirs   => ['/var/lib/mongodb'],
      repos      => ['mongodb'],
      users      => ['mongodb'],
      groups     => ['mongodb'],
      packages   => ['mongodb-org-server','mongodb-org-shell','mongodb-org-tools','mongodb-org-database-tools-extra'],
      notify     => [
                    Helpers::Resetldapgroup['mongodb'],
                    ]
    }
  }

}
