class helpers::mailtrain::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = $helpers::mailtrain::port,
  $node_version   = $helpers::mailtrain::node_version,
) inherits helpers::mailtrain {

  validate_bool($enabled)

  if $enabled {

    $matching	  = "/var/www/mailtrain/.nvm/versions/node/v$node_version/bin/node"
    $binary	  = "/var/www/mailtrain/.nvm/versions/node/v$node_version/bin/node"

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
      protocol_test => {
                       request => '/',
                       },
    }

    monit::check::service { 'mailtrain':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}

