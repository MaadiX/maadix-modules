class helpers::firewall (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    ##recursos firewall
    $firewall_rule = hiera_hash('firewall::rule',{})
    create_resources ('firewall', $firewall_rule)


  }

}
