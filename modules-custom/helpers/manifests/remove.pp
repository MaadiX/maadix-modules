define helpers::remove ( 
  $pre_commands  = undef,
  $packages      = undef, 
  $files         = undef, 
  $dirs          = undef,
  $big_dirs      = undef,
  $mysql_d       = undef,
  $mysql_u       = undef,  
  $psql_d        = undef,
  $psql_u        = undef,  
  $mongo_d       = undef,
  $mongo_u       = undef,  
  $users         = undef,
  $groups        = undef,
  $repos         = undef,
  $tidy          = undef,
  $docker_c      = undef,
  $docker_v      = undef,
  $docker_i      = undef,
  $post_commands = undef,
) {

  if $pre_commands{
    $pre_commands.each|String $value| {
      exec { $value:
        command   => $value,
        path      => ['/usr/bin','/usr/sbin','/bin','/sbin'],
        logoutput => true,
      }
    }
  }


  if $packages{
    $packages.each|String $value| {
      package { $value:
        ensure    => 'purged',
      }
    }
  }

  if $files{
    $files.each|String $value| {
      file { $value:
        ensure    => absent,
      }
    }
  }

  if $tidy{
    $tidy.each|String $key,String $value| {
      tidy { $key:
        recurse   => true,
        matches   => $value,
      }
    }
  }

  if $dirs{
    $dirs.each|String $value| {
      file { $value:
        ensure    => absent,
        recurse   => true,
        purge     => true,
        force     => true,
      }
    }
  }

  if $big_dirs{
    $big_dirs.each|String $value| {
      exec { "purge directory $value":
        command   => "rm -r $value",
        onlyif    => "test -d $value",
        path      => '/usr/bin:/bin',
        timeout   => 3600,
      }
    }
  }

  if $mysql_d{
    $mysql_d.each|String $value| {
      mysql::db { $value:
        ensure    => absent,
        user      => $value,
        password  => $value,
      }
    }
  }

  if $mysql_u{
    $mysql_u.each|String $value| {
      mysql_user { $value:
        ensure    => absent,
      }
    }
  }

  if $psql_d{
    $psql_d.each|String $value| {
      exec { "delete psql ddbb $value":
        command   => "dropdb --if-exists $value",
        path      => ['/usr/bin','/bin'],
        user      => 'postgres',
        cwd       => '/tmp',
      }
    }
  }

  if $psql_u{
    $psql_u.each|String $value| {
      exec { "delete psql user $value":
        command   => "dropuser --if-exists $value",
        path      => ['/usr/bin','/bin'],
        user      => 'postgres',
        cwd       => '/tmp',
      }
    }
  }

  if $mongo_d{
    $mongo_d.each|String $value| {
      exec { "delete mongo ddbb $value":
        command   => "mongo $value --quiet --host localhost --port 27017 --ssl --sslCAFile /opt/mongod/certs/rootCA.crt --eval \"load('/root/.mongorc.js'); db.dropDatabase()\"",
        path      => ['/usr/bin','/bin'],
      }
    }
  }

  if $mongo_u{
    $mongo_u.each|String $value| {
      exec { "delete mongo user $value":
        command   => "mongo $value --quiet --host localhost --port 27017 --ssl --sslCAFile /opt/mongod/certs/rootCA.crt --eval \"load('/root/.mongorc.js'); db.dropUser('$value')\"",
        path      => ['/usr/bin','/bin'],
      }
    }
  }

  if $users{
    $users.each|String $value| {
      user { $value:
        ensure    => absent,
      }
    }
  }

  if $groups{
    $groups.each|String $value| {
      group { $value:
        ensure    => absent,
      }
    }
  }

  if $repos{
    $repos.each|String $value| {
      apt::source { $value:
        ensure  => absent,
      }
    }
  }

  if $docker_c{
    $docker_c.each|String $value| {
      exec { "remove docker container $value":
        command   => "docker container rm $value",
        onlyif    => "docker ps --all | grep $value",
        path      => ['/usr/bin','/usr/sbin','/bin','/sbin'],
        logoutput => true,
      }
    }
  }

  if $docker_v{
    $docker_v.each|String $value| {
      exec { "remove docker volume $value":
        command   => "docker volume rm $value",
        onlyif    => "docker volume ls | grep $value",
        path      => ['/usr/bin','/usr/sbin','/bin','/sbin'],
        logoutput => true,
      }
    }
  }

  if $docker_i{
    $docker_i.each|String $value| {
      exec { "clean docker image $value":
        command   => "docker rmi -f $(docker images '$value' -a -q)",
        onlyif    => "docker images | grep $value",
        path      => ['/usr/bin','/usr/sbin','/bin','/sbin'],
        logoutput => true,
      }
    }
  }


  if $post_commands{
    $post_commands.each|String $value| {
      exec { $value:
        command   => $value,
        path      => ['/usr/bin','/usr/sbin','/bin','/sbin'],
        logoutput => true,
      }
    }
  }

}

