class helpers::apache (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    file{'/var/www':
      ensure => directory,
      owner  => 'www-data',
      group  => 'www-data',
    }

    #we need to purge old libapache2-mod-wsgi before installing new python3 version, to avoid errors later when Exec[purge configuration of uninstalled packages]
    package {'libapache2-mod-wsgi':
      ensure => 'purged',
      before => Class['apache::mod::wsgi'],
    }

    ##oom conf
    file { '/lib/systemd/system/apache2.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/apache2.service.d/local.conf':
      content => template('helpers/service_apache2_local.conf.erb'),
      notify  => Service['httpd'],
    }

  }

}





