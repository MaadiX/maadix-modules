class helpers::rocketchat (
  $enabled           = str2bool($::rocketchat_enabled),
  $domain            = '',
  $node_version      = '',
  $version           = '',
  $version_installed = '',
  $remove            = $::rocketchat_remove,
) {

  #required classes
  require helpers::mongo

  validate_bool($enabled)

  #todo check if domain is ok
  if $enabled {

    #rocketchat dependencies
    if ! defined(Package['graphicsmagick']) {
      ensure_resource('package','graphicsmagick', {
      ensure  => present,
      })
    }
    if ! defined(Package['g++']) {
      ensure_resource('package','g++', {
      ensure  => present,
      })
    }

    #rocketchat user
    user { 'rocketchat':
      ensure       => 'present',
      home         => '/var/www/rocketchat',
      managehome   => true,
      shell        => '/bin/false',
      system       => true,
      require      => Package['httpd'],
    }
    #apache proxy_wstunnel
    #todo, declare as class when updating puppetlabs-apache module
    #apache::mod { 'proxy_wstunnel': }

    #rocketchat mods
    file { 'apache-rocketchat':
        path    => '/etc/apache2/conf-available/rocketchat.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/rocketchat.conf.erb'),
        require => Class['apache'],
    }
    file { '/etc/apache2/conf.d/rocketchat.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/rocketchat.conf',
      notify => Service['httpd'],
      require => Class['apache'],
    }


    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/rocketchat",
        docroot_group   => 'rocketchat',
        docroot_owner   => 'rocketchat',
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
        require => [
                   User['rocketchat'],
                   ],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/rocketchat"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/rocketchat',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/rocketchat",
        directories              => [
                                      { path    => "/var/www/rocketchat",
                                        options => ['FollowSymLinks','MultiViews'],
                                      },
                                    ],
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        override                 => ['All'],
        rewrites                 => [
          {
            comment        => 'rocketchat rewrites websockets',
            rewrite_cond   => ['%{HTTP:Upgrade} =websocket [NC]'],
            rewrite_rule   => ['/(.*)           ws://localhost:4000/$1 [P,L]'],
          },
          {
            comment        => 'rocketchat rewrites non websockets',
            rewrite_cond   => ['%{HTTP:Upgrade} !=websocket [NC]'],
            rewrite_rule   => ['/(.*)           http://localhost:4000/$1 [P,L]'],
          }
        ],
        custom_fragment => 'ProxyPassReverse / http://localhost:4000/',
        notify => Class['apache::service'],
      }
    }

    #override nvn::node::install exec to avoid issues
    Exec <| title == "nvm install node version $node_version for rocketchat" |> {
      timeout  => 7200,
    }

    #rocketchat node
    nvm::install { 'rocketchat':
      home         => '/var/www/rocketchat',
      nvm_dir      => '/var/www/rocketchat/.nvm',
      profile_path => '/var/www/rocketchat/.bashrc',
      require      => [
                      Class['nvm'],
                      User['rocketchat'],
                      ]
    }->
    nvm::node::install {$node_version :
      user         => 'rocketchat',
      set_default  => true,
      nvm_dir      => '/var/www/rocketchat/.nvm',
      before       => Exec['rocketchat npm install'],
    }

    ##rocketchat mongo db and systemd

    #rocketchat passw file
    exec { 'rocketchat mongodb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/mongodbrocketchat'",
      creates => '/etc/maadix/mongodbrocketchat',
      require => [
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }

    #rocketchat script to create mongodb rocketchat ddbb
    file { 'rockechat mongodb script':
      path    => '/etc/maadix/scripts/mongodbrocketchat.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mongodbrocketchat.sh.erb'),
    }

    #rocketchat script to create systemd rocketchat
    file { 'rockechat systemd script':
      path    => '/etc/maadix/scripts/systemdrocketchat.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/systemdrocketchat-$::lsbdistcodename.sh.erb"),
    }


    #create mongodb rocketchat ddbb
    exec { 'create mongodb rocketchat':
      command => "/bin/bash /etc/maadix/scripts/mongodbrocketchat.sh",
      creates => '/etc/maadix/status/mongodbrocketchat',
      logoutput   => true,
      require => [
                  Exec['rocketchat mongodb passw'],
                  File['rockechat mongodb script'],
                  Exec['set mongodb passw'],
                 ],
      before  => Exec['rocketchat npm install'],
    }->
    #create systemd rocketchat
    exec { 'create systemd rocketchat':
      command => "/bin/bash /etc/maadix/scripts/systemdrocketchat.sh",
      logoutput   => true,
      require => [
                  Exec['rocketchat mongodb passw'],
                  Exec['replicaset user oplogger mongodb passw'],
                  File['rockechat systemd script'],
                  Exec['set mongodb passw'],
                 ],
      before  => Exec['rocketchat npm install'],
      subscribe   => [
                     File['rockechat systemd script'],
                     ],
      refreshonly => true,
      notify      => Service['rocketchat'],
    }

    #prepara to update if version changes
    if($version_installed != $version){
      notify{"updating rocketchat started":} ->
      exec { 'prepare updating rocketchat':
        command    => "service monit stop && service rocketchat stop && rm -r /var/www/rocketchat/bundle",
        logoutput  => true,
        onlyif     => "test -d /var/www/rocketchat/bundle && test -f /lib/systemd/system/rocketchat.service",
        before     => Archive["/var/www/rocketchat/rocket.chat-$version.tgz"],
      }   
    }

    #rocketchat build archive
    archive { "/var/www/rocketchat/rocket.chat-$version.tgz":
      ensure        => present,
      extract       => true,
      extract_path  => '/var/www/rocketchat',
      source        => "https://cdn-download.rocket.chat/build/rocket.chat-$version.tgz",
      creates       => '/var/www/rocketchat/bundle',
      cleanup       => false,
      user          => 'rocketchat',
      require       => User['rocketchat'],
    }->
    #rocketchat npm install
    exec { "rocketchat npm install":
      command     => "/bin/bash -c 'source /var/www/rocketchat/.nvm/nvm.sh && npm install'",
      cwd         => '/var/www/rocketchat/bundle/programs/server',
      user        => 'rocketchat',
      timeout     => 7200,
      creates     => "/var/www/rocketchat/bundle/programs/server/node_modules",
      notify      => Service['rocketchat'],
    }

    #set installed version
    if($version_installed != $version){
      exec { 'set rocketchat installed version':
        command    => "/etc/maadix/scripts/setldapgroupfield.sh rocketchat version $version",
        logoutput  => true,
        require    => [
                      Exec['rocketchat npm install'],
                      File['set ldap group field'],
                      ],
      } ->
      notify{"updating rocketchat finished":}
    }


    #rocketchat service / created in script / we leave it in templates for archiving
    #file { '/lib/systemd/system/rocketchat.service':
    #  ensure  => 'file',
    #  content => template("helpers/rocketchat.service.erb")
    #}

    service { 'rocketchat':
      ensure  => 'running',
      enable  => true,
      require => [
                 Service['mongod'],
                 Exec['rocketchat npm install'],
                 Exec['create mongodb rocketchat'],
                 Exec['create systemd rocketchat'],
                 ]
    }

    #rocketchat script to set default settings
    file { 'rocketchat settings script':
      path    => '/etc/maadix/scripts/rocketchatsettings.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/rocketchatsettings.sh.erb'),
    }
    #set default settings
    exec { 'set rocketchat settings':
      command => "/bin/bash /etc/maadix/scripts/rocketchatsettings.sh",
      creates => '/etc/maadix/status/rocketchatsettings',
      logoutput   => true,
      require => [
                  Service['rocketchat'],
                  File['rocketchat settings script'],
                 ],
    }

    #clean old rocketchat domain
    if($::rocketchatdomain_old){
      helpers::cleanvhost{"$::rocketchatdomain_old":
        docroot  => '/var/www/rocketchat',
      }
    }

  } else {

    ##rocketchat service stopped
    service { 'rocketchat':
    	ensure    => $enabled,
    	name      => rocketchat,
    	enable    => $enabled,
    }

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/rocketchat',
      }

    }

  } 

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::rocketchat::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'rocketchat':}
    helpers::remove {'rocketchat':
      files      => ['/etc/maadix/mongodbrocketchat',
                     '/etc/maadix/status/mongodbrocketchat',
                     '/etc/maadix/status/rocketchatsettings',
                     '/etc/apache2/conf-available/rocketchat.conf',
                     '/etc/apache2/conf.d/rocketchat.conf',
                     '/lib/systemd/system/rocketchat.service',
                     '/etc/maadix/scripts/systemdrocketchat.sh'
                    ],
      big_dirs   => ['/var/www/rocketchat'],
      mongo_d    => ['rocketchat'],
      mongo_u    => ['rocketchat'],
      users      => ['rocketchat'],
      notify     => [
                    Helpers::Resetldapgroup['rocketchat'],
                    Service['httpd']
                    ]
    }
  }


}

