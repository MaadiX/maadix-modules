class helpers::onehost::setup (
) {

  case $::lsbdistcodename {
    buster: {
      $package_name = 'opennebula-node'
    }
    bullseye: {
      $package_name = 'opennebula-node-kvm'
    }
    default: {
      fail("Unsupported platform: ${module_name} currently doesn't support ${::osfamily} or ${::operatingsystem}")
    }
  }


  #install package after apt_update
  Exec[apt_update] -> Package["$package_name"]

  package { 'dnsmasq':
    ensure => 'present',
  }->
  package { "$package_name":
    ensure => 'present',
    notify => Service['libvirtd'],
  }->
  file {'/var/tmp/one':
    owner  => 'oneadmin',
    group  => 'oneadmin',
    ensure => directory,
    mode   => '0750',
  }->
  file {'/var/lib/one':
    owner  => 'oneadmin',
    group  => 'oneadmin',
    ensure => directory,
    mode   => '0750',
  }->
  file {'/var/lib/one/.ssh/':
    owner  => 'oneadmin',
    group  => 'oneadmin',
    ensure => directory,
    mode   => '0700',
  }

  ##opennebula sudoers using sudo::conf stretch | buster | bullseye
  #compare to default provided by opennebula sudoers.d/opennebula and sudoers.d/opennebula-node or sudoers.d/opennebula-node-vm with every upgrade of one
  if ($::lsbdistcodename=='buster') or ($::lsbdistcodename=='bullseye'){
    sudo::conf { 'opennebula-node':
        priority => 20,
        content  => template("helpers/opennebula_sudoers-$::lsbdistcodename.erb"),
        require => Package["$package_name"],
    }
  }

  if $::lsbdistcodename=='buster'{
    #remove opennebula sudoers file privided from package to avoid duplication
    file { 'remove opennebula sudo from package':
        path      => '/etc/sudoers.d/opennebula',
        ensure    => absent,
        require => Package['opennebula-node'],
    }
    file { 'remove opennebula-node sudo from package':
        path      => '/etc/sudoers.d/opennebula-node',
        ensure    => absent,
        require => Package['opennebula-node'],
    }
  }

  if $::lsbdistcodename=='bullseye'{
    #remove opennebula sudoers file privided from package to avoid duplication
    file { 'remove opennebula sudo from package':
        path      => '/etc/sudoers.d/opennebula',
        ensure    => absent,
        require => Package['opennebula-node-kvm'],
    }
    file { 'remove opennebula-node-kvm sudo from package':
        path      => '/etc/sudoers.d/opennebula-node-kvm',
        ensure    => absent,
        require => Package['opennebula-node-kvm'],
    }
  }


  service { 'libvirtd':
    ensure    => running,
    name      => libvirtd,
    enable    => true,
    hasstatus => true,
  }

}
