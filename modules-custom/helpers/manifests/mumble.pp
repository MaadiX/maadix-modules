class helpers::mumble (
  Boolean $enabled  = str2bool($::mumble_enabled),
  String $domain    = '',
  String $logfile   = '/var/log/mumble-server/mumble-server.log',
  String $logdays   = '31',
  Boolean $remove   = $::mumble_remove,
) {

  validate_bool($enabled)

  if $enabled {

    #mumble package
    package { 'mumble-server':
      ensure  => 'latest',
      notify  => [
                 Service['mumble-server'],
                 ],
    } ->
    #set high priority
    file_line { 'MURMUR_USE_CAPABILITIES':
      path    => '/etc/default/mumble-server',
      line    => 'MURMUR_USE_CAPABILITIES=1',
      match   => 'MURMUR_USE_CAPABILITIES.*$',
    } ->
    #mumble conf
    file { 'mumble conf':
      path    => '/etc/mumble-server.ini',
      owner   => 'root',
      group   => 'mumble-server',
      mode    => '640',
      content => template("helpers/mumble-server.ini.erb"),
      notify  => Service['mumble-server'],
    }

    #mumble script to set passw
    file { 'mumble password':
      path    => '/etc/maadix/scripts/mumblepass.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mumblepass.sh.erb'),
    }

    ##mumble pass mail script
    file { 'mumble mail password':
      path    => '/etc/maadix/mail/mumble.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mumble_mail_password.sh.erb'),
    }

    #mumble domain and cert
    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/mumble",
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains               => [$domain],
        plugin                => 'webroot',
        webroot_paths         => ["/var/www/mumble"],
        #restart service after cert renewal
        deploy_hook_commands  => ['/usr/sbin/service mumble-server restart'],
        require               => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/mumble',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/mumble",
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        notify => Class['apache::service'],
      }
    } ->
    #mumble SuperUser password
    exec { 'set mumble password':
      command   => "/bin/bash /etc/maadix/scripts/mumblepass.sh",
      creates   => '/etc/maadix/status/mumble',
      logoutput => true,
      require   => [
                   Package['mumble-server'],
                   File['mumble password'],
                   ],
      notify    => Service['mumble-server'],
    }
    
    ##enable mumble-server
    service { 'mumble-server':
      ensure    => $enabled,
      name      => mumble-server,
      enable    => $enabled,
    }


    #clean old mumble domain
    if($::mumbledomain_old){
      helpers::cleanvhost{"$::mumbledomain_old":
        docroot  => '/var/www/mumble',
      }
    }

  } else {

    ##disable mumble-server
    service { 'mumble-server':
      ensure    => $enabled,
      name      => mumble-server,
      enable    => $enabled,
    }

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/mumble',
      }

    }


  }

  #disable old cron
  cron { 'mumble':
    command => '/etc/maadix/scripts/mumble-cron.sh',
    ensure  => 'absent',
    user    => 'root',
    minute  => '5',
    hour    => '*/12',
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'mumble':}
    helpers::remove {'mumble':
      packages   => ['mumble-server'],
      files      => ['/etc/maadix/status/mumble','/etc/default/mumble-server','/etc/mumble-server.ini','/etc/maadix/scripts/mumblepass.sh'],
      dirs       => ['/var/www/mumble','/var/log/mumble-server'],
      notify     => [
                    Helpers::Resetldapgroup['mumble'],
                    Service['httpd']
                    ]
    }
  }

}
