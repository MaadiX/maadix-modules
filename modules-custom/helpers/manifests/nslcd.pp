class helpers::nslcd (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    #nslcd group
    group { 'nslcd':
        ensure          => 'present',
        gid             => 303,
        system          => true,
        before          => Package['nslcd'],
    }

    #nslcd user with fixed uid/gid
    user { 'nslcd':
        ensure          => 'present',
        gid             => 303,
        home            => '/var/run/nslcd',
        managehome      => true,
        shell           => '/bin/false',
        system          => true,
        uid             => 303,
        before          => Package['nslcd'],
    }

    #override nslcd service, restart when ldap tls conf changes
    Service <| title == "nslcd" |> {
      subscribe => Ldapdn['openldap tls'],
    }

    # External checks.
    if defined('::monit') and defined(Class['::monit']) {
      include ::helpers::nslcd::external::monit
    }

  }

}

