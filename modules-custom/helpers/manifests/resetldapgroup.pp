define helpers::resetldapgroup {
  exec { "resetldapgroup ${title}":
    path        => ['/usr/bin'],
    command     => "/etc/maadix/scripts/resetldapgroup.sh $title",
    refreshonly => true,
  }
}
