class helpers::mailtrain2 (
  $enabled                  = str2bool($::mailtrain2_enabled),
  $version                  = '',
  $domain                   = '',
  $domain_old               = '',
  $sandboxdomain            = '',
  $sandboxdomain_old        = '',
  $publicdomain             = '',
  $publicdomain_old         = '',
  $adminuser                = '',
  $remove                   = $::mailtrain2_remove,
  $migrate                  = $::mailtrain2_migrate,
  $mailtrain1domain         = $::mailtraindomain,
) {


  validate_bool($enabled)

  if $enabled {

    #required classes
    require docker
    require helpers::docker

    #apache proxy_wstunnel
    #todo, declare as class when updating puppetlabs-apache module
    #apache::mod { 'proxy_wstunnel': }

    if ($mailtrain2_passwd_change){
      #grab mailtrain2 passwd from ldap
      exec { 'grab mailtrain2 passwd from ldap':
        command     => "/bin/bash -c 'ldapsearch -o ldif-wrap=no -H ldapi:// -Y EXTERNAL -LLL -s base -b \"ou=password,ou=mailtrain2,ou=groups,dc=example,dc=tld\" | grep status: | sed \"s|.*: \(.*\)|\1|\" > /etc/maadix/mailtrain2'",
        onlyif      => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=password,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status',
        notify      => Exec['set default mailtrain2 admin pass'],
      }
    }

    #mailtrain2 mods
    file { 'apache-mailtrain2':
        path    => '/etc/apache2/conf-available/mailtrain2.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/mailtrain2.conf.erb'),
        require => Class['apache'],
    }
    file { '/etc/apache2/conf.d/mailtrain2.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/mailtrain2.conf',
      notify => Service['httpd'],
      require => Class['apache'],
    }

    #mailtrain2 directory
    file { '/var/www/mailtrain2':
        ensure  => directory,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
    } ->
    #mailtrain2 docker-compose
    file { 'mailtrain2 docker-compose':
      path    => '/var/www/mailtrain2/docker-compose.yml',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrain2_docker-compose.yml.erb'),
    } ->
    #mailtrain2 docker-compose.override
    file { 'mailtrain2 conf docker-compose.override':
      path    => '/var/www/mailtrain2/docker-compose.override.yml',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrain2_docker-compose.override.yml.erb'),
      notify  => Service['mailtrain2'],
    } ->
    #mailtrain2 script to set default password in .env file
    file { 'mailtrain2 password script':
      path    => '/etc/maadix/scripts/mailtrain2pass.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrain2pass.sh.erb'),
    } ->
    #set default mailtrain2 admin password when install
    exec { 'set default mailtrain2 admin pass':
      command     => "/bin/bash /etc/maadix/scripts/mailtrain2pass.sh",
      refreshonly => true,
      logoutput   => true,
      notify      => Service['mailtrain2'],
    } -> 
    #systemd
    file { 'systemd mailtrain2':
      path    => '/lib/systemd/system/mailtrain2.service',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("helpers/mailtrain2.service.erb"),
    } ->
    ##oom conf
    file { '/lib/systemd/system/mailtrain2.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/mailtrain2.service.d/local.conf':
      content => template('helpers/service_mailtrain2_local.conf.erb'),
      notify  => Service['mailtrain2'],
    } ->
    #service
    service { 'mailtrain2':
      ensure  => 'running',
      enable  => true,
    } ->
    #set default mailtrain2 admin username when install
    exec { 'set default mailtrain2 admin username':
      command     => "/bin/sleep 120 && docker exec mailtrain2_mysql_1 mysql --password=mailtrain mailtrain -e \"update users set username='$adminuser' where id=1;\" && touch /etc/maadix/status/mailtrain2admin",
      creates     => '/etc/maadix/status/mailtrain2admin',
      logoutput   => true,
    }

    #migrate from mailtrain1
    if $migrate and $mailtrain1domain {
      file { 'mailtrain1 to 2 migration script':
        path       => '/etc/maadix/scripts/mailtrain2_migrate.sh',
        owner      => 'root',
        group      => 'root',
        mode       => '0700',
        content    => template('helpers/mailtrain2_migrate.sh.rb'),
        require    => Exec['set default mailtrain2 admin username'],
      } ->
      exec { 'migrate mailtrain1 to 2':
        command    => "/bin/bash /etc/maadix/scripts/mailtrain2_migrate.sh",
        logoutput  => true,
        timeout    => 3600,
      }
    }

    #vhosts
    if($domain){
      helpers::mailtrain2::domain {"$domain":
        domain    => "$domain",
        webroot   => '/var/www/mailtrain2/trusted',
        port      => '3000',
      }
    }

    if($sandboxdomain){
      helpers::mailtrain2::domain {"$sandboxdomain":
        domain    => "$sandboxdomain",
        webroot   => '/var/www/mailtrain2/sandbox',
        port      => '3003',
      }
    }

    if($publicdomain){
      helpers::mailtrain2::domain {"$publicdomain":
        domain    => "$publicdomain",
        webroot   => '/var/www/mailtrain2/public',
        port      => '3004',
      }
    }

    #cron
    file { 'mailtrain2 cron':
      path        => '/etc/maadix/scripts/mailtrain2_zonemta.sh',
      owner       => 'root',
      group       => 'root',
      mode        => '0700',
      content     => template('helpers/mailtrain2_zonemta.sh'),
    }->
    cron { 'mailtrain2 cron':
      command     => '/bin/bash /etc/maadix/scripts/mailtrain2_zonemta.sh > /dev/null',
      user        => 'root',
      minute      => '*/3',
    }


    #clean old mailtrain2 domain
    if($::mailtrain2domain_old){

      helpers::cleanvhost{"$::mailtrain2domain_old":
        docroot   => '/var/www/mailtrain2/trusted',
        notify    => Exec['update mailtrain2 conf'],
      }

      #update mailtrain2 ddbb when fqdn change
      file { 'mailtrain2 fqdnchange script':
        path        => '/etc/maadix/scripts/mailtrain2_fqdnchange.sh',
        owner       => 'root',
        group       => 'root',
        mode        => '0700',
        content     => template('helpers/mailtrain2_fqdnchange.sh.rb'),
        require     => Service['mailtrain2'],
      } ->
      exec { 'update mailtrain2 conf':
        command     => "/bin/bash /etc/maadix/scripts/mailtrain2_fqdnchange.sh",
        logoutput   => true,
        timeout     => 3600,
      } 
    }


    #clean old mailtrain2 sandbox domain
    if($::mailtrain2sandboxdomain_old){

      helpers::cleanvhost{"$::mailtrain2sandboxdomain_old":
        docroot   => '/var/www/mailtrain2/sandbox',
        notify    => Exec['update mailtrain2 sandbox conf'],
      }

      #update mailtrain2 ddbb when fqdn change
      file { 'mailtrain2 fqdnchange sandbox script':
        path        => '/etc/maadix/scripts/mailtrain2_fqdnchange_sandbox.sh',
        owner       => 'root',
        group       => 'root',
        mode        => '0700',
        content     => template('helpers/mailtrain2_fqdnchange_sandbox.sh.rb'),
        require     => Service['mailtrain2'],
      } ->
      exec { 'update mailtrain2 sandbox conf':
        command     => "/bin/bash /etc/maadix/scripts/mailtrain2_fqdnchange_sandbox.sh",
        logoutput   => true,
        timeout     => 3600,
      } 
    }


    #clean old mailtrain2 public domain
    if($::mailtrain2publicdomain_old){

      helpers::cleanvhost{"$::mailtrain2publicdomain_old":
        docroot   => '/var/www/mailtrain2/public',
        notify    => Exec['update mailtrain2 public conf'],
      }

      #update mailtrain2 ddbb when fqdn change
      file { 'mailtrain2 fqdnchange public script':
        path        => '/etc/maadix/scripts/mailtrain2_fqdnchange_public.sh',
        owner       => 'root',
        group       => 'root',
        mode        => '0700',
        content     => template('helpers/mailtrain2_fqdnchange_public.sh.rb'),
        require     => Service['mailtrain2'],
      } ->
      exec { 'update mailtrain2 public conf':
        command     => "/bin/bash /etc/maadix/scripts/mailtrain2_fqdnchange_public.sh",
        logoutput   => true,
        timeout     => 3600,
      } 
    }

  } else {

    ##mailtrain2 service stopped
    service { 'mailtrain2':
      ensure    => $enabled,
      name      => mailtrain2,
      enable    => $enabled,
    }

    if($domain!=''){
      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/mailtrain2/trusted',
      }
    }
    if($sandboxdomain!=''){
      helpers::cleanvhost{"$sandboxdomain":
        docroot  => '/var/www/mailtrain2/sandbox',
      }
    }
    if($publicdomain!=''){
      helpers::cleanvhost{"$publicdomain":
        docroot  => '/var/www/mailtrain2/public',
      }
    }

    #cron
    cron { 'mailtrain2 cron':
      ensure      => absent,
      command     => '/bin/bash /etc/maadix/scripts/mailtrain2_zonemta.sh > /dev/null',
      user        => 'root',
      minute      => '*/3',
    }

  } 


  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::mailtrain2::external::monit': enabled => $enabled}
  }


  # Uninstall
  if $remove {
    helpers::resetldapgroup{'mailtrain2':}
    helpers::remove {'mailtrain2':
      files      => ['/lib/systemd/system/mailtrain2.service',
                     '/etc/apache2/conf-available/mailtrain2.conf',
                     '/etc/apache2/conf.d/mailtrain2.conf',
                     '/etc/maadix/mailtrain2',
                     '/etc/maadix/status/mailtrain2admin',
                     '/etc/maadix/scripts/mailtrain2_zonemta.sh',
                    ],
      dirs       => ['/var/www/mailtrain2'],
      docker_c   => ['mailtrain2_mongo_1','mailtrain2_redis_1','mailtrain2_mysql_1','mailtrain2_mailtrain_1'],
      docker_v   => ['mailtrain2_mailtrain-files','mailtrain2_mailtrain-static','mailtrain2_mongo-data','mailtrain2_mysql-data','mailtrain2_redis-data','mailtrain_mailtrain-files','mailtrain_mailtrain-static','mailtrain_mongo-data','mailtrain_mysql-data','mailtrain_redis-data'],
      docker_i   => ['mariadb','redis','mailtrain/mailtrain','mongo'],
      notify     => [
                    Helpers::Resetldapgroup['mailtrain2'],
                    Service['httpd']
                    ]
    }
  }


}
