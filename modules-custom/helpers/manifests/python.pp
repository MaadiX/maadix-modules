class helpers::python (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {
 
  class { 'python' :
    version    => 'system',
    pip        => 'present',
    dev        => 'present',
    virtualenv => 'present',
  }

  }

}
