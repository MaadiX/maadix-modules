class helpers::etherpad::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 9001,
  $matching	  = 'src/node/server.js',
  $binary	  = '/var/www/etherpad/etherpad-lite/bin/run.sh',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'etherpad':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
    }
  }

}

