class helpers::puppet {

  ##puppet service stopped
  service { 'puppet':
    ensure    => false,
    enable    => false,
  }

  #runtimeout settings, for slow vms
  ini_setting {'puppet runtimeout':
    ensure            => present,
    section           => 'main',
    key_val_separator => '=',
    path              => '/etc/puppetlabs/puppet/puppet.conf',
    setting           => 'runtimeout',
    value             => '4h',
  }

}

