class helpers::pamldap (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    #uidnext schema
    file { 'uidnext':
        path    => '/etc/ldap/schema/uidnext.schema',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/uidnext.schema.erb'),
    }

    #ldapns schema
    file { 'ldapns':
        path    => '/etc/ldap/schema/ldapns.schema',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/ldapns.schema.erb'),
    }

    #openssh-lpk schema
    file { 'openssh-lpk':
        path    => '/etc/ldap/schema/openssh-lpk.schema',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/openssh-lpk.schema.erb'),
    }
    #/usr/bin/query_keys.sh
    file { 'query_keys.sh':
        path    => '/usr/bin/query_keys.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/query_keys.sh.erb'),
    }


    # doc: https://wiki.debian.org/LDAP/PAM
    file { 'pam_auth_update_mkhomedir_file':
        path    => '/usr/share/pam-configs/mkhomedir',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/mkhomedir.erb'),
        notify  => Exec['pam_auth_update']
    }

    #pam_auth_update command
    $pam_auth_update_command = $::lsbdistcodename ? {
      /(stretch|jessie)/     => 'pam-auth-update',
      'buster'               => 'pam-auth-update --enable mkhomedir',
    }

    exec { 'pam_auth_update':
      command     => $pam_auth_update_command,
      path        => [ '/bin', '/sbin', '/usr/bin', '/usr/sbin' ],
      refreshonly => true,
    }

  }

}





