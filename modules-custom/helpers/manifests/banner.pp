#hardening / banner
class helpers::banner (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #issue.net
    file { 'file issue.net':
        path    => '/etc/issue.net',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/issue.net.erb'),
    }
    #issue
    file { 'file issue':
        path    => '/etc/issue',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/issue.net.erb'),
    }

  }

}

