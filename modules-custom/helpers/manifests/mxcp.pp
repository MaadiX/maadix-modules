# mxcp
#
#
#

class helpers::mxcp (
  $fqdn                  = '',
  $maadix_cpanel_version = '',
  $cpanel_puppet_version = '',
  $system_cron_version   = '',
  $logmail               = '',
  $puppet_cert_domain    = '',
  $ldap_public_url       = false,
  $enabled               = true,
  $acl_version           = '',
) {

  if $enabled {

    ########################## install ###################################

    ##dependencies

    ensure_resource('package','python3-dev', {
      ensure  => present,
      })
    ensure_resource('package','apache2-dev', {
      ensure  => present,
      })
    ensure_resource('package','libpq-dev', {
      ensure  => present,
      })
    ensure_resource('package','libsasl2-dev', {
      ensure  => present,
      })
    ensure_resource('package','libncurses-dev', {
      ensure  => present,
      })

    ##user/group

    #mxcp group
    group { 'mxcp':
        ensure          => 'present',
        system          => true,
    }

    #mxcp user
    user { 'mxcp':
        ensure          => 'present',
        home            => '/usr/share/mxcp',
        managehome      => true,
        shell           => '/bin/false',
        system          => true,
    } ->
    #default .vimrc for mxcp user
    file { '/usr/share/mxcp/.vimrc':
        ensure          => file,
        owner           => 'mxcp',
        group           => 'mxcp',      
        content         => 'set mouse-=a',
    }

    #gpg root and mxcp
    file { 'mxcp: script gpgp root and mxcp':
        path    => '/etc/maadix/scripts/gpg-root.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/gpg-root.sh'),
        require => File['/etc/maadix/scripts']
    } ->
    exec { 'generate gp root and mxcp':
        user         => 'root',
        command      => '/bin/bash /etc/maadix/scripts/gpg-root.sh',
        logoutput    => true,
        path         => '/usr/bin:/usr/sbin:/bin',
        unless       => "gpg --list-keys | grep root@$::hostname",
    }

    ##trash
    file { '/home/.trash':
      ensure => directory,
      mode   => '0710',
      group   => 'mxcp',
      owner   => 'root',
    }
    file { '/home/.trash/backups':
      ensure => directory,
      mode   => '0750',
      group   => 'mxcp',
      owner   => 'root',
    }

    ##repo

    #mxcp repo
    vcsrepo { '/usr/share/mxcp/maadix-cpanel':
        ensure   => latest,
        provider => git,
        source   => 'https://gitlab.com/MaadiX/maadix-cpanel.git',
        user     => 'mxcp',
        revision => $maadix_cpanel_version,
        path     => '/usr/share/mxcp/maadix-cpanel',
        require  => [
                    User['mxcp'],
                    ],
    } ->
    #custom conf
    file { 'mxcp: custom conf':
        path    => '/usr/share/mxcp/maadix-cpanel/mxcp/custom_conf.py',
        owner   => 'mxcp',
        group   => 'mxcp',
        mode    => '0644',
        content => template('helpers/mxcp_custom_conf.py'),
        notify  => Service['mxcp'],
    }

    #auth ldap conf
    file { 'mxcp local_settings conf template':
      path    => '/usr/share/mxcp/maadix-cpanel/mxcp/local_settings_template.py',
      owner   => 'mxcp',
      group   => 'mxcp',
      mode    => '0600',
      content => template('helpers/local_settings_template.py.erb'),
      require    => [
                    Vcsrepo['/usr/share/mxcp/maadix-cpanel'],
                    ],
    }->
    exec { 'mxcp local_settings conf with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/mxcp | xargs -i sed \"s/mxcpPLACEHOLDER/{}/\" /usr/share/mxcp/maadix-cpanel/mxcp/local_settings_template.py > /usr/share/mxcp/maadix-cpanel/mxcp/local_settings.py'",
      require     => [
                     File['mxcp local_settings conf template'],
                     Exec['set mxcp passw'],
                     ],
      subscribe   => [
                     File['mxcp local_settings conf template'],
                     ],
      refreshonly => true,
      notify     => [
                    Service['mxcp'],
                    ],
    } ->
    #set permissions to conf file
    file { 'mxcp local_settings conf':
      path    => '/usr/share/mxcp/maadix-cpanel/mxcp/local_settings.py',
      owner   => 'mxcp',
      group   => 'mxcp',
      mode    => '0600',
      notify  => Service['mxcp'],
    }

    ##virtualenv

    $python3_version = $::lsbdistcodename ? {
      'stretch'    => '3.5',
      'buster'     => '3.7',
    }

    $virtualenv3 = "/usr/share/mxcp/venv3/"

    python::virtualenv { $virtualenv3:
      ensure       => present,
      version      => $python3_version,
      owner        => 'mxcp',
      group        => 'mxcp', 
      distribute   => false,
      #see https://setuptools.readthedocs.io/en/latest/history.html#v50-0-0
      environment  => ['SETUPTOOLS_USE_DISTUTILS=stdlib'],
    } ~>
    exec { 'downgrade setuptools mxcp':
      user         => 'mxcp',
      command      => "bash -c 'source ${virtualenv3}/bin/activate; pip install setuptools==49.6.0'",
      cwd          => "/usr/share/mxcp",
      logoutput    => true,
      refreshonly  => true,
      before	   => Exec['mxcp: pip install venv3 requirements'],
    }

    #pip install/update venv3
    exec { 'mxcp: pip install venv3 requirements':
      user        => 'mxcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; pip install -r /usr/share/mxcp/maadix-cpanel/requirements.txt'",
      cwd         => "/usr/share/mxcp/maadix-cpanel/",
      logoutput   => true,
      require     => [
                     Vcsrepo['/usr/share/mxcp/maadix-cpanel'],
                     ],
      subscribe   => [
                     Vcsrepo['/usr/share/mxcp/maadix-cpanel'],
                     ],
      refreshonly => true,
      notify     => [
                    Exec['mxcp: setup wsgi express'],
                    ],
      timeout    => 3600,

    }


    ########################## api migrations ###########################################

    if ($::api_url != 'https://api2.maadix.org/api/1.0') and ($::api_url !='') {

      #script to update ldap to use new api2
      file { 'api2 update script':
        path    => '/etc/maadix/scripts/api_migrate.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template("helpers/api_migrate.sh"),
      } ->
      exec { 'update ldap to use new api2':
        command => "/bin/bash /etc/maadix/scripts/api_migrate.sh",
        logoutput   => true,
      }
    
    }


    ########################## migrations and statics ###################################

    exec { 'mxcp: migrate':
      user        => 'mxcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; python manage.py migrate'",
      cwd         => "/usr/share/mxcp/maadix-cpanel/",
      logoutput   => true,
      require     => [
                     Exec['mxcp: pip install venv3 requirements'],
                     ],
      subscribe   => [
                     Vcsrepo['/usr/share/mxcp/maadix-cpanel'],
                     ],
      refreshonly => true,
    }

    exec { 'mxcp: static files':
      user        => 'mxcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; python manage.py collectstatic --noinput'",
      cwd         => "/usr/share/mxcp/maadix-cpanel/",
      logoutput   => true,
      require     => [
                     Exec['mxcp: pip install venv3 requirements'],
                     ],
      subscribe   => [
                     Vcsrepo['/usr/share/mxcp/maadix-cpanel'],
                     ],
      refreshonly => true,
    }


    ########################## service ###################################

    #custom extra express conf
    file { 'mxcp: extra express conf':
        path    => '/usr/share/mxcp/mxcp_extra_wsgi.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/mxcp_extra_wsgi.conf'),
        require => [
                   Exec['mxcp: pip install venv3 requirements'],
                   ],
        notify  => Exec['mxcp: setup wsgi express'],
    }

    #setup wsgi express
    exec { 'mxcp: setup wsgi express':
      user        => 'mxcp',
      command     => "bash -c 'source ${virtualenv3}/bin/activate; mod_wsgi-express setup-server mxcp/wsgi.py --host=127.0.0.1 --port=8000 --user mxcp --group mxcp --server-root=/usr/share/mxcp/express --log-directory /usr/share/mxcp/var --url-alias /static ./static --include-file /usr/share/mxcp/mxcp_extra_wsgi.conf'",
      cwd         => "/usr/share/mxcp/maadix-cpanel/",
      logoutput   => true,
      require     => [
                     Exec['mxcp: pip install venv3 requirements'],
                     ],
      refreshonly => true,
      notify      => Service['mxcp'],
    }


    #mxcp systemd
    file { 'mxcp: systemd service':
        path    => '/lib/systemd/system/mxcp.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/mxcp.service.erb'),
        notify  => Service['mxcp'],
    }

    #mxcp service
    service { 'mxcp':
        ensure    => running,
        name      => mxcp,
        enable    => true,
        hasstatus => true,
        require   => [
                     File['mxcp: systemd service'],
                     Exec['mxcp: setup wsgi express'],
                     ],
    }

    ######################### mxcp apache ################################

    #apache cpanel/ldap enabled hosts
    file { '/etc/apache2/ldap-enabled':
        ensure => 'directory',
        require => 'Class[Apache]',
    }

    ########################## migrate mailman domains to ldap  ###################################

    if $::mailman_enabled {
      #script to migrate mailman domains to ldap
      file { 'mxcp: migrate mailman domains to ldap script':
          path    => '/etc/maadix/scripts/mailman_domains_to_ldap.sh',
          owner   => 'root',
          group   => 'root',
          mode    => '0700',
          content => template('helpers/mailman_domains_to_ldap.sh.erb'),
      } ->
      exec { 'mxcp: migrate mailman domains to ldap':
          command    => "/etc/maadix/scripts/mailman_domains_to_ldap.sh",
          logoutput  => true,
          creates    => '/etc/maadix/status/mailman_domains_to_ldap',
      }
    }


    ########################## extra repos and files  ###################################

    #git repo cpanel-puppet
    vcsrepo { '/usr/share/cpanel-puppet':
      ensure   => latest,
      provider => git,
      source   => 'https://github.com/devwwb/cpanel-puppet.git',
      revision => $cpanel_puppet_version,
    }

    ##cpanel-puppet external modules
    #ldapdn
    vcsrepo { '/etc/puppetlabs/code/environments/production/modules/ldapdn':
      ensure   => latest,
      provider => git,
      source   => 'https://github.com/devwwb/puppet_ldapdn.git',
    }
    #ldapdn
    vcsrepo { '/etc/puppetlabs/code/environments/production/modules/posix_acl':
      ensure   => latest,
      provider => git,
      source   => 'https://github.com/voxpupuli/puppet-posix_acl.git',
      revision => $acl_version,
    }

    #git repo system-cron
    exec { 'update remote system-cron':
      command  => "/bin/bash -c 'cd /etc/maadix/system-cron && git remote set-url origin https://gitlab.com/MaadiX/system-cron.git'",
      onlyif   => "/bin/bash -c 'cd /etc/maadix/system-cron'",
      path     => '/usr/bin:/bin',
    } ->
    vcsrepo { '/etc/maadix/system-cron':
      ensure   => latest,
      provider => git,
      source   => 'https://gitlab.com/MaadiX/system-cron.git',
      revision => $system_cron_version,
      require  => File['/etc/maadix'],
    } ->
    #ldapsearch +x
    file { '/etc/maadix/system-cron/ldapsearch.sh' :
      ensure => present,
      owner  => root,
      group  => root,
      mode   => '0700',
    }

    #cron puppet file
    file { '/etc/maadix/system-cron/puppetcron.sh':
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/puppetcron.sh.erb'),
      require => Vcsrepo['/etc/maadix/system-cron'],
    }

    ########################## external facts  ##########################################

    file { '/opt/puppetlabs/facter/facts.d/public_ip.rb':
      ensure  => $ensure,
      owner   => 'root', 
      group   => 'root',
      mode    => '0755',
      content => template('helpers/external_fact_public_ip.rb'),
    }

    ########################## cron  ####################################################

    #cron puppet with output errors
    cron { 'puppet cron':
      command => '/bin/sleep `/usr/bin/numrandom /35..55/`s ; /bin/bash /etc/maadix/system-cron/puppetcron.sh > /dev/null',
      user    => 'root',
      minute  => '*/1',
    }

    #cron local puppet with output errors
    cron { 'local puppet cron':
      command => '/bin/sleep `/usr/bin/numrandom /5..25/`s ; /bin/bash /usr/share/cpanel-puppet/cron/cpanel-puppet.sh > /dev/null',
      user    => 'root',
      minute  => '*/1',
    }

    ####### remove old cron ##############

    #remove cron ldapsearch
    cron { 'cpanel cron':
      command => '/bin/sleep `/usr/bin/numrandom /0..60/`s ; /bin/bash /etc/maadix/system-cron/ldapsearch.sh > /dev/null 2>&1',
      user    => 'root',
      minute  => '*/5',
      ensure  => absent,
    }

    ## remove old cpanel cron
    cron { 'delete old cpanel cron':
      command => '/bin/sleep `/usr/bin/numrandom /0..60/`s ; /bin/bash /usr/share/cpanel/cron/ldapsearch.sh > /dev/null 2>&1',
      user    => 'root',
      minute  => '*/5',
      ensure  => absent,
    }
    cron { 'delete old puppet cron':
      command => '/bin/sleep `/usr/bin/numrandom /30..59/`s ; /bin/bash /usr/share/cpanel/cron/puppetcron.sh > /dev/null',
      user    => 'root',
      minute  => '*/1',
      ensure  => absent,
    }


    ######################### utility scripts ###########################################

    #release timestamp file
    file { '/etc/maadix/system-cron/releasetimestamp.sh':
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/releasetimestamp.sh.erb'),
      require => Vcsrepo['/etc/maadix/system-cron'],
    }

    ##scripts to lock local puppet modules
    #lock reboot module
    file { 'lock reboot script':
      path    => '/etc/maadix/scripts/setlockedreboot.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/setlockedreboot.sh.erb'),
    }

    #set attribute info to reboot in reboot module
    file { 'info to reboot script':
      path    => '/etc/maadix/scripts/setrebootinforeboot.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/setrebootinforeboot.sh.erb'),
    }

    ##script to setlock cpanel status to running
    file { 'cpanel status to running script':
      path    => '/etc/maadix/scripts/setrunningcpanel.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/setrunningcpanel.sh.erb'),
    }

    ##script to get group password from ldap
    file { 'generic script to get group password from ldap':
      path    => '/etc/maadix/scripts/getgrouppass.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/getgrouppass.sh.erb'),
    }

    ##oneshot init script to set status=ready to cpanel object after reboot
    file { 'remove old cpanel ready init script link in rc2.d':
      path   => '/etc/rc2.d/S04setreadycpanel',
      ensure => 'absent',
    } ->
    file { 'cpanel ready init script':
      path    => '/etc/init.d/setreadycpanel',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      content => template('helpers/setreadycpanel.erb'),
    } ->
    exec { 'update-rc.d for cpanel ready script':
      command     => "/bin/bash -c 'update-rc.d setreadycpanel defaults'",
      subscribe   => [
                      File['cpanel ready init script'],
                     ],
      refreshonly => true,
    } ->
    file { 'setreadycpanel conf folder':
      path    => '/etc/systemd/system/setreadycpanel.service.d',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      ensure  => directory
    } ->
    file { 'setreadycpanel conf, requiring slapd':
      path    => '/etc/systemd/system/setreadycpanel.service.d/deps.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/setreadycpanel.conf.erb'),
    }




    ##scripts to manage ldap objects
    #set groups fields/values
    file { 'set ldap group field':
      path    => '/etc/maadix/scripts/setldapgroupfield.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/setldapgroupfield.sh.erb'),
    }

    #set dn attributes
    file { 'set dn attributes':
      path    => '/etc/maadix/scripts/setldapdnattribute.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/setldapdnattribute.sh.erb'),
    }

    #update environment in ldap and puppet.conf
    file { 'update_environment.sh':
      path    => '/etc/maadix/scripts/update_environment.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/update_environment.sh.erb'),
    }

    #reset ldap group
    file { 'reset ldap group':
      path    => '/etc/maadix/scripts/resetldapgroup.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/resetldapgroup.sh.erb'),
    }

    #set php version in ldap
    file { 'setldapphpversion script':
      path    => "/etc/maadix/scripts/setldapphpversion.sh",
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/setldapphpversion.sh'),
    } 

    ######################### fail2ban #####################################################

    #mxcp fail2ban
    exec { 'mxcp: create initial log file if absent':
        command    => 'touch /usr/share/mxcp/var/error_log && chown mxcp:mxcp /usr/share/mxcp/var/error_log',
        logoutput  => true,
        creates    => '/usr/share/mxcp/var/error_log',
        require    => Exec['mxcp: setup wsgi express'],
    } -> 
    file { 'mxcp: fail2ban filter':
        path    => '/etc/fail2ban/filter.d/mxcp.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/mxcp_fail2ban.conf'),
        notify  => Service['fail2ban'],
    } ->
    file { 'mxcp: fail2ban jail':
        path    => '/etc/fail2ban/jail.d/mxcp.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/mxcp_fail2ban_jail.conf'),
        notify  => Service['fail2ban'],
    }

    ########################## service ###################################

    logrotate::rule { 'mxcp':
      path         => '/usr/share/mxcp/var/error_log',
      rotate       => 10,
      rotate_every => 'week',
      create       => true,
      create_mode  => '640',
      create_owner => 'mxcp',
      create_group => 'mxcp',
      size         => '100k',
      postrotate   => '/usr/sbin/service mxcp restart',
    }

    ######################### monit #####################################################

    if defined('::monit') and defined(Class['::monit']) {
      class {'helpers::mxcp::external::monit': enabled => true}
    }

  }
}


