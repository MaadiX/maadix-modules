class helpers::unbound::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 53,
  $matching	  = '/usr/sbin/unbound',
  $binary	  = '/usr/sbin/unbound',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system   = systemd

    $connection_test_tcp = {
      type         => 'connection',
      host         => $ip,
      protocol     => 'dns',
      socket_type  => 'TCP',
      port         => $port,
      action       => 'restart',
    }
    $connection_test_udp = {
      type         => 'connection',
      host         => $ip,
      protocol     => 'dns',
      socket_type  => 'UDP',
      port         => $port,
      action       => 'restart',
    }

    monit::check::service { 'unbound':
      init_system  => $init_system,
      matching	   => $matching,
      binary	   => $binary,
      tests        => [$connection_test_tcp, $connection_test_udp],
    }
  }

}

