#hardening / debsums
class helpers::debsums (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #package
    package { 'debsums':
      ensure => 'present',
    }

    #cron
    cron { 'weekly debsums':
      command => '/bin/sleep `/usr/bin/numrandom /0..1/`m ; /usr/bin/debsums | grep -v OK | grep -v coturn.service | grep -v mkhomedir | grep -v passwdqc | grep -v loc-10-network | grep -v openvpn@.service | grep -v mirrors.dat',
      user    => 'root',
      minute  => 0,
      hour    => 5,
      weekday => 6,
    }

  }

}

