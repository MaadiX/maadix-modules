##General postgresql resources, include this class, in apps with postgresql

class helpers::postgresql {

    #postgresql-server-dev version
    #jessie|stretch|buster
    if $::lsbdistcodename=='jessie'{
      $postgresql_server_dev_version = "9.4"
    }
    if $::lsbdistcodename=='stretch'{
      $postgresql_server_dev_version = "9.6"
    }
    if $::lsbdistcodename=='buster'{
      $postgresql_server_dev_version = "11"
    }

    #needed for psycopg2
    package { "postgresql-server-dev-$postgresql_server_dev_version":
      ensure => 'present',
    }

    # Monit
    if defined('::monit') and defined(Class['::monit']) {
      class {'helpers::postgresql::external::monit': }
    }

}
