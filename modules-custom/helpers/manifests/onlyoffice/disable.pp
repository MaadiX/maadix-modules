class helpers::onlyoffice::disable inherits helpers::onlyoffice {


  ##docker-onlyoffice service stopped
  service { 'docker-onlyoffice':
    ensure    => $enabled,
    name      => 'docker-onlyoffice',
    enable    => $enabled,
    }


  ##remove apache vhost
  if($domain!=''){

    helpers::cleanvhost{"$domain":
      docroot  => '/var/www/onlyoffice',
    }

  }

}

