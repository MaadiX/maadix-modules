class helpers::onlyoffice::install inherits helpers::onlyoffice {


  ##onlyoffice directories 
  file { '/opt/onlyoffice':
    ensure => directory,
  }

  #env file base
  file { 'onlyoffice env file base':
    path    => '/opt/onlyoffice/env_temp',
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => template('helpers/onlyoffice_env.erb'),
  }

  if ($onlyoffice_passwd_change){
    #grab onlyoffice passwd from ldap
    exec { 'grab onlyoffice passwd from ldap':
      command     => "/bin/bash -c 'ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b \"ou=password,ou=onlyoffice,ou=groups,dc=example,dc=tld\" | grep status: | sed \"s|.*: \(.*\)|\1|\" > /etc/maadix/onlyoffice'",
      onlyif      => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=password,ou=onlyoffice,ou=groups,dc=example,dc=tld" | grep status',
      notify      => Exec['onlyoffice env with secrets'],
    }
  }

  #env file with secrets
  exec { 'onlyoffice env with secrets':
    command     => "cat /etc/maadix/onlyoffice | perl -pe 's/\\&/\\\\\\\\&/g' | xargs -i sed 's/jwtsecretPLACEHOLDER/{}/' /opt/onlyoffice/env_temp > /opt/onlyoffice/env",
    umask       => '077',
    require     => [
                   File['onlyoffice env file base'],
                   ],
    subscribe   => [
                   File['onlyoffice env file base'],
                   ],
    refreshonly => true,
    notify      => Service['docker-onlyoffice'],
  }

  ##container pinned to tag
  #docker run -i -t -d -p 127.0.0.1:9080:80 --name=onlyoffice --restart=always --env-file /opt/onlyoffice/env \
  #-v /app/onlyoffice/DocumentServer/logs:/var/log/onlyoffice -v /app/onlyoffice/DocumentServer/data:/var/www/onlyoffice/Data \
  #-v /app/onlyoffice/DocumentServer/lib:/var/lib/onlyoffice -v /app/onlyoffice/DocumentServer/db:/var/lib/postgresql  \
  #onlyoffice/documentserver:5.1.3.35
  docker::run {'onlyoffice':
    tty               => true,
    ports             => '127.0.0.1:9080:80',
    extra_parameters  => ['--restart=always'],
    systemd_restart   => 'always',
    env_file          => '/opt/onlyoffice/env',
    #set fixed hostname to avoid issues with rabbitmq
    #https://stackoverflow.com/a/70625008
    hostname          => 'onlyofficeDC',
    #enable onlyoffice volumes mounting
    volumes           => [
                         'onlyoffice-log:/var/log/onlyoffice',
                         'onlyoffice-www-data:/var/www/onlyoffice/Data',
                         'onlyoffice-lib:/var/lib/onlyoffice',
                         'onlyoffice-db:/var/lib/postgresql',
                         'onlyoffice-redis:/var/lib/redis',
                         'onlyoffice-rabbitmq:/var/lib/rabbitmq',
                         'onlyoffice-fonts:/usr/share/fonts/truetype/custom',
                         ],
    image             => "onlyoffice/documentserver:$version",
    require           => [
                          Class['docker'],
                          Service['docker'],
                         ],

  }

  #add +r to service file to avoid warnings
  #exec { 'chmod +r docker-onlyoffice.service' :
  #  command  => "/bin/bash -c 'chmod 644 /etc/systemd/system/docker-onlyoffice.service'"
  #}

  ##remove old docker image just after new version is started to avoid issues with disk space
  if ($::onlyofficeversion) and ($::onlyofficeversion != $version) {

    #docker rmi -f $(docker images | grep onlyoffice/documentserver | grep $::onlyofficeversion | awk '{ print $3 }')
    exec { 'remove old onlyoffice image':
      command => "/bin/bash -c \"docker rmi -f $(docker images | grep onlyoffice/documentserver | grep $::onlyofficeversion | awk \'{ print \$3 }\')\"",
      require => Docker::Run['onlyoffice'],
    }

    #docker::image { "onlyoffice/documentserver:$::onlyofficeversion":
    #  ensure => 'absent',
    #}

  }

  ##apache proxy
  #onlyoffice apache mods
  file { 'apache-onlyoffice':
    path    => '/etc/apache2/conf-available/onlyoffice.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('helpers/onlyoffice.conf.erb'),
  } ->
  file { '/etc/apache2/conf.d/onlyoffice.conf':
    ensure => 'link',
    target => '/etc/apache2/conf-available/onlyoffice.conf',
    notify => Service['httpd'],
  }

  #onlyoffice domain
  #https://github.com/ONLYOFFICE/document-server-proxy/blob/master/apache/proxy-https-to-http.conf    
  if($domain){
    #apache vhosts and certs
    apache::vhost{"$domain" :
      servername      => $domain,
      port            => 80,
      docroot         => "/var/www/onlyoffice",
      docroot_mode    => '0755',
      priority        => 90,
      override        => ['All'],
      rewrites   => [
        {
          comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
          rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
          rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
        }
      ],
      notify => Class['apache::service'],
    } ~>
    Exec['reload apache'] ->
    letsencrypt::certonly{$domain :
      domains   => [$domain],
      plugin    => 'webroot',
      webroot_paths => ["/var/www/onlyoffice"],
      require       => Package['python-ndg-httpsclient'],
    } ->
    ini_setting { "certbot webroot_map $domain":
      ensure  => present,
      path    => "/etc/letsencrypt/renewal/$domain.conf",
      section => 'webroot_map',
      setting => "$domain",    
      value   => '/var/www/onlyoffice',
      section_prefix => '[[',
      section_suffix => ']]',
    } ->
    apache::vhost{"$domain-ssl" :
      servername               => $domain,
      port                     => 443,
      docroot                  => "/var/www/onlyoffice",
      directories              => [
                                    { path    => "/var/www/onlyoffice",
                                      options => ['FollowSymLinks','MultiViews'],
                                    },
                                  ],
      priority                 => 90,
      ssl                      => true,
      ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
      ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
      ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
      override                 => ['All'],
      custom_fragment          => '
          SetEnvIf Host "^(.*)$" THE_HOST=$1
          RequestHeader setifempty X-Forwarded-Proto https
          RequestHeader setifempty X-Forwarded-Host %{THE_HOST}e
          ProxyAddHeaders Off

          ProxyPassMatch (.*)(\/websocket)$ "ws://localhost:9080/$1$2"
          ProxyPass / "http://localhost:9080/"
          ProxyPassReverse / "http://localhost:9080/"
      ',
      notify => Class['apache::service'],
    }
  }

  ##clean old onlyoffice domain
  if($::onlyofficedomain_old){
    helpers::cleanvhost{"$::onlyofficedomain_old":
      docroot  => '/var/www/onlyoffice',
    }
  }

}

