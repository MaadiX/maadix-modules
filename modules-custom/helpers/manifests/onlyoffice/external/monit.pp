class helpers::onlyoffice::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 9080,
  $matching	  = '/usr/bin/docker start -a onlyoffice',
  $binary         = '/usr/bin/docker',
  $systemd_file   = '/etc/systemd/system/docker-onlyoffice.service',
  $program_start  = '/usr/local/bin/docker-run-onlyoffice-start.sh',
  $program_stop   = '/usr/bin/docker stop --time=0 onlyoffice'
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'docker-onlyoffice':
      init_system   => $init_system,
      systemd_file  => $systemd_file,
      program_start => $program_start,
      program_stop  => $program_stop,
      matching	    => $matching,
      binary        => $binary,
      tests         => [$connection_test, ],
    }
  }

}

