class helpers::onlyoffice::clean inherits helpers::onlyoffice {


  ##remove old docker image
  if ($::onlyofficeversion) and ($::onlyofficeversion != $version) {

    #docker rmi -f $(docker images | grep onlyoffice/documentserver | grep $::onlyofficeversion | awk '{ print $3 }')
    exec { 'remove old onlyoffice image':
      command => "/bin/bash -c \"docker rmi -f $(docker images | grep onlyoffice/documentserver | grep $::onlyofficeversion | awk \'{ print \$3 }\')\"",
    }

    #docker::image { "onlyoffice/documentserver:$::onlyofficeversion":
    #  ensure => 'absent',
    #}

  }


}

