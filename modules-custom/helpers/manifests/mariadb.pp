class helpers::mariadb (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    ##oom conf
    file { '/lib/systemd/system/mariadb.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/mariadb.service.d/local.conf':
      content => template('helpers/service_mariadb_local.conf.erb'),
      notify  => Service['mariadb'],
    }



  }

}





