class helpers::mailtrain2::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 3000,
) inherits helpers::mailtrain2 {

  validate_bool($enabled)

  if $enabled {

    $matching	  = "zone-mta"
    $binary	  = "/usr/local/bin/docker-compose"

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
      protocol_test => { request => '/' },
      tolerance     => { cycles =>5 },
    }

    monit::check::service { 'mailtrain2':
      init_system   => $init_system,
      matching	    => $matching,
      program_start => '/usr/sbin/service mailtrain2 restart',
      binary	    => $binary,
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}

