class helpers::limesurvey (
  $enabled           = str2bool($::limesurvey_enabled),
  $adminuser         = undef,
  $usermail          = undef,
  $version           = '',
  $version_installed = undef,
  $domain            = '',
  $remove            = $::limesurvey_remove,
) {

  #required classes
  require php
  require helpers::php

  validate_bool($enabled)

  if $enabled {


    ##limesurveydb user passw file
    exec { 'limesurveydb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/limesurveydb'",
      creates => '/etc/maadix/limesurveydb',
      require => Package['pwgen'],
    }

    if $adminuser != undef {
      ##limesurvey script
      file { 'limesurvey password':
        path    => '/etc/maadix/scripts/limesurveypass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/limesurveypass.sh.erb'),
      }
    }

    ##limesurvey pass mail script
    file { 'limesurvey mail password':
      path    => '/etc/maadix/mail/limesurvey.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/limesurvey_mail_password.sh.erb'),
    }

    #limesurvey directory
    file { '/var/www/limesurvey':
      ensure => directory,
      mode   => '0750',
      group   => 'fpmlimesurvey',
      owner   => 'fpmlimesurvey',
    } ->
    exec { 'limesurvey files owner':
      command      => 'chown -R fpmlimesurvey:fpmlimesurvey /var/www/limesurvey',
      timeout      => 28800,
      #don't log if not in debug mode
      #loglevel    => 'debug',
    }

    #limesurvey installation directory
    file { '/var/www/limesurvey/limesurvey':
      ensure => directory,
      mode   => '0750',
      group   => 'fpmlimesurvey',
      owner   => 'fpmlimesurvey',
    }

    
    #limesurvey source when installing from scratch
    if $version_installed == '' {
     archive { "/var/www/limesurvey/limesurvey-$version.tar.gz":
      ensure          => present,
      extract         => true,
      #extract to specific folder
      extract_command => 'tar xfz %s -C limesurvey --strip-components=1',
      extract_path    => '/var/www/limesurvey',
      source          => "https://github.com/LimeSurvey/LimeSurvey/archive/refs/tags/$version.tar.gz",
      creates         => '/var/www/limesurvey/limesurvey/application',
      cleanup         => false,
      user            => 'fpmlimesurvey',
      before          => File['limesurvey conf'],
     }
    }

    #limesurvey conf template
    file { 'limesurvey conf':
        path    => '/var/www/limesurvey/limesurvey/application/config/config.php_template',
        owner   => 'fpmlimesurvey',
        group   => 'fpmlimesurvey',
        mode    => '0644',
        content => template('helpers/limesurvey_config.php.erb'),
    } ->
    #limesurvey conf with secrets based on template
    exec { 'limesurvey conf with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/limesurveydb | xargs -i sed \"s/limesurveyPLACEHOLDER/{}/\" /var/www/limesurvey/limesurvey/application/config/config.php_template > /var/www/limesurvey/limesurvey/application/config/config.php && chown fpmlimesurvey:fpmlimesurvey /var/www/limesurvey/limesurvey/application/config/config.php && chmod 600 /var/www/limesurvey/limesurvey/application/config/config.php'",
      subscribe   => [ 
                     File['limesurvey conf'], 
                     ],
      refreshonly => true,
    } ->
    #limesurvey db and installation
    exec { 'limesurvey db and cli installation':
      command => "/bin/bash /etc/maadix/scripts/limesurveypass.sh",
      timeout => 7200,
      creates => '/etc/maadix/status/limesurvey',
      require => [
                 File['limesurvey password'],
                 ],
      notify  => Exec['set limesurvey installed version'],
    }

    #upgrade
    if($version_installed != $version) and ($version_installed != ''){

      $timestamp = Timestamp.new().strftime('%Y_%m_%d-%H_%M_%S')

      #mv old backups to trash
      exec { 'limesurvey: mv old backups to trash':
        command   => 'mv /var/www/limesurvey/limesurvey-* /home/.trash/backups/',
        onlyif    => 'ls -l /var/www/limesurvey/limesurvey-*',
        path      => '/usr/bin/:/bin/',
        require   => Exec['limesurvey db and cli installation'],
      } ->

      exec { "upgrading limesurvey: ddbb backup of $version_installed":
        command   => "mysqldump --defaults-extra-file=/root/.my.cnf limesurvey > /var/www/limesurvey/limesurvey-$version_installed-$timestamp.sql && chmod 700 /var/www/limesurvey/limesurvey-$version_installed-$timestamp.sql",
        logoutput => true,
        timeout   => 28800,
      } ->

      exec { "upgrading limesurvey: backup directory of version installed $version_installed":
        command   => "mv /var/www/limesurvey/limesurvey /var/www/limesurvey/limesurvey-$version_installed-$timestamp",
        path      => '/usr/bin/:/bin/',
        logoutput => true,
        timeout   => 7200,
      } ->

      exec { "upgrading limesurvey: download limesurvey-$version.tar.gz":
        command   => "wget https://github.com/LimeSurvey/LimeSurvey/archive/refs/tags/$version.tar.gz -O limesurvey-$version-$timestamp.tar.gz",
        cwd       => '/var/www/limesurvey',
        user      => 'fpmlimesurvey',
        timeout   => 7200,
      } ->

      exec { "upgrading limesurvey: extract limesurvey-$version.tar.gz":
        command   => "mkdir limesurvey && tar -xzf limesurvey-$version-$timestamp.tar.gz -C limesurvey --strip-components=1",
        cwd       => '/var/www/limesurvey',
        user      => 'fpmlimesurvey',
        timeout   => 7200,
      } ->

      exec { "upgrading limesurvey: copy old files and confs":
        command   => "rm -r limesurvey/upload && cp -Rp limesurvey-$version_installed-$timestamp/upload limesurvey/ && cp -p limesurvey-$version_installed-$timestamp/application/config/security.php limesurvey/application/config/ && cp -p limesurvey-$version_installed-$timestamp/application/config/config.php* limesurvey/application/config/",
        cwd       => '/var/www/limesurvey',
        user      => 'fpmlimesurvey',
        timeout   => 7200,
      } ->

      exec { "upgrading limesurvey: ddbb upgrade":
        cwd       => '/var/www/limesurvey/limesurvey/',
        user      => 'fpmlimesurvey',
        logoutput => true,
        timeout   => 28800,
        command   => 'php application/commands/console.php updatedb',
        notify    => [
                     Exec['set limesurvey installed version'],
                     ],
      }

    }

    #set installed version only run when first installation or upgrade are ok
    exec { 'set limesurvey installed version':
      command     => "/etc/maadix/scripts/setldapgroupfield.sh limesurvey version $version",
      logoutput   => true,
      refreshonly => true,
    }

    #apache vhosts and certs
    apache::vhost{"$domain" :
      servername      => $domain,
      port            => 80,
      docroot         => "/var/www/limesurvey/limesurvey",
      docroot_mode    => '0750',
      docroot_group   => 'fpmlimesurvey',
      docroot_owner   => 'fpmlimesurvey',
      priority        => 90,
      override        => ['All'],
      rewrites   => [
        {
          comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
          rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
          rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
        }
      ],
      notify => Class['apache::service'],
    } ~>
    Exec['reload apache'] ->
    letsencrypt::certonly{$domain :
      domains   => [$domain],
      plugin    => 'webroot',
      webroot_paths => ["/var/www/limesurvey/limesurvey"],
      require       => Package['python-ndg-httpsclient'],
    } ->
    ini_setting { "certbot webroot_map $domain":
      ensure  => present,
      path    => "/etc/letsencrypt/renewal/$domain.conf",
      section => 'webroot_map',
      setting => "$domain",
      value   => '/var/www/limesurvey/limesurvey',
      section_prefix => '[[',
      section_suffix => ']]',
    } ->
    apache::vhost{"$domain-ssl" :
      servername               => $domain,
      port                     => 443,
      docroot                  => "/var/www/limesurvey/limesurvey",
      docroot_mode             => '0750',
      docroot_group            => 'fpmlimesurvey',
      docroot_owner            => 'fpmlimesurvey',
      priority                 => 90,
      ssl                      => true,
      ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
      ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
      ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
      notify => Class['apache::service'],
      custom_fragment => '

## Directories, there should at least be a declaration for /var/www/html
<Directory "/var/www/limesurvey/limesurvey">

  Options +FollowSymLinks
  AllowOverride All

  #Not allow any connection using HTTP
  Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"

</Directory>

#php-fpm
<FilesMatch "\.php$">
  SetHandler "proxy:unix:/run/php/fpmlimesurvey-fpm.sock|fcgi://localhost"
</FilesMatch>

        ',
    }

    #clean old limesurvey domain
    if($::limesurveydomain_old){
      helpers::cleanvhost{"$::limesurveydomain_old":
        docroot  => '/var/www/limesurvey/limesurvey',
      }
    }


  } else {

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/limesurvey/limesurvey',
      }

    }

  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'limesurvey':}
    helpers::remove {'limesurvey':
      files      => ['/etc/maadix/limesurveydb','/etc/maadix/status/limesurvey','/etc/maadix/scripts/limesurveypass.sh'],
      big_dirs   => ['/var/www/limesurvey'],
      mysql_d    => ['limesurvey'],
      notify     => [
                    Helpers::Resetldapgroup['limesurvey'],
                    Service['httpd']
                    ]
    }
  }

}

