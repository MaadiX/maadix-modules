class helpers::opendkim (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    ##opendkim trusted hosts (localhost and loopback ipv6)
    opendkim::trusted{['127.0.0.1','::1']:}

    #override: folder perm /etc/opendkim/keys
    ##keys folder with 755 to allow others to read public keys
    File <| title == "/etc/opendkim/keys" |> {
      mode  => '0755',
    }

    ##fqdn opendkim
    opendkim::domain{["$::fqdn"]:
      selector => 'default',
      notify  => Service['opendkim'],
    } ->
    # Change perms of fqdn public key
    file { "/etc/opendkim/keys/$::fqdn/default.txt":
        owner   => 'opendkim',
        group   => 'opendkim',
        mode    => '0755',
    }

    #delete old fqdn opendkim
    if defined('$::fqdn_domain_old') {

      file { "/etc/opendkim/keys/$::hostname.$::fqdn_domain_old":
        ensure  => absent,
        force   => true,
        recurse => true,
        purge   => true,
        notify  => Service['opendkim'],
      }
      #remove old domain from KeyTable
      file_line { "remove $::fqdn_domain_old from KeyTable":
        ensure  => absent,
        path    => '/etc/opendkim/KeyTable',
        line    => "default._domainkey.$::hostname.$::fqdn_domain_old $::hostname.$::fqdn_domain_old:default:/etc/opendkim/keys/$::hostname.$::fqdn_domain_old/default.private",
        notify  => Service['opendkim'],
      }

      #remove old domain from SigningTable
      file_line { "remove $::fqdn_domain_old from SigningTable":
        ensure  => absent,
        path    => '/etc/opendkim/SigningTable',
        line    => "*@$::hostname.$::fqdn_domain_old default._domainkey.$::hostname.$::fqdn_domain_old",
        notify  => Service['opendkim'],
      }

    }

    ##existent  domains opendkim
    ##DISABLED, cpanel-puppet does this work when user activate opendkim for the vd domain in cpanel
    #opendkim::domain {$::cpaneldomains:
    #  selector => 'default',
    #  notify  => Service['opendkim'],
    #}

    # External checks.
    if defined('::monit') and defined(Class['::monit']) {
      include ::helpers::opendkim::external::monit
    }


  }

}
