class helpers::spamassassin::external::monit(
  $enabled = true,
  $port    = 783,
) {

  validate_bool($enabled)

  if $enabled {
    $pidfile = $::osfamily ? {
      'Debian' => $::lsbdistcodename ? {
        'jessie' => '/var/run/spamassassin.pid',
        'stretch' => '/var/run/spamd.pid',
        'buster' => '/var/run/spamd.pid',
        default  => undef,
      },
      default  => undef,

    }

    $init_system = $::operatingsystem ? {
      'Ubuntu' => $::lsbmajdistrelease ? {
        /(12\.|14\.)/ => 'sysv',
        default       => undef,
      },
      'Debian' => $::lsbdistcodename ? {
        'jessie' => 'systemd',
        'stretch' => 'systemd',
        'buster' => 'systemd',
        default  => undef,
      },
      default  => undef,
    }

    $test = {
      type     => connection,
      protocol => generic,
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'spamassassin':
      init_system => $init_system,
      pidfile     => $pidfile,
      tests         => [$test,],
      binary      => $::osfamily ? {
        'Debian' => '/usr/sbin/spamd',
        default  => undef,
      },
      program_start => '/usr/sbin/service spamassassin start',
      program_stop  => '/usr/sbin/service spamassassin stop',
      notify        => Service['monit'],
    }

  }

}


