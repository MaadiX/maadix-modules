class helpers::mailtrain (
  $enabled           = str2bool($::mailtrain_enabled),
  $domain            = '',
  $node_version      = '',
  $port              = '',
  $revision          = '',
  $adminuser         = '',
#  $version           = '',
#  $version_installed = '',
  $remove            = $::mailtrain_remove,
) {

  validate_bool($enabled)

  #todo check if domain is ok
  if $enabled {

    #mailtrain dependencies
    if ! defined(Package['imagemagick']) {
      ensure_resource('package','imagemagick', {
      ensure  => present,
      })
    }

    #mailtrain user
    user { 'mailtrain':
      ensure       => 'present',
      home         => '/var/www/mailtrain',
      managehome   => true,
      shell        => '/bin/false',
      system       => true,
      require      => Package['httpd'],
    }
    #apache proxy_wstunnel
    #todo, declare as class when updating puppetlabs-apache module
    #apache::mod { 'proxy_wstunnel': }

    #mailtrain mods
    file { 'apache-mailtrain':
        path    => '/etc/apache2/conf-available/mailtrain.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/mailtrain.conf.erb'),
        require => Class['apache'],
    }
    file { '/etc/apache2/conf.d/mailtrain.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/mailtrain.conf',
      notify => Service['httpd'],
      require => Class['apache'],
    }


    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/mailtrain",
        docroot_group   => 'mailtrain',
        docroot_owner   => 'mailtrain',
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
        require => [
                   User['mailtrain'],
                   ],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/mailtrain"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/mailtrain',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/mailtrain",
        directories              => [
                                      { path    => "/var/www/mailtrain",
                                        options => ['FollowSymLinks','MultiViews'],
                                      },
                                    ],
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        override                 => ['All'],
        custom_fragment => "
        ProxyPreserveHost On
        ProxyPass        '/' 'http://127.0.0.1:$port/'
        ProxyPassReverse '/' 'http://127.0.0.1:$port/'
        ",
        notify => Class['apache::service'],
      }
    }

    #override nvn::node::install exec to avoid issues / resource collector
    Exec <| title == "nvm install node version $node_version for mailtrain" |> {
      timeout  => 7200,
    }

    #mailtrain node
    nvm::install { 'mailtrain':
      home         => '/var/www/mailtrain',
      nvm_dir      => '/var/www/mailtrain/.nvm',
      profile_path => '/var/www/mailtrain/.bashrc',
      require      => [
                      Class['nvm'],
                      User['mailtrain'],
                      ]
    }->
    nvm::node::install {"$node_version-mailtrain" :
      version      => "$node_version",
      user         => 'mailtrain',
      set_default  => true,
      nvm_dir      => '/var/www/mailtrain/.nvm',
      before       => Exec['mailtrain npm install'],
      notify       => [
                      Exec['mailtrain npm install'],
                      Exec['mailtrain npm rebuild'],
                      ],
    }

    ##mailtrain mysql db and systemd

    #mailtrain passw file
    exec { 'mailtrain mysql passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/mysqlmailtrain'",
      creates => '/etc/maadix/mysqlmailtrain',
      require => [
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }

    #mailtrain script to set username and password
    file { 'mailtrain password script':
      path    => '/etc/maadix/scripts/mailtrainpass.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrainpass.sh.erb'),
    }

    #mailtrain script to set conf
    file { 'mailtrain conf script':
      path    => '/etc/maadix/scripts/mailtrainconf.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrainconf.sh.erb'),
    }

    #mailtrain script to create mysql mailtrain ddbb
    file { 'mailtrain mysql script':
      path    => '/etc/maadix/scripts/mysqlmailtrain.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mysqlmailtrain.sh.erb'),
    }

    #mailtrain script to create systemd mailtrain
    file { 'mailtrain systemd script':
      path    => '/etc/maadix/scripts/systemdmailtrain.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/systemdmailtrain.sh.erb'),
    }


    #create mysql mailtrain ddbb
    exec { 'create mysql mailtrain':
      command => "/bin/bash /etc/maadix/scripts/mysqlmailtrain.sh",
      creates => '/etc/maadix/status/mysqlmailtrain',
      logoutput   => true,
      require => [
                  Exec['mailtrain mysql passw'],
                  File['mailtrain mysql script'],
                 ],
      before  => Exec['mailtrain npm install'],
    }->
    #create systemd mailtrain
    exec { 'create systemd mailtrain':
      command => "/bin/bash /etc/maadix/scripts/systemdmailtrain.sh",
      logoutput   => true,
      require => [
                  Exec['mailtrain mysql passw'],
                  File['mailtrain systemd script'],
                 ],
      before  => Exec['mailtrain npm install'],
      subscribe   => [
                     File['mailtrain systemd script'],
                     ],
      refreshonly => true,
      notify      => Service['mailtrain'],
    }

    ##oom conf
    file { '/lib/systemd/system/mailtrain.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/mailtrain.service.d/local.conf':
      content => template('helpers/service_mailtrain_local.conf.erb'),
      notify  => Service['mailtrain'],
    }

/*
    #prepara to update if version changes
    if($version_installed != $version){
      notify{"updating mailtrain started":} ->
      exec { 'prepare updating mailtrain':
        command    => "service monit stop && service mailtrain stop && rm -r /var/www/mailtrain/bundle",
        logoutput  => true,
        onlyif     => "test -d /var/www/mailtrain/bundle",
        before     => Archive["/var/www/mailtrain/rocket.chat-$version.tgz"],
      }   
    }

*/


    #mailtrain script to set setting including mysql passwd
    file { 'mailtrain settings script':
      path    => '/etc/maadix/scripts/mailtrainsettings.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrainsettings.sh.erb'),
    }


    vcsrepo{ '/var/www/mailtrain/mailtrain':
      ensure   => present,
      provider => git,
      source   => 'https://github.com/Mailtrain-org/mailtrain.git',
      revision => "$revision",
      user          => 'mailtrain',
      require       => User['mailtrain'],
    } ->
    #mailtrain npm install
    exec { "mailtrain npm install":
      command     => "/bin/bash -c 'source /var/www/mailtrain/.nvm/nvm.sh && npm install --production'",
      timeout     => 7200,
      cwd         => '/var/www/mailtrain/mailtrain',
      user        => 'mailtrain',
      notify      => Service['mailtrain'],
      subscribe   => [
                     Vcsrepo['/var/www/mailtrain/mailtrain'],
                     ],
      refreshonly => true,
    } ->
    exec { "mailtrain npm rebuild":
      command     => "/bin/bash -c 'source /var/www/mailtrain/.nvm/nvm.sh && npm rebuild'",
      cwd         => '/var/www/mailtrain/mailtrain',
      user        => 'mailtrain',
      notify      => Service['mailtrain'],
      refreshonly => true,
    } ->
    file {'mailtrain settings template':
      path    => '/var/www/mailtrain/mailtrain/config/production.toml_template',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/mailtrainproduction.toml.erb'),
    } ->
    exec {'set mysql password in settings mailtrain':
      command => '/etc/maadix/scripts/mailtrainsettings.sh',
      subscribe   => [
                     File['mailtrain settings template'],
                     ],
      require     => File['mailtrain settings script'],
      refreshonly => true,
      notify      => Service['mailtrain'],
    }


    service { 'mailtrain':
      ensure  => 'running',
      enable  => true,
      require => [
                 Exec['mailtrain npm install'],
                 Exec['create mysql mailtrain'],
                 Exec['create systemd mailtrain'],
                 File['mailtrain settings template'],
                 Exec['set mysql password in settings mailtrain'],
                 ]
    } ->
    #update mailtrain user and password when install
    exec { 'update mailtrain user and pass':
      command => "/bin/bash /etc/maadix/scripts/mailtrainpass.sh",
      require => [
                 File['mailtrain password script'],
                 ],
      creates => '/etc/maadix/status/usermailtrain',
      logoutput   => true,
      notify      => Exec['update mailtrain conf'],
    } 

    #clean old mailtrain domain
    if($::mailtraindomain_old){
      helpers::cleanvhost{"$::mailtraindomain_old":
        docroot  => '/var/www/mailtrain',
        notify   => Exec['update mailtrain conf'],
      }
    }

    #update mailtrain conf when install or fqdn change
    exec { 'update mailtrain conf':
      command     => "/bin/bash /etc/maadix/scripts/mailtrainconf.sh",
      require     => File['mailtrain conf script'],
      logoutput   => true,
      refreshonly => true,
    } 


  } else {

    ##mailtrain service stopped
    service { 'mailtrain':
    	ensure    => $enabled,
    	name      => mailtrain,
    	enable    => $enabled,
    }

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/mailtrain',
      }

    }

  } 

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::mailtrain::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'mailtrain':}
    helpers::remove {'mailtrain':
      files      => ['/lib/systemd/system/mailtrain.service',
                     '/etc/apache2/conf-available/mailtrain.conf',
                     '/etc/apache2/conf.d/mailtrain.conf',
                     '/etc/maadix/status/mysqlmailtrain',
                     '/etc/maadix/status/usermailtrain',
                     '/etc/maadix/mysqlmailtrain',
                     '/etc/maadix/scripts/systemdmailtrain.sh',
                    ],
      dirs       => ['/lib/systemd/system/mailtrain.service.d'],
      big_dirs   => ['/var/www/mailtrain'],
      mysql_d    => ['mailtrain'],
      mysql_u    => ['mailtrain@%'],
      users      => ['mailtrain'],
      notify     => [
                    Helpers::Resetldapgroup['mailtrain'],
                    Service['httpd']
                    ]
    }
  }

}

