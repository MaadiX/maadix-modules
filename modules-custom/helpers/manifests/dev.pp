class helpers::dev {

  ## ldapvi

  #ldapvpi package
  if ! defined(Package['ldapvi']) {
    ensure_resource('package','ldapvi', {
      ensure  => present,
    })
  }

  #/etc/ldapvi.conf
  file { '/etc/ldapvi.conf':
    content => template('helpers/ldapvi.conf'),
    require => Package['ldapvi'],
  }

  ## phpldapadmin
  vcsrepo{ "/var/www/html/$::fqdn/phpldapadmin":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/breisig/phpLDAPadmin.git',
    revision => 'bbb2f6dc6671f676d3f47d03a7fcbbb5fa0beaf4',
  } ->
  file { "/var/www/html/$::fqdn/phpldapadmin/config/config.php":
    content => template('helpers/phpldapadmin_config.php'),
  }


}

