class helpers::ldap (
  $enabled = true,
  $public  = false,
) {

  validate_bool($enabled)

  if $enabled {

    ##cert directory
    file { '/etc/ldap/certs':
      ensure => directory,
      owner  => 'openldap',
      group  => 'openldap',
      mode   => '0755',
      require => Class['openldap::server'],
    }

    #ldap schemas
    file { '/etc/ldap/schema/gnupg-ldap-schema.ldif':
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/gnupg-ldap-schema.ldif.erb'),
    }

    ##script to generate certs
    file { 'openldap certs script':
      path    => '/etc/maadix/scripts/openldap-selfsigned-certs.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/openldap-selfsigned-certs.sh.erb'),
      require => [
                  File['/etc/maadix/scripts'],
                 ]
    }
    exec { 'generate openldap certs':
      command => "/bin/bash /etc/maadix/scripts/openldap-selfsigned-certs.sh",
      creates => '/etc/maadix/status/openldap-selfsigned-certs',
      require => [
                  File['/etc/ldap/certs'],
                  File['/etc/maadix/status'],
                 ],
    }

    #if ldap listen in the public ip with fqdn tls certs
    if $public {
      #directories to store live/archive certbot certs owned by opnldap
      file { '/etc/ldap/certs/live':
        ensure   => directory,
        owner    => 'openldap',
        group    => 'openldap',
        mode     => '0700',
        require  => File['/etc/ldap/certs'],
      }
      file { '/etc/ldap/certs/archive':
        ensure   => directory,
        owner    => 'openldap',
        group    => 'openldap',
        mode     => '0700',
        require  => File['/etc/ldap/certs'],
      }

      #post hook certbot script to expose fqdn certs to openldap user
      file { 'certbot slapd post hook':
        path     => '/etc/ldap/certs/certbot-post-hook.sh',
        owner    => 'root',
        group    => 'root',
        mode     => '0700',
        content  => template("helpers/certbot-post-hook.sh.erb"),
        require  => Package['slapd'],
      } ->
      #run script for first time
      exec { 'sync certbot fqdn certs with slapd':
        command  => "/etc/ldap/certs/certbot-post-hook.sh",
        creates  => "/etc/ldap/certs/live/$::fqdn",
        require  => Letsencrypt::Certonly["$::fqdn"],
      }

      #TLS_CACERT in /etc/ldap/ldap.conf
      file_line{ "TLS_CACERT in /etc/ldap/ldap.conf":
        ensure   => absent,
        path     => '/etc/ldap/ldap.conf',
        line     => "TLS_CACERT /etc/ldap/certs/rootCA.crt",
      }

      #tls openldap conf
      ldapdn{ "openldap tls":
        dn                => "cn=config",
        attributes        => [ "olcTLSCACertificateFile: /etc/ldap/certs/live/$::fqdn/fullchain.pem",
                               "olcTLSCertificateFile: /etc/ldap/certs/live/$::fqdn/cert.pem",
                               "olcTLSCertificateKeyFile: /etc/ldap/certs/live/$::fqdn/privkey.pem",
                               "olcLocalSSF: 256",
                               "olcSecurity: ssf=256" ],
        unique_attributes => ["olcTLSCACertificateFile", "olcTLSCertificateFile", "olcTLSCertificateKeyFile"],
        ensure            => present,
        require           => Exec['sync certbot fqdn certs with slapd'],
      }

    #if ldap listen on localhost only
    } else {

      #TLS_CACERT in /etc/ldap/ldap.conf
      file_line{ "TLS_CACERT in /etc/ldap/ldap.conf":
        path     => '/etc/ldap/ldap.conf',
        line     => "TLS_CACERT /etc/ldap/certs/rootCA.crt",
        notify   => Service['slapd'],
      }

      #tls openldap conf
      ldapdn{ "openldap tls":
        dn                => "cn=config",
        attributes        => [ "olcTLSCACertificateFile: /etc/ldap/certs/rootCA.crt",
                               "olcTLSCertificateFile: /etc/ldap/certs/localhost.crt",
                               "olcTLSCertificateKeyFile: /etc/ldap/certs/localhost.key",
                               "olcLocalSSF: 256",
                               "olcSecurity: ssf=256" ],
        unique_attributes => ["olcTLSCACertificateFile", "olcTLSCertificateFile", "olcTLSCertificateKeyFile"],
        ensure            => present,
        require           => Exec['generate openldap certs'],
      }

    }

    #Add schemas before access and index confs
    Openldap::Server::Schema <| |> ->  Openldap::Server::Access <| |> -> Openldap::Server::Dbindex <| |> 

    #systemd slapd service
    file { 'systemd slapd.service':
        path    => '/lib/systemd/system/slapd.service',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template("helpers/slapd.service.erb"),
        require => Package['slapd'],
        notify  => Service['slapd'],
    }

    #purge not declared acls
    #doc: https://github.com/voxpupuli/puppet-openldap#configuring-acpsacls
    #this IS NOT working, disabled until fixed in openldap module
    #resources { 'openldap_access':
    #  purge => true
    #}

    ##oom conf
    file { '/lib/systemd/system/slapd.service.d':
      ensure  => directory,
    } ->
    file { '/lib/systemd/system/slapd.service.d/local.conf':
      content => template('helpers/service_slapd_local.conf.erb'),
      notify  => Service['slapd'],
    }

  }

}
