class helpers::lool (
  $enabled           = str2bool($::lool_enabled),
  $domain            = '',
  $wopi              = '',
  $version           = '',
  $memproportion     = '',
  $remove            = $::lool_remove,
) {

  #service name, used for service and monit
  $service = $::lsbdistcodename ? {
    'jessie'              => 'libreoffice-online',
     /(stretch|buster)/   => 'coolwsd',
  }
  #protocol, used for monit
  $protocol = $::lsbdistcodename ? {
    'jessie'              => 'https',
     /(stretch|buster)/   => 'https',
  }

  validate_bool($enabled)

  if $enabled {


    ##lool vhost directory
    file { '/var/www/lool':
      ensure => directory,
      mode   => '0755',
    }

    #lool apache mods
    file { 'apache-lool':
        path    => '/etc/apache2/conf-available/lool.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/lool.conf.erb'),
        require => Class['apache'],
    }
    file { '/etc/apache2/conf.d/lool.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/lool.conf',
      notify => Service['httpd'],
      require => Class['apache'],
    }


    #install libreoffice-online in jesssie and collabora online in stretch/buster
    case $::lsbdistcodename {
      'jessie' : { 
        class { helpers::lool::libreoffice:
          domain        => $domain,
          wopi          => $wopi,
          version       => $version,
        } 
      }
      'stretch','buster': { 
        class { helpers::lool::collabora:
          domain        => $domain,
          wopi          => $wopi,
          memproportion => $memproportion,
        } 
      }
    }

    #clean old lool domain
    if($::looldomain_old){
      helpers::cleanvhost{"$::looldomain_old":
        docroot  => '/var/www/lool',
      }
    }

  } else {


    ##libreoffice-online / coolwsd service stopped
    service { $service:
    	ensure    => $enabled,
    	name      => $service,
    	enable    => $enabled,
    }

    if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/lool',
      }

    }

  } 

  # Monit
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::lool::external::monit': 
      enabled  => $enabled,
      service  => $service,
      protocol => $protocol,
    }
  }

  # Ensure old loolwsd Monit and Service are disabled
  if defined('::monit') and defined(Class['::monit']) {
    file { '/etc/monit/conf.d/20_loolwsd':
      ensure    => absent,
      notify    => Service['monit'],
    }
  }
  file { '/lib/systemd/system/loolwsd.service':
    ensure    => absent,
  } ~>
  exec { 'systemctl reset-failed loolwsd':
    refreshonly => true,
    returns     => [0,1],
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'lool':}
    helpers::remove {'lool':
      files      => ['/etc/apache2/conf-available/lool.conf','/etc/apache2/conf.d/lool.conf','/etc/maadix/status/cool-selfsigned-certs'],
      dirs       => ['/var/www/lool','/etc/coolwsd','/opt/ssl','/opt/cool','/opt/collaboraoffice'],
      big_dirs   => ['/var/cache/coolwsd'],
      packages   => ['hunspell',
                     'hunspell-es',
                     'hunspell-de-de-frami',
                     'hunspell-fr',
                     'hunspell-en-gb',
                     'hunspell-ca',
                     'coolwsd',
                     'collaboraoffice',
                     'code-brand',
                     'collaboraofficebasis-core',
                     'collaboraoffice-ure',
                     'collaboraofficebasis-ooofonts'
                    ],
      repos      => ['lool'],
      users      => ['cool'],
      notify     => [
                    Helpers::Resetldapgroup['lool'],
                    Service['httpd']
                    ]
    }
  }

}

