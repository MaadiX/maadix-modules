class helpers::mysql {

  #mysqldump settings
  ini_setting {'mysqldump max_allowed_packet':
    ensure            => present,
    section           => 'mysqldump',
    key_val_separator => '=',
    path              => '/etc/mysql/conf.d/mysqldump.cnf',
    setting           => 'max_allowed_packet',
    value             => '128M',
    require           => Class['mysql::server']
  }

}

