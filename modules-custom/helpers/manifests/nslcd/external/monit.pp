class helpers::nslcd::external::monit(
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {
    $pidfile = $::osfamily ? {
      'Debian' => '/var/run/nslcd/nslcd.pid',
    }

    $init_system = $::operatingsystem ? {
      'Ubuntu' => $::lsbmajdistrelease ? {
        /(12\.|14\.)/ => 'sysv',
        default       => undef,
      },
      'Debian' => $::lsbdistcodename ? {
        'jessie' => 'sysv',
        'stretch' => 'sysv',
        'buster' => 'sysv',
        default  => undef,
      },
      default  => undef,
    }


    monit::check::service { 'nslcd':
      init_system => $init_system,
      pidfile     => $pidfile,
      binary      => $::osfamily ? {
        'Debian' => '/usr/sbin/nslcd',
        default  => undef,
      },
      program_start => '/etc/init.d/nslcd start',
      program_stop  => '/etc/init.d/nslcd stop',
      notify        => Service['monit'],
    }

  }

}
