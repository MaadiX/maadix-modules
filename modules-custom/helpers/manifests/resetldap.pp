class helpers::resetldap {

  ##delete temporary ldap object / fqdn_domain_old
  exec { 'delete fqdn_domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=fqdn_domain_old,ou=conf,ou=cpanel,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=fqdn_domain_old,ou=conf,ou=cpanel,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / rocketchat domain_old
  exec { 'delete rocketchat domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=rocketchat,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=rocketchat,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / lool domain_old
  exec { 'delete lool domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=lool,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=lool,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / discourse domain_old
  exec { 'delete discourse domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=discourse,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=discourse,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / mailtrain domain_old
  exec { 'delete mailtrain domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=mailtrain,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=mailtrain,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / onlyoffice domain_old
  exec { 'delete onlyoffice domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=onlyoffice,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=onlyoffice,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / jitsi domain_old
  exec { 'delete jitsi domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=jitsi,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=jitsi,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / mumble domain_old
  exec { 'delete mumble domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=mumble,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=mumble,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / nextcloud domain_old
  exec { 'delete nextcloud domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=nextcloud,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=nextcloud,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / etherpad domain_old
  exec { 'delete etherpad domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=etherpad,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=etherpad,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / limesurvey domain_old
  exec { 'delete limesurvey domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=limesurvey,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=limesurvey,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / dolibarr domain_old
  exec { 'delete dolibarr domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=dolibarr,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=dolibarr,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / ifcviewer domain_old
  exec { 'delete ifcviewer domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=ifcviewer,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=ifcviewer,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / odoo domain_old
  exec { 'delete odoo domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=odoo,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=odoo,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / mailtrain2 domain_old
  exec { 'delete mailtrain2 domain_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=domain_old,ou=mailtrain2,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=domain_old,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / mailrain2 public domain_old
  exec { 'delete mailtrain2 public_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=public_old,ou=mailtrain2,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=public_old,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status',
  }

  ##delete temporary ldap object / mailrain2 sandbox_old
  exec { 'delete mailtrain2 sandbox_old':
    command => '/usr/bin/ldapdelete -H ldapi:/// -Y EXTERNAL "ou=sandbox_old,ou=mailtrain2,ou=groups,dc=example,dc=tld"',
    onlyif  => '/usr/bin/ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s base -b "ou=sandbox_old,ou=mailtrain2,ou=groups,dc=example,dc=tld" | grep status',
  }

  ########### send notification email if fqdn change ##########

  ##fqdn chage mail notificaction
  file { 'fqdn chage mail notificaction':
    path        => '/etc/maadix/mail/fqdn-notification.sh',
    owner       => 'root',
    group       => 'root',
    mode        => '0700',
    content     => template('helpers/fqdn-notification.sh.erb'),
  }
  ## send fqdn chage mail notificaction
  ## only when an fqdn changes is involved. don't send in server setup
  if defined('$::fqdn_domain_old') {
    exec { 'send fqdn chage mail notificaction':
      command     => '/etc/maadix/mail/fqdn-notification.sh',
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      require     => File['fqdn chage mail notificaction'],
    }
  }


  #clean php sessions
  Exec {'clean php sessions':
    command            => 'find /var/lib/php/sessions -type f -exec rm -f {} \;',
    refreshonly        => true,
  }

}
