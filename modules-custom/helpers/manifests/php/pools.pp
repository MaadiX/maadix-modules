class helpers::php::pools (
  $version     = '8.1',
  $pools       = $::php_fpm_pools,
  $pools_purge = $::php_fpm_pools_purge,
  $pools_apps  = $::php_fpm_pools_apps,
) {

  #add and configure web pools
  $pools.each |String $pool, Hash $conf| {

    #notify {"pooooool $pool $conf": }

    #allow pools to be disabled
    case $conf['type'] {
      'disabled': {
        $ensure = 'absent'
      }
      default: {
        $ensure = 'present'
      }
    }

    if ($facts['php_fpm_max'] >= $conf['max']){
      $max_spare = $conf['max']
    } else {
      $max_spare = $facts['php_fpm_max']
    }

    if ($facts['php_fpm_min'] >= $conf['max']){
      $min_spare = $conf['max']
    } else {
      $min_spare = $facts['php_fpm_min']
    }

    user { $pool:
      ensure        => 'present',
      managehome    => false,
      #system false, to avoid conflicts with os_hardening
      system        => false,
      #os_hardening/manifests/minimize_access.pp params
      shell         => '/usr/sbin/nologin',
      password      => '*',
      password_max_age => '200000',
    }

    php::fpm::pool{$pool:
      ensure                  => $ensure,
      user                    => $pool,
      group                   => $pool,
      listen_owner            => 'www-data',
      listen_group            => 'www-data',
      listen_mode             => '0660',
      listen                  => "/run/php/${pool}-fpm.sock",
      #child process mode static/dynamic/ondemand and max requests per child
      pm                      => $conf['type'],
      pm_max_requests         => 4000,
      #static/dynamic/ondemand / max childs
      pm_max_children         => $conf['max'],
      #dynamic / start-min-max childs
      pm_start_servers        => $min_spare,
      pm_min_spare_servers    => $min_spare,
      pm_max_spare_servers    => $max_spare,
      #ondemand / time to kill idle child
      pm_process_idle_timeout => '10s',
      notify                  => Exec['clean php sessions'],
    }

    User <| title == www-data |> { 
      groups        +> "$pool",
      notify        => Service['httpd'],
    }

  }

  #add and configure app pools
  $pools_apps.each |String $pool, Hash $conf| {


    #allow pools to be disabled, leaving fpm user/group and www-data groups unchanged
    case $conf['enabled'] {
      'disabled': {
        $ensure = 'absent'
      }
      default: {
        $ensure = 'present'
      }
    }

    if $ensure=='present' {

      #notify {"pooooool $pool $conf": }

      if ($facts['php_fpm_max'] >= $conf['max']){
        $max_spare = $conf['max']
      } else {
        $max_spare = $facts['php_fpm_max']
      }

      if ($facts['php_fpm_min'] >= $conf['max']){
        $min_spare = $conf['max']
      } else {
        $min_spare = $facts['php_fpm_min']
      }

      user { $pool:
        ensure        => 'present',
        managehome    => false,
        #system false, to avoid conflicts with os_hardening
        system        => false,
        #os_hardening/manifests/minimize_access.pp params
        shell         => '/usr/sbin/nologin',
        password      => '*',
        password_max_age => '200000',
      }

      php::fpm::pool{$pool:
        ensure                  => $ensure,
        user                    => $pool,
        group                   => $pool,
        listen_owner            => 'www-data',
        listen_group            => 'www-data',
        listen_mode             => '0660',
        listen                  => "/run/php/${pool}-fpm.sock",
        #child process mode static/dynamic/ondemand and max requests per child
        pm                      => $conf['type'],
        pm_max_requests         => 4000,
        #static/dynamic/ondemand / max childs
        pm_max_children         => $conf['max'],
        #dynamic / start-min-max childs
        pm_start_servers        => $min_spare,
        pm_min_spare_servers    => $min_spare,
        pm_max_spare_servers    => $max_spare,
        #ondemand / time to kill idle child
        pm_process_idle_timeout => '10s',
        notify                  => Exec['clean php sessions'],
      }

      User <| title == www-data |> {
        groups        +> "$pool",
        notify        => Service['httpd'],
      }

    } else {
      #disable unused pool apps
      php::fpm::pool{$pool:
        ensure       => $ensure,
      }
    
    }

  }

  #purge unused web pools
  $pools_purge.each |String $pool_purge| {

    #notify {"purge pooooool $pool_purge": }

    php::fpm::pool{$pool_purge:
      ensure        => absent,
      notify        => Exec['clean php sessions'],
    } ->
    exec { "restart fpm after removing group $pool_purge":
      command       => "service php$version-fpm restart",
    } ->
    exec { "remove group $pool_purge from www-data user":
      command       => "/usr/sbin/deluser www-data $pool_purge",
    } ->
    user { $pool_purge:
      ensure        => 'absent',
    } ->
    group { $pool_purge:
      ensure        => 'absent',
    } ->
    exec { "remove ldap conf of pool $pool_purge":
      command       => "/usr/bin/ldapdelete -H ldapi:/// -Y external 'ou=$pool_purge,ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld'",
      returns       => [0,32],
    }

  }

}
