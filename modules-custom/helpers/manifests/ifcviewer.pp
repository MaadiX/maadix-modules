# ifcviewer
#
#
#

class helpers::ifcviewer (
  $enabled           = str2bool($::ifcviewer_enabled),
  $domain            = '',
  $version           = '',
  $help              = '',
  $purge_days        = '15',
  $remove            = $::ifcviewer_remove,
  $custom_fragment   = undef,
) {

  validate_bool($enabled)

  if $enabled {

    #required classes
    require docker

    ########################## install ###################################

    ##ifcviewer repo
    vcsrepo { '/var/www/ifcviewer/ifcviewer':
        ensure   => latest,
        provider => git,
        source   => 'https://github.com/AECgeeks/ifc-pipeline',
        revision => $version,
        path     => '/var/www/ifcviewer/ifcviewer',
        notify   => Service['ifcviewer'],
    }

    ########################## overrides ################################

    #ifcviewer docker-compose.override.yml
    file { 'ifcviewer docker-compose overrides docker-compose.override.yml':
      path    => '/var/www/ifcviewer/ifcviewer/docker-compose.override.yml',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/ifcviewer_docker-compose.override.yml'),
      require => [
                 Vcsrepo['/var/www/ifcviewer/ifcviewer'],
                 ],
      notify  => Service['ifcviewer'],
    }

    file { '/var/www/ifcviewer/ifcviewer/overrides':
      ensure  => directory,
      require => [
                 Vcsrepo['/var/www/ifcviewer/ifcviewer'],
                 ],     
    } ->
    #ifcviewer entrypoint.sh
    file { 'ifcviewer entrypoint.sh':
      path    => '/var/www/ifcviewer/ifcviewer/overrides/entrypoint.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      content => template('helpers/ifcviewer_entrypoint.sh'),
      notify  => Service['ifcviewer'],
    } ->
    #ifcviewer index.html 
    file { 'ifcviewer index.html':
      path    => '/var/www/ifcviewer/ifcviewer/overrides/index.html',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/ifcviewer_index.html.erb'),
      notify  => Service['ifcviewer'],
    }

    ########################## service ###################################

    #ifcviewer systemd
    file { 'ifcviewer: systemd service':
      path    => '/lib/systemd/system/ifcviewer.service',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/ifcviewer.service.erb'),
      notify  => Service['ifcviewer'],
    }

    #ifcviewer service
    service { 'ifcviewer':
        ensure    => running,
        name      => ifcviewer,
        enable    => true,
        hasstatus => true,
        require   => [
                     Vcsrepo['/var/www/ifcviewer/ifcviewer'],
                     File['ifcviewer docker-compose overrides docker-compose.override.yml'],
                     File['ifcviewer: systemd service'],
                     ],
    }


    ######################### ifcviewer cron ##################################

    file { 'ifcviewer cron':
        path    => '/etc/cron.d/ifcviewer',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/ifcviewer_cron.erb'),
    }

    ######################### ifcviewer apache ################################

    #vhost custom fragment
    if $custom_fragment {
       $vhost_fragment = $custom_fragment
    } else {
       $vhost_fragment = @(EOF)
         ProxyRequests Off
         ProxyPass        '/' 'http://127.0.0.1:5000/'
         ProxyPassReverse '/' 'http://127.0.0.1:5000/'
       EOF
    }

    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/ifcviewer",
        docroot_group   => 'www-data',
        docroot_owner   => 'www-data',
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ~>
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/ifcviewer"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      ini_setting { "certbot webroot_map $domain":
        ensure  => present,
        path    => "/etc/letsencrypt/renewal/$domain.conf",
        section => 'webroot_map',
        setting => "$domain",
        value   => '/var/www/ifcviewer',
        section_prefix => '[[',
        section_suffix => ']]',
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/var/www/ifcviewer",
        directories              => [
                                      { path    => "/var/www/ifcviewer",
                                        options => ['FollowSymLinks','MultiViews'],
                                      },
                                    ],
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        override                 => ['All'],
        custom_fragment          => "$vhost_fragment",
        notify                   => Class['apache::service'],
      }
    }

    #clean old ifcviewer domain
    if($::ifcviewerdomain_old){
      helpers::cleanvhost{"$::ifcviewerdomain_old":
        docroot  => '/var/www/ifcviewer',
      }
    }

  } else {

    ##disable app   
    #ifcviewer service
    service { 'ifcviewer':
        ensure    => false,
        name      => ifcviewer,
        enable    => false,
        hasstatus => true,
    }

   if($domain!=''){

      helpers::cleanvhost{"$domain":
        docroot  => '/var/www/ifcviewer',
      }

    }

  }

  ######################### monit #####################################################
  if defined('::monit') and defined(Class['::monit']) {
    class {'helpers::ifcviewer::external::monit': enabled => $enabled}
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'ifcviewer':}
    helpers::remove {'ifcviewer':
      files         => ['/lib/systemd/system/ifcviewer.service','/etc/cron.d/ifcviewer'],
      big_dirs      => ['/var/www/ifcviewer'],
      #redis image already delcared in mailtrain2
      docker_i      => ['ifcviewer_frontend','postgres','debian'],
      notify        => [
                       Helpers::Resetldapgroup['ifcviewer'],
                       ]
    }
  }


}


