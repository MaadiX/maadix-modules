#hardening / arpwatch
class helpers::arpwatch (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #package
    package { 'arpwatch':
      ensure => 'present',
    }

    #service
    service { 'arpwatch':
      ensure  => 'running',
      enable  => true,
      require => [
                 Package['arpwatch'],
                 ]
    }


  }

}

