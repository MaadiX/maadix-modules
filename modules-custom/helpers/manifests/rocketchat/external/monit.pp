class helpers::rocketchat::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 4000,
  $node_version   = $helpers::rocketchat::node_version,
) inherits helpers::rocketchat {

  validate_bool($enabled)

  if $enabled {

    $matching	  = "/var/www/rocketchat/.nvm/versions/node/v$node_version/bin/node"
    $binary	  = "/var/www/rocketchat/.nvm/versions/node/v$node_version/bin/node"

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
      protocol_test => {
                       request => '/',
                       },
    }

    monit::check::service { 'rocketchat':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}

