class helpers::mongo::upgrade {

  if mongodb_version {

    ##script to upgrade mongodb from 4.2 to 4.4
    file { 'mongod upgrade script':
      path    => '/etc/maadix/scripts/update_mongodb_42-44.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/update_mongodb_42-44.sh.erb'),
      require => [
                  File['/etc/maadix/scripts'],
                 ]
    } ->
    exec { 'upgrade mongod':
      command   => "/bin/bash /etc/maadix/scripts/update_mongodb_42-44.sh",
      logoutput => true,
      timeout   => 3600,
      creates   => '/etc/maadix/status/update_mongodb_42-44',
      require   => [
                   File['/etc/maadix/status'],
                   ],
    }

  }

}





