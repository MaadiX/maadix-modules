class helpers::mongo::external::monit(
  $enabled        = true,
  $ip             = '127.0.0.1',
  $port           = 27017,
  $matching	  = '/usr/bin/mongod',
  $binary	  = '/usr/bin/mongod',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      host     => $ip,
      protocol => 'http',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'mongod':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
    }
  }

}

