# == Class: helpers
#
# Full description of class helpers here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'helpers':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class helpers {

 ##recursos ldapdn
 #movido a una clase independiente
 #$ldapdn_entry = hiera_hash('ldapdn::entry',{})
 #create_resources ('ldapdn', $ldapdn_entry)

 ##recursos postfix
 $postfix_config = hiera_hash('postfix::config',{})
 create_resources ('postfix::config', $postfix_config)

 ##recursos file_line
 $file_line = hiera_hash('file_line',{})
 create_resources ('file_line', $file_line)

 ##recurso apache::vhost
 $apache_vhost = hiera_hash('apache::vhost',{})
 create_resources ('apache::vhost', $apache_vhost)

 ##recurso tcpwrappers
 $tcpwrappers_allow = hiera_hash('tcpwrappers::allow',{})
 create_resources ('tcpwrappers::allow', $tcpwrappers_allow)
 $tcpwrappers_deny = hiera_hash('tcpwrappers::deny',{})
 create_resources ('tcpwrappers::deny', $tcpwrappers_deny)

 ##recursos firewall
 #movido a una clase independiente
 #$firewall_rule = hiera_hash('firewall::rule',{})
 #create_resources ('firewall', $firewall_rule)

 ##recursos letsencrypt
 #$letsencrypt_certonly = hiera_hash('letsencrypt::certonly',{})
 #create_resources ('letsencrypt::certonly', $letsencrypt_certonly)

 ##recursos apache
 #$apache_vhost = hiera_hash('apache::vhost',{})
 #create_resources ('apache::vhost', $apache_vhost)

 ##recursos limits
 $limits = hiera('limits::fragment', {})
 create_resources('limits::fragment', $limits)

 ##recursos sysctl
 $sysctl = hiera('sysctl', {})
 create_resources('sysctl', $sysctl)

# require ::helpers::nonsslvhosts
# require ::helpers::letsencryptcerts
# require ::helpers::sslvhosts

# if defined('::postfix::packages') and defined(Class['::postfix::packages']) {
#  anchor { 'helpers::begin': } ->
#   Class['::helpers::nonsslvhosts' ] ->
#   Class['::helpers::letsencryptcerts' ] ->
#   Class['::postfix::packages' ] ->
#   Class['::helpers::sslvhosts' ] ->
#  anchor { 'helpers::end': }
# }

 if defined('::letsencrypt') and defined(Class['::letsencrypt']) {
   require ::helpers::fqdncerts
 }

}

