#hardening / samhain
class helpers::samhain (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #custom script to install samhain without triggering service
    file { 'install samhain script':
        path    => '/etc/maadix/scripts/install_samhain.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/install_samhain.sh'),
    } ->
    #install samhain with /usr/sbin/policy-rc.d disallowing triggering samhain service
    exec { 'install samhain':
        command => '/etc/maadix/scripts/install_samhain.sh',
        unless  => 'dpkg -l | grep samhain',
    } ->
    #conf
    file { 'samhainrc':
      path    => '/etc/samhain/samhainrc',
      owner   => 'root',
      group   => 'root',
      content => template('helpers/samhainrc'),
    } ->
    #service, stopped by default
    service { 'samhain':
      ensure  => false,
      enable  => false,
    } ->
    #initialize database
    exec { 'init samhain file':
      command => "/bin/bash -c 'rm /var/lib/samhain/samhain_file && samhain -t init && touch /etc/maadix/status/samhain'",
      creates => '/etc/maadix/status/samhain',
      require => File['/etc/maadix/status'],
      timeout => 14400,
    }


  }

}

