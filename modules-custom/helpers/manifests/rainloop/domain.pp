define helpers::rainloop::domain (
  $domain = $name,
) {

    # Create domain conf
    file { "/var/www/rainloop/data/_data_/_default_/domains/${domain}.ini":
        owner   => 'mxcp',
        group   => 'mxcp',
        mode    => '0644',
        content => template('helpers/rainloop_domain.ini.erb'),
    }

}
