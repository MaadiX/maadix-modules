class helpers::onefrontend (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    include helpers::onefrontend::repo
    include helpers::onefrontend::setup

  anchor { 'helpers::onefrontend::begin': } ->
   Class['::helpers::onefrontend::repo' ] ->
   Class['::helpers::onefrontend::setup' ] ->
  anchor { 'helpers::onefrontend::end': }

  }

}





