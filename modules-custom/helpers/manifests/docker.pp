#hardening / docker
class helpers::docker (
  $enabled           = str2bool($::docker_enabled),
  $service           = 'docker',
  $remove            = $::docker_remove,
) {

  validate_bool($enabled)

  if $enabled {

    #hardening docker
    file { '/etc/docker/daemon.json':
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/daemon.json.erb'),
      notify  => Service['docker'],
      require => Class['docker::config'],
    }
    #we can't use DOCKER_CONTENT_TRUST with discourse nor spotify/docker-gc
    #file_line { 'docker content trust':
    #  path    => '/etc/environment',
    #  line    => 'DOCKER_CONTENT_TRUST=1',
    #  match   => '.*DOCKER_CONTENT_TRUST.*$',
    #  notify  => Service['docker'],
    #}


    #docker audit rules
    #enabled in next release
    /*
    file { '/etc/audit/rules.d/docker.audit.rules':
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/docker.audit.rules.erb'),
      notify  => Service['auditd'],
      require => [
                 File['/etc/audit/rules.d'],
                 ],
    }
    */

    ##docker unit socket started
    service { 'docker.socket':
        ensure    => $enabled,
        enable    => $enabled,
        require   => Class['docker::config'],
    }


  } else {


    ##docker unit socket stopped
    service { 'docker.socket':
        ensure    => $enabled,
        enable    => $enabled,
        require   => [
                     #add here all classes where containers are stopped
                     Class['helpers::discourse'],
                     Class['helpers::onlyoffice'],
                     Class['helpers::ifcviewer'], 
                     ],
    } ->
    ##docker service stopped
    service { $service:
        ensure    => $enabled,
        name      => $service,
        enable    => $enabled,
        notify    => Exec['remove docker iptables rules'],
    } ->
    ##docker containerd service stopped
    service { 'containerd':
        ensure    => $enabled,
        name      => 'containerd',
        enable    => $enabled,
    }

    #remove docker iptables rules and chains
    file { '/etc/maadix/scripts/docker-iptables.sh':
      owner       => 'root',
      group       => 'root',
      mode        => '0700',
      content     => template('helpers/docker-iptables.sh'),
      require     => [
                     File['/etc/maadix/scripts'],
                     ],
    } ->
    exec { 'remove docker iptables rules':
      command     => '/bin/bash /etc/maadix/scripts/docker-iptables.sh',
      refreshonly => true,
    }
  }

  # Uninstall
  if $remove {
    helpers::resetldapgroup{'docker':}
    helpers::remove {'docker':
      files         => ['/etc/default/docker'],
      dirs          => ['/etc/docker'],
      big_dirs      => ['/var/lib/docker','/opt/containerd','/var/lib/containerd'],
      packages      => ['docker-ce','docker-ce-cli','docker-ce-rootless-extras','docker-scan-plugin','containerd.io'],
      repos         => ['docker'],
      groups        => ['docker'],
      notify        => [
                       Helpers::Resetldapgroup['docker'],
                       ]
    }
  }

}
