class helpers::mxcp::external::monit(
  $enabled        = true,
  $port           = 8000,
  $matching	  = '/usr/share/mxcp/express/httpd.conf',
  $binary	  = '/usr/share/mxcp/express/apachectl',
) {

  validate_bool($enabled)

  if $enabled {

    $init_system = systemd

    $connection_test = {
      type     => 'connection',
      port     => $port,
      action   => 'restart',
    }

    monit::check::service { 'mxcp':
      init_system   => $init_system,
      matching	    => $matching,
      binary	    => $binary,
      tests         => [$connection_test, ],
      notify        => Service['monit'],
    }
  }

}

