class helpers::upgrades {

  case $::operatingsystem {
          #Debian with php sury repo
          'Debian': {
            $origins = ["origin=$::operatingsystem,codename=$::lsbdistcodename,label=$::operatingsystem-Security", 
                       "origin=$::operatingsystem,codename=$::lsbdistcodename,label=$::operatingsystem",
                       'site=packages.sury.org']
          }
          #Add ubuntu support
          'Ubuntu': {
            $origins = ['${distro_id}:${distro_codename}','${distro_id}:${distro_codename}-security']
          }
          default: {
            fail("Unsupported platform: ${module_name} currently doesn't support ${::osfamily} or ${::operatingsystem}")
          }
  }

  #sury repo keys
  if $::isvm {
    exec { 'update sury key':
      command   => 'apt-key adv --keyserver keyserver.ubuntu.com --recv-keys B188E2B695BD4743',
      logoutput => true,
    }
  }

  #upgrade one-context
  if $::isvm {
    exec { 'update one-context':
      command => 'wget https://github.com/OpenNebula/addon-context-linux/releases/download/v5.12.0.2/one-context_5.12.0.2-1.deb && dpkg -i one-context_*deb || apt-get install -fy',
      #only if installed and outdated
      onlyif  => 'apt-show-versions | grep one-context | grep 5.12.0.1',
      cwd     => '/tmp',
    }
  }

  #unattended upgrades auto ONLY for vms
  #TODO, include params in the manifest for better coding
  if $::isvm {
    class { 'unattended_upgrades':
      mail      => { 'to'            => 'root', 
                     'only_on_error' => false,
                   },
      enable    => 1,
      #allow upgrading kernels
      #blacklist => ['linux-image*'],
      upgrade   => 7,
      update    => 1,
      random_sleep => 0,
      verbose   => 0,
      upgradeable_packages => { download_only => 0,
                                debdelta => 0,
                              },
      options   => { force_confdef => true,
                     force_confold => true,
                   },
      #equivalent to 'apt-get autoremove': needed to purge old kernels with correspondent initrd.img from boot, to prevent /boot partition from getting full
      #doc: https://help.ubuntu.com/community/RemoveOldKernels
      auto      => { 'remove'      => true },
      origins   => $origins,
    }
  } else {
    class { 'unattended_upgrades':
      mail      => { 'to'            => 'root',
                     'only_on_error' => false,
                   },
      enable    => 1,
      blacklist => ['linux-image*'],
      upgrade   => 0,
      update    => 0,
      verbose   => 0,
      upgradeable_packages => { download_only => 0,
                                debdelta => 0,
                              },
      options   => { force_confdef => true,
                     force_confold => true,
                   },
      auto      => { 'remove'      => false },
      origins   => $origins,
    }
  }


  ##packages
  package { 'apt-listchanges':
    ensure => 'present',
  }

  package { 'apticron':
    ensure => 'present',
  }

  #install apt-transport-https only if it's not installed
  exec { 'install apt-transport-https':
    command => 'apt-get install -y apt-transport-https --force-yes',
    unless  => 'dpkg -l | grep apt-transport-https',
  }

  ##add random sleep time to cron daily
  file_line { 'random apticron cron':
      path    => '/etc/cron.d/apticron',
      line    => '26 3  * * *   root    /bin/sleep `/usr/bin/numrandom /0..150/`m ; if test -x /usr/sbin/apticron; then /usr/sbin/apticron --cron; else true; fi',
      match   => '.*usr/sbin/apticron.*$',
      require => Package['apticron'],
  }

  #remove puppet repo if present
  exec {'remove puppet repo':
    command => 'rm /etc/apt/sources.list.d/puppet.list',
    onlyif => "test -f /etc/apt/sources.list.d/puppet.list"
  }

  if $::lsbdistcodename=='jessie'{
    #in march 2019 jessie-backports change from ftp.debian.org to archive.debian.org and jessie-updates is deleted and merged with jessie repo
    exec { 'fix jesssie apt':
      command     => "/bin/bash -c 'echo \"Acquire::Check-Valid-Until \'false\';\" > /etc/apt/apt.conf.d/90ignore-release-date'",
      creates     => '/etc/apt/apt.conf.d/90ignore-release-date',
    }->    
    file { 'fix jessie repo script':
      path    => '/usr/local/bin/fixjessierepo.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/fixjessierepo.sh.erb'),
    } ~>
    exec { 'fix jessie repo':
      command     => "/bin/bash /usr/local/bin/fixjessierepo.sh",
      refreshonly => true,
    }

    exec { 'apt get update upgrades':
      command => 'apt-get -o Acquire::Check-Valid-Until=false update && echo upgrades',
    }
  } else {

    exec { 'apt get update upgrades':
      command => 'apt-get update && echo upgrades',
    }

    file {'/etc/apt/apt.conf.d/90ignore-release-date':
      ensure => absent,
    }

  }

  if $::isvm {
    ##unattended-upgrades
    exec { 'update packages from security repo':
      #command   => 'echo "unattended-upgrades FAKE"',
      #command   => '/usr/bin/unattended-upgrades --dry-run',
      command   => '/usr/bin/unattended-upgrades',
      logoutput => true,
      timeout   => 7200,
      require   => [Package[
                           'unattended-upgrades',
                           'apt-listchanges',
                          ],
                   Apt::Conf['unattended-upgrades'],
                   Apt::Conf['periodic'],
                   Apt::Conf['options'],
                   Exec['apt get update upgrades'],
                   ],
    } ->
    #notify cpanel when reboot is required, only if mxcp is installed
    exec { 'notify cpanel if reboot is required':
      command   => '/bin/bash -c "/etc/maadix/scripts/setrebootinforeboot.sh"',
      onlyif    => 'test -f /var/run/reboot-required && test -f /etc/maadix/scripts/setrebootinforeboot.sh',
      logoutput => true,
    }
  }

  #hardening / purge packages
  exec { 'purge configuration of uninstalled packages':
    command   => "apt-get remove --purge -y `dpkg --list | grep ^rc | /usr/bin/awk '{ print \$2; }'`",
    logoutput => true,
    timeout   => 7200,
    onlyif    => 'dpkg --list | grep ^rc'
  }

}

