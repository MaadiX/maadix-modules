#hardening / compilers
class helpers::compilers (
  $enabled           = true,
) {

  validate_bool($enabled)

  if $enabled {

    #change compilers permissions to allow apt postscripts to use them
    exec { "/bin/chmod 755 /usr/bin/x86_64-linux-gnu-as":
      onlyif  => "test -f /usr/bin/x86_64-linux-gnu-as"
    }
    exec { "/bin/chmod 755 /usr/bin/x86_64-linux-gnu-gcc-6":
      onlyif  => "test -f /usr/bin/x86_64-linux-gnu-gcc-6"
    }

  }

}

