class helpers::openvpn::external::monit(
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {
    $pidfile = $::osfamily ? {
      'Debian' => "/run/openvpn/$::fqdn.pid",
    }

    $init_system = $::operatingsystem ? {
      'Ubuntu' => $::lsbmajdistrelease ? {
        /(12\.|14\.)/ => 'sysv',
        default       => undef,
      },
      'Debian' => $::lsbdistcodename ? {
        'jessie' => 'sysv',
        'stretch' => 'systemd',
        'buster'  => 'systemd',
        default  => undef,
      },
      default  => undef,
    }

    monit::check::service { "openvpn_$::fqdn":
      init_system => $init_system,
      pidfile     => $pidfile,
      binary      => $::osfamily ? {
        'Debian' => '/usr/sbin/openvpn',
        default  => undef,
      },
      systemd_file  => "/lib/systemd/system/openvpn.service",
      program_start => "/usr/sbin/service openvpn@$::fqdn start",
      program_stop  => "/usr/sbin/service openvpn@$::fqdn stop",
      notify        => Service['monit'],
    }

  }

}


