class helpers::srs(
  $enabled        = true,
) {

  validate_bool($enabled)

  if $enabled {

    #srs
    package{'postsrsd':
      ensure  => present,
      require => Package['postfix'],
    } ->
    file_line { 'postsrsd forward domain':
      path    => '/etc/default/postsrsd',
      line    => "SRS_DOMAIN='$::fqdn'",
      match   => 'SRS_DOMAIN.*$',
      notify  => Service['postsrsd']
    } ->
    file_line { 'postsrsd excluded domain':
      path    => '/etc/default/postsrsd',
      line    => "SRS_EXCLUDE_DOMAINS=$::fqdn",
      match   => '.*SRS_EXCLUDE_DOMAINS.*$',
      notify  => Service['postsrsd']
    }
    service { 'postsrsd':
      ensure  => 'running',
      enable  => true,
      require => [
                 Package['postsrsd'],
                 ]
    }    

  }

}
