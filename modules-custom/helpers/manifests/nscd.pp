class helpers::nscd (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    #nscd package
    package { 'nscd':
      ensure => 'present',
    }

    #nscd conf
    file { 'nscd.conf':
        path    => '/etc/nscd.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/nscd.conf.erb'),
        require => Package['nscd'],
        notify  => Service['nscd'],
    }

    ##nscd service
    service { 'nscd':
        ensure    => running,
        name      => nscd,
        enable    => true,
    }

  }

}





