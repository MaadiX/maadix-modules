class helpers::ldapdn (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    ##recursos ldapdn
    #movido a una clase independiente
    $ldapdn_entry = hiera_hash('ldapdn::entry',{})
    create_resources ('ldapdn', $ldapdn_entry)


  }

}
