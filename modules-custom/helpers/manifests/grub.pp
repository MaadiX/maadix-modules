class helpers::grub (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    ##password directory
    file { '/etc/maadix':
      ensure => directory,
      mode   => '0700',
    }

    ##password status directory
    file { '/etc/maadix/status':
      ensure => directory,
      mode   => '0700',
    }

    ##password scripts directory
    file { '/etc/maadix/scripts':
      ensure => directory,
      mode   => '0700',
    }

    ##log directory
    file { '/etc/maadix/logs':
      ensure => directory,
      mode   => '0700',
    }

    ##conf directory
    file { '/etc/maadix/conf':
      ensure => directory,
      mode   => '0700',
    }

    ##pwgen package
    package { 'pwgen':
      ensure => 'present',
    }

    #hardening / grub password
    ##grub user passw file
    exec { 'grub passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/grub'",
      creates => '/etc/maadix/grub',
      require => Package['pwgen'],
    }

    #hardening / grub password
    ##grub pass script
    file { 'grub password':
      path    => "/etc/maadix/scripts/grubpass.sh",
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template("helpers/grubpass.sh.erb"),
    }

    #hardening / grub password
    ##grub conf
    file_line { 'grub cmdline CLASS':
      path      => '/etc/grub.d/10_linux',
      line      => 'CLASS="--class gnu-linux --class gnu --class os --unrestricted"',
      match     => '.*CLASS="--class gnu-linux --class gnu --class os.*$',
    } ->
    exec { 'set grub passw':
      command   => "/bin/bash /etc/maadix/scripts/grubpass.sh",
      creates   => '/etc/maadix/status/grub',
      logoutput => true,
      require   => File['grub password'],
    }

  }

}
