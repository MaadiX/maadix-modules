#doc: https://www.chriscowley.me.uk/blog/2016/05/10/letsencrypt-with-apache-and-puppet/

class helpers::fqdncerts (
  $enabled = true,
) {

  validate_bool($enabled)

  if $enabled {

    include helpers::fqdncerts::nonsslvhosts
    include helpers::fqdncerts::letsencryptcerts
    include helpers::fqdncerts::sslvhosts

    exec {'reload apache':
      command => 'service apache2 restart',
      refreshonly => true,
    }

  anchor { 'helpers::fqdncerts::begin': } ->
   Class['::helpers::fqdncerts::nonsslvhosts' ] ->
   Exec['reload apache'] ->
   Class['::helpers::fqdncerts::letsencryptcerts' ] ->
   Class['::postfix::packages' ] ->
   Class['::dovecot' ] ->
   Class['::helpers::fqdncerts::sslvhosts' ] ->
  anchor { 'helpers::fqdncerts::end': }


  }

}





