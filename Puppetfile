forge 'http://forge.puppetlabs.com'

##############################################################[ Vms conf ]###
mod 'nodes',
  :git    => 'gitolite3@git.maadix.net:x-nodes',
  :branch => :control_branch,
  :install_path => 'hieradata/inventories/vm'

##############################################################[ Puppet base ]###

#actualizar / OK actualizado por postfix a la 1.6.0
#puppet v6, actualizacion
mod 'camptocamp/augeas', '1.9.0'
mod 'puppetlabs/augeas_core', '1.0.5'
mod 'puppetlabs/puppetserver_gem', '1.1.1'

#puppet v6, actualizacion
mod 'herculesteam/augeasproviders_core', '2.6.0'

#actualizar stretch / buster / ok
mod 'herculesteam/augeasproviders_shellvar', '4.0.0'

#actualizar / OK actualizado por ssh a la 2.2.1 / luego por apache a la 5.3.0
#puppet v6, actualizacion
mod 'puppetlabs/concat', '7.1.1'

#actualizar / OK actualizado por letsencrypt
#puppet v6, actualizacion
mod 'puppetlabs/inifile', '5.2.0'

#actualizar / OK
#puppet v6, actualizacion
mod 'puppetlabs/stdlib', '6.6.0'

#actualizar / OK actualizado por letsencrypt
#puppet v6, actualizacion
mod 'puppetlabs/vcsrepo', '5.3.0'

#puppet v6, requerido por r10k
mod 'puppetlabs/ruby', '1.0.1'


############################################################[ Puppet master ]###

#puppet v6, actualizacion
mod 'puppetlabs/puppetdb', '7.5.0'

#puppet v6, actualizacion
mod 'puppet/puppetserver', '3.0.1'

#puppet v6, actualizacion
mod 'puppet/r10k', '8.3.0'

#actualizar / ok
#puppet v6, actualizacion
mod 'puppetlabs/firewall', '2.4.0'

#actualizar / ok
#puppet v6, actualizacion
mod 'puppetlabs/postgresql', '6.6.0'


##############################################################[ Development ]###

mod 'actionjack/mailcatcher', '0.1.11'


##########################################################[ System defaults ]###

#actualizar stretch / buster / ok
#latest 7.6.0 gives dependency cycle error
mod 'puppetlabs/apt', '6.3.0'

#no existe en sbitio, sustituido por helpers::backports
#mod 'apt_extras',
#  :git => 'https://github.com/sbitio/puppet-apt_extras.git'

#actualizar stretch / buster / ok
mod 'defaults',
  :git => 'https://github.com/sbitio/puppet-defaults.git',
  :ref => '004ad10'

#actualizar stretch / buster / ok
mod 'ducktape',
  :git => 'https://github.com/sbitio/puppet-ducktape.git',
  :ref => 'bfe2afd'

mod 'thomasvandoren/etckeeper', '0.0.9'

#actualizar stretch / buster / bullseye
mod 'puppet-fail2ban', '4.1.0'
mod 'puppet-extlib', '5.0.0'

#actualizar / OK
mod 'adamcrews/mlocate', '0.4.0'

#actualizar stretch / buster / ok
mod 'puppetlabs/ntp', '8.4.0'

mod 'pam',
  :git => 'https://github.com/sbitio/puppet-pam.git'

#actualizar stretch / buster / ok
mod 'saz/resolv_conf', '4.2.1'

#actualizar stretch / buster / ok
mod 'saz/ssh', '6.2.0'

#actualizar stretch / buster / ok
mod 'saz/sudo', '6.0.0'

#actualizar stretch / buster / ok
mod 'saz/vim', '2.7.0'

#actualizar stretch / buster / ok
mod 'puppet/unattended_upgrades', '5.1.0'

mod 'danfoster/sysfs', '0.2.0'

#####################################################[ Logging & Monitoring ]###

#actualizar stretch / buster / ok
mod 'logcheck',
  :git => 'https://github.com/sbitio/puppet-logcheck.git',
  :ref => '18da98e'

#actualizar stretch / buster / ok
mod 'puppet/logrotate', '5.0.0'

#actualizar stretch / buster / ok
mod 'logwatch',
  :git => 'https://github.com/sbitio/puppet-logwatch.git'

#actualizar stretch / buster / ok
mod 'monit',
  :git => 'https://github.com/sbitio/puppet-monit.git',
  :ref => 'db118f2'

#actualizar
mod 'munin',
  :git => 'https://github.com/sbitio/puppet-munin.git'

#actualizar stretch / buster / ok
mod 'puppet-rsyslog', '5.0.1'

##################################################################[ Webapps ]###

#actualizar stretch / buster / ok
mod 'puppetlabs/apache', '5.5.0'

# We want always the latest code.
mod 'drush',
  :git => 'https://github.com/jonhattan/puppet-drush.git'

mod 'saz/memcached', '2.8.1'

#actualizar / ok
#puppet v6
#actualizar bullseye
mod 'puppetlabs/mysql', '13.1.0'
mod 'puppetlabs/resource_api', '1.1.0'

#nuevo, lo trae mysql y lo pide apache
#puppet v6
mod 'puppetlabs/translate', '2.2.0'

#actualizar stretch / buster / ok
mod 'puppet/php', '8.0.2'

#actualizar stretch / buster / ok / requerido por php
mod 'puppet/zypprepo', '4.0.1'

#actualizar
mod 'arioch/redis', '1.2.2'

mod 'solr',
  :git => 'https://github.com/sbitio/puppet-solr.git'

mod 'tomcat',
  :git => 'https://github.com/sbitio/puppet-tomcat.git'

mod 'webapp',
  :git => 'https://github.com/sbitio/puppet-webapp.git'


######################################################################[ VPN ]###

#actualizar stretch / buster / ok
mod 'puppet/openvpn', '8.3.0'

#####################################################################[ LDAP ]###

#actualizar stretch / buster / ok
#actualizar a la versión de la forja cuando este commit esté incorporado:
#https://github.com/camptocamp/puppet-openldap/commit/9c06c208ce4f4ba8c7cef3935f2476c2f87df592
#mod 'camptocamp/openldap', '2.0.0'
mod 'camptocamp/openldap',
  :git => 'https://github.com/camptocamp/puppet-openldap.git',
  :ref => '9ecdd0392d680a78ecc1b147413d64e35b8de695'

#actualizar / ok
mod 'spantree/phpldapadmin', '0.1.2'

mod 'ldapdn',
  :git => 'https://github.com/gtmtechltd/puppet_ldapdn.git',
  :ref => '34cfc4b'

##############################################################[ Mail system ]###

#actualizar stretch / buster / ok
mod 'camptocamp/postfix', '1.10.0'

#actualizar stretch / buster / ok
mod 'puppet/alternatives', '3.0.0'

#puppet v6
mod 'puppetlabs/mailalias_core', '1.0.6'

#puppet v6, actualizacion
mod 'puppet/archive', '4.5.0'

#actualizar stretch / buster / ok
mod 'bi4o4ek/opendkim', '0.0.7'

##############################################################[ Certbot ]###

#actualizar stretch / buster / ok
mod 'puppet/letsencrypt', '6.0.0'

##############################################################[ Pam ldap ]###

#actualizar stretch / buster / ok
mod 'geekix/nslcd', '0.4.1'

#actualizar stretch / buster / ok
mod 'trlinkin/nsswitch', '2.2.0'

##############################################################[ Nodejs ]###

#not used
mod 'puppet/nodejs', '2.3.0'

#mod 'artberri-nvm', '1.1.1'
# fork: Converted class to resource to be able to use nvm with more than one user #12
# https://github.com/artberri/puppet-nvm/pull/12
#mod 'puppet-nvm',
#  :git    => 'https://github.com/dsbaars/puppet-nvm.git',
#  :branch => 'nvm_resource'
# we use wwbdev fork of dsbaars/puppet-nvm, for now
# this is the same as 'artberri-nvm', '1.1.1' but with a little fix
mod 'nvm',
  :git    => 'https://github.com/devwwb/puppet-nvm.git',
  :branch => 'git_clone_fix'

##############################################################[ Python ]###

#actualizar stretch / buster / ok
mod 'puppet/python', '5.0.0'

##############################################################[ Mongo ]####

#puppet v6, actualizacion
mod 'puppet/mongodb', '4.1.1'

mod 'puppet/systemd', '3.10.0'

##############################################################[ Docker ]####

#actualizar stretch / buster / ok
mod 'puppetlabs/docker', '3.12.0'
mod 'puppetlabs/powershell', '4.0.0'
mod 'puppetlabs/pwshlib', '0.5.1'
mod 'puppetlabs/reboot', '2.4.0'

#actualizar stretch / buster / ok / actualizado por python
mod 'puppet/epel', '3.0.1'

##############################################################[ Hardening ]####

#actualizar stretch / buster / ok
mod 'herculesteam/augeasproviders_sysctl', '2.5.0'

#actualizar stretch / buster / ok
mod 'hardening/os_hardening', '2.2.8'

#actualizar stretch / buster / ok
mod 'camptocamp/kmod', '2.5.0'

#actualizar stretch / buster / ok
mod 'puppetlabs/limits', '0.1.0'

#tcpwrappers
mod 'MiamiOH-tcpwrappers', '1.3.0'
mod 'example42-puppi', '2.2.11'
